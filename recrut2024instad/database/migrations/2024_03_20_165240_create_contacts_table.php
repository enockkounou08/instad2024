<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('num_tel')->nullable();
            $table->string('num_whatsapp')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('password_confirmation')->nullable();
            $table->string('ville')->nullable();
            $table->string('type_piece')->nullable();
            $table->string('num_piece')->nullable();
            $table->string('ifu')->nullable();
            $table->string('exp_day')->nullable();
            $table->string('exp_month')->nullable();
            $table->string('exp_year')->nullable();
            $table->boolean('role')->default(0); // Ajout de la colonne role avec une valeur par défaut de 0
            // $table->string('depart_residence')->nullable();
            // $table->string('commune_residence')->nullable();
         
            $table->boolean('is_verified')->default(false);
            $table->string('activation_code')->nullable();
            $table->string('activation_token')->nullable();
            
            $table->timestamps();

            // Définition de la clé étrangère
            $table->foreign('id')
            ->references('id')
            ->on('personnes')
            ->onDelete('cascade');
        });


       
    }
    

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contacts');
    }
};
