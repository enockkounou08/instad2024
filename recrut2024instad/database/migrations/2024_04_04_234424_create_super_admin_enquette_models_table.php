<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
  public function up(): void
{
    Schema::create('super_admin_enquette_models', function (Blueprint $table) {
        $table->id();
        $table->string('nom')->nullable();
        $table->text('info')->nullable();
        $table->integer('id_postulant')->nullable();
        $table->string('nbr_postule')->nullable();
        $table->enum('etat_enquete', ['activé', 'désactivé'])->default('désactivé'); // Utilisation d'un enum pour l'état
        $table->datetime('date_expiration')->nullable();
        $table->timestamps();
    });
}


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('super_admin_enquette_models');
    }
};
