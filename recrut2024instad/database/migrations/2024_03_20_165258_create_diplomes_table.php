<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('diplomes', function (Blueprint $table) {
            $table->id();
            $table->string('diplome')->nullable(); // Champ pour le diplôme
            $table->string('autrediplome')->nullable(); // Champ pour le CV
            $table->string('enquete_participe')->nullable();
            $table->string('zone_de_travail')->nullable();
            $table->boolean('receive_newsletter')->default(false); // Champ pour la newsletter
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('diplomes');
    }
};
