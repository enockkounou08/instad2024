<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('enquetedisponibles', function (Blueprint $table) {
            $table->id();
            
            $table->string('nom_enquete')->nullable();
            $table->text('contenu')->nullable();
            $table->date('date_expiration')->nullable();
            $table->text('nombre_postulant')->nullable();
            $table->text('postulant')->nullable();
            $table->timestamps();
        });

   
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('enquetedisponibles');
    }
};
