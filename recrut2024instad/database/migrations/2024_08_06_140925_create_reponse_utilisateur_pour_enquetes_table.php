<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
   public function up(): void
{
    Schema::create('reponse_utilisateur_pour_enquetes', function (Blueprint $table) {
        $table->id();
        $table->Integer('user_id'); // ID de l'utilisateur
        $table->Integer('enquete_id'); // ID de l'enquête
        $table->Integer('nbr_question')->nullable();
        $table->Integer('nbr_reponse')->nullable(); 
        $table->Integer('question_id'); // ID de la question
        $table->text('question_texte')->nullable(); 
        $table->text('reponse')->nullable(); // Réponse de l'utilisateur
        
        $table->timestamps();
    });
}


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reponse_utilisateur_pour_enquetes');
    }
};
