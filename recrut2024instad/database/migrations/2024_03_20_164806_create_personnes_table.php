<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('personnes', function (Blueprint $table) {
            $table->id();
            $table->string('prenom');
            $table->string('nom');
            $table->string('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('nationalite')->nullable();
            $table->string('autre_nationalite')->nullable();
            $table->string('langue1')->nullable();
            $table->string('langue2')->nullable();
            $table->string('langue3')->nullable();
            $table->timestamps();

            
    
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('personnes', function (Blueprint $table) {
            //
            $table->dropForeign(['id']);
            $table->dropColumn('id');
        });
        
        Schema::dropIfExists('personnes');

       
    }
};
