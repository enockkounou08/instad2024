<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('question', function (Blueprint $table) {
            $table->id();
            $table->string('titre')->nullable();
            $table->text('interrogation')->nullable();
            $table->integer('id_reponse')->nullable();
            $table->integer('id_evaluation')->nullable();
            $table->timestamps();

            
        });
  

        // Création de l'utilisateur par défaut et insertion de données dans une autre table
Schema::table('question', function (Blueprint $table) {
    $table->after('id_evaluation', function ($table) {
        // Créer l'utilisateur par défaut
        App\Models\QuestionModel::create([
            'titre' => 'Question 1',
            'interrogation' => '
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad aperiam ex, 
            consequatur aut consequuntur vero labore porro nesciunt deleniti delectus odio 
            dolores, possimus officiis nobis distinctio architecto! Consequuntur, illum illo.',
            // Ajoutez d'autres champs si nécessaire
        ]);

        App\Models\QuestionModel::create([
            'titre' => 'Question 2',
            'interrogation' => '
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad aperiam ex, 
            consequatur aut consequuntur vero labore porro nesciunt deleniti delectus odio 
            dolores, possimus officiis nobis distinctio architecto! Consequuntur, illum illo.',
            // Ajoutez d'autres champs si nécessaire
        ]);


        App\Models\QuestionModel::create([
            'titre' => 'Question 3',
            'interrogation' => '
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad aperiam ex, 
            consequatur aut consequuntur vero labore porro nesciunt deleniti delectus odio 
            dolores, possimus officiis nobis distinctio architecto! Consequuntur, illum illo.',
            // Ajoutez d'autres champs si nécessaire
        ]);
    });
});


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
