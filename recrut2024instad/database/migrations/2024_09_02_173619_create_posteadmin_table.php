<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posteadmin', function (Blueprint $table) {
            $table->id();
            $table->string('nom_poste');
           $table->string('role_id');

           // Définir la contrainte de clé étrangère    
            $table->timestamps();

           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // D'abord supprimer la clé étrangère, puis la colonne
        Schema::table('posteadmin', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
        });
    
        Schema::dropIfExists('posteadmin');
    }

};
