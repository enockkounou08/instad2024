<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('reponse', function (Blueprint $table) {
            $table->id();
            $table->string('solution')->nullable();
            $table->integer('id_question')->nullable();
            $table->integer('id_evaluation')->nullable();
            $table->timestamps();

            
        });


        App\Models\ReponseModel::create([
            'solution' => 'oui',
            'id_question' => '1',
            // Ajoutez d'autres champs si nécessaire
        ]);


        App\Models\ReponseModel::create([
            'solution' => 'oui',
            'id_question' => '2',
            // Ajoutez d'autres champs si nécessaire
        ]);


        App\Models\ReponseModel::create([
            'solution' => 'oui',
            'id_question' => '3',
            // Ajoutez d'autres champs si nécessaire
        ]);

    
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
