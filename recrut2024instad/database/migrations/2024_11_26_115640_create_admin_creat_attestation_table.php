<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin_creat_attestation', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->text('contenu');
             // Ajout de la clé étrangère pour l'enquête
            $table->unsignedBigInteger('enquete_id')->nullable(); // Colonne pour la clé étrangère
            $table->foreign('enquete_id') // Définition de la clé étrangère
                  ->references('id')
                  ->on('super_admin_enquette_models')
                  ->onDelete('set null'); // Si l'enquête est supprimée, la relation est annulée

             $table->unique('enquete_id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admin_creat_attestation');
    }
};
