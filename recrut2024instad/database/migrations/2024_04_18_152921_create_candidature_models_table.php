<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('candidature_models', function (Blueprint $table) {
            $table->id();
            $table->integer('id_personne');
            $table->string('nom_personne'); 
            $table->string('email_personne');
            $table->integer('id_enquete');
            $table->string('nom_enquete');
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('candidature_models');
    }
};
