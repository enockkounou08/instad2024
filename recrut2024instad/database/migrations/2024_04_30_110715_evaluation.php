<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('evaluation', function (Blueprint $table) {
            $table->id();
            $table->text('solution')->nullable();
            $table->integer('id_uitisateur')->nullable();
            $table->integer('nbr_repnse_trouver')->nullable();
            $table->string('id_repnse_incorrecte')->nullable();
            $table->integer('note')->nullable();
            $table->timestamps();

            
        });

        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
