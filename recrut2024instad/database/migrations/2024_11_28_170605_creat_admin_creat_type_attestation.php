<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin_creat_type_attestation', function (Blueprint $table) {
            $table->id();
            $table->string('nom');  // Le nom du type d'attestation
            $table->timestamps();
        });

        // Ajouter la colonne 'type_attestation_id' dans la table 'admin_creat_attestation'
        Schema::table('admin_creat_attestation', function (Blueprint $table) {
            $table->unsignedBigInteger('type_attestation_id')->nullable();  // La relation peut être nulle
            $table->foreign('type_attestation_id')  // Définition de la clé étrangère
                  ->references('id')
                  ->on('admin_creat_type_attestation')
                  ->onDelete('set null');  // Si le type d'attestation est supprimé, la relation est annulée
                   $table->unique('type_attestation_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('admin_creat_attestation', function (Blueprint $table) {
            $table->dropForeign(['type_attestation_id']);
            $table->dropColumn('type_attestation_id');
        });

        Schema::dropIfExists('admin_creat_type_attestation');
    }
};
