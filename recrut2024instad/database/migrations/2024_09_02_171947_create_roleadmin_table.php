<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roleadmin', function (Blueprint $table) {
            $table->id();
            $table->string('nom_role');
            $table->string('classe_desactive');
            $table->string('poste_id'); // Ajout de la colonne pour la clé étrangère
            
            $table->timestamps();
        });

        // 
        DB::table('roleadmin')->insert([
            ['nom_role' => "Touts boutons d'exportation", 'classe_desactive' => 'btn-export', 'poste_id' => null],
            [
                'nom_role' => "Exportation PDF",
                'classe_desactive' => 'btn-export-pdf',
                'poste_id' => null
              ],
            ['nom_role' => "Exportztion excel", 'classe_desactive' => 'btn-export-excel', 'poste_id' => null],
            ['nom_role' => "Exportation word", 'classe_desactive' => 'btn-export-word', 'poste_id' => null],
            ['nom_role' => "Bouton créer en quette", 'classe_desactive' => 'btn-creer-enquete', 'poste_id' => null],
            ['nom_role' => "Etat des enquete", 'classe_desactive' => 'btn-etat', 'poste_id' => null],
            ['nom_role' => "Bouton des action modifier et supprimer", 'classe_desactive' => 'btn-action', 'poste_id' => null],
            ['nom_role' => "Bouton de suppression", 'classe_desactive' => 'btn-action-sup', 'poste_id' => null],
            [
                'nom_role' => "Bouton de modification",
                'classe_desactive' => 'btn-action-modif',
                'poste_id' => null
              ],  
              [
                'nom_role' => "Bar Vertical de navigation",
                'classe_desactive' => 'sidebar_admin',
                'poste_id' => null
              ],
               [
                'nom_role' => "Utilisateurs Inscrit sur le site",
                'classe_desactive' => 'userRegister',
                'poste_id' => null
              ],
               [
                'nom_role' => "Utilisateurs Inscrit sur le site",
                'classe_desactive' => 'userRegister',
                'poste_id' => null
              ],
               [
                'nom_role' => "Listes des Enquettes disponibles",
                'classe_desactive' => 'enqueteRegister',
                'poste_id' => null
              ],
               [
                'nom_role' => "Réponse des Utilisateurs sur la plate-forme",
                'classe_desactive' => 'ansUser',
                'poste_id' => null
              ],
               [
                'nom_role' => "Accès au profile administrateur",
                'classe_desactive' => 'adminProfil',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'ajout de l'administrateur (Créer administrateur)",
                'classe_desactive' => 'formulaire-add-admin',
                'poste_id' => null
              ],
               [
                'nom_role' => "Voir la liste des administrateurs",
                'classe_desactive' => 'list-admini',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire de création de poste",
                'classe_desactive' => 'ajouter-poste',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'affection de poste",
                'classe_desactive' => 'affecter-un-poste',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'ajout de ville",
                'classe_desactive' => 'formulaire-add-ville',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'ajout de lang",
                'classe_desactive' => 'formulaire-add-lang',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'ajout d'attestation",
                'classe_desactive' => 'formulaire-add-attestaion',
                'poste_id' => null
              ],
               [
                'nom_role' => "Formulaire d'ajout de type d'attestation",
                'classe_desactive' => 'formulaire-add-type-attestaion',
                'poste_id' => null
              ],
             

        ]);

     
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roleadmin');
    }
};
