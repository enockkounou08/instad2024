<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminActionModelsTable extends Migration
{
    public function up(): void
    {
        Schema::create('admin_action_models', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->string('password');
            // $table->string('droit');
            $table->timestamps();
        });

        // Ajouter un utilisateur après la création de la table
        DB::table('admin_action_models')->insert([
            'nom' => 'NomUtilisateur',
            'prenom' => 'PrenomUtilisateur',
            'email' => 'email@example.com',
            'password' => bcrypt('motdepasse'), 
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('admin_action_models');
    }
}

