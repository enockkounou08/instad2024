<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('roleadmin')->insert([
            'id' => 1,  // Assurez-vous que cet ID ne soit pas déjà utilisé
            'nom_role' => 'Administrateur',  // Le nom du rôle
            'attribut' => 'Ajouter une enquête',  // L'attribut 
        ]);
    }
}
