$(document).ready(function() {
    function updateForm() {
        $.get(url)
            .done(function(response) {
                if (response.etat_enquete === 'activé') {
                    // Afficher le formulaire et enlever le message d'erreur
                    $('#surveyForm').show();
                    $('#maintenanceMessage').hide();
                    $('#surveyForm').find('input, textarea, select, button').prop('disabled', false);
                    $('#surveyForm').find('.error-message').text('');
                } else {
                    // Cacher le formulaire et afficher le message de maintenance
                    $('#surveyForm').hide();
                    $('#maintenanceMessage').show();
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.error('Erreur AJAX:', textStatus, errorThrown);
                alert('Erreur lors de la récupération de l\'état de l\'enquête.');
            });
    }

    // Met à jour le formulaire immédiatement
    updateForm();

    // Met à jour le formulaire toutes les 4 secondes
    setInterval(updateForm, 4000);
});




document.getElementById('fileInput').addEventListener('change', function () {
    const file = this.files[0];  // Récupère le fichier sélectionné
    const maxSize = 128 * 1024;  // 128 Ko en octets
    const errorMessage = document.getElementById('error-message');  // Sélectionne la zone de message d'erreur

    if (file.size > maxSize) {
        // Si le fichier dépasse 128 Ko, affiche un message d'erreur
        errorMessage.textContent = 'La taille du fichier ne doit pas dépasser 128 Ko.';
        errorMessage.style.display = 'block';  // Affiche le message d'erreur
        this.value = '';  // Réinitialise le champ de fichier
    } else {
        // Si le fichier est valide, cache le message d'erreur
        errorMessage.style.display = 'none';
    }
});



function validateForm() {
    // Validation des cases à cocher
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
    checkboxes.forEach((checkbox) => {
        const minRequired = checkbox.dataset.minRequired;
        const checked = document.querySelectorAll(`input[name="${checkbox.name}"]:checked`).length;
        if (checked < minRequired) {
            document.getElementById(`error_${checkbox.name.split('_')[1]}`).textContent = `Veuillez sélectionner au moins ${minRequired} option(s).`;
            return false;
        }
        document.getElementById(`error_${checkbox.name.split('_')[1]}`).textContent = '';
    });

    return true; // Retourner true pour permettre l'envoi du formulaire
}

function validateNumericInput(input) {
    input.value = input.value.replace(/[^0-9]/g, ''); // Autoriser uniquement les chiffres
}





