const input = document.getElementById("password");
    const showHide = document.querySelector(".show_hide");
    const indicator = document.querySelector(".indicator");
    const text = document.querySelector(".text");

    showHide.addEventListener("click", () => {
        if (input.type === "password") {
            input.type = "text";
            showHide.classList.replace("fa-eye-slash", "fa-eye");
        } else {
            input.type = "password";
            showHide.classList.replace("fa-eye", "fa-eye-slash");
        }
    });

    const alphabet = /[a-zA-Z]/; // lettres de a à z et A à Z
    const numbers = /[0-9]/; // chiffres de 0 à 9
    const scharacters = /[!,@,#,$,%,^,&,*,?,_,(,),-,+,=,~]/; // caractères spéciaux

    input.addEventListener("keyup", () => {
        indicator.classList.add("active");

        const val = input.value;
        if (val.match(alphabet) || val.match(numbers) || val.match(scharacters)) {
            text.textContent = "Le mot de passe est faible";
            text.style.color = "#FF6333";
        }
        if (val.match(alphabet) && val.match(numbers) && val.length >= 6) {
            text.textContent = "Le mot de passe est moyen";
            text.style.color = "#cc8500";
        }
        if (val.match(alphabet) && val.match(numbers) && val.match(scharacters) && val.length >= 8) {
            text.textContent = "Le mot de passe est fort";
            text.style.color = "#22C32A";
        }

        if (val === "") {
            indicator.classList.remove("active");
            text.textContent = "";
        }
    });


document.addEventListener("DOMContentLoaded", () => {
    const confirmationInput = document.getElementById("password_confirmation");
    const showHideConfirmation = document.querySelector(".show_hide_confirmation");

    showHideConfirmation.addEventListener("click", () => {
        if (confirmationInput.type === "password") {
            confirmationInput.type = "text";
            showHideConfirmation.classList.replace("fa-eye-slash", "fa-eye");
        } else {
            confirmationInput.type = "password";
            showHideConfirmation.classList.replace("fa-eye", "fa-eye-slash");
        }
    });
});

  


    const togglePassword = document.querySelector("#togglePassword");
    const passwordConfirmation = document.querySelector("#password_confirmation");
    const icon = togglePassword.querySelector("i");

    togglePassword.addEventListener("click", function () {
        // Toggle the type attribute
        const type = passwordConfirmation.getAttribute("type") === "password" ? "text" : "password";
        passwordConfirmation.setAttribute("type", type);

        // Toggle the eye / eye-slash icon
        if (type === "password") {
            icon.classList.remove("fa-eye");
            icon.classList.add("fa-eye-slash");
        } else {
            icon.classList.remove("fa-eye-slash");
            icon.classList.add("fa-eye");
        }
    });


                                                           


    document.addEventListener('DOMContentLoaded', function () {
  var diplomeSelect = document.getElementById('diplome');
  var optdiplomeSelect = document.getElementById('optdiplome');
  var autreDiplomeDiv = document.getElementById('autreDiplome');
  var autreDiplomeInput = document.getElementById('autreDiplomeInput');
  var autreDiplomeError = document.getElementById('autreDiplomeError');
  var optionDiplomeDiv = document.getElementById('optionDiplomeDiv');
  var autreOptionDiv = document.getElementById('autreOptionDiv');
  var autreOptionInput = document.getElementById('autreOptionInput');
  var autreOptionError = document.getElementById('autreOptionError');

  var options = {
      "CEP": [
          { value: "0", text: "Aucune option disponible pour ce diplôme" }
      ],
      "BEPC": [
          { value: "", text: "Sélectionner l'option de votre BEPC" },
          { value: "Langue", text: "Langue (Espagnole, Allemand)" },
          { value: "Physique", text: "Physique" },
          { value: "Physique + Langue", text: "Physique + Langue" },
          { value: "Autre", text: "Autre" }
      ],
      "CAP": [
          { value: "", text: "Sélectionner l'option de votre CAP" },
          { value: "Eau et Assainissement(EA)", text: "Eau et Assainissement(EA)" },
          { value: "Mécanique Générale(F1)", text: "Mécanique Générale(F1)" },
          { value: "Electronique(F2)", text: "Electronique(F2)" },
          { value: "Electricité(F3)", text: "Electricité(F3)" },
          { value: "Maçonnerie(F4)", text: "Maçonnerie(F4)" },
          { value: "Opérateur Géomètre(OG)", text: "Opérateur Géomètre(OG)" },
          { value: "Comptabilité(G2)", text: "Comptabilité(G2)" },
          { value: "Mécanique Automobile(MA)", text: "Mécanique Automobile(MA)" },
          { value: "Installation Maintenance Informatique(IMI)", text: "Installation Maintenance Informatique(IMI)" },
          { value: "autre", text: "Autre" }
      ],
      "BAC": [
          { value: "", text: "Sélectionner l'option de votre BAC" },
          { value: "A", text: "A" },
          { value: "B", text: "B" },
          { value: "C", text: "C" },
          { value: "D", text: "D" },
          { value: "E", text: "E" },
          { value: "Eau et Assainissement(EA)", text: "Eau et Assainissement(EA)" },
          { value: "Mécanique Générale(F1)", text: "Mécanique Générale(F1)" },
          { value: "Electronique(F2)", text: "Electronique(F2)" },
          { value: "Electricité(F3)", text: "Electricité(F3)" },
          { value: "Maçonnerie(F4)", text: "Maçonnerie(F4)" },
          { value: "Opérateur Géomètre(OG)", text: "Opérateur Géomètre(OG)" },
          { value: "Comptabilité(G2)", text: "Comptabilité(G2)" },
          { value: "Mécanique Automobile(MA)", text: "Mécanique Automobile(MA)" },
          { value: "Installation Maintenance Informatique(IMI)", text: "Installation Maintenance Informatique(IMI)" },
          { value: "autre", text: "Autre" }
      ],
      "ADS": [
          { value: "0", text: "Aucune option disponible pour ce diplôme" }
      ],
      "ATS": [
          { value: "0", text: "Aucune option disponible pour ce diplôme" }
      ],
      "BTS": [
          { value: "", text: "Sélectionner l'option de votre BTS" },
          { value: "Informatique", text: "Informatique" },
          { value: "Gestion", text: "Gestion" },
          { value: "Marketing", text: "Marketing" },
          { value: "Ressources Humaines", text: "Ressources Humaines" },
          { value: "autre", text: "Autre" }
      ],
      "LICENCE": [
          { value: "", text: "Sélectionner l'option de votre LICENCE" },
          { value: "Informatique", text: "Informatique" },
          { value: "Gestion", text: "Gestion" },
          { value: "Statistique", text: "Statistique" },
          { value: "Marketing", text: "Marketing" },
          { value: "Ressources Humaines", text: "Ressources Humaines" },
          { value: "Droit", text: "Droit" },
          { value: "autre", text: "Autre" }
      ],
      "MASTER": [
          { value: "", text: "Sélectionner l'option de votre MASTER" },
          { value: "Informatique", text: "Informatique" },
          { value: "Gestion", text: "Gestion" },
          { value: "Statistique", text: "Statistique" },
          { value: "Marketing", text: "Marketing" },
          { value: "Ressources Humaines", text: "Ressources Humaines" },
          { value: "Droit", text: "Droit" },
          { value: "autre", text: "Autre" }
      ]
  };

  // Fonction pour afficher les options du diplôme
  function displayOptions(selectedDiplome) {
      optdiplomeSelect.innerHTML = '';
      if (selectedDiplome && selectedDiplome !== 'autre') {
          var diplomeOptions = options[selectedDiplome] || [];
          diplomeOptions.forEach(function (option) {
              var opt = document.createElement('option');
              opt.value = option.value;
              opt.textContent = option.text;
              optdiplomeSelect.appendChild(opt);
          });
      }
  }

  // Fonction pour initialiser les champs après soumission
  function initializeFields() {
      var selectedDiplome = "{{ old('diplome') }}";
      var selectedOption = "{{ old('optdiplome') }}";

      if (selectedDiplome) {
          diplomeSelect.value = selectedDiplome;
          displayOptions(selectedDiplome);

          if (selectedOption) {
              optdiplomeSelect.value = selectedOption;
          }

          if (selectedDiplome === 'autre') {
              autreDiplomeDiv.style.display = 'block';
          } else {
              autreDiplomeDiv.style.display = 'none';
          }

          if (selectedOption === 'autre') {
              autreOptionDiv.style.display = 'block';
          } else {
              autreOptionDiv.style.display = 'none';
          }
      }
  }

  diplomeSelect.addEventListener('change', function () {
      var selectedDiplome = this.value;

      displayOptions(selectedDiplome);

      if (selectedDiplome === '' || selectedDiplome === 'autre') {
          optionDiplomeDiv.style.display = 'none';
          autreOptionDiv.style.display = 'none';
      } else {
          optionDiplomeDiv.style.display = 'block';
      }

      if (selectedDiplome === 'autre') {
          autreDiplomeDiv.style.display = 'block';
      } else {
          autreDiplomeDiv.style.display = 'none';
      }
  });

  optdiplomeSelect.addEventListener('change', function () {
      var selectedOption = this.value;

      if (selectedOption === 'autre') {
          autreOptionDiv.style.display = 'block';
      } else {
          autreOptionDiv.style.display = 'none';
      }
  });

  autreOptionInput.addEventListener('blur', function () {
      if (autreOptionDiv.style.display === 'block' && autreOptionInput.value.trim() === '') {
          autreOptionError.style.display = 'block';
      } else {
          autreOptionError.style.display = 'none';
      }
  });

  autreOptionInput.addEventListener('focus', function () {
      autreOptionError.style.display = 'none';
  });

  // Initialiser les champs après le chargement de la page
  initializeFields();
});






//  Inclure jQuery 

$(document).ready(function() {
    // Attacher un gestionnaire d'événements au champ d'email pour déclencher la vérification lorsqu'il perd le focus (l'utilisateur clique ailleurs)
    $('#email').blur(function() {
        var email = $(this).val(); // Récupérer la valeur de l'email
        emailExistjs(email);
    });
});

function emailExistjs(email){
    var url = $('#email').attr('url-emailExist');
    var token = $('#email').attr('token');

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            '_token': token,
            email: email
        },
        success:function(result){
            // Mettre à jour le contenu du champ de résultat avec la réponse de la vérification
            $('#emailExistResult').text(result.response);
        }
    });
}





    const passwordInput = document.getElementById('password');
    const confirmPasswordInput = document.getElementById('password_confirmation');
    const passwordMatchMessage = document.getElementById('passwordMatchMessage');

    // Fonction pour vérifier si les mots de passe correspondent
    function checkPasswordMatch() {
        if (passwordInput.value !== confirmPasswordInput.value) {
            passwordMatchMessage.style.display = 'block';
        } else {
            passwordMatchMessage.style.display = 'none';
        }
    }

    // Événement lorsque l'utilisateur quitte le champ de confirmation
    confirmPasswordInput.addEventListener('blur', checkPasswordMatch);









    // Récupérer les éléments du formulaire
    const dayInput = document.getElementById('day');
    const monthInput = document.getElementById('month');
    const yearInput = document.getElementById('year');

    // Ajouter des écouteurs d'événements blur aux champs
    dayInput.addEventListener('blur', validateDay);
    monthInput.addEventListener('blur', validateMonth);
    yearInput.addEventListener('blur', validateYear);

    function validateDay() {
        const day = parseInt(dayInput.value.trim());
        const dayError = document.getElementById('dayError');

        if (isNaN(day) || day < 1 || day > 31) {
            dayError.style.display = 'inline';
        } else {
            dayError.style.display = 'none';
        }
    }

    function validateMonth() {
        const month = parseInt(monthInput.value.trim());
        const monthError = document.getElementById('monthError');

        if (isNaN(month) || month < 1 || month > 12) {
            monthError.style.display = 'inline';
        } else {
            monthError.style.display = 'none';
        }
    }

    function validateYear() {
        const year = parseInt(yearInput.value.trim());
        const yearError = document.getElementById('yearError');
        const currentYear = new Date().getFullYear();

        if (isNaN(year) || year < currentYear - 15 || year > currentYear - 114) {
            // Vérifier si l'utilisateur a au moins 15 ans
            yearError.style.display = 'inline';
        } else {
            yearError.style.display = 'none';
        }
    }




        document.addEventListener("DOMContentLoaded", function() {
            const yearInput = document.getElementById('year');

            yearInput.addEventListener('blur', function() {
                const yearValue = yearInput.value.trim();
                const errorMessageContainer = document.getElementById('error-messages');

                if (yearValue === '') {
                    displayErrorMessage("Vous devez remplir le champ année.");
                } else if (isNaN(yearValue)) {
                    displayErrorMessage("Veuillez entrer une année valide.");
                } else if (yearValue.length !== 4) {
                    displayErrorMessage("Entrez une année de naissance valide (AAAA).");
                } else {
                    const currentYear = new Date().getFullYear();
                    const birthYear = parseInt(yearValue, 10);
                    const age = currentYear - birthYear;

                    if (age < 18) {
                        displayErrorMessage("Vous devez avoir plus de 18 ans pour postuler.");
                    } else {
                        clearErrorMessages();
                    }
                }
            });

            function displayErrorMessage(message) {
                clearErrorMessages();
                const errorMessageElement = document.createElement('div');
                errorMessageElement.classList.add('error-message');
                errorMessageElement.textContent = message;
                document.getElementById('error-messages').appendChild(errorMessageElement);
            }

            function clearErrorMessages() {
                const errorMessageContainer = document.getElementById('error-messages');
                while (errorMessageContainer.firstChild) {
                    errorMessageContainer.removeChild(errorMessageContainer.firstChild);
                }
            }
        });


    // Récupérer les éléments du formulaire
    ocument.addEventListener('DOMContentLoaded', function() {
        const dayInput = document.getElementById('day');
        const monthInput = document.getElementById('month');
        const yearInput = document.getElementById('year');
    
        if (dayInput && monthInput && yearInput) {
            // Ajoutez ici le code qui manipule les inputs de jour, mois et année
        } else {
            console.error("Les éléments d'entrée pour le jour, le mois, et l'année n'existent pas dans le DOM.");
        }
    });

    // Ajouter des écouteurs d'événements blur aux champs
    dayInput.addEventListener('blur', validateDay);
    monthInput.addEventListener('blur', validateMonth);
    yearInput.addEventListener('blur', validateYear);

    function validateDay() {
        const day = parseInt(dayInput.value);
        const dayError = document.getElementById('dayError');

        if (isNaN(day) || day < 1 || day > 31) {
            dayError.style.display = 'inline';
        } else {
            dayError.style.display = 'none';
        }
    }

    function validateMonth() {
        const month = parseInt(monthInput.value);
        const monthError = document.getElementById('monthError');

        if (isNaN(month) || month < 1 || month > 12) {
            monthError.style.display = 'inline';
        } else {
            monthError.style.display = 'none';
        }
    }

    function validateYear() {
        const year = parseInt(yearInput.value);
        const yearError = document.getElementById('yearError');

        if (isNaN(year)) {
            yearError.style.display = 'inline';
        } else {
            yearError.style.display = 'none';
        }
    }




    const diplomeSelect = document.getElementById('diplome');
    const autreDiplomeDiv = document.getElementById('autreDiplome');
    const autreDiplomeInput = document.getElementById('autreDiplomeInput');
    const autreDiplomeError = document.getElementById('autreDiplomeError');

    diplomeSelect.addEventListener('change', function() {
        if (diplomeSelect.value === 'autre') {
            autreDiplomeDiv.style.display = 'block';
        } else {
            autreDiplomeDiv.style.display = 'none';
            autreDiplomeInput.value = ''; // Réinitialiser la valeur du champ autreDiplomeInput si une autre option est sélectionnée
            autreDiplomeError.style.display = 'none'; // Cacher le message d'erreur lorsque le champ est masqué
        }
    });

    // Vérifier si le champ autreDiplomeInput est rempli lors de la soumission du formulaire
    document.querySelector('form').addEventListener('submit', function(event) {
        if (diplomeSelect.value === 'autre' && autreDiplomeInput.value.trim() === '') {
            autreDiplomeError.style.display = 'inline'; // Afficher le message d'erreur si le champ est vide
            event.preventDefault(); // Empêcher la soumission du formulaire
        }
    });

    // Vérifier si le champ autreDiplomeInput est rempli lorsqu'il perd le focus
    autreDiplomeInput.addEventListener('blur', function() {
        if (diplomeSelect.value === 'autre' && autreDiplomeInput.value.trim() === '') {
            autreDiplomeError.style.display = 'inline'; // Afficher le message d'erreur si le champ est vide
        } else {
            autreDiplomeError.style.display = 'none'; // Cacher le message d'erreur si le champ est rempli
        }
    });



        function toggleCheckboxes() {
            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            var aucuneCheckbox = document.querySelector('input[name="aucune"]');
            checkboxes.forEach(function(checkbox) {
                if (aucuneCheckbox.checked) {
                    if (checkbox !== aucuneCheckbox) {
                        checkbox.disabled = true;
                    }
                } else {
                    checkbox.disabled = false;
                }
            });
        }



    document.addEventListener('DOMContentLoaded', function () {
        const expDay = document.getElementById('exp_day');
        const expMonth = document.getElementById('exp_month');
        const expYear = document.getElementById('exp_year');
        const errorMessage = document.getElementById('error_message');

        function validateDay() {
            const day = parseInt(expDay.value, 10);
            if (isNaN(day) || day < 1 || day > 31) {
                errorMessage.textContent = 'Vous devez entrer une date d\'expiration entre 1 et 31.';
            } else {
                errorMessage.textContent = '';
                validateExpirationDate();
            }
        }

        function validateMonth() {
            const month = parseInt(expMonth.value, 10);
            if (isNaN(month) || month < 1 || month > 12) {
                errorMessage.textContent = 'Vous devez entrer un mois d\'expiration entre 1 et 12.';
            } else {
                errorMessage.textContent = '';
                validateExpirationDate();
            }
        }

        function validateYear() {
            const year = parseInt(expYear.value, 10);
            const currentYear = new Date().getFullYear();
            if (isNaN(year) || expYear.value.length !== 4) {
                errorMessage.textContent = 'Vous devez entrer une année d\'expiration valide (4 chiffres).';
            } else if (year < currentYear) {
                errorMessage.textContent = 'Votre pièce est déjà expirée, veuillez choisir une pièce valide.';
            } else {
                errorMessage.textContent = '';
                validateExpirationDate();
            }
        }

        function validateExpirationDate() {
            const day = parseInt(expDay.value, 10);
            const month = parseInt(expMonth.value, 10);
            const year = parseInt(expYear.value, 10);

            if (!isNaN(day) && !isNaN(month) && !isNaN(year)) {
                const expirationDate = new Date(year, month - 1, day);
                const today = new Date();
                today.setHours(0, 0, 0, 0); // Réinitialise l'heure à 00:00:00 pour aujourd'hui

                if (expirationDate <= today) {
                    if (expirationDate.getTime() === today.getTime()) {
                        errorMessage.textContent = 'Votre pièce expire aujourd\'hui, veuillez la renouveler.';
                    } else {
                        errorMessage.textContent = 'votre pièce a déjà expiré.';
                    }
                } else {
                    errorMessage.textContent = '';
                }
            }
        }

        expDay.addEventListener('blur', validateDay);
        expMonth.addEventListener('blur', validateMonth);
        expYear.addEventListener('blur', validateYear);
    });




document.getElementById('myForm').addEventListener('submit', function(e) {
    var submitButton = document.getElementById('submitButton');
    var loadingMessage = document.getElementById('loadingMessage');
    
    submitButton.disabled = true;
    loadingMessage.style.display = 'inline-block';

    // Optionnel : Pour des raisons de démonstration, simuler un délai de soumission de formulaire
    // e.preventDefault();
    // setTimeout(function() {
    //     submitButton.disabled = false;
    //     loadingMessage.style.display = 'none';
    // }, 3000);
});


                                                            
