document.addEventListener('DOMContentLoaded', function () {
        const typeReponseInput = document.getElementById('typeReponse');
        const nbrOptionInput = document.getElementById('nbrOption');
        const optionFields = document.querySelectorAll('.option-field');
        const invalidTypeError = document.getElementById('invalidTypeError');

        const validTypes = ['input', 'textarea', 'radio', 'checkbox', 'select', 'numericTel', 'numericAge', 'date', 'email', 'ifu', 'file', 'listeDep', 'langue', 'expInstad'];
        
        function toggleOptions() {
            const typeReponse = typeReponseInput.value;
            const nbrOption = parseInt(nbrOptionInput.value, 10) || 0;

            // Vérifier le type de réponse
            if (!validTypes.includes(typeReponse)) {
                invalidTypeError.style.display = 'block';
                optionFields.forEach(field => {
                    field.style.display = 'none';
                });
                return;
                
            } else {
                invalidTypeError.style.display = 'none';
            }

            if (['radio', 'checkbox', 'select'].includes(typeReponse)) {
                nbrOptionInput.removeAttribute('disabled');
                optionFields.forEach((field, index) => {
                    const optionInput = field.querySelector('input');
                    if (index < nbrOption) {
                        field.style.display = 'block';
                        optionInput.setAttribute('required', 'required');
                    } else {
                        field.style.display = 'none';
                        optionInput.removeAttribute('required');
                        optionInput.value = '';
                    }
                });
            } else {
                nbrOptionInput.setAttribute('disabled', true);
                optionFields.forEach(field => {
                    field.style.display = 'none';
                    const optionInput = field.querySelector('input');
                    optionInput.removeAttribute('required');125;
                    optionInput.value = '';
                });
            }
        }

        toggleOptions();
        typeReponseInput.addEventListener('change', toggleOptions);
        nbrOptionInput.addEventListener('input', toggleOptions);
    });

