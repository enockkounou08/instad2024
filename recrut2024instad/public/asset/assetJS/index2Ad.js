document.addEventListener('DOMContentLoaded', function() {
        function handleExcelExportButtonClick(event) {
            event.preventDefault(); // Empêche le comportement par défaut du bouton
            var button = event.target;
            button.classList.add('disabled-button', 'loading');
            // Simule un téléchargement de fichier après un délai
            setTimeout(function() {
                // Ajoutez ici le code pour télécharger le fichier Excel
                // Par exemple :
                // window.location.href = 'url_vers_votre_fichier_excel';
  window.location.href = excelExportUrl;

                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
            }, 2000); // Simule 2 secondes de chargement
        }

        var excelExportButton = document.getElementById('confirmExportExcel');
        if (excelExportButton) {
            excelExportButton.addEventListener('click', handleExcelExportButtonClick);
        }
    });




    document.addEventListener('DOMContentLoaded', function() {
        function handleWordExportButtonClick(event) {
            event.preventDefault(); // Empêche le comportement par défaut du bouton
            var button = event.target;
            button.classList.add('disabled-button', 'loading');
            // Simule un téléchargement de fichier après un délai
            setTimeout(function() {
                // Ajoutez ici le code pour télécharger le fichier Word
                // Par exemple :
                // window.location.href = 'url_vers_votre_fichier_word';
                  window.location.href = enquetteDispoExW ;

                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
            }, 2000); // Simule 2 secondes de chargement
        }

        var wordExportButton = document.getElementById('confirmExportWord');
        if (wordExportButton) {
            wordExportButton.addEventListener('click', handleWordExportButtonClick);
        }
    });



    document.addEventListener('DOMContentLoaded', function() {
        function handleExportButtonClick(event) {
            event.preventDefault(); // Empêche le comportement par défaut du bouton
            var button = event.target;
            button.classList.add('disabled-button', 'loading');
            // Simule un téléchargement de fichier après un délai
            setTimeout(function() {
                // Ajoutez ici le code pour télécharger le fichier PDF
                // Par exemple :
                // window.location.href = 'url_vers_votre_fichier_pdf';
  window.location.href = exportPdf ;

                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
            }, 2000); // Simule 2 secondes de chargement
        }

        var exportButton = document.getElementById('confirmExportPdf');
        if (exportButton) {
            exportButton.addEventListener('click', handleExportButtonClick);
        }
    });




    document.addEventListener('DOMContentLoaded', () => {
        const toggles = document.querySelectorAll('.toggle');

        toggles.forEach(toggle => {
            toggle.addEventListener('click', () => {
                const id = toggle.getAttribute('data-id');
                const currentState = toggle.getAttribute('data-state');
                const newState = currentState === 'activé' ? 'désactivé' : 'activé';
                const stateText = newState === 'activé' ? 'Activé' : 'Désactivé';

                // Mettre à jour l'état local du bouton
                toggle.setAttribute('data-state', newState);
                toggle.classList.toggle('active');
                
                // Mettre à jour le texte de l'état
                const stateTextElement = toggle.nextElementSibling;
                stateTextElement.textContent = stateText;

                // Effectuer une requête AJAX pour mettre à jour l'état dans la base de données
                fetch(`/update-enquete-state/${id}`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                    },
                    body: JSON.stringify({ state: newState }), // newState sera soit "activé" soit "désactivé"
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        console.log('L\'état a été mis à jour avec succès.');
                    } else {
                        console.error('Erreur lors de la mise à jour de l\'état.');
                    }
                })
                .catch(error => console.error('Erreur:', error));
            });
        });
    });





    document.addEventListener('DOMContentLoaded', function () {
        function afficherListeQuestions(enqueteId, enqueteNom) {
            document.getElementById('enqueteNom').textContent = `Questions pour l'enquête : ${enqueteNom}`;
            
            const questionsList = document.getElementById('questionsList');
            questionsList.innerHTML = ''; // Vider la liste des questions pour une nouvelle requête
            
            // Remplacer l'ID de l'enquête dans l'URL
            const url = questionsRouteUrl.replace(':id', enqueteId);
            
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    // Parcourir les questions reçues
                    data.forEach(question => {
                        const questionContainer = document.createElement('div');
                        questionContainer.classList.add('question-container', 'mb-4');
                        
                        // Contenu de la question
                        let content = `<h5><strong>Question:</strong> ${question.question || 'Non spécifiée'}</h5>`;
                        
                        // Type de réponse et options
                        if (question.typeReponse) {
                            content += `<p><strong>Type de Réponse:</strong> ${question.typeReponse}</p>`;
                        }
                        if (question.nbrOption) {
                            content += `<p><strong>Nombre d'Options:</strong> ${question.nbrOption}</p>`;
                            
                            for (let i = 1; i <= question.nbrOption; i++) {
                                const option = question[`option${i}`];
                                if (option) {
                                    content += `<p><strong>Option ${i}:</strong> ${option}</p>`;
                                }
                            }
                        }
    
                        // Ajout des boutons d'édition et de suppression avec les liens
                        const editUrl = editQuestionRouteUrl
                            .replace(':enqueteId', enqueteId)
                            .replace(':questionId', question.id);
    
                        const deleteUrl = deleteQuestionRouteUrl
                            .replace(':enqueteId', enqueteId)
                            .replace(':questionId', question.id);
                        
                        content += `
                            <div class="question-actions mt-2">
                                <a href="${editUrl}" 
                                   class="material-icons edit-icon" data-toggle="tooltip" title="Edit">
                                    &#xE254;
                                </a>
                                <a href="${deleteUrl}" class="material-icons delete-icon" data-toggle="tooltip" title="Delete">
                                    &#xE872;
                                </a>
                            </div>
                        `;
                        
                        questionContainer.innerHTML = content;
                        questionsList.appendChild(questionContainer);
                    });
    
                    // Initialiser les tooltips
                    $('[data-toggle="tooltip"]').tooltip();
                })
                .catch(error => {
                    console.error('Erreur:', error);
                });
        }
    
        // Déclenche l'affichage des questions quand le bouton 'viewQuestionsBtn' est cliqué
        document.getElementById('viewQuestionsBtn').addEventListener('click', function () {
            const enqueteId = $(this).data('enquete-id');
            const enqueteNom = $(this).data('enquete-nom');
            
            afficherListeQuestions(enqueteId, enqueteNom);
        });
    
        // Assigner les IDs de l'enquête au bouton 'viewQuestionsBtn' quand une enquête est sélectionnée
        $('.view-enquete').on('click', function () {
            const enqueteId = $(this).data('enquete-id');
            const enqueteNom = $(this).data('enquete-nom');
            
            $('#viewQuestionsBtn').data('enquete-id', enqueteId);
            $('#viewQuestionsBtn').data('enquete-nom', enqueteNom);
        });
    });




  document.addEventListener('DOMContentLoaded', function () {
      // Sélectionnez tous les boutons "Voir"
      const viewButtons = document.querySelectorAll('.view-enquete');

      viewButtons.forEach(button => {
          button.addEventListener('click', function () {
              // Récupérez les données de l'enquête depuis les attributs de la ligne
              const enqueteRow = this.closest('.enquete-row');
              const enqueteId = enqueteRow.getAttribute('data-id');
              const enqueteNom = enqueteRow.getAttribute('data-nom');

              // Mettez à jour le modal avec les informations de l'enquête
              document.getElementById('enqueteIdHidden').value = enqueteId;
              document.getElementById('enqueteName').textContent = enqueteNom;
          });
      });
  });






// Ajout d'un écouteur d'événement sur le bouton d'ajout de question


document.getElementById('addQuestionButton').addEventListener('click', function() {
const questionFrame = document.createElement('div');
questionFrame.className = 'question-frame';

// Champ de texte pour la question
const inputField = document.createElement('input');
inputField.type = 'text';
inputField.className = 'form-control';
inputField.placeholder = 'Votre question';
inputField.required = true;

// Message d'erreur pour le champ de texte
const errorMessage = document.createElement('div');
errorMessage.className = 'error-message';
errorMessage.style.display = 'none';
errorMessage.textContent = 'Ce champ est obligatoire';

// Menu déroulant pour le type de réponse
const selectField = document.createElement('select');
selectField.className = 'form-control mt-2';
selectField.innerHTML = `
    <option value="input">Champ uniligne</option>
    <option value="textarea">Champ multiligne</option>
    <option value="radio">Bouton radio</option>
    <option value="checkbox">Case à cocher</option>
    <option value="select">Liste déroulante</option>
    <option value="numericTel">Champ numérique (Tel)</option>
    <option value="numericAge">Champ numérique (Age)</option>
    <option value="date">Champ de date</option>
    <option value="email">Champ email</option>
    <option value="ifu">Champ ifu</option>
    <option value="file">Champ fichier</option>
    <option value="listeDep">Liste des départements</option>
    <option value="langue">Liste des langues</option>
    <option value="expInstad">Expériences avec INStaD</option>
`;
selectField.required = true;

// Conteneur pour les options dynamiques
const optionsContainer = document.createElement('div');
optionsContainer.className = 'options-container';

// Bouton de suppression
const deleteButton = document.createElement('button');
deleteButton.className = 'btn btn-danger mt-2';
deleteButton.textContent = 'Supprimer';

// Ajout d'un événement pour supprimer le cadre de question
deleteButton.addEventListener('click', function() {
    questionFrame.remove();
});

// Ajout des éléments créés au cadre de question
questionFrame.appendChild(inputField);
questionFrame.appendChild(errorMessage);
questionFrame.appendChild(selectField);
questionFrame.appendChild(optionsContainer); // Ajouter le conteneur pour les options
questionFrame.appendChild(deleteButton); // Bouton de suppression ajouté en dernier

// Ajout du cadre de question au conteneur de questions
document.getElementById('questionsContainer').appendChild(questionFrame);

// Écouteur d'événement pour la validation du champ de texte
inputField.addEventListener('input', function() {
    if (this.value) {
        errorMessage.style.display = 'none';
    }
});

// Écouteur d'événement pour le changement de type de réponse
selectField.addEventListener('change', function() {
    // Supprimer les options existantes
    optionsContainer.innerHTML = '';

    if (this.value === 'radio' || this.value === 'checkbox' || this.value === 'select') {
        const numOptionsLabel = document.createElement('label');
        numOptionsLabel.textContent = 'Nombre d\'options:';
        numOptionsLabel.className = 'num-options-label';

        const numOptionsField = document.createElement('input');
        numOptionsField.type = 'number';
        numOptionsField.className = 'form-control mt-2 num-options-field';
        numOptionsField.min = 1;
        numOptionsField.max = 6;
        numOptionsField.required = true;

        const numOptionsError = document.createElement('div');
        numOptionsError.className = 'error-message num-options-error';
        numOptionsError.style.display = 'none';
        numOptionsError.textContent = 'Le nombre d\'options est obligatoire et doit être entre 1 et 6';

        optionsContainer.appendChild(numOptionsLabel);
        optionsContainer.appendChild(numOptionsField);
        optionsContainer.appendChild(numOptionsError);

        numOptionsField.addEventListener('input', function() {
            if (this.value >= 1 && this.value <= 6) {
                numOptionsError.style.display = 'none';
            } else {
                numOptionsError.style.display = 'block';
            }

            // Supprimer les anciennes options
            optionsContainer.querySelectorAll('.option-field, .option-error').forEach(option => option.remove());

            // Ajouter les nouvelles options
            for (let i = 0; i < this.value; i++) {
                const optionField = document.createElement('input');
                optionField.type = 'text';
                optionField.className = 'form-control mt-2 option-field';
                optionField.placeholder = `Option ${i + 1}`;
                optionField.required = true;

                const optionErrorMessage = document.createElement('div');
                optionErrorMessage.className = 'error-message option-error';
                optionErrorMessage.style.display = 'none';
                optionErrorMessage.textContent = 'Ce champ est obligatoire';

                optionsContainer.appendChild(optionField);
                optionsContainer.appendChild(optionErrorMessage);

                optionField.addEventListener('input', function() {
                    if (this.value) {
                        optionErrorMessage.style.display = 'none';
                    }
                });
            }
        });

        numOptionsField.addEventListener('change', function() {
            if (this.value < 1 || this.value > 6) {
                numOptionsError.style.display = 'block';
            } else {
                numOptionsError.style.display = 'none';
            }
        });
    }
});
});




// Fonction pour ajouter une question
function addQuestion() {
// Code pour ajouter une nouvelle question (votre code existant ici)

// Retirer le message d'erreur si une nouvelle question est ajoutée
const errorContainer = document.getElementById('noQuestionError');
if (errorContainer) {
    errorContainer.remove();
}
}

// Ajout d'un écouteur d'événement sur le bouton + pour ajouter une question
document.getElementById('addQuestionButton').addEventListener('click', addQuestion);

// Fonction pour enregistrer les questions




function saveQuestions() {
const questions = document.querySelectorAll('.question-frame');
let isValid = true;
let questionsData = [];
let questionsSummary = '';

// Vérifiez s'il y a des questions
if (questions.length === 0) {
    // Afficher un message d'erreur si aucune question n'est présente
    let errorContainer = document.getElementById('noQuestionError');
    if (!errorContainer) {
        errorContainer = document.createElement('div');
        errorContainer.id = 'noQuestionError';
        errorContainer.style.color = 'red';
        errorContainer.style.marginBottom = '10px';
        errorContainer.textContent = 'Veuillez appuyer sur le bouton + pour pouvoir créer une question.';
        document.getElementById('addQuestionButton').insertAdjacentElement('beforebegin', errorContainer);
    }
    return;
} else {
    // Retirer le message d'erreur s'il y a des questions
    const errorContainer = document.getElementById('noQuestionError');
    if (errorContainer) {
        errorContainer.remove();
    }
}

// Traitement des questions si présentes
questions.forEach(question => {
    const inputField = question.querySelector('input[type="text"]');
    const selectField = question.querySelector('select');
    const numOptionsField = question.querySelector('.num-options-field');
    const optionFields = question.querySelectorAll('.option-field');
    const errorMessage = inputField.nextElementSibling;
    const numOptionsError = numOptionsField?.nextElementSibling;

    // Validation des champs de texte et des options
    if (!inputField.value.trim()) {
        isValid = false;
        errorMessage.style.display = 'block';
    } else {
        errorMessage.style.display = 'none';
    }

    let questionData = {
        question: inputField.value,
        typeReponse: selectField.value,
        nbrOption: 0,
        idEnquete: document.getElementById('enqueteIdHidden').value,
        nomEnquete: document.getElementById('enqueteName').textContent
    };

    // Ajout des options si nécessaire
    if (selectField.value === 'radio' || selectField.value === 'checkbox' || selectField.value === 'select') {
        if (!numOptionsField || numOptionsField.value < 1 || numOptionsField.value > 6) {
            isValid = false;
            numOptionsError.style.display = 'block';
        } else {
            numOptionsError.style.display = 'none';
        }

        questionData.nbrOption = numOptionsField.value;
        optionFields.forEach((optionField, index) => {
            const optionErrorMessage = optionField.nextElementSibling;
            if (!optionField.value.trim()) {
                isValid = false;
                optionErrorMessage.style.display = 'block';
            } else {
                optionErrorMessage.style.display = 'none';
            }
            questionData[`option${index + 1}`] = optionField.value;
        });
    }

    // Ajouter la question aux données de question
    questionsData.push(questionData);

    // Construire un résumé des questions pour l'alerte
    questionsSummary += `Question: ${inputField.value}\nType de réponse: ${selectField.value}\n`;
    if (selectField.value === 'radio' || selectField.value === 'checkbox' || selectField.value === 'select') {
        for (let i = 0; i < numOptionsField.value; i++) {
            questionsSummary += `Option ${i + 1}: ${questionData[`option${i + 1}`]}\n`;
        }
    }
    questionsSummary += '\n';
});

if (isValid) {
    // Afficher le résumé des questions dans une alerte
    const userConfirmed = confirm(`Veuillez vérifier les questions avant de les enregistrer:\n\n${questionsSummary}`);

    if (userConfirmed) {
        // Envoyer les données au serveur si l'utilisateur confirme
        fetch('/questionParEnquete', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            body: JSON.stringify({ questions: questionsData })
        })
        .then(response => response.json())
        .then(data => {
            alert('Questions enregistrées avec succès !');
            // Vider le conteneur de questions
            document.getElementById('questionsContainer').innerHTML = '';
            $('#questionnaireModal').modal('hide');
        })
        .catch(error => {
            console.error('Erreur lors de l\'enregistrement des questions:', error);
        });
    }
}
}


// Ajout d'un écouteur d'événement sur le bouton d'enregistrement
document.getElementById('saveQuestionsButton').addEventListener('click', saveQuestions);




document.addEventListener('DOMContentLoaded', function() {
        function handleButtonClick(event) {
            event.preventDefault(); // Empêche la soumission du formulaire
            var button = event.target;
            button.classList.add('disabled-button', 'loading');
            // Envoyez le formulaire après un délai simulé
            setTimeout(function() {
                button.closest('form').submit();
            }, 2000); // Simule 2 secondes de chargement
        }

        var saveButton = document.querySelector('.save-button');
        if (saveButton) {
            saveButton.addEventListener('click', handleButtonClick);
        }
    });




    
    // Fonction pour vérifier la date d'expiration
    function verifierDateExpiration() {
        var dateExpiration = document.getElementById("date_expiration").value;
        var today = new Date().toISOString().split('T')[0];
        
        if (dateExpiration <= today) {
            document.getElementById("message").innerText = "La date doit être supérieure à la date d'aujourd'hui.";
            document.getElementById("message").style.color = "red";
        } else {
            document.getElementById("message").innerText = "";
        }
    }

    // Écouter les changements dans le champ de date
    document.getElementById("date_expiration").addEventListener("input", verifierDateExpiration);


   
   
   
   
   
    $(document).ready(function(){
        $(".xp-menubar").on('click',function(){
          $("#sidebar").toggleClass('active');
          $("#content").toggleClass('active');
        });
        
        $('.xp-menubar,.body-overlay').on('click',function(){
           $("#sidebar,.body-overlay").toggleClass('show-nav');
        });
        
     });

     
document.addEventListener('DOMContentLoaded', function () {
// Gestionnaire d'événement pour le bouton d'exportation PDF
document.querySelector('#exportPdfModal .btn-danger').addEventListener('click', function () {
  console.log('Exporter en PDF');
  // Ajoutez votre logique d'exportation PDF ici
  // Exemple avec jsPDF :
  /*
  var doc = new jsPDF();
  doc.text('Liste des enquêtes', 10, 10);
  doc.save('liste-enquetes.pdf');
  */
  $('#exportPdfModal').modal('hide');
});

// Gestionnaire d'événement pour le bouton d'exportation Excel
//   document.querySelector('#exportExcelModal .btn-success').addEventListener('click', function () {
//     console.log('Exporter en Excel');
//     // Ajoutez votre logique d'exportation Excel ici
//     $('#exportExcelModal').modal('hide');
//   });

// Gestionnaire d'événement pour le bouton d'exportation Word
document.querySelector('#exportWordModal .btn-info').addEventListener('click', function () {
  console.log('Exporter en Word');
  // Ajoutez votre logique d'exportation Word ici
  $('#exportWordModal').modal('hide');
});

// Gestionnaire d'événement pour le bouton de création d'enquête
document.querySelector('#createSurveyModal .btn-primary').addEventListener('click', function () {
  console.log('Créer une enquête');
  // Ajoutez votre logique de création d'enquête ici
  $('#createSurveyModal').modal('hide');
});
});







                      




