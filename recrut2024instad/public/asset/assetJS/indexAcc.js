$(document).ready(function() {
    function updateButton(enqueteId, button, jobItem) {
        const url = getEnqueteStateUrl.replace(':id', enqueteId);

        $.get(url)
            .done(function(response) {
                if (response.etat_enquete === 'activé') {
                    // Si l'enquête est activée, on affiche le bouton et la carte de l'enquête
                    jobItem.show(); 
                    button.removeClass('btn-warning')
                          .addClass('btn-primary')
                          .attr('href', detailsEnqueteShowUrl.replace(':id', enqueteId))
                          .text('Consulter')
                          .removeAttr('disabled');
                } else {
                    // Si l'enquête est en maintenance, on masque l'enquête entière
                    jobItem.hide(); 
                    button.removeClass('btn-primary')
                          .addClass('btn-warning')
                          .attr('href', '#')
                          .text('Nous sommes en maintenance')
                          .attr('disabled', 'disabled');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.error('Erreur AJAX:', textStatus, errorThrown);
                button.text('Erreur de chargement');
            });
    }

    $('.postuler-button').each(function() {
        const buttonId = $(this).attr('id');
        const enqueteId = buttonId.split('-')[2];
        const button = $(this);
        const jobItem = $(this).closest('.job-item'); // Sélectionner la carte d'enquête correspondante

        updateButton(enqueteId, button, jobItem);
    });
});