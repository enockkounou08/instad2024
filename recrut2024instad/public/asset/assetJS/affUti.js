document.addEventListener('DOMContentLoaded', function() {
    function handleExportButtonClick(event, url) {
        event.preventDefault(); // Empêche le comportement par défaut du bouton
        const button = event.target;
        if (!button.classList.contains('loading')) {
            button.classList.add('disabled-button', 'loading');
            button.disabled = true; // Désactiver le bouton après le clic

            // Simule un téléchargement de fichier après un délai
            setTimeout(function() {
                window.location.href = url;
                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
                button.disabled = false; // Réactivez le bouton après le chargement
            }, 2000); // Simule 2 secondes de chargement
        }
    }

    // Attacher les événements aux boutons export
    const pdfExportButton = document.getElementById('confirmExportPdf');
    if (pdfExportButton) {
        pdfExportButton.addEventListener('click', function(event) {
            handleExportButtonClick(event, routes.exportPdfUrl);
        });
    }

    const wordExportButton = document.getElementById('confirmExportWord');
    if (wordExportButton) {
        wordExportButton.addEventListener('click', function(event) {
            handleExportButtonClick(event, routes.exportWordUrl);
        });
    }

    const excelExportButton = document.getElementById('confirmExportExcel');
    if (excelExportButton) {
        excelExportButton.addEventListener('click', function(event) {
            handleExportButtonClick(event, routes.exportExcelUrl);
        });
    }
});





document.addEventListener('DOMContentLoaded', function() {
    function handlePdfExportButtonClick(event) {
        event.preventDefault(); // Empêche le comportement par défaut du bouton
        const button = event.target;
        if (!button.classList.contains('loading')) {
            button.classList.add('disabled-button', 'loading');
            button.disabled = true; // Désactiver le bouton après le clic

            // Simule un téléchargement de fichier après un délai
            setTimeout(function() {
                window.location.href = exportPdfUrl; // Utilisation de la variable PDF export URL
                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
            }, 2000); // Simule 2 secondes de chargement
        }
    }

    const pdfExportButton = document.getElementById('confirmExportPdf');
    if (pdfExportButton) {
        pdfExportButton.addEventListener('click', handlePdfExportButtonClick);
    }
});


	
// Fonction pour vérifier la date d'expiration
function verifierDateExpiration() {
    var dateExpiration = document.getElementById("date_expiration").value;
    var today = new Date().toISOString().split('T')[0];
    
    if (dateExpiration <= today) {
        document.getElementById("message").innerText = "La date doit être supérieure à la date d'aujourd'hui.";
        document.getElementById("message").style.color = "red";
    } else {
        document.getElementById("message").innerText = "";
    }
}

// Écouter les changements dans le champ de date
document.getElementById("date_expiration").addEventListener("input", verifierDateExpiration);

                   
                   

// Fonction pour vérifier la date d'expiration
function verifierDateExpiration() {
    var dateExpiration = document.getElementById("date_expiration").value;
    var today = new Date().toISOString().split('T')[0];
    
    if (dateExpiration <= today) {
        document.getElementById("message").innerText = "La date doit être supérieure à la date d'aujourd'hui.";
        document.getElementById("message").style.color = "red";
    } else {
        document.getElementById("message").innerText = "";
    }
}

// Écouter les changements dans le champ de date
document.getElementById("date_expiration").addEventListener("input", verifierDateExpiration);
