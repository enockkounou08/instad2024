$(document).ready(function() {
    const linkElement = $('#postulez-link'); // ID du lien à mettre à jour

    function updateLink() {
        $.get(url)
            .done(function(response) {
                if (response.etat_enquete === 'activé') {
                    linkElement.attr('href', questionsRoute.replace(':id', enquêteId))
                               .text('Postulez ici')
                               .removeClass('disabled-link');
                } else {
                    linkElement.attr('href', '#')
                               .text('Nous sommes en maintenance')
                               .addClass('disabled-link');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.error('Erreur AJAX:', textStatus, errorThrown);
                alert('Erreur lors de la récupération de l\'état de l\'enquête.');
            });
    }

    // Met à jour le lien immédiatement
    updateLink();

    // Met à jour le lien toutes les 4 secondes
    setInterval(updateLink, 4000);
});




    document.addEventListener('DOMContentLoaded', function() {
        function handleButtonClick(event) {
            event.preventDefault(); // Empêche la navigation immédiate
            var button = event.target;
            button.classList.add('disabled-button', 'loading');
            // Envoyez le formulaire après un délai simulé
            setTimeout(function() {
                button.closest('form').submit();
            }, 2000); // Simule 2 secondes de chargement
        }

        var connexionButton = document.getElementById('connexionButton');
        if (connexionButton) {
            connexionButton.addEventListener('click', handleButtonClick);
        }

        var postulerButtons = document.querySelectorAll('.postuler-button, .postuler-btn');
        postulerButtons.forEach(function(button) {
            button.addEventListener('click', handleButtonClick);
        });
    });


