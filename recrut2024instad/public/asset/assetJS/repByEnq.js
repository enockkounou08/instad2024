document.addEventListener('DOMContentLoaded', function () {
    function handleExportButtonClick(event, url) {
        event.preventDefault(); // Empêche le comportement par défaut du bouton
        const button = event.target;

        if (!button.classList.contains('loading')) {
            button.classList.add('disabled-button', 'loading');
            button.disabled = true; // Désactiver le bouton après le clic

            // Simule un téléchargement de fichier après un délai
            setTimeout(function () {
                window.location.href = url;
                // Après le téléchargement, enlevez la classe de chargement
                button.classList.remove('loading');
                button.disabled = false; // Réactivez le bouton après le chargement
            }, 2000); // Simule 2 secondes de chargement
        }
    }

    // Configuration des boutons avec les URLs dynamiques
    const pdfExportButton = document.getElementById('confirmExportPdf');
    if (pdfExportButton) {
        pdfExportButton.addEventListener('click', function (event) {
            handleExportButtonClick(event, routes.pdfExport);
        });
    }

    const wordExportButton = document.getElementById('confirmExportWord');
    if (wordExportButton) {
        wordExportButton.addEventListener('click', function (event) {
            handleExportButtonClick(event, routes.wordExport);
        });
    }

    const excelExportButton = document.getElementById('confirmExportExcel');
    if (excelExportButton) {
        excelExportButton.addEventListener('click', function (event) {
            handleExportButtonClick(event, routes.excelExport);
        });
    }
});




document.getElementById('filterNom').addEventListener('change', function () {
    const selectedNom = this.value;
    const url = new URL(window.location.href);
    url.searchParams.set('nom', selectedNom);
    url.searchParams.set('page', 1); // Reset to page 1 on filter change
    window.location.href = url.toString();
});



document.getElementById('exportExcel').addEventListener('click', function() {
    // Créer un tableau pour stocker les données filtrées
    const data = [];
    const headers = [];
    
    // Récupérer les en-têtes de la table
    document.querySelectorAll('#candidatureTable thead th').forEach(th => {
        headers.push(th.innerText.trim()); // S'assurer d'enlever les espaces superflus
    });
    data.push(headers); // Ajouter les en-têtes comme première ligne du fichier Excel

    // Récupérer les lignes visibles après filtrage
    document.querySelectorAll('#candidatureTable tbody tr').forEach(row => {
        if (row.style.display !== 'none') { // Ne sélectionner que les lignes visibles
            const rowData = [];
            row.querySelectorAll('td').forEach(td => {
                rowData.push(td.innerText.trim()); // Enlever espaces ou contenus inutiles
            });
            data.push(rowData); // Ajouter chaque ligne visible aux données
        }
    });

    // Gérer les filtres actifs pour générer le nom du fichier
    let filterNames = [];

    // Vérifier les filtres appliqués sur les noms et prénoms
    const filterNom = document.getElementById('filterNom').value.trim();
    if (filterNom) {
        filterNames.push('Nom-' + filterNom);
    }
    
    const filterPrenom = document.getElementById('filterPrenom').value.trim();
    if (filterPrenom) {
        filterNames.push('Prenom-' + filterPrenom);
    }

    // Vérifier les filtres appliqués sur les réponses aux questions
    document.querySelectorAll('.filterQuestion').forEach(select => {
        const selectedValue = select.value.trim();
        if (selectedValue) {
            const questionText = select.closest('th').innerText.replace('Filtrer par ', '').trim(); // Supprimer "Filtrer par"
            // Limiter les informations du filtre au premier élément sélectionné
            filterNames.push(questionText + '-' + selectedValue);  // Prendre seulement la première valeur filtrée
        }
    });

    // Limiter la longueur du nom du fichier pour éviter des noms trop longs
    const maxFilters = 3; // Limiter à 3 filtres pour éviter un nom de fichier trop long
    const fileName = filterNames.length > 0 
        ? filterNames.slice(0, maxFilters).join('_') // Ne prendre que les 3 premiers filtres appliqués
        : 'Candidatures_ total_sans_filtrees';

    // Utiliser SheetJS pour créer et exporter le fichier Excel
    const ws = XLSX.utils.aoa_to_sheet(data); // Créer une feuille à partir des données
    const wb = XLSX.utils.book_new();         // Créer un nouveau workbook
    XLSX.utils.book_append_sheet(wb, ws, "Candidatures"); // Ajouter la feuille au workbook

    // Exporter le fichier avec le nom basé sur les filtres et leurs valeurs
    XLSX.writeFile(wb, `${fileName}.xlsx`); // Nom du fichier Excel
});


document.addEventListener('DOMContentLoaded', function() {
    const tableRows = document.querySelectorAll('#candidatureTable tbody tr');
    
    // Fonction pour filtrer les lignes du tableau en fonction des sélections
    function filterTable() {
        const filterNom = document.getElementById('filterNom').value.toLowerCase();
        const filterPrenom = document.getElementById('filterPrenom').value.toLowerCase();

        // Filtrage des questions
        const questionFilters = Array.from(document.querySelectorAll('.filterQuestion')).reduce((filters, select) => {
            const questionId = select.getAttribute('data-question-id');
            const selectedValue = select.value.toLowerCase();
            if (selectedValue) {
                filters[questionId] = selectedValue;
            }
            return filters;
        }, {});

        tableRows.forEach(row => {
            const nom = row.querySelector('td[data-nom]').getAttribute('data-nom').toLowerCase();
            const prenom = row.querySelector('td[data-prenom]').getAttribute('data-prenom').toLowerCase();
            
            let showRow = true;

            // Vérifier le nom et prénom
            if (filterNom && nom !== filterNom) {
                showRow = false;
            }

            if (filterPrenom && prenom !== filterPrenom) {
                showRow = false;
            }

            // Vérifier les questions filtrées
            for (const [questionId, filterValue] of Object.entries(questionFilters)) {
                const reponse = row.querySelector(`td[data-question-id="${questionId}"]`).getAttribute('data-reponse').toLowerCase();
                if (filterValue && reponse !== filterValue) {
                    showRow = false;
                    break;
                }
            }

            // Montrer ou masquer la ligne en fonction des filtres
            row.style.display = showRow ? '' : 'none';
        });
    }

    // Appliquer les filtres lorsque les listes déroulantes changent
    document.getElementById('filterNom').addEventListener('change', filterTable);
    document.getElementById('filterPrenom').addEventListener('change', filterTable);
    document.querySelectorAll('.filterQuestion').forEach(select => {
        select.addEventListener('change', filterTable);
    });
});


$(document).ready(function(){
    $(".xp-menubar").on('click',function(){
      $("#sidebar").toggleClass('active');
      $("#content").toggleClass('active');
    });
    
    $('.xp-menubar,.body-overlay').on('click',function(){
       $("#sidebar,.body-overlay").toggleClass('show-nav');
    });
    
 });

