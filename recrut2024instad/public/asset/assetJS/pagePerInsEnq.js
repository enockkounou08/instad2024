// Fonction pour vérifier la date d'expiration
    function verifierDateExpiration() {
        var dateExpiration = document.getElementById("date_expiration").value;
        var today = new Date().toISOString().split('T')[0];
        
        if (dateExpiration <= today) {
            document.getElementById("message").innerText = "La date doit être supérieure à la date d'aujourd'hui.";
			document.getElementById("message").style.color = "red";
		} else {
            document.getElementById("message").innerText = "";
        }
    }

    // Écouter les changements dans le champ de date
    document.getElementById("date_expiration").addEventListener("input", verifierDateExpiration);

	

    $(document).ready(function(){
        $(".xp-menubar").on('click',function(){
          $("#sidebar").toggleClass('active');
          $("#content").toggleClass('active');
        });
        
        $('.xp-menubar,.body-overlay').on('click',function(){
           $("#sidebar,.body-overlay").toggleClass('show-nav');
        });
        
     });