document.addEventListener('DOMContentLoaded', function () {
    var togglePassword = document.querySelector('#editModal #togglePassword');
    var passwordField = document.querySelector('#editModal #edit-password');
    var eyeIcon = togglePassword.querySelector('i');

    togglePassword.addEventListener('click', function () {
        var type = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordField.setAttribute('type', type);

        // Toggle the eye icon
        if (type === 'password') {
            eyeIcon.classList.remove('fa-eye-slash');
            eyeIcon.classList.add('fa-eye');
        } else {
            eyeIcon.classList.remove('fa-eye');
            eyeIcon.classList.add('fa-eye-slash');
        }
    });
});

    // Script pour remplir le formulaire de suppression
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Bouton qui a ouvert le modal
        var id = button.data('id');
        var nom = button.data('nom');
        var prenom = button.data('prenom');
        var email = button.data('email');
        
        var modal = $(this);
        modal.find('#delete-nom').text(nom);
        modal.find('#delete-prenom').text(prenom);
        modal.find('#delete-email').text(email);
        modal.find('form').attr('action', '{{ route("admin.actions.delete", ":id") }}'.replace(':id', id)); // URL de l'action pour la suppression
    });

    // Script pour remplir le formulaire d'édition
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Bouton qui a ouvert le modal
        var id = button.data('id');
        var nom = button.data('nom');
        var prenom = button.data('prenom');
        var email = button.data('email');
        
        var modal = $(this);
        modal.find('#edit-id').val(id);
        modal.find('#edit-nom').val(nom);
        modal.find('#edit-prenom').val(prenom);
        modal.find('#edit-email').val(email);
        modal.find('form').attr('action', '{{ route("admin.actions.update", ":id") }}'.replace(':id', id)); // URL de l'action pour la mise à jour
    });





