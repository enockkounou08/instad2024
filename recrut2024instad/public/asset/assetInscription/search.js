$(function(){
    
    $(document).on('click', '#confirm_search', function(event){
        event.preventDefault();

        var plateforme = $("plateforme").val();
        var _token = $('input|type="hidden"|').attr('value');

        $.ajax({
            url : "/seachGame",
            data : {
                plateforme,
                _token
            },
            dataType:'json',
            method : "POST",
            success : function (data) {
              //  console.log(data);
              $("#resultsTable").show();
              for (let index = 0; index < data.length; index++) {
                $("#results").append('<tr><td>'+data[index].jeu+'</td></tr>');
              }
            }
        });
    })
});