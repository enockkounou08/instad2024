const nextButton = document.querySelector('.btn-next');
const prevButton = document.querySelector('.btn-prev');
const steps = document.querySelectorAll('.step');
const form_steps = document.querySelectorAll('.form-step');
let active = 1;

nextButton.addEventListener('click',()=> {
    active++;
    if(active > steps.length) {
        active = steps.length;
    }
    updateProgress();
})

prevButton.addEventListener('click', () => {
    active--;
    if (active < 1) {
        active = 1;
    }
    updateProgress();

})

const updateProgress = () => {
    console.log('steps.length =>' + steps.length);
    console.log('active =>' + active);

    //
    steps.forEach((step, i) => {
        if(i == (active-1)) {
            step.classList.add('active');
            form_steps[i].classList.add('active');
            console.log('i =>' +i);
        } else {
            step.classList.remove('active');
            form_steps[i].classList.remove('active');
        }
    });
    //

    if (active === 1) {
        prevButton.disabled = true;
    }else if (active === steps.length){
        nextButton.disabled = true;

    }else {
        
        prevButton.disabled = false;
        nextButton.disabled = false;
    }
}

/****///
document.addEventListener("DOMContentLoaded", function() {
    var selectElement = document.getElementById("nationalite");
    var autreNationaliteDiv = document.getElementById("autreNationalite");

    selectElement.addEventListener("change", function() {
        if (selectElement.value === "autre") {
            autreNationaliteDiv.style.display = "block";
        } else {
            autreNationaliteDiv.style.display = "none";
        }
    });
});


/****************************** */
$(document).ready(function() {
    $('#email').on('input', function() {
        var email = $(this).val();
        var table = 'contacts'; // Remplacez 'users' par le nom de votre table
        var column = 'email'; // Remplacez 'email' par le nom de votre colonne

        $.ajax({
            url: '/check-email',
            method: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                email: email,
                table: table,
                column: column
            },
            success: function(response) {
                if (response.exists) {
                    $('#emailError').text('Cette adresse e-mail est déjà utilisée.');
                    $('#emailError').show();
                } else {
                    $('#emailError').hide();
                }
            }
        });
    });
});


