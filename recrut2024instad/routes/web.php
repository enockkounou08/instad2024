<?php

use App\Models\Personne;
use App\Exports\EnquetesExport;
use App\Exports\UtilisateursExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Route;
use App\Exports\UtilisateursWordExport;
use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MotDePassOublie;
use App\Http\Controllers\VilleController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SelftCandidature;
use App\Http\Controllers\EnqueteController;
use App\Http\Controllers\ReponseController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\ConnexionController;
use App\Http\Controllers\test\testcontroller;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\InteractionUserAdmin;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\SuperAdminControlleur;
use App\Http\Middleware\VerifUserSessionActive;
use App\Http\Controllers\AdminProfileController;
use App\Http\Controllers\postulationControlleur;
use App\Http\Controllers\PosteRoleadminController;
use App\Http\Controllers\QuestionSuperAdminControlleur;
use App\Http\Controllers\ReponseUserPourQuesttionController;
use App\Http\Controllers\AdminCreatAttestationControlleur;
use App\Http\Controllers\AdminCreatTypeAttestationControlleur;


//use Maatwebsite\Excel\Facades\Excel;

//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



// Routes pour les évaluations
Route::get('/pageEvaluation', [EvaluationController::class, 'Evalution'])->middleware('checkUserSession');

// Routes pour les questions
Route::get('/pageQuestion', [QuestionController::class, 'question'])->middleware('checkUserSession');

// Routes pour la connexion (ne nécessitent pas d'authentification)
Route::get('/pageConnexionQuestion', [QuestionController::class, 'connexionquestion'])->name('questionpourevaluation');

Route::post('/pageConnexionQuestion', [QuestionController::class, 'connexionquestionTraitement']);

// Route pour répondre aux questions (nécessite une session active)
Route::post('/pageQuestione', [ReponseController::class, 'pageQuestion'])->middleware('checkUserSession');



//Route::get('/', function () {
   // return view('welcome');
//});

// Route::get('/r', function () {
//    return view('SuperAdmin.homer');
// });


 Route::get('/verify/{token}', [testcontroller::class, 'verify']);


//route pour aciver son compte avec code d'activaion
Route::post('/activate', [InscriptionController::class, 'activateAccount'])->name('activateAccount');

Route::get('/activation', [InscriptionController::class, 'showActivationForm'])->name('showActivationForm');


//L'utilisateur connecté pourra voir ces candidatures
Route::get('/mes-candidatures', [SelftCandidature::class, 'showCandidatures'])->name('mes-candidatures')->middleware('VerifUserSessionActive');
//6

//l'utilisateur qui a oublié son mot de passe pourra chnger sont mot de passe
Route::get('/ChagerMotDePasse',[MotDePassOublie::class, 'forgotPassword'])->name('MotDePassOublie');

// Route pour afficher le formulaire de demande de réinitialisation de mot de passe
Route::get('/forgot-password', [MotDePassOublie::class, 'showRequestForm'])->name('password.request');

// Route pour envoyer l'e-mail de réinitialisation de mot de passe
Route::post('/forgot-password', [MotDePassOublie::class, 'sendResetLink'])->name('password.email');

// Route pour afficher le formulaire de réinitialisation de mot de passe
Route::get('/reset-password/{token}', [MotDePassOublie::class, 'showResetForm'])->name('password.reset');

// Route pour réinitialiser le mot de passe
Route::post('/reset-password', [MotDePassOublie::class, 'resetPassword'])->name('password.update');
     
//affchiage de la page d'accueil
Route::get('/',[HomeController::class, 'index'])->name('Accueil_page')
//->middleware('VerifUserSessionActive')
;
Route::get('/pageAccueil',[HomeController::class, 'index'])->name('Accueil_page')//->middleware('VerifUserSessionActive')
;

 // A propos de nous
Route::get('/profileUserView',[HomeController::class, 'profileUserView'])->name('profileUserView');
Route::get('/profileUser',[HomeController::class, 'profileUser'])->name('profileUser');
Route::post('/profileUserPost',[HomeController::class, 'profileUserPost'])->name('profileUserPost');
Route::get('/aproposdenous',[HomeController::class, 'about'])->name('aproposdenous');

// services dans le footer
Route::get('/services',[HomeController::class, 'service'])->name('services');

// politique d'utilisation
Route::get('/politique',[HomeController::class, 'politique'])->name('politique');

 // Contact
Route::get('/contact',[HomeController::class, 'contact'])->name('contactez_nous');

 Route::post('/contact',[HomeController::class, 'sendInformation']);

 //connexion
 Route::get('/pageConnexion',[ConnexionController::class, 'connexion'])->name('connexion_page');

 Route::get('/connexpage',[ConnexionController::class, 'connex']); 

 Route::post('/pageConnexion',[ConnexionController::class, 'connexionTraitement']);
 
 //inscription
 Route::get('/pageInscription',[InscriptionController::class, 'inscription'])->name('inscription_page');

Route::post('/exist_email',[InscriptionController::class, 'existEmail'])->name('app_exist_email');

 Route::post('/pageInscription',[InscriptionController::class, 'inscriptionTraitement'])->name('inscription_page_traitement');

 Route::get('/afterInscription',[InscriptionController::class, 'inscriptionSuccess'])->name('apresinscription_page');

 //postulation pour une enquête
 Route::get('/contenuEnquête',[postulationControlleur::class, 'postuler'])->name('contenuenquete_page')->middleware('VerifUserSessionActive');
//1

//déconnection
//
Route::get('/logout', [InscriptionController::class, 'logout'])->name('logout');
 
//Route protéger
 
Route::get('/search', [InteractionUserAdmin::class, 'search'])->name('search');

//Interraction du super Utilisateur et page d'acceuil
Route::post('/postuler/{id}', [InteractionUserAdmin::class, 'postuler'])->name('postuler')->middleware('VerifUserSessionActive');
//2

Route::get('/pageUserContenuEnquette/{id}', [InteractionUserAdmin::class, 'infoEnquette'])->name('detailsenquete.show');

// Route::post('/postuler-ou-annuler/{idEnquete}', [InteractionUserAdmin::class, 'postulerOuAnnuler'])
// ->name('postuler-ou-annuler')->middleware('VerifUserSessionActive');
   //3

 //postulaion pour une enquête
 Route::get('/contenuEnquête',[postulationControlleur::class, 'postuler'])->middleware('VerifUserSessionActive');
 //4



// Route pour gérer l'annulation de la postulation à une enquête
Route::post('/enquetes/{id}/annuler-postulation', [EnqueteController::class, 'annulerPostulation'])->name('enquetes.annuler_postulation')->middleware('VerifUserSessionActive');
//5


// Route de secours pour les utilisateurs non connectés
//


Route::get('/ListeDesEnqueteDisponiblePDF', [SuperAdminControlleur::class, 'exportPdf'])->name('export.pdf');

Route::get('/ListeDesEnqueteDisponibleExcel', [SuperAdminControlleur::class, 'EnqueteDisponible'])->name('enqueteDisponibleExcel');

Route::get('/ListeDesEnqueteDisponibleWord',  [SuperAdminControlleur::class, 'exportToWord'])->name('enqueteDisponibleExcelWord');



Route::get('/pageUserAdminPersonneInscritPourEnquette/{id}',[SuperAdminControlleur::class, 'candidaturesByEnqu'])->name('inscritpourenquete.show');

Route::get('/pageUserAdminReponsePersonneParEnquette/{id}',[SuperAdminControlleur::class, 'reponseByEnqu'])->name('reponsepourenquete.show');

Route::get('/exportListePostuleBy/{id}',[SuperAdminControlleur::class, 'candidatPDF'])->name('candidateurPDF');

Route::get('/exportListeCandidatBy/{id}',[SuperAdminControlleur::class, 'exportToExcelCandidat'])->name('candidateurExcel');

Route::get('/exportListeCandidatureBy/{id}',[SuperAdminControlleur::class, 'exportToWordCandidat'])->name('candidateurWord');
Route::get('/exportListeCandidatureBy/{id}/-to-word', [SuperAdminControlleur::class, 'exportToWordCandidature'])->name('exportToWordCandidature');

Route::get('/contenu/{id}', [SuperAdminControlleur::class, 'afficherContenu'] )->name('contenu.enquete');


Route::get('/pageUserAdminEnquetteEdit/{id}',[SuperAdminControlleur::class, 'updateEnquette']);

Route::post('/pageUserAdminEnquetteEdit',[SuperAdminControlleur::class, 'updateEnquette_traitement'])->middleware('check.admin.session');


Route::get('/pageUserAdmin',[SuperAdminControlleur::class, 'pageAccueil'])->middleware('check.admin.session')->name('accueiladmin_page');

// Route::get('/accueiladmin_page', [AdminController::class, 'accueil'])->middleware('checkSession')->name('accueiladmin_page');


Route::post('/AdminSaveEnquette',[SuperAdminControlleur::class, 'createEnquette'])->name('Creer.une.enquet')->middleware('check.admin.session');

Route::get('/pageUserAdminEnquetteDelate/{id}', [SuperAdminControlleur::class, 'delateEnquette'])->middleware('check.admin.session')->name('suppressionenquete.show');

Route::delete('/pageUserAdminEnquetteDelated/{id}', [SuperAdminControlleur::class, 'delateEnquetteTraitement'])->middleware('check.admin.session');


Route::get('/affichageUtilisateur', [SuperAdminControlleur::class, 'utilisateurInscrit'])->name('utilisateurinscrit_page')-> middleware('check.admin.session');

Route::get('/utilisateurs/pdf-preview', [SuperAdminControlleur::class, 'pdfPreview'])->name('listeDesUtilisateurInscritsPDF')->middleware('VerifUserSessionActive');

Route::get('/utilisateurs/word-preview', [SuperAdminControlleur::class, 'lesInscritExportToWord'])->name('listeDesUtilisateurInscritsWord')->middleware('VerifUserSessionActive');

Route::get('/utilisateurs/export-excel', [SuperAdminControlleur::class, 'exportExcel'])->name('utilisateurs.export-excel')->middleware('VerifUserSessionActive');






 Route::post('/questionParEnquete', [SuperAdminControlleur::class, 'question'])->name('question.par.e');
Route::get('/questionsParEnquete/{id}', [SuperAdminControlleur::class, 'getQuestions'])->name('questions.par.enquete');
Route::get('/questionsParEnquete/{idEnquete}', [SuperAdminControlleur::class, 'getQuestionsByEnquete']);



Route::get('/questionDetailsModif/{id}', [SuperAdminControlleur::class, 'pageModifierQuestion'])->name('question.details.Modif');
Route::get('/questionDetails/{id}', [SuperAdminControlleur::class, 'showQuestionDetails'])->name('question.details');
Route::put('/editQuestion/{id}', [SuperAdminControlleur::class, 'editQuestion'])->name('editQuestion');
Route::delete('/deleteQuestion/{id}', [SuperAdminControlleur::class, 'deleteQuestion'])->name('deleteQuestion');


// Route::delete('/deleteQuestion/{id}', [SuperAdminControlleur::class, 'deleteQuestion']);
// Route::put('/editQuestion/{id}', [SuperAdminControlleur::class, 'editQuestion']);


 // Route::get('/QuestionsDisponibles', [SuperAdminControlleur::class, 'DisponibleQuestions'])->name('questions.dispo.par.enquete')->middleware('VerifUserSessionActive');//------------
Route::get('/questions-dispo-par-enquete/{id}', [InteractionUserAdmin::class, 'showEnqueteQuestions'])->name('questions.dispo.par.enquete');

Route::post('/saveQuestions', [QuestionSuperAdminControlleur::class, 'saveQuestions']);
Route::post('/save-questions', [QuestionSuperAdminControlleur::class, 'saveQuestions'])->name('save.questions');
// web.php
Route::post('/questions/store', [SuperAdminControlleur::class, 'store'])->name('questions.submit');
// In routes/web.php or routes/api.php
Route::post('/questions/submit', [SuperAdminControlleur::class, 'storeQuestions'])->name('questions.submit');
// routes/web.php ou routes/api.php

Route::post('/store-questions', [SuperAdminControlleur::class, 'storeQuestions']);
Route::post('/path/to/your/target/page', [App\Http\Controllers\QuestionSuperAdminControlleur::class, 'handleFormSubmission']);
// web.php
Route::post('/save-questions', [QuestionSuperAdminControlleur::class, 'saveQuestions']);



Route::post('/store-responses', [ReponseUserPourQuesttionController::class, 'submitSurvey'])->name('submitSurvey');

Route::get('/questions-dispo-pour-enquete/{id}', [QuestionSuperAdminControlleur::class, 'showEnqueteQuestions'])->name('admin.questions.dispo.par.enquete');




Route::post('/submit-survey/{idEnquete}', [ReponseUserPourQuesttionController::class, 'reponseCandidature'])->name('reponseCandidature')->middleware('VerifUserSessionActive');




Route::post('/check-candidature-status', [ReponseUserPourQuesttionController::class, 'checkUserCandidature'])->name('check.candidature.status');


Route::get('/survey/{id}/check-application', [ReponseUserPourQuesttionController::class, 'checkApplicationStatus'])->name('check.application.status');
Route::get('/afterReponse', [ReponseUserPourQuesttionController::class, 'afterReponse'])->name('afterReponse');



Route::get('/pageUserAdmin/admin-profile', [AdminProfileController::class, 'showAdminProfile'])->name('admin.profile');
Route::post('/pageUserAdmin/creer-Administrateur', [AdminProfileController::class, 'submitForm'])->name('admin.create');



Route::post('/pageUserAdmin/admin-actions/update/{id}', [AdminProfileController::class, 'update'])->name('admin.actions.update');
Route::delete('/pageUserAdmin/admin-actions/delete/{id}', [AdminProfileController::class, 'destroy'])->name('admin.actions.delete');


Route::get('/pageConnexion/-admin-profile', [AdminProfileController::class, 'showLoginForm'])->name('admin.profile.connexion');



Route::post('/pageConnexion/-admin-profile', [AdminProfileController::class, 'showLoginFormLog'])->name('admin.profile.login');


// Routes en rapport avec l'ajout d'une nouvelle langue 

Route::get('/speaklanguage', [FormController::class, 'AffichageFormLangue'])->name('voirForm.langue');

Route::post('/speaklanguage', [FormController::class, 'SoumissionFormLangue'])->name('valider.langue');

// Routes en relation avec l'ajout d'une nouvele ville

Route::get('/addCity', [FormController::class, 'ShowFormCity'])->name('voirForm.ville');

Route::post('/addCity', [FormController::class, 'SubmitFormCity'])->name('valider.ville');

// Routes en rapport avec l'ajout d'un nouveau rôle administrateur  

Route::get('/pageAdmin/permission/ajoutRole', [SuperAdminControlleur::class, 'ShowFormAdminAddRole'])->name('showRole.admin');

Route::post('/pageAdmin/permission/ajoutRole', [SuperAdminControlleur::class, 'SubmitFormAdminAddRole'])->name('submitRole.admin');

// Routes en rapport avec l'ajout d'un poste 

Route::get('/posteAdmin/add', [SuperAdminControlleur::class, 'ShowFormAdminAddPoste'])->name('voirForm.poste');

Route::post('/posteAdmin/add', [SuperAdminControlleur::class, 'SubmitFormAdminAddPoste'])->name('valider.poste');




Route::post('/update-enquete-state/{id}', [AdminProfileController::class, 'updateState'])->name('update.enquete.state');

Route::get('/get-enquete-state/{id}', [AdminProfileController::class, 'getState'])->name('get.enquete.state');



Route::get('/getQuestion/{id}', [EnqueteController::class, 'getQuestion'])->name('get.question');
Route::get('/admin/edit-question/{id}', [EnqueteController::class, 'editQuestion'])->name('admin.edit.question');

Route::put('/admin/update-question/{id}', [EnqueteController::class, 'updateQuestion'])->name('admin.update.question');

Route::get('/questions/edit/{id}', [EnqueteController::class, 'edit'])->name('admin.edit.question');// Route pour mettre à jour une question
Route::put('/questions/update/{id}', [EnqueteController::class, 'update'])->name('admin.update.question');




// Route pour afficher le formulaire d'édition d'une question
Route::get('/edit-question/{enqueteId}/{questionId}', [EnqueteController::class, 'editQuestion'])
    ->name('admin.edit.question');

// Route pour mettre à jour une question
Route::post('/update-question/{questionId}', [EnqueteController::class, 'updateQuestion'])
    ->name('admin.update.question');


    Route::get('/admin/delete-question/{enqueteId}/{questionId}', [EnqueteController::class, 'deletePage'])
    ->name('admin.delete.question');


// Route pour mettre à jour la question
Route::put('/enquete/{enqueteId}/question/{questionId}/update', [EnqueteController::class, 'updateQuestion'])->name('question.update');

// Route pour supprimer la question
Route::delete('/enquete/{enqueteId}/question/{questionId}/delete', [EnqueteController::class, 'deleteQuestion'])->name('question.delete');

//route pour créer une atestation

    Route::resource('attestation', AdminCreatAttestationControlleur::class);
    Route::resource('attestationType', AdminCreatTypeAttestationControlleur::class);

// Route pour afficher le formulaire d'édition d'une question
Route::get('/searchEnquete', [AdminCreatAttestationControlleur::class, 'searchEnquete'])->name('admin.searchEnquete');

//route faire les affectations 

Route::get('/affectation', [FormController::class, 'affecter_role_form'])->name('affectation');

Route::post('/affectation', [FormController::class, 'affecter_role']);



// Exporter en PDF
Route::get('/attestation/export/pdf', [AdminCreatAttestationControlleur::class, 'exportPDF'])->name('attestation.export.pdf');

// Exporter en Excel
Route::get('/attestation/export/excel', [AdminCreatAttestationControlleur::class, 'exportExcel'])->name('attestation.export.excel');




