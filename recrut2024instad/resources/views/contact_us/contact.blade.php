{{-- @extends('menu')

@section('title', 'A propos de nous !')

@section('content') --}}

   <!DOCTYPE html>
      <html lang="fr">
      <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Contactez-Nous !!!</title>
          <link rel="stylesheet" href="{{ asset('asset/assetHome/css/stylecontact.css') }}">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
      </head>
      <body>
          <div class="container">
              <div class="form-container">
                  <center>
                      @if(session('flash_message'))
                          <div class="alert alert-success">
                              {{ session('flash_message') }}
                          </div>
                      @endif
                      @if(session('error'))
                          <div class="alert alert-danger">
                              {{ session('error') }}
                          </div>
                      @endif
                  </center>
                  <form action="/contact" method="POST">
                      @csrf
                      <h1>Contactez-nous</h1>
                      <div class="input-group">
                          <label>Votre Nom Complet</label>
                          <div class="input-field">
                              <input type="text" autocomplete="off" name="nom" value="{{ old('nom') }}">
                              <i class="fas fa-user"></i>
                          </div>
                          @error('nom')
                              <p class="error">{{ $message }}</p>
                          @enderror
                      </div>
                      <div class="input-group">
                          <label>Votre adresse e-mail</label>
                          <div class="input-field">
                              <input type="email" autocomplete="off" name="email" value="{{ old('email') }}">
                              <i class="fas fa-envelope"></i>
                          </div>
                          @error('email')
                              <p class="error">{{ $message }}</p>
                          @enderror
                      </div>
                      <div class="input-group">
                          <label>Votre téléphone</label>
                          <div class="input-field">
                              <input type="text" autocomplete="off" name="tel" value="{{ old('tel') }}">
                              <i class="fas fa-mobile"></i>
                          </div>
                          @error('tel')
                              <p class="error">{{ $message }}</p>
                          @enderror
                      </div>
                      <div class="input-group">
                          <label>Message</label>
                          <div class="input-field">
                              <textarea placeholder="Saisissez ici..." name="message">{{ old('message') }}</textarea>
                          </div>
                          @error('message')
                              <p class="error">{{ $message }}</p>
                          @enderror
                      </div>
                      <div class="button-group">
                          <button type="button" onclick="window.history.go(-1); return false;">Retour</button>
                          <button type="submit">Envoyer le message</button>
                      </div>
                  </form>
              </div>
          </div>
      </body>
      </html>
      
      {{-- @endsection --}}