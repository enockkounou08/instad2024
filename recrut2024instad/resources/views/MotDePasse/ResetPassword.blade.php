<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/reset.css') }}">
    <title>Réinitialisation du Mot de Passe</title>
    
</head>
<body>
    <div class="container">
        <h2>Réinitialiser le Mot de Passe</h2>
        <form action="{{ route('password.update') }}" method="post">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <label for="password">Nouveau Mot de Passe</label>
            <input type="password" name="password" required>

            <label for="password_confirmation">Confirmer le Mot de Passe</label>
            <input type="password" name="password_confirmation" required>

            <button type="submit">Réinitialiser le Mot de Passe</button>

            @error('password')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </form>
    </div>
</body>
</html>
