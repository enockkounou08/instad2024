<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/reset.css') }}">
    <title>Réinitialiser le mot de passe</title>
    
</head>
<body>
    <div class="form-container">
        <h1>Réinitialiser le mot de passe</h1>
        <form action="{{ route('password.email') }}" method="post">
            @csrf
            <label for="email">Veuillez entrer votre adresse email</label>
            <input type="email" name="email" required>
            <button type="submit">Envoyer le mail</button>
            @if (session('status'))
                <div class="status-message">{{ session('status') }}</div>
            @endif
            @error('email')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </form>
    </div>
</body>
</html>
