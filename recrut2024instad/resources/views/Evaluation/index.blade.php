<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="{{ asset('asset/assetsEvaluation/style.css') }}">
</head>
<body>
  <div class="container">
    <div class="container-onglets">
    <div class="onglets active" 

    data-anim="1" >
          
    @if(session()->has('user_nom') && session()->has('user_id') && session()->has('user_prenom'))
        <font size="4px">  Informations Mr {{ session('user_nom') }} </font>
    @else
    Informations Mr
    @endif
  
  </div>
    <div class="onglets" data-anim="2" >Note sur 20 obtenus </div>
     <div class="onglets" data-anim="3" >Résultats </div>
    </div>

    <div class="contenu activeContenu" data-anim="1" >
      <h3>Nous vous informons</h3>
      <hr>
      <p>Nous vous informons que <strong> <mark> <big>vous avez déjà participez à cet exament</big> </mark></strong> cliquez sur l'onglet suivant pour voir votre note</p>
    </div>

  

    <div class="contenu " data-anim="2" >
      <h3>Votre note</h3>
      <hr>


   


    @if(session()->has('user_nom') && session()->has('user_id') && session()->has('user_prenom'))


      <p>Après avoir passer le notre teste vous avez obtenue une note de : 
        <strong> <font color=" green" >
        @if(isset($noteUtilisateur))
         {{ $noteUtilisateur }}  /20</font>
    @endif
     
      
      </font></strong> </p>
      @endif
    </div>

    <div class="contenu " data-anim="3" >
      <h3>Résultats</h3>
      <hr>
      <p>Lorem ipsum dolor sit amet
         consectetur adipisicing elit. Nisi
          reiciendis laborum aut minus, non
           sapiente neque expedita ipsam, saepe sint veniam modi temporibus id illum illo
            impedit, voluptates repudiandae quisquam. Lorem ipsum dolor sit amet consectetur 
            adipisicing elit. Corporis, dicta voluptatibus laborum quaerat fuga accusantium magnam in ex et
         est dolor. Ad voluptates laborum alias?</p>
    </div>  
  </div>
  <script src=" {{ asset('asset/assetsEvaluation/app.js') }}"></script>
</body>
</html>