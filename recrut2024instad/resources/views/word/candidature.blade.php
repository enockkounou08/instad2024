<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapport d'enquête</title>
</head>
<body>
    <h1>Rapport d'enquête</h1>
    
    <h2>Enquête</h2>
    <p>Nom : {{ $enquete->nom }}</p>
    <p>Date d'expiration : {{ $enquete->date_expiration }}</p>
    <!-- Ajoutez d'autres détails de l'enquête ici -->

    <h2>Candidatures</h2>
    <table border="1">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Date de Naissance</th>
                <th>Nationalité</th>
                <th>Numéro WhatsApp</th>
                <th>Numéro Téléphone</th>
                <th>Email</th>
                <th>Type de Pièce</th>
                <th>Numéro de Pièce</th>
                <th>Langue 1</th>
                <th>Langue 2</th>
                <th>Langue 3</th>
                <th>Diplôme</th>
            </tr>
        </thead>
        <tbody>
            @foreach($candidatures as $candidature)
                <tr>
                    <td>{{ $candidature->nom ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->prenom ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->date_of_birth ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->nationalite ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->num_whatsapp ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->num_tel ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->email ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->type_piece ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->num_piece ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->langue1 ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->langue2 ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->langue3 ?? 'Non disponible' }}</td>
                    <td>{{ $candidature->diplome ?? 'Non disponible' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
     @include('sweetalert::alert')
    
</body>
</html>
