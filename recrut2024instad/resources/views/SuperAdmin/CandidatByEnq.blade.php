<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Page Administrateur</title>
	    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/bootstrap.min.css') }}">
	
	    <!----css3---->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/repEnq.css') }}">
		
		
		<!--google fonts -->
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
	
	
	   <!--google material icon-->
      <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

  </head>
  <body>
  


<div class="wrapper">
     
	  <div class="body-overlay"></div>
	 
	 <!-------sidebar--design------------>
	 
	  @include('SuperAdmin.SideBar')
   <!-------sidebar--design- close----------->
   
   
   
      <!-------page-content start----------->
   
      <div id="content">
	     
		  <!------top-navbar-start-----------> 
		     
	                  @include('SuperAdmin.NavBar')

		  <!------top-navbar-end-----------> 
		  
		  
		   <!------main-content-start-----------> 
		     
		      <div class="main-content">
			     <div class="row">
				    <div class="col-md-12">
					   <div class="table-wrapper">
					   <center>
								<div>
									@if (session('creationEnquette'))
								<div class="alert alert-success">
								{{ (session('creationEnquette')) }}
								</div>
								@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('status'))
						<div class="alert alert-success">
						{{ (session('status')) }}
						</div>
						@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('statusSup'))
						<div class="alert alert-danger">
						{{ (session('statusSup')) }}
						</div>
						@endif
							</div>
						</center>
						
						
<!-- 
					   <div class="table-title">
					     <div class="row">
						     <div class="col-sm-6 p-0 flex justify-content-lg-start justify-content-center">
							 <h2 class="ml-lg-2"></h2>

							 </div>
							 <div class="col-sm-6 p-0 flex justify-content-lg-end justify-content-center">
							   <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
							   <i class="material-icons">&#xE147;</i>
							   <span>Créer une enquête</span>
							   </a>
							  
							   </a>
							 </div>
					     </div>
					   </div> -->
					 

					   
					   
<div class="table-title">
  <div class="row">
    <div class="col-12 d-flex justify-content-between align-items-center p-0">
      <h2 class="ml-lg-2">Nom de l'enquête:  {{ $enque->nom }}</h2>
      <div class="button-group">
        
        <a href="#exportExcelModal" class="btn btn-success" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/excel.png') }}" alt="Exporter Excel" style="height: 20px; width: 20px;">
          <span>Exporter la liste excel</span>
        </a>
        <a href="#exportWordModal" class="btn btn-info" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/word.png') }}" alt="Exporter Word" style="height: 20px; width: 20px;">
          <span>Exporter la liste word</span>
        </a>
        <a href="#exportPdfModal" class="btn btn-danger" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/pdf.png') }}" alt="Exporter PDF" style="height: 20px; width: 20px;">
          <span>Exporter la liste pdf</span>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals -->

<!-- Create Survey Modal -->
<div id="createSurveyModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content create-survey-modal">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez créer une nouvelle enquête ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary">Créer</button>
      </div>
    </div>
  </div>
</div>

<!-- Export Excel Modal -->
<div id="exportExcelModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-excel-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en Excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en Excel ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <!-- <button type="button" class="btn btn-success">Exporter</button> -->
        <button type="button" class="btn btn-success" id="confirmExportExcel">Télécharger</button>

      </div>
    </div>
  </div>
</div>


<!-- Export Word Modal -->
<div id="exportWordModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-word-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en Word</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en Word ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <!-- <button type="button" class="btn btn-info">Exporter</button> -->

        <button type="button" class="btn btn-info" id="confirmExportWord">Télécharger le word</button>

      </div>
    </div>
  </div>
</div>






<div id="exportPdfModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-pdf-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en PDF</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en PDF ?</p>
        <!-- <iframe id="pdfPreviewFrame" src="" style="width: 100%; height: 400px; border: none;"></iframe> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-danger" id="confirmExportPdf">Télécharger le pdf</button>
		

      </div>
    </div>
  </div>
</div>

<script>
  const pdfExportUrl = "{{ route('candidateurPDF', ['id' => $enque->id]) }}";
  const wordExportUrl = "{{ route('exportToWordCandidature', ['id' => $enque->id]) }}";
  const excelExportUrl = "{{ route('candidateurExcel', ['id' => $enque->id]) }}";
</script>

									 
<table class="table table-striped table-hover">
        <thead>
            <tr>
                {{-- <th>Nom enquête</th> --}}
                <th>Nom postulant</th>
				<th>Prénoms postulant</th>
                <th>Date de naissance postulant</th>
                <th>Nationalité postulant</th>
				<th>Numéro whatshapp postulant</th>
                <th>Téléphone du postulant</th>
                <th>Email postulant</th>
                <th>Type de pièce</th>
                <th>Numéro pièce</th>
                <th>1ère langue parlée</th>
                <th>2ème langue parlée</th>
                <th>3ème langue parlée</th>
                <th>Diplôme</th>
				
            </tr>
        </thead>
        <tbody>
            @if($candidatures->isEmpty())
                <tr>
                    <td colspan="13" style="text-align: center;">Pas de postulant pour le moment.</td>
                </tr>
            @else
                @foreach($candidatures as $candidature)
                    <tr>
                        {{-- <td>{{ $enquete->nom }}</td> <!-- Nom de l'enquête --> --}}
                        <td>{{ $candidature->nom ?? 'Non disponible' }}</td>
						            <td>{{ $candidature->prenom ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->date_of_birth ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->nationalite ?? 'Non disponible' }}</td>
						            <td>{{ $candidature->num_whatsapp ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->num_tel ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->email ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->type_piece ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->num_piece ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->langue1 ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->langue2 ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->langue3 ?? 'Non disponible' }}</td>
                        <td>{{ $candidature->diplome ?? 'Non disponible' }}</td>
                        <td></td>
						
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    <div class="pagination-wrapper">
      {{ $candidatures->links('vendor.pagination.bootstrap-5') }}
    </div>
					
					
					   </div>
					</div>
					

					   <!----add-modal start--------->
		<div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			

	  <form action="{{asset('/AdminSaveEnquette')}}" method="POST">
    	@csrf
      <div class="modal-body">
			<div class="form-group">
				<label>Nom de l'enquête </label>
				<input name="nom" type="text" class="form-control" required >
				@error('nom')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
	
			<div class="form-group">
				<label>Contenu de l'enquête</label>
				<textarea name="contenu"class="form-control" required ></textarea>
				@error('contenu')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
			<div class="form-group">
            <label>Choisissez la date d'expiration de la candidature à l'enquête</label>
            <input type="date"  id="date_expiration" name="date_expiration" class="form-control" required >
            @error('date_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
			<p id="message"></p>

        </div>

		<div class="form-group">
            <label>Choisissez l'heure d'expiration de la candidature à l'enquête</label>
            <input type="time" id="heure_expiration" name="heure_expiration" class="form-control" required >
            @error('heure_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
        </div>
      	</div>
      <div class="modal-footer">
	  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success" >Enregistrer</button>
		<!--a type="submit" class="btn btn-success" href="{{ ('/AdminSaveEnquette')}}">Add</a-->
      </div>

	</form>
    </div>
  </div>
</div>

				  

<form action="#" method="POST">
								@csrf
								<div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Employees</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <p>Voulez-vous vraiment modifier cet Article?</p>
                </div>
                <p id="articleId"></p> <!-- Placeholder to display the article ID -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">NON</button>
                <a id="editButton" class="btn btn-success" href="">OUI</a>
            </div>
        </div>
    </div>
</div>
 

			</form>
		


			

					   <!----edit-modal end--------->	   
					   
					   
					 <!----delete-modal start--------->
		<div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Records</p>
		<p class="text-warning"><small>this action Cannot be Undone,</small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Delete</button>
      </div>
    </div>
  </div>
</div>

					   <!----edit-modal end--------->   
					   
					 	
					
				 
			     </div>
			  </div>
		  
		    <!------main-content-end-----------> 
		  
		 
		 
		 <!----footer-design------------->
		  
	                                                  @include('SuperAdmin.footer')

		 
 
	  </div>
   
</div>



<!-------complete html----------->



<script src="{{ asset('asset/assetJS/candidatbyenq.js') }}"></script>

  
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="{{ asset('asset/assetSuperAdmin/js/jquery-3.3.1.slim.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/popper.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/bootstrap.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/jquery-3.3.1.min.js')}}"></script>
  
  
  <script type="text/javascript">
       $(document).ready(function(){
	      $(".xp-menubar").on('click',function(){
		    $("#sidebar").toggleClass('active');
			$("#content").toggleClass('active');
		  });
		  
		  $('.xp-menubar,.body-overlay').on('click',function(){
		     $("#sidebar,.body-overlay").toggleClass('show-nav');
		  });
		  
	   });
  </script>
  
  



  </body>
  
  </html>


