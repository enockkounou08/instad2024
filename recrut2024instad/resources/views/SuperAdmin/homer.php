<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modals imbriqués</title>
    <link rel="stylesheet" href="styles.css">
    <style>
      /* Styles de base pour le body */
body {
    font-family: Arial, sans-serif;
}

/* Style des modals */
.modal {
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
    padding-top: 60px;
}

/* Style du contenu des modals */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* Style du bouton de fermeture */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

    </style>
</head>
<body>
    <button id="openModal1">Ouvrir Modal 1</button>

    <!-- Modal 1 -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <span class="close" data-close="modal1">&times;</span>
            <p>Contenu du Modal 1</p>
            <button id="openModal2">Ouvrir Modal 2</button>
        </div>
    </div>

    <!-- Modal 2 -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <span class="close" data-close="modal2">&times;</span>
            <p>Contenu du Modal 2</p>
            <button id="openModal3">Ouvrir Modal 3</button>
        </div>
    </div>

    <!-- Modal 3 -->
    <div id="modal3" class="modal">
        <div class="modal-content">
            <span class="close" data-close="modal3">&times;</span>
            <p>Contenu du Modal 3</p>
        </div>
    </div>

    <script src="script.js"></script>
</body>
</html>


<script>
    
document.addEventListener('DOMContentLoaded', () => {
    // Fonction pour fermer tous les modals ouverts
    function fermerTousLesModals() {
        document.querySelectorAll('.modal').forEach(modal => {
            modal.style.display = 'none';
        });
    }

    // Ouvrir les modals
    document.getElementById('openModal1').addEventListener('click', () => {
        fermerTousLesModals();
        document.getElementById('modal1').style.display = 'block';
    });

    document.getElementById('openModal2').addEventListener('click', () => {
        fermerTousLesModals();
        document.getElementById('modal2').style.display = 'block';
    });

    document.getElementById('openModal3').addEventListener('click', () => {
        fermerTousLesModals();
        document.getElementById('modal3').style.display = 'block';
    });

    // Fermer les modals
    document.querySelectorAll('.close').forEach(button => {
        button.addEventListener('click', () => {
            const modalId = button.getAttribute('data-close');
            document.getElementById(modalId).style.display = 'none';
        });
    });

    // Fermer tous les modals lorsqu'on clique à l'extérieur
    window.addEventListener('click', (event) => {
        if (event.target.classList.contains('modal')) {
            fermerTousLesModals();
        }
    });
});

</script>
