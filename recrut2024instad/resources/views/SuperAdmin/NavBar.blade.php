
        <div class="top-navbar">
             <div class="xd-topbar">
                     <div class="row">
                            <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                                <div class="xp-menubar">
                                    <span class="material-icons text-white">signal_cellular_alt</span>
                                </div>
                            </div>
                    
                            <div class="col-md-5 col-lg-3 order-3 order-md-2">
                            </div>
                            <!-- End XP Col -->

                            <!-- Start XP Col -->
                            <div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
                                    <div class="xp-profilebar text-right">
                                        <nav class="navbar p-0">
                                            <ul class="nav navbar-nav flex-row ml-auto">
                                              {{--   <li class="dropdown nav-item active">
                                                  <a href="#" class="nav-link" data-toggle="dropdown">
                                                       <span class="material-icons">notifications</span>
                                                       <span class="notification">4</span> 
                                                   </a> 
                                                   <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="#">You have 5 new messages</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">You're now friend with Mike</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Wish Mary on her birthday!</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">5 warnings in Server Console</a>
                                                        </li>
                                                      
                                                    </ul> 
                                                </li> --}}

                                                <li class="nav-item">
                                                        <!-- <a class="nav-link" href="#">
                                                             <span class="material-icons">question_answer</span>
                                                        </a> -->
                                                </li>

                                                    @if(Session::has('user_nom') && Session::has('user_prenom'))
                                                        <li class="nav-item dropdown">
                                                            <a class="nav-link" href="#" data-toggle="dropdown">
                                                            <img src="{{ asset('asset/assetSuperAdmin/img/R.png')}}" style="width:40px; border-radius:50%;"/>
                                                            <span class="xp-user-live"></span>
                                                            </a>
                                                            <ul class="dropdown-menu small-menu">
                                                                <li>
                                                                    <a href="{{ route('admin.profile') }}" class="adminProfil">
                                                                        <span class="material-icons"> person_outline
                                                                        </span> {{ Session::get('user_nom') }} {{ Session::get('user_prenom') }} 
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#">{{ Session::get('user_email') }}</a>
                                                                </li>
                                
                                                                <li>
                                                                    <a href="#" onclick="confirmLogout(event)">
                                                                        <span class="material-icons">logout</span> Déconnection
                                                                    </a>
                                                                </li>

                                                                <script>
                                                                    function confirmLogout(event) {
                                                                        event.preventDefault();  // Empêche l'exécution immédiate du lien

                                                                        // Afficher la boîte de dialogue de confirmation
                                                                        Swal.fire({
                                                                            title: 'Êtes-vous sûr?',
                                                                            text: "Vous allez être déconnecté!",
                                                                            icon: 'warning',
                                                                            showCancelButton: true,
                                                                            confirmButtonText: 'Oui, déconnectez-moi!',
                                                                            cancelButtonText: 'Annuler',
                                                                        }).then((result) => {
                                                                            if (result.isConfirmed) {
                                                                                // Si l'utilisateur confirme, soumettre le formulaire de déconnexion
                                                                                window.location.href = '{{ route('logout') }}';  // Rediriger vers la route de déconnexion
                                                                            }
                                                                        });
                                                                    }
                                                                </script>
                                                            </ul>
                                                        </li>
                                                    @else
                                                   @endif
                                            </ul>
                                        </nav> 
                                    </div>
                            </div>
                     </div>
                 <div class="xp-breadcrumbbar text-center">
                    <h4 class="page-title">
                                @if(Route::is('accueiladmin_page'))
                                     Page des Enquêtes Créées
                                @elseif(Route::is('utilisateurinscrit_page'))
                                        Pages des personnes inscrites
                                @elseif(Route::is('inscritpourenquete.show'))
                                        Liste des candidatures 
                                @elseif(Route::is('reponsepourenquete.show'))
                                        Réponses des candidats
                                @endif

                </h4>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-curent="page">Page Administrateur</li>
                          <li class="breadcrumb-item active" aria-curent="page">Droit retirer  : <strong>{{ $user->poste->nom_poste}}</strong> </li>
                          
                        </ol>
                 </div>
             </div>
        </div>

         <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


        