<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Page Administrateur</title>
	    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/bootstrap.min.css') }}">
	
	    <!----css3---->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/aff_util.css') }}">
		
		
		<!--google fonts -->
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
	
	
	   <!--google material icon-->
      <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

  </head>
  <body>
  


<div class="wrapper">
     
	  <div class="body-overlay"></div>
	 
	 <!-------sidebar--design------------>
	 
	 @include('SuperAdmin.SideBar')
   <!-------sidebar--design- close----------->
   
   
   
      <!-------page-content start----------->
   
      <div id="content">
	     
		  <!------top-navbar-start-----------> 
		     
		                    @include('SuperAdmin.NavBar')

		  <!------top-navbar-end-----------> 
		  
		  
		   <!------main-content-start-----------> 
		     
		      <div class="main-content">
			     <div class="row">
				    <div class="col-md-12">
					   <div class="table-wrapper">
					   <center>
								<div>
									@if (session('creationEnquette'))
								<div class="alert alert-success">
								{{ (session('creationEnquette')) }}
								</div>
								@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('status'))
						<div class="alert alert-success">
						{{ (session('status')) }}
						</div>
						@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('statusSup'))
						<div class="alert alert-danger">
						{{ (session('statusSup')) }}
						</div>
						@endif
							</div>
						</center>
					
					 

					   
<div class="table-title">
  <div class="row">
    <div class="col-12 d-flex justify-content-between align-items-center p-0">
      <h2 class="ml-lg-2">Liste des utilisateurs inscrits</h2>
      <div class="button-group">
        
        <a href="#exportExcelModal" class="btn btn-success" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/excel.png') }}" alt="Exporter Excel" style="height: 20px; width: 20px;">
          <span>Exporter la liste excel</span>
        </a>
        <a href="#exportWordModal" class="btn btn-info" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/word.png') }}" alt="Exporter Word" style="height: 20px; width: 20px;">
          <span>Exporter la liste word</span>
        </a>
        <a href="#exportPdfModal" class="btn btn-danger" data-toggle="modal">
          <img src="{{ asset('asset/assetSuperAdmin/img/images/pdf.png') }}" alt="Exporter PDF" style="height: 20px; width: 20px;">
          <span>Exporter la liste pdf</span>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals -->

<!-- Create Survey Modal -->
<div id="createSurveyModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content create-survey-modal">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez créer une nouvelle enquête ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-primary">Créer</button>
      </div>
    </div>
  </div>
</div>

<!-- Export Excel Modal -->
<div id="exportExcelModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-excel-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en Excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en Excel ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <!-- <button type="button" class="btn btn-success">Exporter</button> -->
        <button type="button" class="btn btn-success" id="confirmExportExcel">Télécharger</button>

      </div>
    </div>
  </div>
</div>

<!-- 
<script>
document.getElementById('confirmExportExcel').addEventListener('click', function () {
  window.location.href = '{{ route('utilisateurs.export-excel') }}';
});
</script> -->

<!-- Export Word Modal -->
<div id="exportWordModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-word-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en Word</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en Word ?</p>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
    <button type="button" class="btn btn-info" id="confirmExportWord">
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        Télécharger le word
    </button>
</div>
    </div>
  </div>
</div>
 
<script>
  const routes = {
      exportPdfUrl: "{{ route('listeDesUtilisateurInscritsPDF') }}",
      exportWordUrl: "{{ route('listeDesUtilisateurInscritsWord') }}",
      exportExcelUrl: "{{ route('utilisateurs.export-excel') }}"
  };
</script>

<div id="exportPdfModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content export-pdf-modal">
      <div class="modal-header">
        <h5 class="modal-title">Exporter la liste en PDF</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vous voulez exporter la liste en PDF ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-danger" id="confirmExportPdf">Télécharger le pdf</button>
      </div>
    </div>
  </div>
</div>



					   <table class="table table-striped table-hover">
							<thead>
								<tr>
                  <th>N°</th>
									<th>Nom</th>
									<th>Prénoms</th>
									<th>Email</th>
									<th>Date de Naissance</th>
									<th>Nationalité</th>
									<th>Langue 1</th>
									<th>Langue 2</th>
									<th>Langue 3</th>
                  <th>Type de Pièce</th>
                  <th>Numéro de Pièce</th>
                  <th>Numéro IFU</th>
									<th>Numéro Téléphone</th>
									<th>Numéro Whatshapp</th>
									<th>Diplôme</th>
									
								</tr>
							</thead>
							<tbody>

								@php
                    $ide = ($results->currentPage() - 1) * $results->perPage() + 1;
                @endphp

									@foreach($results as $result)
                  
										<tr>
                        <td>{{ $ide }}</td>
												<td>{{ $result->nom }}</td>
												<td>{{ $result->prenom }}</td>
												<td>{{ $result->email }}</td>
												<td>{{ $result->date_of_birth }}</td>
												<td>{{ $result->nationalite }}</td>
												<td>{{ $result->langue1 }}</td>
												<td>{{ $result->langue2 }}</td>
												<td>{{ $result->langue3 }}</td>
                        <td>{{ $result->type_piece }}</td>
                        <td>{{ $result->num_piece }}</td>
                        <td>{{ $result->ifu }}</td>
												<td>{{ $result->num_tel }}</td>
												<td>{{ $result->num_whatsapp }}</td>
												<td>{{ $result->diplome }}</td>
                        
												
										</tr>

										@php
											$ide += 1;
										@endphp
                    
									@endforeach
							</tbody>
						</table>
				
            <div class="pagination-wrapper">
              {{ $results->links('vendor.pagination.bootstrap-5') }}
            </div>
        </div>
					


					



					
									   <!----add-modal start--------->
		<div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			 

	  <form action="{{asset('/AdminSaveEnquette')}}" method="POST">
    	@csrf
      <div class="modal-body">
			<div class="form-group">
				<label>Nom de l'enquête</label>
				<input name="nom" type="text" class="form-control" required >
				@error('nom')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
	
			<div class="form-group">
				<label>Contenu de l'enquette</label>
				<textarea name="contenu"class="form-control" required ></textarea>
				@error('contenu')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
			<div class="form-group">
            <label>Choisissez la date d'expiration de l'enquête</label>
            <input type="date"  id="date_expiration" name="date_expiration" class="form-control" required >
            @error('date_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
			<p id="message"></p>

        </div>

		<div class="form-group">
            <label>Choisissez l'heure d'expiration de l'enquête</label>
            <input type="time" id="heure_expiration" name="heure_expiration" class="form-control" required >
            @error('heure_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
        </div>
      	</div>
      <div class="modal-footer">
	  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success" >Enrégistrer</button>
		<!--a type="submit" class="btn btn-success" href="{{ asset('/AdminSaveEnquette')}}">Add</a-->
      </div>

	</form>
    </div>
  </div>
</div>



<form action="#" method="POST">
								@csrf
								<div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Employees</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <p>Voulez-vous vraiment modifier cet Article?</p>
                </div>
                <p id="articleId"></p> <!-- Placeholder to display the article ID -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">NON</button>
                <a id="editButton" class="btn btn-success" href="">OUI</a>
            </div>
        </div>
    </div>
</div>
 

			</form>
		


			

					   <!----edit-modal end--------->	   
					   
					   
					 <!----delete-modal start--------->
		<div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Records</p>
		<p class="text-warning"><small>this action Cannot be Undone,</small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Delete</button>
      </div>
    </div>
  </div>
</div>

					   <!----edit-modal end--------->   
					   
					
					
				 
			     </div>
			  </div>
		  
		    <!------main-content-end-----------> 
		  
		 
		 
		 <!----footer-design------------->
		 
		                                                  @include('SuperAdmin.footer')

		 
		 
		 
		 
	  </div>
   
</div>



<!-------complete html----------->





  
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="{{ asset('asset/assetSuperAdmin/js/jquery-3.3.1.slim.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/popper.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/bootstrap.min.js')}}"></script>
   <script src="{{ asset('asset/assetSuperAdmin/js/jquery-3.3.1.min.js')}}"></script>
  
  
  <script type="text/javascript">
       $(document).ready(function(){
	      $(".xp-menubar").on('click',function(){
		    $("#sidebar").toggleClass('active');
			$("#content").toggleClass('active');
		  });
		  
		  $('.xp-menubar,.body-overlay').on('click',function(){
		     $("#sidebar,.body-overlay").toggleClass('show-nav');
		  });
		  
	   });
  </script>
  
  



  </body>
  
  </html>


