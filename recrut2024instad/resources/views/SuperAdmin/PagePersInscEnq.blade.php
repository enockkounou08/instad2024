<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Page Administrateur</title>
	    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/bootstrap.min.css') }}">
	
	    <!----css3---->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/custom.css') }}">
		
		
		<!--google fonts -->
	    <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
	
	
	   <!--google material icon-->
      <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">

  </head>
  <body>
  


<div class="wrapper">
     
	  <div class="body-overlay"></div>
	 
	 <!-------sidebar--design------------>
	 
	 <div id="sidebar">
	    <div class="sidebar-header">
		   <h3><img src="{{ asset('asset/assetSuperAdmin/img/OIP.jpg') }}" class="img-fluid"/><span>Réservé à L'Administrateur</span></h3>
		</div>
		<ul class="list-unstyled component m-0">
		  <li class="dropdown">
		  <a href=" {{route('accueiladmin_page')}}" class="dashboard"><i class="material-icons">dashboard</i>dashboard </a>
		  </li>
		  
		  <li class="dropdown">
		  <a href="{{ route('utilisateurinscrit_page') }}" data-toggle="" aria-expanded="" 
		  class="">
		  <i class="material-icons">aspect_ratio</i>Utilisateurs inscrits
		  </a>
		
		  </li>
		  
		  
		   <li class=" active">
		

		  <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" 

		  class="dropdown-toggle">
		  <i class="material-icons">apps</i>Enquêtes disponibles
		  </a>
		  @foreach($enquetes as $enquete)

			<!--th>{{ $enquete->N° }}</th>
				<th>{{ $enquete->id }}</th-->
				
		  <ul class="collapse list-unstyled menu" id="homeSubmenu2">
		     {{-- <li><a href="/pageUserAdminPersonneInscritPourEnquette/{{ $enquete->id }}">{{ $enquete->nom }}</a></li> --}}
			 <li><a href="{{ route('inscritpourenquete.show', ['id' => $enquete->id]) }}">{{ $enquete->nom }}</a></li>

		  </ul>
		
			@endforeach
		  </li>
		  
		
		</ul>
	 </div>


	 <div id="content">
	     
		  <!------top-navbar-start-----------> 
		     
		  <div class="top-navbar">
		     <div class="xd-topbar">
			     <div class="row">
				     <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
					    <div class="xp-menubar">
						    <span class="material-icons text-white">signal_cellular_alt</span>
						</div>
					 </div>
					 
					
				 </div>
				 
				 <div class="xp-breadcrumbbar text-center">
				    <h4 class="page-title">Pages de Candidatures</h4>
					<ol class="breadcrumb">
					  <li class="breadcrumb-item active" aria-curent="page">Page Administrateur</li>
					</ol>
				 </div>
				 
				 
			 </div>
		  </div>
		  <!------top-navbar-end-----------> 
		  
		  
		   <!------main-content-start-----------> 
		     
		      <div class="main-content">
			     <div class="row">
				    <div class="col-md-12">
					   <div class="table-wrapper">
					   <center>
								<div>
									@if (session('creationEnquette'))
								<div class="alert alert-success">
								{{ (session('creationEnquette')) }}
								</div>
								@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('status'))
						<div class="alert alert-success">
						{{ (session('status')) }}
						</div>
						@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('statusSup'))
						<div class="alert alert-danger">
						{{ (session('statusSup')) }}
						</div>
						@endif
							</div>
						</center>
						
						

					   <div class="table-title">
					     <div class="row">
						     <div class="col-sm-6 p-0 flex justify-content-lg-start justify-content-center">
							 
							    <h2 class="ml-lg-2">Nom de l'enquête:  {{ $enque->nom }}  - {{ $enque->id }}</h2>
							 </div>
							 <div class="col-sm-6 p-0 flex justify-content-lg-end justify-content-center">
							   <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
							   <i class="material-icons">&#xE147;</i>
							   <span>Créer une enquête</span>
							   </a>
							 
							   </a>
							 </div>
					     </div>
					   </div>
												
							<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th>Nom de l'enquête</th>
											<th>Nom postulant</th>
											<th>Date de naissance postulant</th>
											<th>Nationalité postulant</th>
											<th>Téléphone postulant</th>
											<th>Email postulant</th>
											<th>Type de pièce</th>
											<th>Numéro pièce</th>
										</tr>
									</thead>
									<tbody>
										@if($candidatures->isEmpty())
											<tr>
												<td  style="text-align: center;">Pas de postulant pour le moment.</td>
											</tr>
										@else
											@foreach($candidatures as $candidature)
												<tr>
													<td>{{ $enquete->nom }}</td> <!-- Nom de l'enquête -->
													<td>{{ $candidature->nom ?? 'Non disponible' }}</td>
													<td>{{ $candidature->date_of_birth ?? 'Non disponible' }}</td>
													<td>{{ $candidature->nationalite ?? 'Non disponible' }}</td>
													<td>{{ $candidature->num_tel ?? 'Non disponible' }}</td>
													<td>{{ $candidature->email ?? 'Non disponible' }}</td>
													<td>{{ $candidature->type_piece ?? 'Non disponible' }}</td>
													<td>{{ $candidature->num_piece ?? 'Non disponible' }}</td>
													<td></td>
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>

					   <div class="clearfix">
					     <div class="hint-text">showing <b>5</b> out of <b>25</b></div>
					     <ul class="pagination">
						    <li class="page-item disabled"><a href="#">Previous</a></li>
							<li class="page-item "><a href="#"class="page-link">1</a></li>
							<li class="page-item "><a href="#"class="page-link">2</a></li>
							<li class="page-item active"><a href="#"class="page-link">3</a></li>
							<li class="page-item "><a href="#"class="page-link">4</a></li>
							<li class="page-item "><a href="#"class="page-link">5</a></li>
							<li class="page-item "><a href="#" class="page-link">Next</a></li>
						 </ul>
					   </div>
					
					   </div>
					</div>
					


					



					
									   <!----add-modal start--------->
		<div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			

	  <form action="{{asset('/AdminSaveEnquette')}}" method="POST">
    	@csrf
      <div class="modal-body">
			<div class="form-group">
				<label> Quel est le nom de l'enquête</label>
				<input name="nom" type="text" class="form-control" required >
				@error('nom')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
	
			<div class="form-group">
				<label>Contenu de l'enquête</label>
				<textarea name="contenu"class="form-control" required ></textarea>
				@error('contenu')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
			<div class="form-group">
            <label>Choisissez la date d'expiration de l'enquête</label>
            <input type="date"  id="date_expiration" name="date_expiration" class="form-control" required >
            @error('date_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
			<p id="message"></p>

        </div>

		<div class="form-group">
            <label>Choisissez l'heure d'expiration de l'enquête</label>
            <input type="time" id="heure_expiration" name="heure_expiration" class="form-control" required >
            @error('heure_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
        </div>
      	</div>
      <div class="modal-footer">
	  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success" >Enrégistrer</button>
		<!--a type="submit" class="btn btn-success" href="{{ asset('/AdminSaveEnquette')}}">Add</a-->
      </div>

	</form>
    </div>
  </div>
</div>

			  


<form action="#" method="POST">
								@csrf
								<div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">  
                <h5 class="modal-title">Edit Employees</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <p>Voulez-vous vraiment modifier cet Article?</p>
                </div>
                <p id="articleId"></p> <!-- Placeholder to display the article ID -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">NON</button>
                <a id="editButton" class="btn btn-success" href="">OUI</a>
            </div>
        </div>
    </div>
</div>
 

			</form>
		


			<div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Employees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this Records</p>
		<p class="text-warning"><small>this action Cannot be Undone,</small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success">Delete</button>
      </div>
    </div>
  </div>
</div>
				
				 
	</div>
</div>
		  

		 
		 <footer class="footer">
		    <div class="container-fluid">
			   <div class="footer-in">
			      <p class="mb-0">&copy 2024 .Page réservé uniquement à l'Administrateur</p>
			   </div>
			</div>
		 </footer>
		 
		 
		 
		 
	  </div>
   
</div>


	<script src="{{ ('asset/assetJS/pagePerInsEnq.js') }}"></script>


     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.slim.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/popper.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/bootstrap.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.min.js')}}"></script>
  

  </body>
  
  </html>


