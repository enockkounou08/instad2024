<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil Admin</title>
    <!-- Lien vers le CSS de Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/profileAdmin.css') }}">
</head>
<body>

    <!-- Affichage des messages de succès -->
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <!-- Affichage des messages d'erreur -->
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

 
                                        <a href="{{ route('accueiladmin_page') }}">
                                          <span class="">Retour</span> 
                                        </a>
                                    


    <div class="main-content content">
    <h3>Profil Administrateur</h3>

    <p>Nom: {{ $user_nom }}</p>
    <p>Prénom: {{ $user_prenom }}</p>
    <p>Email: {{ $user_email }}</p>

    <div class="button-group">
    <!-- Bouton pour ouvrir le modal de création -->
    <button type="button" class="btn btn-primary formulaire-add-admin {{ in_array('formulaire-add-admin', $disabledClasses) ? 'd-none' : '' }}" data-toggle="modal" data-target="#userModal">
        Ouvrir le formulaire
    </button>

    

    <!-- Modal pour le formulaire utilisateur -->
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userModalLabel">Formulaire Admin Utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Formulaire à l'intérieur du modal -->
                   <!-- Formulaire à l'intérieur du modal -->
<form action="{{ route('admin.create') }}" method="POST">
    @csrf
    <span style="color : red;">Après crétion d'un administrateur veuiller lui affecter un poste si non il aura touts les droit</span>
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name="nom" value="{{ old('nom') }}" required>
    </div>
    <div class="form-group">
        <label for="prenom">Prénom</label>
        <input type="text" class="form-control" id="prenom" name="prenom" value="{{ old('prenom') }}" required>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
    </div>
   <div class="form-group">
    <label for="edit-password">Mot de passe</label>
    <div class="input-group">
        <input type="password" class="form-control" id="edit-password" name="password">
        <div class="input-group-append">
            <span class="input-group-text" id="togglePassword">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </span>
        </div>
    </div>
</div>


    <button type="submit" class="btn btn-success">Enregistrer</button>
</form>

                </div>
            </div>
        </div>
    </div>

    <!-- Bouton pour ouvrir le modal d'affichage des actions -->
    <button type="button" class="btn btn-info list-admini {{ in_array('list-admini', $disabledClasses) ? 'd-none' : '' }}" data-toggle="modal" data-target="#adminActionsModal">
        Afficher toutes les actions
    </button>

    <!-- Bouton pour ouvrir le modal d'affichage des actions -->
   <!--  <button type="button" class="btn btn-info" onclick="window.location.href='{{ route('showRole.admin') }}'">
        Ajouter un rôle admin
    </button> -->

    <!-- Bouton pour ouvrir le modal d'affichage des actions -->
    <button type="button" class="btn btn-info ajouter-poste {{ in_array('ajouter-poste', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('voirForm.poste') }}'">
        Ajouter un poste
    </button>

    <button type="button" class="btn btn-info affecter-un-poste{{ in_array('affecter-un-poste', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('affectation') }}'">
        Affecter un poste admin
    </button>

{{-- 
    <button type="button" class="btn btn-info formulaire-add-ville {{ in_array('formulaire-add-ville', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('voirForm.ville') }}'">Ajouter une ville</button>

    <button type="button" class="btn btn-info formulaire-add-lang {{ in_array('formulaire-add-lang', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('voirForm.langue') }}'">
        Ajouter une langue
    </button>

    <button type="button" class="btn btn-info formulaire-add-type-attestaion {{ in_array('formulaire-add-type-attestaion', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('attestationType.index') }}'">
        Créer un Type d'attestation
    </button>
    <button type="button" class="btn btn-info formulaire-add-attestaion {{ in_array('formulaire-add-attestaion', $disabledClasses) ? 'd-none' : '' }}" onclick="window.location.href='{{ route('attestation.create') }}'">
        Créer une attestion
    </button> --}}

</div>
</div>

    <!-- Modal pour afficher les actions admin -->
    <div class="modal fade" id="adminActionsModal" tabindex="-1" role="dialog" aria-labelledby="adminActionsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="adminActionsModalLabel">Liste des Actions Admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Table pour afficher les données -->
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Email</th>
                                <th>Rôles</th>
                                <th>Créé le</th>
                                <th>Mis à jour le</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($actions as $action)
                                <tr>
                                    <td>{{ $action->id }}</td>
                                    <td>{{ $action->nom }}</td>
                                    <td>{{ $action->prenom }}</td>
                                    <td>{{ $action->email }}</td>
                                   <td>
                                        @if ($action->poste)
                                            <span class="badge bg-info">{{ $action->poste->nom_poste }}</span>
                                        @else
                                            Aucun poste
                                        @endif
                                    </td>
                                    <td>{{ $action->created_at }}</td>
                                    <td>{{ $action->updated_at }}</td>
                                    <td>
                                        <!-- Bouton Modifier -->
                                        <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal" 
                                                data-id="{{ $action->id }}" 
                                                data-nom="{{ $action->nom }}" 
                                                data-prenom="{{ $action->prenom }}" 
                                                data-email="{{ $action->email }}" 
                                               >
                                            Modifier
                                        </button>
                                        <!-- Bouton Supprimer -->
                                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal" 
                                                data-id="{{ $action->id }}" 
                                                data-nom="{{ $action->nom }}" 
                                                data-prenom="{{ $action->prenom }}" 
                                                data-email="{{ $action->email }}">
                                            Supprimer
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Modifier -->
    <!-- Modal Modifier -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Modifier l'Action</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editForm" method="POST" action="">
                @csrf
                @method('POST')
                <div class="modal-body">
                    <input type="hidden" id="edit-id" name="id">
                    <div class="form-group">
                        <label for="edit-nom">Nom</label>
                        <input type="text" class="form-control" id="edit-nom" name="nom">
                    </div>
                    <div class="form-group">
                        <label for="edit-prenom">Prénom</label>
                        <input type="text" class="form-control" id="edit-prenom" name="prenom">
                    </div>
                    <div class="form-group">
                        <label for="edit-email">Email</label>
                        <input type="email" class="form-control" id="edit-email" name="email">
                    </div>
                    
                    <div class="form-group">
                            <label for="edit-password">Mot de passe</label>
                            <div class="input-group">
                                <input type="password" class="form-control" id="edit-password" name="password" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="togglePassword">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>


</div>

    <!-- Modal Supprimer -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Supprimer l'Action</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteForm" method="POST" action="">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Êtes-vous sûr de vouloir supprimer cet enregistrement ?</p>
                        <p><strong>Nom:</strong> <span id="delete-nom"></span></p>
                        <p><strong>Prénom:</strong> <span id="delete-prenom"></span></p>
                        <p><strong>Email:</strong> <span id="delete-email"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Scripts JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

   <script src="{{ asset('asset/assetJS/profileAdmin.js') }}"></script>
     @include('sweetalert::alert')

</body>
</html>








