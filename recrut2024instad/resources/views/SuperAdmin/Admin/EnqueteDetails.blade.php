<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>


<a href="{{ route('admin.profile') }}">Retour aux options</a>

	<!-- resources/views/SuperAdmin/Admin/EnqueteDetails.blade.php -->

<h1>Détails de l'enquête</h1>



<!-- Liste des types d'attestations disponibles pour cette enquête -->
<h2>Types d'attestations pour cet enquette.</h2>

@foreach ($typesAttestationexit as $attestation)
   {{-- <p><a href="{{ route('attestation.edit', $attestation->id) }}">{{$attestation->titre }}</a></p>--}}

  {{--  <p>ID: {{ $attestation->id }}</p> --}}
    <p> {{ $attestation->type->nom }} <a href="{{ route('attestation.edit', $attestation->id) }}"> voir </a> </p>
@endforeach



<table>
    <tr>
        <th>ID</th>
        <td>{{ $enquete->id }}</td>
    </tr>
    <tr>
        <th>Nom</th>
        <td>{{ $enquete->nom }}</td>
    </tr>
    <tr>
        <th>Description</th>
        <td>{{ $enquete->info }}</td>
    </tr>
    <tr>
        <th>Date d'expiration</th>
        <td>{{ $enquete->date_expiration }}</td>
    </tr>
    <tr>
        <th>État</th>
        <td>{{ $enquete->etat_enquete }}</td>
    </tr>
</table>

<a href="{{ route('attestation.create') }}">Retour à la recherche</a>


<div id="form-section" style="">

	    <form action="{{ route('attestation.store') }}" method="POST">
		    @csrf
		    <!-- ID de l'enquête -->
		    <input type="hidden" name="enquete_id" id="enquete_id">

			<div>
			    <!-- Champ pour le type d'attestation -->
				<label for="type_attestation">Type d'attestation :</label>
				<select name="type_attestation_id" id="type_attestation">
				    <option value="" disabled selected>-- Sélectionnez un type --</option>
				    @foreach ($typesAttestation as $type)
				        <option value="{{ $type->id }}">{{ $type->nom }}</option>
				    @endforeach
				</select>
				@error('type_attestation')
				    <div class="text-danger">{{ $message }}</div>
				@enderror
			</div>

			<div>

				<input type="hidden" name="enquete_id" id="enquete_id" value="{{ $enquete->id ?? '' }}">
			    <label for="titre">Titre :</label>
			    <input type="text" name="titre" id="titre" value="{{ old('titre') }}" placeholder="Entrez un titre">
			    @error('titre')
			        <div class="text-danger">{{ $message }}</div>
			    @enderror
			 </div>

			 <div>
			    <label for="contenu">Contenu :</label>
			    <textarea name="contenu" id="contenu" placeholder="Entrez le contenu">{{ old('contenu') }}</textarea>
			    @error('contenu')
			        <div class="text-danger">{{ $message }}</div>
			    @enderror
			 </div>

		    <button type="submit">Créer</button>
		</form>

</div>

	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	@include('sweetalert::alert')

</body>
</html>