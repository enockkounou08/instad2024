<!-- resources/views/form.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Formulaire d'ajout d'un nouveau rôle</title>
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/VilleLangueRolePoste.css') }}">
</head>
<body>

@if(session('success'))
    <div style="color: green; margin-bottom: 20px;">
        {{ session('success') }}
    </div>
@endif

@if ($errors->any())
    <div style="color: red; margin-bottom: 20px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('submitRole.admin') }}" method="POST">
    @csrf
    <p><h1>Ajout d'un nouveau rôle</h1></p>
    <label for="nom_role" >Nom :</label>
    <input type="text" id="nom_role" name="nom_role" value="{{ old('nom_role') }}">

    <label for="attribut" >Attribut :</label>
    <input type="text" id="attribut" name="attribut" value="{{ old('attribut') }}">
   
    <button type="submit">Ajouter</button>
</form>
     @include('sweetalert::alert')

</body>
</html>
