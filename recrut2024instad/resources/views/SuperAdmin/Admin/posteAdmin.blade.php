<!-- resources/views/form.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/VilleLangueRolePoste.css') }}">
    <title> Formulaire d'ajout d'un nouveau poste</title>
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
 
  <style>
        /* Style CSS */
        /* Style pour le conteneur de sélection */
        .custom-select {
            position: relative;
            width: 100%;
            font-family: Arial, sans-serif;
        }

        .custom-select input[type="text"] {
            width: 100%;
            padding: 10px;
            font-size: 14px;
            border: 1px solid #ccc;
            border-radius: 4px;
            outline: none;
            box-sizing: border-box;
            transition: border-color 0.3s;
        }

        .custom-select input[type="text"]:focus {
            border-color: #007bff;
            box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
        }

        /* Liste des options */
        .custom-select ul {
            list-style-type: none;
            padding: 0;
            margin: 5px 0 0;
            border: 1px solid #ccc;
            border-radius: 4px;
            max-height: 200px;
            overflow-y: auto;
            background-color: #fff;
            position: absolute;
            width: 100%;
            z-index: 1000;
            display: none;
        }

        .custom-select ul li {
            padding: 10px;
            font-size: 14px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        .custom-select ul li:hover {
            background-color: #f0f0f0;
        }

        /* Tags pour les éléments sélectionnés */
        .selected-items {
            display: flex;
            flex-wrap: wrap;
            margin: 10px 0;
            gap: 5px;
        }

        .selected-tag {
            background-color: #007bff;
            color: #fff;
            padding: 5px 10px;
            font-size: 12px;
            border-radius: 12px;
            display: flex;
            align-items: center;
            cursor: default;
        }

        .selected-tag span {
            margin-left: 8px;
            background-color: #fff;
            color: #007bff;
            font-size: 12px;
            padding: 0 6px;
            border-radius: 50%;
            cursor: pointer;
            transition: background-color 0.3s, color 0.3s;
        }

        .selected-tag span:hover {
            background-color: #007bff;
            color: #fff;
        }

        /* Barre de défilement personnalisée */
        .custom-select ul::-webkit-scrollbar {
            width: 8px;
        }

        .custom-select ul::-webkit-scrollbar-thumb {
            background-color: #007bff;
            border-radius: 4px;
        }

        .custom-select ul::-webkit-scrollbar-thumb:hover {
            background-color: #0056b3;
        }
    </style>
  
</head>
<body>

@if(session('success'))
    <div style="color: green; margin-bottom: 20px;">
        {{ session('success') }}
    </div>
@endif

@if ($errors->any())
    <div style="color: red; margin-bottom: 20px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 @if ($errors->has('role_id'))
        <span class="text-danger">{{ $errors->first('role_id') }}</span>
    @endif

<form action="{{ route('valider.poste') }}" method="POST">
    @csrf
    <p><h3>Ajout d'un nouveau poste admin</h3></p>
    <label for="nom_poste">Nom :</label>
    <input type="text" id="nom_poste" name="nom_poste" value="{{ old('nom_poste') }}">
    @if ($errors->has('nom_poste'))
        <span class="text-danger">{{ $errors->first('nom_poste') }}</span>
    @endif



    <label for="dynamic-select">Rôle :</label><br>
    <div class="custom-select">
        <input 
            type="text" 
            id="roleSearch" 
            placeholder="Rechercher un rôle" 
            onkeyup="handleDropdown(event, 'roleSearch', 'roleOptions', 'selectedRoles', 'roleIds')" 
            onclick="handleDropdown(event, 'roleSearch', 'roleOptions', 'selectedRoles', 'roleIds')">
        
        <div class="selected-items" id="selectedRoles"></div>
        
        <ul id="roleOptions" style="display: none;">
            @foreach ($roles as $role)
                <li data-id="{{ $role->id }}" 
                    onclick="handleDropdown(event, 'roleSearch', 'roleOptions', 'selectedRoles', 'roleIds')">
                    {{ $role->nom_role }}
                </li>
            @endforeach
        </ul>
        
        <!-- input caché pour stocker les ids des rôles sélectionnés -->
        <div id="roleIds"></div>
    </div>

    <button type="submit">Ajouter</button>
    <a href="{{ route('affectation') }}" class="btn btn-primary">
    affecter un poste
</a>


</form>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>

    let selectedRoles = []; // Tableau pour stocker les IDs des rôles sélectionnés

function handleDropdown(event, searchId, listId, selectedId, hiddenInputContainerId) {
    const searchInput = document.getElementById(searchId);
    const list = document.getElementById(listId);
    const selectedContainer = document.getElementById(selectedId);
    const hiddenInputContainer = document.getElementById(hiddenInputContainerId); // Conteneur des inputs cachés

    // Filtrer les options
    const filter = searchInput.value.toLowerCase();
    const options = list.getElementsByTagName("li");

    for (let option of options) {
        const text = option.textContent.toLowerCase();
        const isSelected = selectedRoles.includes(option.dataset.id);

        // Masquer les options déjà sélectionnées
        option.style.display = isSelected || !text.includes(filter) ? "none" : "";
    }

    // Afficher la liste
    list.style.display = "block";

    // Ajouter un rôle sélectionné
    if (event.type === "click" && event.target.tagName === "LI") {
        const selectedText = event.target.textContent;
        const selectedId = event.target.dataset.id;

        // Vérifier si l'ID est déjà sélectionné
        if (!selectedRoles.includes(selectedId)) {
            // Ajouter un tag
            const tag = document.createElement("div");
            tag.classList.add("selected-tag");
            tag.innerHTML = `${selectedText} <span class="bi bi-x-circle-fill" onclick="removeTag(this, '${hiddenInputContainerId}', '${selectedId}', '${listId}')"></span>`;
            selectedContainer.appendChild(tag);

            // Ajouter l'ID dans le tableau
            selectedRoles.push(selectedId);

            // Créer un nouvel input caché pour chaque rôle sélectionné
            const hiddenInput = document.createElement("input");
            hiddenInput.type = "hidden";
            hiddenInput.name = "role_id[]";
            hiddenInput.value = selectedId;
            hiddenInputContainer.appendChild(hiddenInput);
        }

        // Masquer la liste après sélection
        list.style.display = "none";
    }
}

function removeTag(element, hiddenInputContainerId, valueToRemove, listId) {
    const hiddenInputContainer = document.getElementById(hiddenInputContainerId);
    const list = document.getElementById(listId);

    // Retirer le tag
    element.parentElement.remove();

    // Supprimer l'ID du tableau
    selectedRoles = selectedRoles.filter(id => id !== valueToRemove);

    // Supprimer l'input caché correspondant
    const hiddenInputs = hiddenInputContainer.querySelectorAll('input[name="role_id[]"]');
    hiddenInputs.forEach(input => {
        if (input.value === valueToRemove) {
            input.remove();
        }
    });
}

// Avant la soumission du formulaire, supprimer les inputs cachés avec des valeurs nulles ou vides
function cleanHiddenInputsBeforeSubmit() {
    const hiddenInputs = document.querySelectorAll('input[name="role_id[]"]');
    hiddenInputs.forEach(input => {
        if (!input.value) { // Si l'input caché a une valeur vide
            input.remove(); // Supprimez-le
        }
    });
}

// Ajouter un écouteur de soumission de formulaire pour nettoyer les inputs avant soumission
document.querySelector('form').addEventListener('submit', function() {
    cleanHiddenInputsBeforeSubmit();
});


</script>


     @include('sweetalert::alert')

</body>
</html>
