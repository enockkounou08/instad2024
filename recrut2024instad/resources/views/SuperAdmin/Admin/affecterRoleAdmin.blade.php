<!-- resources/views/form.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Formulaire d'affectation de rôle</title>
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/VilleLangueRolePoste.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

@if(session('success'))
    <div style="color: green; margin-bottom: 20px;">
        {{ session('success') }}
    </div>
@endif



<form action="" method="POST">
    @csrf
    <h3>Affecter un Poste à un administrateur</h3>
    <div class="row">
        <!-- Administrateur -->
        <div class="col-12">
            <label for="admin">Administrateur :</label><br>
            <select id="admin" name="administrateur" class="form-control" required>
                <option disabled selected>Choisissez un administrateur</option>
                @foreach ($admins as $admin)
                    <option value="{{ $admin->id }}">{{ $admin->nom }} {{ $admin->prenom }}</option>
                @endforeach
            </select>
            @if ($errors->has('administrateur'))
                <span class="text-danger">{{ $errors->first('administrateur') }}</span>
            @endif
        </div>
        <!-- Rôle -->
        <div class="col-12 mt-3">
            <label for="dynamic-select">Poste :</label><br>
            <select id="dynamic-select" name="poste" class="form-control" required>
                <option disabled selected>Choisissez un poste</option>
                @foreach ($postes as $poste)
                    <option value="{{ $poste->id }}">{{ $poste->nom_poste }}</option>
                @endforeach
            </select>
            @if ($errors->has('poste'))
                <span class="text-danger">{{ $errors->first('poste') }}</span>
            @endif
        </div>

        <!-- <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
            <label class="control-label">Ad Description <small>Enter long description for your project</small></label>
            <textarea name="editor1" id="editor1" rows="12" class="form-control" placeholder=""></textarea>
        </div> -->
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Affecter</button>
     <a href="{{ route('accueiladmin_page') }}">
     <span class="btn btn-primary">Page Acceuil</span> 
 </a>
</form>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Ckeditor 
<script src="js/ckeditor/ckeditor.js" ></script>

<script>
CKEDITOR.replace( 'editor1' ); -->

</script>


@include('sweetalert::alert')

</body>
</html>
