<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Créer une attestation</title>
    <style>
        #message-section h2 {
            color: #007bff;
            font-weight: bold;
        }
        #form-section {
            margin-top: 20px;
            padding: 15px;
            border: 1px solid #ddd;
            border-radius: 5px;
            background: #f9f9f9;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        table, th, td {
            border: 1px solid #ddd;
        }
        th, td {
            padding: 10px;
            text-align: left;
        }
        .text-danger {
            color: red;
            font-size: 12px;
        }
    </style>
</head>
<body>

<a href="{{ route('admin.profile') }}">Retour aux options</a>


	<h1>Page de création des attestations</h1>

	<!-- Formulaire de recherche d'une enquête -->
	<h2>Rechercher une enquête</h2>
	<form action="{{ route('attestation.create') }}" method="GET">
	    @csrf
	    <label for="search">Nom de l'enquête :</label>
	    <input type="text" name="search" id="search" value="{{ old('search') }}" placeholder="Entrez un nom...">
	    <button type="submit">Rechercher</button>
	</form>


		<!-- Exporter en PDF et Excel -->
	@if(isset($enquetes) && count($enquetes) > 0)
	    <div>
	        <a href="{{ route('attestation.export.pdf', ['search' => request('search')]) }}" class="btn btn-primary">Exporter en PDF</a>
	        <a href="{{ route('attestation.export.excel', ['search' => request('search')]) }}" class="btn btn-success">Exporter en Excel</a>
	    </div>
	@endif


	
	<!-- Résultats de la recherche -->
	@if(isset($enquetes) && count($enquetes) > 0)
	    <h3>Résultats de la recherche</h3>
		<table>
		    <thead>
		        <tr>
		            <th>ID</th>
		            <th>Nom</th>
		            <th>Description</th>
		            <th>Date d'expiration</th>
		            <th>État</th>
		        </tr>
		    </thead>
		    <tbody>
		        @foreach($enquetes as $enquete)
		            <tr class="clickable-row">
		                <td><a href="{{ route('attestation.show', $enquete->id) }}">{{ $enquete->id }}</a></td>
		                <td><a href="{{ route('attestation.show', $enquete->id) }}">{{ $enquete->nom }}</a></td>
		                <td><a href="{{ route('attestation.show', $enquete->id) }}">{{ $enquete->info }}</a></td>
		                <td><a href="{{ route('attestation.show', $enquete->id) }}">{{ $enquete->date_expiration }}</a></td>
		                <td><a href="{{ route('attestation.show', $enquete->id) }}">{{ $enquete->etat_enquete }}</a></td>
		            </tr>
		        @endforeach
		    </tbody>
		</table>

	@elseif(request()->has('search'))
	    <p>Aucune enquête trouvée pour le terme "{{ request('search') }}".</p>
	@endif

	<!-- Section pour afficher le message et le formulaire -->
	<div id="message-section" style="display: none; margin-bottom: 10px;">
	    <h2 id="enquete-message"></h2>
	</div>

	

	<script>
	    document.addEventListener('DOMContentLoaded', function () {
	        const rows = document.querySelectorAll('.clickable-row');
	        const formSection = document.getElementById('form-section');
	        const messageSection = document.getElementById('message-section');
	        const messageElement = document.getElementById('enquete-message');
	        const titreInput = document.getElementById('titre');
	        const contenuInput = document.getElementById('contenu');
	        const enqueteIdInput = document.getElementById('enquete_id');

	        rows.forEach(row => {
	            row.addEventListener('click', function () {
	                // Récupérer les données de la ligne cliquée
	                const id = this.getAttribute('data-id');
	                const nom = this.getAttribute('data-nom');
	                const info = this.getAttribute('data-info');

	                // Préremplir les champs du formulaire
	                enqueteIdInput.value = id; // Stocker l'ID de l'enquête
	                titreInput.value = `Attestation pour ${nom}`;
	                contenuInput.value = `Cette attestation certifie la participation à l'enquête "${nom}".`;

	                // Mettre à jour le message
	                messageElement.textContent = `Créer une attestation pour l'enquête : ${nom}`;
	                messageSection.style.display = 'block';

	                // Afficher le formulaire
	                formSection.style.display = 'block';
	                formSection.scrollIntoView({ behavior: 'smooth' });
	            });
	        });
	    });
	</script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	@include('sweetalert::alert')

</body>
</html>
