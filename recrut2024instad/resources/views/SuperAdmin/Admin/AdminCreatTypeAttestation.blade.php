<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<a href="{{ route('attestation.create') }}">Retour à la recherche</a>

	<form action="{{ route('attestationType.store') }}" method="POST">
	    @csrf
	    <div class="form-group">
	        <label for="nom">Nom du type d'attestation</label>
	        <input type="text" name="nom" id="nom" class="form-control" required>
	    </div>

	    <button type="submit" class="btn btn-primary">Créer</button>
	</form>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	@include('sweetalert::alert')
</body>
</html>