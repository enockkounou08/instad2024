<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>Page Administrateur</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/bootstrap.min.css') }}">
    
        <!----css3---->
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/accueil_index.css' ) }}">

        
        
        <!--google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    
    
       <!--google material icon-->
      <link href="https://fonts.googleapis.com/css2?family=Material+Icons"rel="stylesheet">



  </head>
  <body>
  

         @if(isset($error))
    <div class="alert alert-warning">
        {{ $error }}
    </div>
@endif

           @include('sweetalert::alert')


<div class="wrapper">
     
    <div class="body-overlay"></div>
     
     <!-------sidebar--design------------>
     
   @include('SuperAdmin.SideBar')
   <!-------sidebar--design- close----------->
      <!-------page-content start----------->
   
      <div id="content">
         
          <!------top-navbar-start-----------> 
                  @include('SuperAdmin.NavBar')

          <!------top-navbar-end-----------> 
          
          
           <!------main-content-start-----------> 
             
              <div class="main-content">
                 <div class="row">
                        <div class="col-md-12">
                            <div class="table-wrapper">
                                <center>
                                        <div>
                                            @if (session('creationEnquette'))
                                                <div class="alert alert-success">
                                                  {{ (session('creationEnquette')) }}
                                                </div>
                                            @endif
                                        </div>
                                </center>

                                <center>
                                    <div>
                                        @if (session('status'))
                                                <div class="alert alert-success">
                                                  {{ (session('status')) }}
                                                </div>
                                        @endif
                                    </div>
                                </center>

                                <center>
                                    <div>
                                        @if (session('statusSup'))
                                                <div class="alert alert-danger">
                                                     {{ (session('statusSup')) }}
                                                </div>
                                        @endif
                                    </div>
                                    <div class="container">
                                        <!-- Affichage des messages de succès -->
                                        @if(session('success'))
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>
                                        @endif

                                        <!-- Affichage des messages d'erreur -->
                                        @if(session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                        @endif
                                    </div>
                                </center>
        
                                <div class="table-title {{ in_array('table-title', $disabledClasses) ? 'd-none' : '' }}">
                                  <div class="row">
                                        <div class="col-12 d-flex justify-content-between align-items-center p-0">
                                          <h2 class="ml-lg-2">Liste des enquêtes</h2>
                                            <div class="button-group {{ in_array('button-group', $disabledClasses) ? 'd-none' : '' }} ">
                                                <!--a href="#createSurveyModal" class="btn btn-primary" data-toggle="modal">
                                                  <i class="material-icons">&#xE147;</i>
                                                  <span>Créer une enquête</span>
                                                </a-->
                                                <a href="#addEmployeeModal" class="btn btn btn-primary btn-creer-enquete {{ in_array('btn-creer-enquete', $disabledClasses) ? 'd-none' : '' }} " data-toggle="modal">
                                                    <i class="material-icons">&#xE147;</i>
                                                    <span>Créer une enquête</span>
                                                </a>
                                                <a href="#exportExcelModal" class="btn btn-success btn-export btn-export-excel {{ in_array('btn-export-excel', $disabledClasses) ? 'd-none' : '' }}" data-toggle="modal">
                                                  <img src="{{ asset('asset/assetSuperAdmin/img/images/excel.png') }}" alt="Exporter Excel" style="height: 20px; width: 20px;">
                                                  <span>Exporter la liste excel</span>
                                                </a>
                                                <a href="#exportWordModal" class="btn btn-info btn-export btn-export-word {{ in_array('btn-export-word', $disabledClasses) ? 'd-none' : '' }}" data-toggle="modal">
                                                  <img src="{{ asset('asset/assetSuperAdmin/img/images/word.png') }}" alt="Exporter Word" style="height: 20px; width: 20px;">
                                                  <span>Exporter la liste word</span>
                                                </a>
                                                <a href="#exportPdfModal" class="btn btn-danger btn-export btn-export-pdf {{ in_array('btn-export-pdf', $disabledClasses) ? 'd-none' : '' }}" data-toggle="modal">
                                                  <img src="{{ asset('asset/assetSuperAdmin/img/images/pdf.png') }}" alt="Exporter PDF" style="height: 20px; width: 20px;">
                                                  <span>Exporter la liste pdf</span>
                                                </a>
                                            </div>
                                        </div>
                                  </div>
                                </div>

                                    <!-- Modals -->

                                    <!-- Create Survey Modal -->
                                <div id="createSurveyModal" class="modal fade" tabindex="-1" role="dialog">
                                   <div class="modal-dialog" role="document">
                                        <div class="modal-content create-survey-modal">
                                              <div class="modal-header">
                                                    <h5 class="modal-title">Créer une enquête</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                              </div>
                                              <div class="modal-body">
                                                 <p>Vous voulez créer une nouvelle enquête ?</p>
                                              </div>
                                              <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="button" class="btn btn-primary">Créer</button>
                                              </div>
                                        </div>
                                   </div>
                                </div>

                                   <!-- Export Excel Modal  -->
                                <div id="exportExcelModal" class="modal fade" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content export-excel-modal">
                                              <div class="modal-header">
                                                    <h5 class="modal-title">Exporter la liste en Excel</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                           <span aria-hidden="true">&times;</span>
                                                        </button>
                                              </div>
                                              <div class="modal-body">
                                                    <p>Vous voulez exporter la liste en Excel ?</p>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                <button type="button" class="btn btn-success" id="confirmExportExcel">Télécharger</button>
                                              </div>
                                        </div>
                                   </div>
                                </div>

                                <script>
                                    const excelExportUrl = "{{ route('enqueteDisponibleExcel') }}";
                                </script>

                                <!-- Export Word Modal -->
                                <div id="exportWordModal" class="modal fade" tabindex="-1" role="dialog">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content export-word-modal">
                                          <div class="modal-header">
                                                <h5 class="modal-title">Exporter la liste en Word</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                          </div>
                                          <div class="modal-body">
                                            <p>Vous voulez exporter la liste en Word ?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                            <!-- <button type="button" class="btn btn-info">Exporter</button> -->
                                            <button type="button" class="btn btn-info" id="confirmExportWord">Télécharger le word</button>
                                          </div>
                                    </div>
                                  </div>
                                </div>


                                    <script>
                                        const enquetteDispoExW = "{{ route('enqueteDisponibleExcelWord') }}";
                                    </script>

                                    <!-- Export PDF Modal -->
                                    <div id="exportPdfModal" class="modal fade" tabindex="-1" role="dialog">
                                      <div class="modal-dialog" role="document">
                                         <div class="modal-content export-pdf-modal">
                                              <div class="modal-header">
                                                        <h5 class="modal-title">Exporter la liste en PDF</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                              </div>
                                              <div class="modal-body">
                                                <p>Vous voulez exporter la liste en PDF ?</p>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                <button type="button" class="btn btn-danger" id="confirmExportPdf">Télécharger le pdf</button>
                                              </div>
                                         </div>
                                      </div>
                                    </div>

                                    <script>
                                        const exportPdf = "{{ route('export.pdf') }}";
                                    </script>
                      
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nom de l'enquête</th>
                                                <th>Contenu de l'enquête</th>
                                                <th>Date d'expiration</th>
                                                <th class="btn-creer-question {{ in_array('btn-creer-question', $disabledClasses) ? 'd-none' : '' }}" >Question</th>
                                                <th class="btn-etat  {{ in_array('btn-etat', $disabledClasses) ? 'd-none' : '' }}" >Etat</th>
                                                <th class="btn-action {{ in_array('btn-action ', $disabledClasses) ? 'd-none' : '' }}" >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                    $ide = ($enquetes->currentPage() - 1) * $enquetes->perPage() + 1;
                                            @endphp

                                            @foreach($enquetes as $index => $enquete)
                                                <tr data-id="{{ $enquete->id }}" data-nom="{{ $enquete->nom }}" class="enquete-row">
                                                    
                                                    <th scope="row">{{ $ide }}</th> <!-- Numérotation des enquêtes -->
                                                    <th>{{ $enquete->nom }}</th>
                                                    <th>{{ $enquete->info }}</th>
                                                    <th>Date Limite : {{ \Carbon\Carbon::parse($enquete->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY HH:mm') }}</th>
                                                    <th class="btn-creer-question {{ in_array('btn-creer-question', $disabledClasses) ? 'd-none' : '' }}" >
                                                        <button type="button" class="btn btn-primary view-enquete" data-toggle="modal" data-target="#optionsModal" data-enquete-id="{{ $enquete->id }}" data-enquete-nom="{{ $enquete->nom }}">
                                                        Création du questionnaire
                                                        </button>
                                                    </th>
                                                    <th class="btn-etat {{ in_array('btn-etat', $disabledClasses) ? 'd-none' : '' }}" >
                                                        <div class="toggle-container">
                                                            <div class="toggle" data-id="{{ $enquete->id }}" data-state="{{ $enquete->etat_enquete == 'activé' ? 'activé' : 'désactivé' }}"></div>
                                                            <span class="toggle-state">{{ $enquete->etat_enquete == 'activé' ? 'Activé' : 'Désactivé' }}</span>
                                                        </div>
                                                    </th>
                                                    <th class="btn-action {{ in_array('btn-action', $disabledClasses) ? 'd-none' : '' }}">
                                                        <a href="/pageUserAdminEnquetteEdit/{{ $enquete->id }}" class="edit btn-action-modif">
                                                            <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                                        </a>
                                                        <a href="{{ route('suppressionenquete.show', ['id' => $enquete->id]) }}" class="delete btn-action-sup">
                                                            <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                        </a>
                                                    </th>
                                                </tr>
                                                @php
                                                    $ide += 1;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>

                                  <!-- Affichage de la pagination -->
                                    <div class="pagination-wrapper">
                                        {{ $enquetes->links('vendor.pagination.bootstrap-5') }}
                                    </div>

                                        <style>
                                            .toggle {
                                                position: relative;
                                                height: 30px;
                                                width: 60px;
                                                border-radius: 15px;
                                                background-color: #fff;
                                                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
                                                cursor: pointer;
                                            }

                                            .toggle::before {
                                                content: "";
                                                position: absolute;
                                                height: 24px;
                                                width: 24px;
                                                background: #4070f4;
                                                border-radius: 50%;
                                                top: 50%;
                                                left: 4px;
                                                transform: translateY(-50%);
                                                transition: all 0.3s ease-in-out;
                                            }

                                            .toggle[data-state="activé"]::before {
                                                background-color: #18191a;
                                                left: calc(100% - 24px - 4px);
                                            }

                                            .toggle-state {
                                                margin-left: 10px;
                                                font-weight: bold;
                                            }
                                        </style>
                                            {{-- css1 --}}
                                            {{-- 

                                                                   <div class="clearfix">
                                                                     <div class="hint-text">showing <b>5</b> out of <b>25</b></div>
                                                                     <ul class="pagination">
                                                                        <li class="page-item disabled"><a href="#">Previous</a></li>
                                                                        <li class="page-item "><a href="#"class="page-link">1</a></li>
                                                                        <li class="page-item "><a href="#"class="page-link">2</a></li>
                                                                        <li class="page-item active"><a href="#"class="page-link">3</a></li>
                                                                        <li class="page-item "><a href="#"class="page-link">4</a></li>
                                                                        <li class="page-item "><a href="#"class="page-link">5</a></li>
                                                                        <li class="page-item "><a href="#" class="page-link">Next</a></li>
                                                                     </ul>
                                                                   </div>
                                                                
                                                                   </div>
                                                                </div>
                                            --}}
                                              <!-- Modal Intermédiaire -->
                                              {{-- css2 --}}
                                        <div class="modal fade" id="optionsModal" tabindex="-1" role="dialog" aria-labelledby="optionsModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document"> <!-- Modal large -->
                                                <div class="modal-content">
                                                        <div class="modal-header">
                                                                <h5 class="modal-title" id="optionsModalLabel">Que voulez-vous faire ?</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row text-center">
                                                                <!-- Colonne pour le bouton "Créer une question" -->
                                                                <div class="col-12 mb-2">
                                                                    <button type="button" class="btn btn-success btn-block btn-lg" data-toggle="modal" data-target="#questionnaireModal" data-dismiss="modal">
                                                                        Créer une question
                                                                    </button>
                                                                </div>
                                                                <!-- Colonne pour le bouton "Voir la liste des questions" -->
                                                                <div class="col-12 mb-2">
                                                                    <button type="button" class="btn btn-info btn-block btn-lg" id="viewQuestionsBtn" data-toggle="modal" data-target="#listeQuestionsModal" data-dismiss="modal">
                                                                        Voir la liste des questions
                                                                    </button>
                                                                </div>
                                                                <!-- Colonne pour le bouton "Fermer le modal" -->
                                                                <div class="col-12 mb-2">
                                                                    <button type="button" class="btn btn-secondary btn-block btn-lg" data-dismiss="modal">
                                                                        Fermer le modal
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>

                                    <!-- Modal pour afficher la liste des questions -->
                                    <div class="modal fade" id="listeQuestionsModal" tabindex="-1" role="dialog" aria-labelledby="listeQuestionsModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="listeQuestionsModalLabel">Questions pour l'enquête</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h6 id="enqueteNom">Nom de l'enquête</h6>
                                                    <div id="questionsList">
                                                        <!-- Les questions seront insérées ici dynamiquement -->
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- css3 --}}
                                    <script>
                                        const questionsRouteUrl = "{{ route('admin.questions.dispo.par.enquete', ['id' => ':id']) }}";
                                        const editQuestionRouteUrl = "{{ route('admin.edit.question', ['enqueteId' => ':enqueteId', 'questionId' => ':questionId']) }}";
                                        const deleteQuestionRouteUrl = "{{ route('admin.delete.question', ['enqueteId' => ':enqueteId', 'questionId' => ':questionId']) }}";
                                    </script>

                                    {{-- css4 --}}
                                      <!-- Modal -->
                                            <div class="modal fade" id="questionnaireModal" tabindex="-1" role="dialog" aria-labelledby="questionnaireModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-custom1" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="questionnaireModalLabel">Ajouter des questions pour : <span id="enqueteName"></span></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" id="enqueteIdHidden">
                                                            <button type="button" class="btn btn-success" id="addQuestionButton">+</button>
                                                            <div id="questionsContainer">
                                                                <!-- Les questions existantes seront ajoutées ici -->
                                                            </div>
                                                            <div id="dataContainer">
                                                                <!-- Les données récupérées seront ajoutées ici -->
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                            <button type="button" class="btn btn-primary" id="saveQuestionsButton">Enregistrer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <!----add-modal start--------->
                                            <div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
                                                  <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title">Créer une enquête</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                             <form action="{{route('Creer.une.enquet')}}" method="POST">
                                                                    @csrf
                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label>Nom de l'enquête</label>
                                                                                    <input name="nom" type="text" class="form-control" required >
                                                                                    @error('nom')
                                                                                                        <p class="error" style="color:red;" >{{ $message }}</p>
                                                                                    @enderror
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Contenu de l'enquête</label>
                                                                                    <textarea name="contenu"class="form-control" required ></textarea>
                                                                                    @error('contenu')
                                                                                                        <p class="error" style="color:red;" >{{ $message }}</p>
                                                                                    @enderror
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Choisissez la date d'expiration de la candidature à l'enquête</label>
                                                                                    <input type="date"  id="date_expiration" name="date_expiration" class="form-control" required >
                                                                                    @error('date_expiration')
                                                                                        <p class="error" style="color:red;" >{{ $message }}</p>
                                                                                    @enderror
                                                                                    <p id="message"></p>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Choisissez l'heure d'expiration de la candidature à l'enquête</label>
                                                                                    <input type="time" id="heure_expiration" name="heure_expiration" class="form-control" required >
                                                                                    @error('heure_expiration')
                                                                                        <p class="error" style="color:red;" >{{ $message }}</p>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>
                                                                          <div class="modal-footer">
                                                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                                                                              <button type="submit" class="btn btn-success save-button">Enregistrer</button>
                                                                          </div>
                                                             </form>
                                                              <!-- Export Excel Modal  -->
                                                                <div id="exportExcelModal" class="modal fade" tabindex="-1" role="dialog">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content export-excel-modal">
                                                                          <div class="modal-header">
                                                                            <h5 class="modal-title">Exporter la liste en Excel</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                              <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                          </div>
                                                                          <div class="modal-body">
                                                                            <p>Vous voulez exporter la liste en Excel ?</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                                            <button type="button" class="btn btn-success" id="confirmExportExcel">Télécharger</button>
                                                                          </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                             {{-- css5 --}}
                                                        </div>
                                                  </div>
                                            </div>
                                           <form action="#" method="POST">
                                                @csrf
                                                    <div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Edit Employees</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div>
                                                                        <p>Voulez-vous vraiment modifier cet Article?</p>
                                                                    </div>
                                                                    <p id="articleId"></p> <!-- Placeholder to display the article ID -->
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">NON</button>
                                                                    <a id="editButton" class="btn btn-success" href="">OUI</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                           </form>
                                       <!----edit-modal end--------->      
                                      <!----delete-modal start--------->
                                        <div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title">Delete Employees</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p>Are you sure you want to delete this Records</p>
                                                    <p class="text-warning"><small>this action Cannot be Undone,</small></p>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="button" class="btn btn-success">Delete</button>
                                                  </div>
                                            </div>
                                          </div>
                                        </div>
                                   <!----edit-modal end--------->   
                            </div>
                        </div>
                                <!------main-content-end-----------> 
                                                  @include('SuperAdmin.footer')
   
            </div>
   
        </div>
     @include('sweetalert::alert')

<script src="{{ ('asset/assetJS/index2Ad.js') }}"></script>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.slim.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/popper.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/bootstrap.min.js')}}"></script>
   <script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.min.js')}}"></script>
  
</body>


{{-- 
@php
    // Convertir l'array PHP en JSON pour l'utiliser en JavaScript
    // $disabledClassesJson = json_encode($disabledClasses);
@endphp --}}

    <!-- <script>
        // Récupérer les classes désactivées passées du backend
        const disabledClasses = @json($disabledClasses);

        // Fonction pour compter les balises avec ces classes
        let count = 0;
        disabledClasses.forEach(className => {
            // Rechercher toutes les balises avec cette classe
            const elements = document.querySelectorAll('.' + className);
            count += elements.length;
        });

        // Afficher le nombre de balises trouvées
        console.log('Nombre de balises trouvées avec les classes désactivées :', count);
    </script>
     -->
{{-- 
@php
    // Convertir l'array PHP en JSON pour l'utiliser en JavaScript
    $disabledClassesJson = json_encode($disabledClasses);
@endphp --}}



<!-- Optionnel : afficher le résultat dans un élément HTML -->
