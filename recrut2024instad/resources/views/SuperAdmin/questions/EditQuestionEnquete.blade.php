<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/editModQuestionEnq.css') }}">
    <title>Document</title>
</head>
<body>
<div class="container">
    <h2>Modifier la question</h2>

    <!-- Affichage des messages d'erreur -->
    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif

    <!-- Formulaire d'édition de la question -->
    <form id="questionForm" action="{{ route('question.update', ['enqueteId' => $enqueteId, 'questionId' => $question->id]) }}" method="POST">
        @csrf
        @method('PUT')

        <!-- Champ pour le texte de la question -->
        <div class="form-group">
            <label for="question">Texte de la question :</label>
            <input type="text" name="question" id="question" class="form-control" value="{{ old('question', $question->question) }}" required>
            @error('question')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <!-- Champ pour le type de réponse -->
        <div class="form-group">
            <label for="typeReponse">Type de réponse :</label>
            <select name="typeReponse" id="typeReponse" class="form-control" required>
                <option value="input" {{ $question->typeReponse == 'input' ? 'selected' : '' }}>Champ uniligne</option>
                <option value="textarea" {{ $question->typeReponse == 'textarea' ? 'selected' : '' }}>Champ multiligne</option>
                <option value="radio" {{ $question->typeReponse == 'radio' ? 'selected' : '' }}>Bouton radio</option>
                <option value="checkbox" {{ $question->typeReponse == 'checkbox' ? 'selected' : '' }}>Case à cocher</option>
                <option value="select" {{ $question->typeReponse == 'select' ? 'selected' : '' }}>Liste déroulante</option>
                <option value="numericTel" {{ $question->typeReponse == 'numericTel' ? 'selected' : '' }}>Champ numérique (Tel)</option>
                <option value="numericAge" {{ $question->typeReponse == 'numericAge' ? 'selected' : '' }}>Champ numérique (Age)</option>
                <option value="date" {{ $question->typeReponse == 'date' ? 'selected' : '' }}>Champ de date</option>
                <option value="email" {{ $question->typeReponse == 'email' ? 'selected' : '' }}>Champ email</option>
                <option value="ifu" {{ $question->typeReponse == 'ifu' ? 'selected' : '' }}>Champ IFU</option>
                <option value="file" {{ $question->typeReponse == 'file' ? 'selected' : '' }}>Champ fichier</option>
                <option value="listeDep" {{ $question->typeReponse == 'listeDep' ? 'selected' : '' }}>Liste des départements</option>
                <option value="langue" {{ $question->typeReponse == 'langue' ? 'selected' : '' }}>Liste des langues</option>
                <option value="expInstad" {{ $question->typeReponse == 'expInstad' ? 'selected' : '' }}>Expériences avec INStaD</option>
            </select>
            @error('typeReponse')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            <div id="invalidTypeError" class="text-danger" style="display: none;">Votre champ est invalide.</div>
        </div>

        <!-- Champ pour le nombre d'options -->
        <div class="form-group">
            <label for="nbrOption">Nombre d'options :</label>
            <input type="number" name="nbrOption" id="nbrOption" class="form-control" value="{{ old('nbrOption', $question->nbrOption) }}" min="0">
        </div>

        <!-- Champs pour les options -->
        <div id="optionsContainer">
            @for ($i = 1; $i <= 6; $i++)
                <div class="form-group option-field" id="optionBlock{{ $i }}">
                    <label for="option{{ $i }}">Option {{ $i }} :</label>
                    <input type="text" name="option{{ $i }}" id="option{{ $i }}" class="form-control" value="{{ old('option' . $i, $question->{'option' . $i}) }}">
                    @error('option' . $i)
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            @endfor
        </div>

        <!-- Bouton pour soumettre le formulaire -->
        <div class="button-container">
            <button type="submit" class="btn btn-primary">Enregistrer les modifications</button>
        </div>

    </form>
</div>

<script src="{{ asset('asset/assetJS/editQuestEnq.js') }}"></script>
</body>
</html>
