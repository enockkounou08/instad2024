<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="container">
    <center>
        <div class="container">
            <!-- Affichage des messages de succès -->
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <!-- Affichage des messages d'erreur -->
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </center>

    <h2>Modifier la Question</h2>
    <form action="{{ route('editQuestion', $question->id) }}" method="POST">
        @csrf

        <!-- Affichage de la question -->
        <div class="form-group">
            <label for="question">Question</label>
            <p id="question" class="form-control-static">{{ $question->question }}</p>
        </div>

        <!-- Affichage du type de réponse -->
        <div class="form-group">
            <label for="typeReponse">Type de réponse</label>
            <p id="typeReponse" class="form-control-static">
                @switch($question->typeReponse)
                    @case('champs uniligne')
                        champs uniligne
                        @break
                    @case('radio')
                        champs radio
                        @break
                    @case('select')
                        liste déroulante
                        @break
                    @case('checkbox')
                        case à cocher
                        @break
                    @case('textarea')
                        champs multiligne
                        @break
                    @default
                        Non défini
                @endswitch
            </p>
        </div>

        <!-- Affichage des options si elles existent -->
        @if($question->typeReponse == 'radio' || $question->typeReponse == 'checkbox' || $question->typeReponse == 'select')
            <div class="form-group">
                <label for="options">Options</label>
                <ul id="options" class="form-control-static">
                    @for ($i = 1; $i <= 6; $i++)
                        @php
                            $option = 'option' . $i;
                        @endphp
                        @if (!empty($question->$option))
                            <li>{{ $question->$option }}</li>
                        @endif
                    @endfor
                </ul>
            </div>
        @endif

        <a href="{{ route('question.details.Modif', $question->id) }}">Modifier cette question</a>

        <!-- Lien pour supprimer la question -->
        <a href="{{ route('deleteQuestion', $question->id) }}" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette question ?')">Supprimer</a>
    </form>

    <a href="{{ route('accueiladmin_page') }}">Retour</a>
</div>
</body>
</html>
