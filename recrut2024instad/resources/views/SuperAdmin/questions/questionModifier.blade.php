<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    


<div class="container">
    <h2>Modifier la Question</h2>
    <form action="{{ route('editQuestion', $question->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="question">Question</label>
            <input type="text" class="form-control" id="question" name="question" value="{{ $question->question }}" required>
        </div>
        <div class="form-group">
            <label for="typeReponse">Type de réponse</label>
            <select class="form-control" id="typeReponse" name="typeReponse" required>
                <option value="champs uniligne" {{ $question->typeReponse == 'champs uniligne' ? 'selected' : '' }}>champs uniligne</option>
                <option value="liste optionnelle" {{ $question->typeReponse == 'liste optionnelle' ? 'selected' : '' }}>liste optionnelle</option>
                <option value="champs multiligne" {{ $question->typeReponse == 'champs multiligne' ? 'selected' : '' }}>champs multiligne</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Enrégistrer la modification </button>
        <a href="{{ route('accueiladmin_page') }}" > retour</a>
    </form>
</div>

</body>
</html>