<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Processed Questions</title>
</head>
<body>
    <h1>Processed Questions</h1>
    @foreach ($questions as $index => $question)
        <p>Question {{ $index + 1 }}: {{ $question['text'] }}</p>
        <p>Type de réponse: {{ $question['typeReponse'] }}</p>
        @if (isset($question['numOptions']))
            <p>Nombre d'options: {{ $question['numOptions'] }}</p>
            @foreach ($question['options'] as $optionIndex => $optionText)
                <p>Option {{ $optionIndex + 1 }}: {{ $optionText }}</p>
            @endforeach
        @endif
        <br>
    @endforeach
</body>
</html>
