<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    
    <div class="container">
    <h1>Édition de la Question</h1>
    
    <!-- Formulaire d'édition -->
    <form action="{{ route('admin.update.question', $question->id) }}" method="POST">
        @csrf
        @method('PUT')
        
        <div class="form-group">
            <label for="question">Question:</label>
            <input type="text" class="form-control" id="question" name="question" value="{{ $question->question }}" required>
        </div>
        
        <div class="form-group">
            <label for="typeReponse">Type de Réponse:</label>
            <input type="text" class="form-control" id="typeReponse" name="typeReponse" value="{{ $question->typeReponse }}">
        </div>
        
        <!-- Ajoutez d'autres champs selon vos besoins -->
        
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
    </form>
</div>



</body>
</html>