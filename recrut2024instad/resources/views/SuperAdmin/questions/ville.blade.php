<!-- resources/views/form.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/VilleLangueRolePoste.css') }}">
    <title> Formulaire d'ajout de ville</title>
    
</head>
<body>

@if(session('success'))
    <div style="color: green; margin-bottom: 20px;">
        {{ session('success') }}
    </div>
@endif

@if ($errors->any())
    <div style="color: red; margin-bottom: 20px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<form action="{{ route('valider.ville') }}" method="POST">
    @csrf
    <p><h1>Ajout d'une nouvelle ville</h1></p>
    <label for="ville">Ville :</label>
    <input type="text" id="ville" name="ville" value="{{ old('ville') }}">
    <button type="submit">Ajouter</button>
</form>

</body>
</html>
