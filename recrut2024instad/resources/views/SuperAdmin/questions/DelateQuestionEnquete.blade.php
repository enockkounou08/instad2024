<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Supprimer la Question</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/delateQuestionEnquette.css') }}">
    
</head>
<body>

<div class="container">
    <h2>Supprimer la Question</h2>

    <!-- Affichage de la question à supprimer -->
    <p>Êtes-vous sûr de vouloir supprimer cette question : <strong>{{ $question->question }}</strong> ?</p>
    <p>Type de champ : <strong>{{ $question->typeReponse }}</strong></p>
    <p>Nombre d'options : <strong>{{ $question->nbrOption }}</strong></p>
    <p>Enquête : <strong>{{ $question->nomEnquete }}</strong></p>

    <!-- Affichage des options s'il y en a -->
    @if ($question->nbrOption > 0)
        <h4>Options :</h4>
        <ul>
            @if ($question->option1) <li>Option 1 : {{ $question->option1 }}</li> @endif
            @if ($question->option2) <li>Option 2 : {{ $question->option2 }}</li> @endif
            @if ($question->option3) <li>Option 3 : {{ $question->option3 }}</li> @endif
            @if ($question->option4) <li>Option 4 : {{ $question->option4 }}</li> @endif
            @if ($question->option5) <li>Option 5 : {{ $question->option5 }}</li> @endif
            @if ($question->option6) <li>Option 6 : {{ $question->option6 }}</li> @endif
        </ul>
    @else
        <p>Aucune option pour cette question.</p>
    @endif

    <!-- Affichage des messages d'erreur -->
    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif

    <!-- Formulaire de suppression -->
    <form action="{{ route('question.delete', ['enqueteId' => $enqueteId, 'questionId' => $question->id]) }}" method="POST">
        @csrf
        @method('DELETE')
        
        <!-- Boutons de confirmation et d'annulation -->
        <div class="actions">
            <button type="submit" class="btn btn-danger">Confirmer la suppression</button>
            <a href="{{ route('accueiladmin_page') }}" class="btn btn-secondary">Annuler</a>
        </div>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>
