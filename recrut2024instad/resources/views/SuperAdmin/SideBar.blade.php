<div id="sidebar" class="sidebar_admin">
        <div class="sidebar-header">
             <h3><img src="{{ asset('asset/assetSuperAdmin/img/OIP.jpg') }}" class="img-fluid"/><span>Réservé à L'Administrateur</span></h3>
        </div>
        <ul class="list-unstyled component m-0 dashboard">
            <li class="{{ Request::is('pageUserAdmin') ? 'active' : '' }}">
                 <a  href=" {{route('accueiladmin_page')}}" class="dashboard"><i class="material-icons">dashboard</i>dashboard </a>
            </li>
           {{-- @if(!in_array('userRegister', $disabledClasses)) --}}
            {{-- <li class="userRegister  {{ Request::is('affichageUtilisateur') ? 'active' : 'dropdown' }}"> --}}
            <li class="userRegister  {{ in_array('userRegister', $disabledClasses) ? 'd-none' : (Request::is('affichageUtilisateur') ? 'active' : 'dropdown') }}">
                  <a href="{{ route('utilisateurinscrit_page') }}" data-toggle="" aria-expanded="false" 
                  class="link signup-link">
                       <i class="material-icons">aspect_ratio</i>Utilisateurs inscrits
                  </a>
            </li>
            {{-- @endif --}}
            {{-- <li class="enqueteRegister {{ Request::is('pageUserAdminPersonneInscritPourEnquette*') ? 'active' : 'dropdown' }}"> --}}
            <li class="enqueteRegister {{ in_array('enqueteRegister', $disabledClasses) ? 'd-none' : (Request::is('pageUserAdminPersonneInscritPourEnquette*') ? 'active' : 'dropdown') }}">
                <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" 
                            class="dropdown-toggle">
                    <i class="material-icons">apps</i>Enquêtes disponibles
                </a>
                @foreach($enquetes as $enquete)
                    <ul class="collapse list-unstyled menu" id="homeSubmenu2">
                        <li><a href="{{ route('inscritpourenquete.show', ['id' => $enquete->id]) }}">{{ $enquete->nom }}</a></li>
                        
                    </ul>
                @endforeach
            </li>
          
            {{-- <li class="ansUser {{ Request::is('pageUserAdminReponsePersonneParEnquette*') ? 'active' : 'dropdown' }}"> --}}
            <li class="ansUser {{ in_array('ansUser', $disabledClasses) ? 'd-none' : (Request::is('pageUserAdminReponsePersonneParEnquette*') ? 'active' : 'dropdown') }}">
                    <a href="#pageSubmenu5" data-toggle="collapse" aria-expanded="false" 
                    class="dropdown-toggle">
                    <i class="material-icons">border_color</i><span>Réponses utilisateurs</span></a>
                    <ul class="collapse list-unstyled menu" id="pageSubmenu5">
                        @foreach($enquetes as $enquete)
                            <li>
                                <a href="{{ route('reponsepourenquete.show', ['id' => $enquete->id]) }}">{{ $enquete->nom }}</a>
                            </li>
                        @endforeach
                    </ul>
            </li>
        </ul>               
</div>