<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Page Administrateur</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/bootstrap.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/css/custom.css') }}">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">

    <!-- Google Material Icons -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <div class="body-overlay"></div>
		<li class="active">
			<a href="#" class="dashboard"><i class="material-icons">dashboard</i>Liste des inscrits </a>
		</li> 
		  
		  <!--li class="dropdown">
		  <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">aspect_ratio</i>Layouts
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu1">
		     <li><a href="#">layout 1</a></li>
			 <li><a href="#">layout 2</a></li>
			 <li><a href="#">layout 3</a></li>
		  </ul>
		  </li-->
		  
		  
		   <li class="dropdown">
		  <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">apps</i>Enquettes disponibles
		  </a>
		  @foreach($enquetes as $enquete)

			<!--th>{{ $enquete->N° }}</th>
				<th>{{ $enquete->id }}</th-->
				
		  <ul class="collapse list-unstyled menu" id="homeSubmenu2">
		     <li><a href="/pageUserAdminPersonneInscritPourEnquette/{{ $enquete->id }}">{{ $enquete->nom }}</a></li>
		  </ul>
		
			@endforeach
		  </li>
		  
		   <!--li class="dropdown">
		  <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">equalizer</i>charts
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu3">
		     <li><a href="#">Pages 1</a></li>
			 <li><a href="#">Pages 2</a></li>
			 <li><a href="#">Pages 3</a></li>
		  </ul>
		  </li>
		  
		  
		   <li class="dropdown">
		  <a href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">extension</i>UI Element
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu4">
		     <li><a href="#">Pages 1</a></li>
			 <li><a href="#">Pages 2</a></li>
			 <li><a href="#">Pages 3</a></li>
		  </ul>
		  </li>
		  
		   <li class="dropdown">
		  <a href="#homeSubmenu5" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">border_color</i>forms
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu5">
		     <li><a href="#">Pages 1</a></li>
			 <li><a href="#">Pages 2</a></li>
			 <li><a href="#">Pages 3</a></li>
		  </ul>
		  </li>
		  
		  <li class="dropdown">
		  <a href="#homeSubmenu6" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">grid_on</i>tables
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu6">
		     <li><a href="#">table 1</a></li>
			 <li><a href="#">table 2</a></li>
			 <li><a href="#">table 3</a></li>
		  </ul>
		  </li>
		  
		  
		  <li class="dropdown">
		  <a href="#homeSubmenu7" data-toggle="collapse" aria-expanded="false" 
		  class="dropdown-toggle">
		  <i class="material-icons">content_copy</i>Pages
		  </a>
		  <ul class="collapse list-unstyled menu" id="homeSubmenu7">
		     <li><a href="#">Pages 1</a></li>
			 <li><a href="#">Pages 2</a></li>
			 <li><a href="#">Pages 3</a></li>
		  </ul>
		  </li>
		  
		   
		  <li class="">
		  <a href="#" class=""><i class="material-icons">date_range</i>copy </a>
		  </li>
		  <li class="">
		  <a href="#" class=""><i class="material-icons">library_books</i>calender </a>
		  </li-->
		
		</ul>
	 </div>
	 
   <!-------sidebar--design- close----------->
   
   
   
      <!-------page-content start----------->
   
      <div id="content">
	     
		  <!------top-navbar-start-----------> 
		     
		  <div class="top-navbar">
		     <div class="xd-topbar">
			     <div class="row">
				     <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
					    <div class="xp-menubar">
						    <span class="material-icons text-white">signal_cellular_alt</span>
						</div>
					 </div>
					 
					 <div class="col-md-5 col-lg-3 order-3 order-md-2">
					     <div class="xp-searchbar">
						     <!--form >
							    <div class="input-group">
								  <input type="search" class="form-control"
								  placeholder="Search">
								  <div class="input-group-append">
								     <button class="btn" type="submit" id="button-addon2">Go
									 </button>
								  </div>
								</div>
							 </form -->
						 </div>
					 </div>
					 
					 
					 <!--div class="col-10 col-md-6 col-lg-8 order-1 order-md-3">
					     <div class="xp-profilebar text-right">
						    <nav class="navbar p-0">
							   <ul class="nav navbar-nav flex-row ml-auto">
							   <li class="dropdown nav-item active">
							     <a class="nav-link" href="#" data-toggle="dropdown">
								  <span class="material-icons">notifications</span>
								  <span class="notification">4</span>
								 </a>
								  <ul class="dropdown-menu">
								     <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
									 <li><a href="#">You Have 4 New Messages</a></li>
								  </ul>
							   </li>
							   
							   <li class="nav-item">
							     <a class="nav-link" href="#">
								   <span class="material-icons">question_answer</span>
								 </a>
							   </li>
							   
							   <li class="dropdown nav-item">
							     <a class="nav-link" href="#" data-toggle="dropdown">
								  <img src="{{ ('asset/assetSuperAdmin/img/user.jpg')}}" style="width:40px; border-radius:50%;"/>
								  <span class="xp-user-live"></span>
								 </a>
								  <ul class="dropdown-menu small-menu">
								     <li><a href="#">
									 <span class="material-icons">person_outline</span>
									 Profile
									 </a></li>
									 <li><a href="#">
									 <span class="material-icons">settings</span>
									 Settings
									 </a></li>
									 <li><a href="#">
									 <span class="material-icons">logout</span>
									 Logout
									 </a></li>
									 
								  </ul>
							   </li>
							   
							   
							   </ul>
							</nav>
						 </div>
					 </div-->
					 
				 </div>
				 
				 <div class="xp-breadcrumbbar text-center">
				    <h4 class="page-title">Page des Enquêtes Créer</h4>
					<ol class="breadcrumb">
					  <li class="breadcrumb-item active" aria-curent="page">Page Administrateur</li>
					</ol>
				 </div>
				 
				 
			 </div>
		  </div>
		  <!------top-navbar-end-----------> 
		  
		  
		   <!------main-content-start-----------> 
		     
		      <div class="main-content">
			     <div class="row">
				    <div class="col-md-12">
					   <div class="table-wrapper">
					   <center>
								<div>
									@if (session('creationEnquette'))
								<div class="alert alert-success">
								{{ (session('creationEnquette')) }}
								</div>
								@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('status'))
						<div class="alert alert-success">
						{{ (session('status')) }}
						</div>
						@endif
							</div>
						</center>

						<center>
							<div>

							@if (session('statusSup'))
						<div class="alert alert-danger">
						{{ (session('statusSup')) }}
						</div>
						@endif
							</div>
						</center>
						
						

					   <div class="table-title">
					     <div class="row">
						     <div class="col-sm-6 p-0 flex justify-content-lg-start justify-content-center">
							    <h2 class="ml-lg-2">Liste des enquêtes</h2>
							 </div>
							 <div class="col-sm-6 p-0 flex justify-content-lg-end justify-content-center">
							   <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
							   <i class="material-icons">&#xE147;</i>
							   <span>Créer une enquêtes</span>
							   </a>
							   <!--a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal">
							   <i class="material-icons">&#xE15C;</i>
							   <span>Supprimer</span-->
							   </a>
							 </div>
					     </div>
					   </div>
					 

					   <table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>N°</th>
									
									<th>Nom de l'enquête</th>
									<th>Contenu de l'enquête</th>
									<th>Date d'expiration</th>
									
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
									$ide = 1;
								@endphp

								@foreach($enquetes as $enquete)
									<tr>
										<th scope="row">{{ $ide }}</th>
										<th>{{ $enquete->nom }}</th>
										<th>{{ $enquete->info }}</th>
										<th>Date Limite : {{ \Carbon\Carbon::parse($enquete->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY HH:mm') }}</th>
        
										
										<th>
											<a href="/pageUserAdminEnquetteEdit/{{ $enquete->id }}" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
											<a href="/pageUserAdminEnquetteDelate/{{ $enquete->id }}" class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
										</th>
									</tr>

									@php
										$ide += 1;
									@endphp
								@endforeach
							</tbody>
						</table>

					   <div class="clearfix">
					     <div class="hint-text">showing <b>5</b> out of <b>25</b></div>
					     <ul class="pagination">
						    <li class="page-item disabled"><a href="#">Previous</a></li>
							<li class="page-item "><a href="#"class="page-link">1</a></li>
							<li class="page-item "><a href="#"class="page-link">2</a></li>
							<li class="page-item active"><a href="#"class="page-link">3</a></li>
							<li class="page-item "><a href="#"class="page-link">4</a></li>
							<li class="page-item "><a href="#"class="page-link">5</a></li>
							<li class="page-item "><a href="#" class="page-link">Next</a></li>
						 </ul>
					   </div>
					
					   </div>
					</div>
					


					



					
									   <!----add-modal start--------->
		<div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Créer une enquête</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			

	  <form action="{{('/AdminSaveEnquette')}}" method="POST">
    	@csrf
      <div class="modal-body">
			<div class="form-group">
				<label>Quel est le nom de l'enquête</label>
				<input name="nom" type="text" class="form-control" required >
				@error('nom')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
	
			<div class="form-group">
				<label>Contenu de l'enquette</label>
				<textarea name="contenu"class="form-control" required ></textarea>
				@error('contenu')
									<p class="error" style="color:red;" >{{ $message }}</p>
				@enderror
			</div>
			<div class="form-group">
            <label>Choisissez la date d'expiration de l'enquête</label>
            <input type="date"  id="date_expiration" name="date_expiration" class="form-control" required >
            @error('date_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
			<p id="message"></p>

        <!-- Sidebar -->
        <div id="sidebar">
            <div class="sidebar-header">
                <h3><img src="{{ asset('asset/assetSuperAdmin/img/OIP.jpg') }}" class="img-fluid" /><span>Réservé à L'Administrateur</span></h3>
            </div>
            <ul class="list-unstyled component m-0">
                <li class="active">
                    <a href="#" class="dashboard"><i class="material-icons">dashboard</i>dashboard</a>
                </li>
                <li class="dropdown">
                    <a href="/affichageUtilisateur" data-toggle="collapse" aria-expanded="false">
                        <i class="material-icons">apps</i>Liste des utilisateurs
                    </a>
                    <ul class="collapse list-unstyled" >
                        <!-- Add user list items here -->
                        {{-- <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Prénoms</th>
                                    <th>Email</th>
                                    <th>Numéro de téléphone</th>
                                    <th>Adresse</th>
                                    <th>Diplôme</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->prenom }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->telephone }}</td>
                                    <td>{{ $user->adresse }}</td>
                                    <td>{{ $user->diplome }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table> --}}
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#surveySubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="material-icons">apps</i>Enquêtes disponibles
                    </a>
                    <ul class="collapse list-unstyled" id="surveySubmenu">
                        @foreach($enquetes as $enquete)
                        <li><a href="/pageUserAdminPersonneInscritPourEnquette/{{ $enquete->id }}">{{ $enquete->nom }}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>

        <!-- Page Content -->
        <div id="content">
            <!-- Top Navbar -->
            <div class="top-navbar">
                <div class="xd-topbar">
                    <div class="row">
                        <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                            <div class="xp-menubar">
                                <span class="material-icons text-white">signal_cellular_alt</span>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-3 order-3 order-md-2">
                            <div class="xp-searchbar"></div>
                        </div>
                    </div>
                    <div class="xp-breadcrumbbar text-center">
                        <h4 class="page-title">Page des enquêtes créées</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Page Administrateur</li>
                        </ol>
                    </div>
                </div>
            </div>
		<div class="form-group">
            <label>Choisissez l'heure d'expiration de l'enquête</label>
            <input type="time" id="heure_expiration" name="heure_expiration" class="form-control" required >
            @error('heure_expiration')
                <p class="error" style="color:red;" >{{ $message }}</p>
            @enderror
        </div>
      	</div>
      <div class="modal-footer">
	  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success" >Enrégistrer</button>
		<!--a type="submit" class="btn btn-success" href="{{ ('/AdminSaveEnquette')}}">Add</a-->
      </div>

            <!-- Main Content -->
            <div class="main-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-wrapper">
                            <center>
                                <div>
                                    @if (session('creationEnquette'))
                                    <div class="alert alert-success">
                                        {{ session('creationEnquette') }}
                                    </div>
                                    @endif
                                </div>
                            </center>
                            <center>
                                <div>
                                    @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                    @endif
                                </div>
                            </center>
                            <center>
                                <div>
                                    @if (session('statusSup'))
                                    <div class="alert alert-danger">
                                        {{ session('statusSup') }}
                                    </div>
                                    @endif
                                </div>
                            </center>

                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-6 p-0 flex justify-content-lg-start justify-content-center">
                                        <h2 class="ml-lg-2">Liste des Enquêtes</h2>
                                    </div>
                                    <div class="col-sm-6 p-0 flex justify-content-lg-end justify-content-center">
                                        <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal">
                                            <i class="material-icons">&#xE147;</i>
                                            <span>Créer une enquête</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Enquêtes</th>
                                        <th>Description</th>
                                        <th>Date d'expiration</th>
                                        <th>Nombre de postulants</th>
                                        <th>Nom et prénom des postulants</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $ide = 1; @endphp
                                    @foreach($enquetes as $enquete)
                                    <tr>
                                        <th scope="row">{{ $ide }}</th>
                                        <td>{{ $enquete->nom }}</td>
                                        <td>{{ $enquete->info }}</td>
                                        <td>Date Limite : {{ \Carbon\Carbon::parse($enquete->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY') }}</td>
                                        <td>{{ $enquete->occurrences }}</td>
                                        <td>{{ $enquete->personnes_associées }}</td>
                                        <td>
                                            <a href="/pageUserAdminEnquetteEdit/{{ $enquete->id }}" class="edit">
                                                <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                            </a>
                                            <a href="/pageUserAdminEnquetteDelate/{{ $enquete->id }}" class="delete">
                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                            </a>
                                        </td>
                                    </tr>
                                    @php $ide += 1; @endphp
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="clearfix">
                                <div class="hint-text">showing <b>5</b> out of <b>25</b></div>
                                <ul class="pagination">
                                    <li class="page-item disabled"><a href="#">Previous</a></li>
                                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                                    <li class="page-item active"><a href="#" class="page-link">3</a></li>
                                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                                    <li class="page-item"><a href="#" class="page-link">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Add Modal -->
                    <div class="modal fade" tabindex="-1" id="addEmployeeModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Créer une enquête</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ url('/AdminSaveEnquette') }}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Nom de l'enquête</label>
                                            <input name="nom" type="text" class="form-control" required>
                                            @error('nom')
                                            <p class="error">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="contenu" class="form-control" required></textarea>
                                            @error('contenu')
                                            <p class="error">{{ $message }}</p>
											@enderror
										</div>
										<div class="form-group">
											<label>Date d'expiration de l'enquête</label>
											<input type="date" id="date_expiration" name="date_expiration"
												class="form-control" required>
											@error('date_expiration')
											<p class="error">{{ $message }}</p>
											@enderror
											<p id="message"></p>

										</div>


									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Cancel</button>
										<button type="submit" class="btn btn-success">Enrégistrer</button>
										<!--a type="submit" class="btn btn-success" href="{{ ('/AdminSaveEnquette')}}">Add</a-->
									</div>

								</form>
							</div>
						</div>
					</div>

					<script>
						// Fonction pour vérifier la date d'expiration
    function verifierDateExpiration() {
        var dateExpiration = document.getElementById("date_expiration").value;
        var today = new Date().toISOString().split('T')[0];
        
        if (dateExpiration <= today) {
            document.getElementById("message").innerText = "La date doit être supérieure à la date d'aujourd'hui.";
			document.getElementById("message").style.color = "red";
		} else {
            document.getElementById("message").innerText = "";
        }
    }

    // Écouter les changements dans le champ de date
    document.getElementById("date_expiration").addEventListener("input", verifierDateExpiration);
					</script>
					<!----edit-modal end--------->





					<!----edit-modal start--------->



					<form action="#" method="POST">
						@csrf
						<div class="modal fade" tabindex="-1" id="editEmployeeModal" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Edit Employees</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div>
											<p>Voulez-vous vraiment modifier cet Article?</p>
										</div>
										<p id="articleId"></p> <!-- Placeholder to display the article ID -->
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">NON</button>
										<a id="editButton" class="btn btn-success" href="">OUI</a>
									</div>
								</div>
							</div>
						</div>


					</form>





					<!----edit-modal end--------->


					<!----delete-modal start--------->
					<div class="modal fade" tabindex="-1" id="deleteEmployeeModal" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Delete Employees</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<p>Are you sure you want to delete this Records</p>
									<p class="text-warning"><small>this action Cannot be Undone,</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-success">Delete</button>
								</div>
							</div>
						</div>
					</div>

					<!----edit-modal end--------->




				</div>
			</div>

			<!------main-content-end----------->



			<!----footer-design------------->

			<footer class="footer">
				<div class="container-fluid">
					<div class="footer-in">
						<p class="mb-0">&copy 2024 .Page réservé uniquement à l'Administrateur</p>
					</div>
				</div>
			</footer>




		</div>

	</div>



	<!-------complete html----------->






	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.slim.min.js')}}"></script>
	<script src="{{ ('asset/assetSuperAdmin/js/popper.min.js')}}"></script>
	<script src="{{ ('asset/assetSuperAdmin/js/bootstrap.min.js')}}"></script>
	<script src="{{ ('asset/assetSuperAdmin/js/jquery-3.3.1.min.js')}}"></script>


	<script type="text/javascript">
		$(document).ready(function(){
	      $(".xp-menubar").on('click',function(){
		    $("#sidebar").toggleClass('active');
			$("#content").toggleClass('active');
		  });
		  
		  $('.xp-menubar,.body-overlay').on('click',function(){
		     $("#sidebar,.body-overlay").toggleClass('show-nav');
		  });
		  
	   });

       $('.dashboard').on('click', function(e) {
    e.preventDefault();
    $('#userListSubmenu').hide();
});


	</script>





</body>

</html>