<!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Bootstrap demo</title>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
            <link rel="stylesheet" href="{{ asset('asset/assetSuperAdmin/csspersonnalisé/supprEnq.css') }}">


              <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css" rel="stylesheet">

        </head>
        <body>
            @include('sweetalert::alert')

                
            <div class="contaier text-center">
                <hr >
                     <h1  >Formulaire de modification</h1>
                <hr>

                @if (session('status'))
                  <div class="alert alert-success">
                    {{ (session('status')) }}
                  </div>
                @endif

              
                <ul>
                    @foreach ($errors->all() as $error)

                      <li class="alert alert-danger">{{ $error}}</li>
                    @endforeach
                </ul>
            </div>

            <div class="container" >
                <form action="/pageUserAdminEnquetteEdit" method="post">
                    @csrf
                    <input type="text" name="id" style="display: none;" value="{{ $contenus->id }}">

                    <div class="mb-3">
                        <label for="Nom" class="form-label">Nom</label>
                        <input type="text" class="form-control" id="Nom" name="nom" value="{{ $contenus->nom }}" required>
                    </div>
                    
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1">Enquête</label>
                        <textarea name="contenu" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{ $contenus->info }}</textarea>
                    </div>

                    <div class="mb-3">
                        <label for="expiration_date" class="form-label">Date d'expiration</label>
                        <input type="date" class="form-control" id="expiration_date" name="expiration_date" value="{{ $contenus->date_expiration ? $contenus->date_expiration->format('Y-m-d') : '' }}" required>
                    </div>
                    
                    <div class="mb-3">
                        <label for="expiration_time" class="form-label">Heure d'expiration</label>
                        <input type="time" class="form-control" id="expiration_time" name="expiration_time" value="{{ $contenus->date_expiration ? $contenus->date_expiration->format('H:i') : '' }}" required>
                    </div>

                    <div class="actions">
                        <a href="{{ route('accueiladmin_page') }}" class="btn btn-danger">Retour</a>
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
            </div>

                   
                  



                 <!-- SweetAlert2 JS -->
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        </body>
    </html>
