<!DOCTYPE html>
<html>
<head>
    <title>Test Mail</title>
</head>
<body>
    <h1>{{ $subject }}</h1>
    <p>{{ $mailMessage }}</p>
    <p><strong>Nouveau:</strong> {{ $mailMessage }}</p>
    <p><strong>Email:</strong> {{ $subject }}</p>
    
    <p><a href="{{ url('/verify/' . $activation_token) }}">Cliquez ici pour activer votre compte</a></p>
</body>
</html>
