<!DOCTYPE html>
<html>
<head>
    <title>Formulaire de test</title>
</head>
<body>
    <form action="{{ url('/register') }}" method="post">
        @csrf
        <label for="nom">Nom</label>
        <input type="text" name="nom" value="{{ old('nom') }}">

        @error('nom')
            <span class="error" style="color: red;">{{ $message }}</span>
        @enderror 

        <label for="email">Email</label>
        <input id="email" type="email" name="email" value="{{ old('email') }}" token="{{ csrf_token() }}">

        @error('email')
            <span class="error" style="color: red;">{{ $message }}</span>
        @enderror
        
        <button type="submit">Envoyer</button>
    </form>

    @if (session('success'))
        <p style="color: green;">{{ session('success') }}</p>
    @endif
</body>
</html>
