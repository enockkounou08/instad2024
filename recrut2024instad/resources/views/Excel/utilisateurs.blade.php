<!-- <!DOCTYPE html>
<html>
<head>
    <title>Liste des Utilisateurs</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénoms</th>
                <th>Email</th>
                <th>Date de Naissance</th>
                <th>Nationalité</th>
                <th>Langue</th>
                <th>Tel 1</th>
                <th>Tel 2</th>
            </tr>
        </thead>
        <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{ $result->nom }}</td>
                    <td>{{ $result->prenom }}</td>
                    <td>{{ $result->email }}</td>
                    <td>{{ $result->date_of_birth }}</td>
                    <td>{{ $result->nationalite }}</td>
                    <td>{{ $result->langue1 }}</td>
                    <td>{{ $result->num_tel }}</td>
                    <td>{{ $result->num_whatsapp }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html> -->



<!DOCTYPE html>
<html>
<head>
    <title>Utilisateurs</title>
</head>
<body>
    <h1>Liste des Utilisateurs</h1>
    <table border="1">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénoms</th>
                <th>Email</th>
                <th>Date de Naissance</th>
                <th>Nationalité</th>
                <th>Langue</th>
                <th>Tel 1</th>
                <th>Tel 2</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $personne)
                <tr>
                    <td>{{ $personne->nom }}</td>
                    <td>{{ $personne->prenom }}</td>
                    <td>{{ $personne->email }}</td>
                    <td>{{ $personne->date_of_birth }}</td>
                    <td>{{ $personne->nationalite }}</td>
                    <td>{{ $personne->langue1 }}</td>
                    <td>{{ $personne->num_tel }}</td>
                    <td>{{ $personne->num_whatsapp }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <a href="/telecharger-utilisateurs">Télécharger Excel</a>
</body>
</html>

