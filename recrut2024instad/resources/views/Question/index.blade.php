<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- displays site properly based on user's device -->

    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="{{ asset('asset/assetsQuestion/images/favicon-32x32.png') }}"
    />

    <link rel="stylesheet" href="{{ asset('asset/assetsQuestion/style.css') }}" />

    <title>Frontend Mentor | FAQ accordion</title>

    <!-- Feel free to remove these styles or customise in your own stylesheet 👍 -->
    <style>
      .attribution {
        font-size: 11px;
        text-align: center;
      }
      .attribution a {
        color: hsl(228, 45%, 44%);
      }
      .plus{
        color:green;
       width: 30px;
      }
     form button{
      padding: 10px;
      color:white;
      background-color: green;
      border-radius:8px;
     }
     h4 span{
      margin-left: 20px;
      font-size: 13px;
     }
    </style>
  </head>
  <body>

  
  @if (session('status'))
  <div class="alert alert-success">
  {{ (session('success')) }}
  </div>
   @endif
 

@if(session()->has('user_nom') && session()->has('user_id') && session()->has('user_prenom'))
    <p>Bonjour, {{ session('user_nom') }} {{ session('user_id') }}</p>
@endif

<!-- Vérifier si la variable $success est définie -->
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }} <!-- Afficher le message de succès -->
    </div>
@endif

    <div class="container">
      <div class="heading">
        <!--img src="{{ asset('asset/assetsQuestion/images/icon-star.svg') }}" alt="" /-->
        <form action="{{ url('/pageQuestione') }}" method="post">
    @csrf
    @foreach($contenus as $index => $contenu)
    <div class="question">
        <h4>{{ $contenu->titre }} {{ $contenu->id }} <span>point: 1pt sur 20</span></h4>
        <img src="{{ asset('asset/assetsQuestion/images/icons8-plus.svg') }}" alt="" class="plus" style="color: green;" />
        <img src="{{ asset('asset/assetsQuestion/images/icon-minus.svg') }}" alt="" class="cross hidden" />
    </div>

    <p class="para hidden">
        {{ $contenu->interrogation }}
        <br><br>

        <span class="choix">
            <input type="radio" name="choix[{{ $index }}]" value="oui" checked> oui
            <input type="radio" name="choix[{{ $index }}]" value="non"> Non
        </span>
          <!-- Champ caché pour l'ID de la question -->
    <input type="hidden" name="id_question[{{ $index }}]" value="{{ $contenu->id }}">

    </p>
    <hr />
    @endforeach

    <br>
    <center>
        <button type="submit">Envoyer</button>
    </center>
</form>
   
      <div>
        <h2>Résultats</h2>
        <p>Total des points obtenus : {{ $totalPoints }}</p>
    </div>
  

    </div> 
    <div class="attribution">
      Challenge by
      <a href="https://www.frontendmentor.io?ref=challenge" target="_blank"
        >Frontend Mentor</a
      >. Coded by <a href="#">yisségnon Enock</a>.
    </div>

    <script src="{{ asset('asset/assetsQuestion/script.js') }}"></script>
  </body>
</html>
