<!DOCTYPE html>
<!-- Coding by CodingLab | www.codinglabweb.com-->
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Page de connexion à votre compte </title>

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetConnexion/css/style.css') }}">
                
        <!-- Boxicons CSS -->
        <link href="{{ asset('https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css') }}" rel='stylesheet'>
                        
    </head>
    <body>
        <section class="container forms">
            <div class="form login">
                <div class="form-content">
                    <header> Connexion </header>

                    @if(session('error'))
                        <div class="alert alert-danger" >
                                {{ session('error') }}
                        </div>
                     @endif
            
                   
                    <form action="{{ url('/pageConnexionQuestion') }}" method="POST" >
                        @csrf
                        <div class="field input-field">
                            <input name="email" type="email" placeholder="Email" class="input" value="{{old('email')}}" >
                                    <!-- Afficher les messages d'erreur sous le champ d'email -->
                            @error('email')
                                <p class="error" style=" color: red;">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="field input-field">
                            <input name="password" type="password" placeholder="Mot de passe" class="password" >
                            <i class='bx bx-hide eye-icon'></i>
                                      <!-- Afficher les messages d'erreur sous le champ d'email -->
                            @error('password')
                                <p class="error" style=" color: red;" >{{ $message }}</p>
                            @enderror
                        </div><br>

                        <!--div class="form-link">
                            <a href="#" class="forgot-pass">Forgot password?</a>
                        </div-->

                        <div class="field button-field">
                            <button type="submit"> Connexion </button>
                           
                        </div>
                    </form>

                </div>

               
            <!-- Signup Form -->

        </section>

        <!-- JavaScript -->
        <script src="{{ asset('asset/assetConnexion/js/script.js') }}"></script>
    </body>
</html>