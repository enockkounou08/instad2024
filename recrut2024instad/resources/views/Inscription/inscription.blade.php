<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'inscription</title>
    <link rel="stylesheet" href="{{ asset('asset/assetInscription/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/afterInscription.css') }}">
</head>
<body>
    <div id="page" class="site">
        <div class="container">
            <div class="form-box">
                <div class="progress">
                    <div class="logo"><a href="{{ route('Accueil_page') }}"><span>INStaD</span>.bj</a></div>
                    <ul class="progress-steps">
                        <li class="step active">
                            <span>1</span>
                            <p>Personnel<br><span>30 secs pour completer</span></p>
                        </li>
                        <li class="step">
                            <span>2</span>
                            <p>Contact<br><span>60 secs pour completer</span></p>
                        </li>
                        <li class="step">
                            <span>3</span>
                            <p>Diplôme<br><span>25 secs pour completer</span></p>
                        </li>
                       
                    </ul>
                </div>
                            
                  
                <form  action="{{ route('inscription_page_traitement') }}" method="post" enctype="multipart/form-data">
                    @csrf
                             @if($errors)
                                        @foreach ($errors->all() as $error)
                                                 <li style="color: red;"> {{ $error }}</li>
                                        @endforeach

                                @endif  

                                        

                               
                                             

                                <div class="form-one form-step active">
                                        <div class="bg-svg"></div>
                                        <h2> Informations Personnelles</h2>
                                        <p>Cliquez dans le vide chaque fois, après avoir fini de renseigner un champs</p>

                                        @error('autreNationaliteInput')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror

                                        <div>
                                            <label>Nom</label>
                                            <input name="nom" type="text" placeholder="ex: Paul"  value="{{old('nom')}}">
                                        <div>
                                        @error('nom')
                                                <li class="error" style=" color: red;">{{ $message }}</li>
                                            @enderror
                                        </div> 
                                        </div>

                                        <div>
                                            <label>Prénoms</label>
                                            <input name="prenom" type="text" placeholder="ex: John" value="{{old('prenom')}}">
                                            @error('prenom')
                                                <li class="error" style="color: red;">{{ $message }}</li>
                                            @enderror
                                        </div>
                                        

                                        <div class="radio">
                                                <label>Sexe</label>

                                                <label>
                                                    <input type="radio" name="gender" value="male" {{ old('gender') == 'male' ? 'checked' : '' }}>
                                                    Masculin
                                                </label>
                                                <label>
                                                    <input type="radio" name="gender" value="female" {{ old('gender') == 'female' ? 'checked' : '' }}>
                                                    Féminin
                                                </label>

                                                @error('gender')
                                                    <li class="error" style=" color: red;">{{ $message }}</li>
                                                @enderror
                                            <span id="passwordMatchMessage" class="error" style="color: red; display: none;">Les mots de passe ne se correspondent pas.</span>

                                            </div>


                                       
                                        <div class="birth">
                                            <label>Date de naissance</label>
                                            <div class="labelfont" >
                                                <label for="" style="margin-left: 15px;">jour</label>
                                                <label for="" style="margin-left: 52px;">mois</label>
                                                <label for="" style="margin-left: 50px;">année</label>
                                            </div>
                                                <div class="grouping">
                                                    <input type="text" name="day" id="day" value="{{ old('day') }}" placeholder="JJ" maxlength="2">
                                                    <input type="text" name="month" id="month" value="{{ old('month') }}" placeholder="MM" maxlength="2">
                                                    <input type="text" name="year" id="year" value="{{ old('year') }}" placeholder="AAAA" maxlength="4">
                                                </div>
                                                <span id="dayError" class="error" style="display:none; color: red;">Le jour doit être compris entre 1 et 31.</span>
                                                <span id="monthError" class="error" style="display:none; color: red;">Le mois doit être compris entre 1 et 12.</span>
                                                <span id="yearErro" class="error" style="display:none; color: red;">Veuillez entrer une année valide.</span>
                    
                                                <div id="error-messages" style=" color: red;">
                                                    <!-- Les messages d'erreur seront affichés ici -->
                                                </div>
                                                @error('day')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                @error('month')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                @error('year')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                </div>
                                            <div>


                                            <label>Nationalité</label>
                                            <select name="nationalite" id="nationalite">
                                                    <option value="">Votre nationalité</option>
                                                    <option value="beninoise" {{ old('nationalite') == 'beninoise' ? 'selected' : '' }}>Béninoise</option>
                                                    <option value="autre" {{ old('nationalite') == 'autre' ? 'selected' : '' }}>Autre</option>
                                            </select>
                                            @error('nationalite')
                                                <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div id="autreNationalite" style="display:none;"  >
                                            <label>Autre nationalité</label>
                                            <input type="text" name="autreNationaliteInput" id="autreNationaliteInput" value="{{ old('autreNationaliteInput') }}">
                                                @error('autreNationaliteInput')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                        </div>




                                        <div>
                                            <label>1ère Langue nationale Parlée</label>
                                            <select name="langue1" id="nationalite">
                                                <option value="">Langue que vous parlez le plus</option>
                                                <option value="Français" {{ old('langue1') == 'Français' ? 'selected' : '' }}>Français</option>
                                                <option value="Anglais" {{ old('langue1') == 'Anglais' ? 'selected' : '' }}>Anglais</option>
                                                <option value="Fon" {{ old('langue1') == 'Fon' ? 'selected' : '' }}>Fon </option>
                                                <option value="Goun" {{ old('langue1') == 'goun' ? 'selected' : '' }}>Goun</option>
                                                <option value="Minan" {{ old('langue1') == 'Minan' ? 'selected' : '' }}>Minan</option>
                                                <option value="Adja" {{ old('langue1') == 'Adja' ? 'selected' : '' }}>Adja</option>
                                                <option value="Yoruba" {{ old('langue1') == 'Yoruba' ? 'selected' : '' }}>Yoruba</option>
                                                <option value="Nago" {{ old('langue1') == 'Nago' ? 'selected' : '' }}>Nago</option>
                                                <option value="Xwla" {{ old('langue1') == 'Xwla' ? 'selected' : '' }}>Xwla</option>
                                                <option value="Watchi" {{ old('langue1') == 'Watchi' ? 'selected' : '' }}>Watchi</option>
                                                <option value="Sahouè" {{ old('langue1') == 'Sahouè' ? 'selected' : '' }}>Sahouè</option>
                                            </select>
                                            @error('langue1')
                                                <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div>
                                            <label>2ème Langue nationale Parlée</label>
                                            <select name="langue2" id="nationalite">
                                                <option value="">Langue que vous parlez moyennement</option>
                                                <option value="Français" {{ old('langue2') == 'Français' ? 'selected' : '' }}>Français</option>
                                                <option value="Anglais" {{ old('langue2') == 'Anglais' ? 'selected' : '' }}>Anglais</option>
                                                <option value="Fon" {{ old('langue2') == 'Fon' ? 'selected' : '' }}>Fon </option>
                                                <option value="Goun" {{ old('langue2') == 'Goun' ? 'selected' : '' }}>Goun</option>
                                                <option value="Minan" {{ old('langue2') == 'Minan' ? 'selected' : '' }}>Minan</option>
                                                <option value="Adja" {{ old('langue2') == 'Adja' ? 'selected' : '' }}>Adja</option>
                                                <option value="Yoruba" {{ old('langue2') == 'Yoruba' ? 'selected' : '' }}>Yoruba</option>
                                                <option value="Nago" {{ old('langue2') == 'Nago' ? 'selected' : '' }}>Nago</option>
                                                <option value="Xwla" {{ old('langue2') == 'Xwla' ? 'selected' : '' }}>Xwla</option>
                                                <option value="Watchi" {{ old('langue2') == 'Watchi' ? 'selected' : '' }}>Watchi</option>
                                                <option value="Sahouè" {{ old('langue2') == 'Sahouè' ? 'selected' : '' }}>Sahouè</option>
                                            </select>
                                            @error('langue2')
                                                <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div>
                                            <label>3ème Langue nationale Parlée</label>
                                            <select name="langue3" id="nationalite">
                                                <option value="">Une autre langue que vous parlez </option>
                                                <option value="Français" {{ old('langue3') == 'Français' ? 'selected' : '' }}>Français</option>
                                                <option value="Anglais" {{ old('langue3') == 'Anglais' ? 'selected' : '' }}>Anglais</option>
                                                <option value="Fon" {{ old('langue3') == 'Fon' ? 'selected' : '' }}>Fon </option>
                                                <option value="Goun" {{ old('langue3') == 'Goun' ? 'selected' : '' }}>Goun</option>
                                                <option value="Minan" {{ old('langue3') == 'Minan' ? 'selected' : '' }}>Minan</option>
                                                <option value="Adja" {{ old('langue3') == 'Adja' ? 'selected' : '' }}>Adja</option>
                                                <option value="Yoruba" {{ old('langue3') == 'Yoruba' ? 'selected' : '' }}>Yoruba</option>
                                                <option value="Nago" {{ old('langue3') == 'Nago' ? 'selected' : '' }}>Nago</option>
                                                <option value="Xwla" {{ old('langue3') == 'Xwla' ? 'selected' : '' }}>Xwla</option>
                                                <option value="Watchi" {{ old('langue3') == 'Watchi' ? 'selected' : '' }}>Watchi</option>
                                                <option value="Sahouè" {{ old('langue3') == 'Sahouè' ? 'selected' : '' }}>Sahouè</option>
                                            </select>
                                            @error('langue3')
                                                <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    
                                    </div>




                                    <div class="form-two form-step">                      
                                                        <div class="bg-svg"></div>
                                                        <h2>Contact</h2>
                                        <p>Cliquez dans le vide chaque fois, après avoir fini de renseigner un champs</p>

                                        @error('autreDiplomeInput')
                                        <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror

                                            @error('autreOptionInput')
                                                <span class="error" style="color: red;">{{ $message }}</span>
                                            @enderror

                                                        <div class="telW">
                                                            <label for="telWhatsapp">Numéro Whatshapp</label>
                                                            <div class="telnum">
                                                                <select name="pays_whatsapp" id="pays_whatsapp">
                                                                    <option value="">+229</option>
                                                                </select>
                                                                <input type="text" name="num_whatsapp" id="num_whatsapp" placeholder="Numéro de téléphone WhatsApp" value="{{ old('num_whatsapp') }}">
                                                            </div>
                                                            @error('num_whatsapp')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="telx">
                                                            <label for="tel">Numéro de téléphone</label>
                                                            <div class="telnum">
                                                                <select name="pays_tel" id="pays_tel">
                                                                    <option value="">+229</option>
                                                                </select>
                                                                <input type="text" name="num_tel" id="num_tel" placeholder="Numéro de téléphone" value="{{ old('num_tel') }}">
                                                            </div>
                                                            @error('num_tel')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        

                                                        <div>
                                                            <label>Email</label>
                                                            <input type="email" name="email" id="email" placeholder="Emai@gmail.com" value="{{ old('email') }}"  url-emailExist="{{ route('app_exist_email') }}" token="{{ csrf_token() }}">
                                                                    <div id="emailExistResult" style=" color: red;"></div>

                                                            @error('email')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                            <span id="emailError" style="color: red;"></span><br><br>
                                                                @if(isset($message))
                                                                    <div class="alert alert-info">
                                                                        {{ $message }}
                                                                    </div>
                                                                @endif
                                                        </div>


                                                           
<div>
    <label>Mot de passe</label>
    <div class="password-container">
        <input type="password" name="password" id="password" placeholder="Mot de passe">
        <span class="show_hide fa fa-eye-slash"></span>
    </div>
    <div class="indicator">
        <span class="icon-text">Force : </span><span class="text"></span>
    </div>
    @error('password')
        <span class="error" style="color: red;">{{ $message }}</span>
    @enderror
</div>




 <div style="position: relative;">
    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer mot de passe">
    <span id="togglePassword" style="position: absolute; right: 10px; top: 50%; transform: translateY(-50%); cursor: pointer;">
        <i class="fas fa-eye-slash"></i>
    </span>
    @error('password_confirmation')
    <span class="error" style="color: red;">{{ $message }}</span>
    @enderror
    <span id="passwordMatchMessage" class="error" style="color: red; display: none;">Les mots de passe ne correspondent pas.</span>
</div>



                                                        <div>
                                                            <label>Ville</label>
                                                            <input type="text" name="ville" placeholder="Ville" value="{{ old('ville') }}">
                                                            @error('ville')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>


                                                        <div>
                                                            <label>Numéro IFU</label>
                                                            <input type="text" name="ifu" placeholder="0123456789123" value="{{ old('ifu') }}">
                                                            @error('ifu')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                


                                                        <div>
                                                            <label>Type de pièce</label>
                                                            <select name="type_piece" id="type_piece">
                                                                <option value="">vide</option>
                                                                <option value="Carte d'Identité Nationale" {{ old('type_piece') == 'Carte d\'Identité Nationale' ? 'selected' : '' }}>Carte d'identité Nationale</option>
                                                                <option value="Certificat d'Identification Personnel" {{ old('type_piece') == 'Certificat d\'Identification Personnel' ? 'selected' : '' }}>Certificat d'Identification Personnel</option>
                                                                <!-- Assurez-vous d'ajouter les valeurs réelles pour chaque option -->
                                                            </select>
                                                            @error('type_piece')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div>
                                                            <label>Numéro de la pièce</label>
                                                            <input type="text" name="num_piece" placeholder="19851521" value="{{ old('num_piece') }}">
                                                            @error('num_piece')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        <div class="birth">
                                                        <label>Date d'expiration de la pièce</label>
                                                            <div class="grouping">
                                                                <input type="text" id="exp_day" name="exp_day" placeholder="JJ" value="{{ old('exp_day') }}" maxlength="2">
                                                                <input type="text" id="exp_month" name="exp_month" placeholder="MM" value="{{ old('exp_month') }}" maxlength="2">
                                                                <input type="text" id="exp_year" name="exp_year" placeholder="AAAA" value="{{ old('exp_year') }}" maxlength="4">
                                                            </div>
                                                            <div id="error_message" style="color: red;"></div>

                                                            @error('exp_day')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                            @error('exp_month')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                            @error('exp_year')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                            @error('exp_date')
                                                            <div id="error_message" style="color: red;">{{ $message }}</div>
                                                        @enderror


                                                        </div>
                                                      

                                    </div>
                                    <div class="form-three form-step">

                                                                
                            <div class="bg-svg"></div>
                                <h2> Diplôme</h2>
                                <p>Veuillez vérifier tous les champs avant de valider</p>
                                <div>
                                    <label for="diplome">Sélectionnez votre diplôme :</label>
                                    <select id="diplome" name="diplome">
                                        <option value="">Sélectionner votre diplôme</option>
                                        <option value="CEP" {{ old('diplome') == 'CEP' ? 'selected' : '' }}>CEP</option>
                                        <option value="BEPC" {{ old('diplome') == 'BEPC' ? 'selected' : '' }}>BEPC</option>
                                        <option value="CAP" {{ old('diplome') == 'CAP' ? 'selected' : '' }}>CAP</option>
                                        <option value="ADS" {{ old('diplome') == 'ADS' ? 'selected' : '' }}>ADS</option>
                                        <option value="ATS" {{ old('diplome') == 'ATS' ? 'selected' : '' }}>ATS</option>
                                        <option value="BAC" {{ old('diplome') == 'BAC' ? 'selected' : '' }}>BAC</option>
                                        <option value="BTS" {{ old('diplome') == 'BTS' ? 'selected' : '' }}>BTS</option>
                                        <option value="LICENCE" {{ old('diplome') == 'LICENCE' ? 'selected' : '' }}>LICENCE</option>
                                        <option value="MASTER" {{ old('diplome') == 'MASTER' ? 'selected' : '' }}>MASTER</option>
                                        <option value="autre" {{ old('diplome') == 'autre' ? 'selected' : '' }}>Autre</option>
                                    </select>
                                    @error('diplome')
                                        <span class="error" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div id="autreDiplome" style="display:none;">
                                    <label for="autreDiplomeInput">Précisez votre diplôme :</label>
                                    <input type="text" id="autreDiplomeInput" name="autreDiplomeInput" value="{{ old('autreDiplomeInput') }}">
                                    <span id="autreDiplomeError" style="display:none; color:red;">Veuillez précisez votre diplôme.</span>
                                    @error('autreDiplomeInput')
                                    <span class="error" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div id="optionDiplomeDiv" style="display:none;">
                                    <label for="optdiplome">Quel est l'option de votre diplôme ?</label>
                                    <select id="optdiplome" name="optdiplome">
                                        <!-- Les options seront ajoutées par JavaScript -->
                                    </select>
                                    @error('optdiplome')
                                    <span class="error" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div id="autreOptionDiv" style="display:none;">
                                    <label for="autreOptionInput">Précisez l'option :</label>
                                    <input type="text" id="autreOptionInput" name="autreOptionInput" value="{{ old('autreOptionInput') }}">
                                    <span id="autreOptionError" style="display:none; color:red;">Vous devez remplir ce champ.</span>
                                    @error('autreOptionInput')
                                    <span class="error" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>


                                    <div>

                                    </div>
                    
                                
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn-prev" disabled>Retour</button>
                                        <button type="button" class="btn-next">Suivant</button>
                                        <button type="submit" class="btn-submit" id="submitButton">Soumettre</button>
                                        <div id="loadingMessage" class="loading-message" style="display:none;">Loading<span class="dots">...</span></div>
                                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- <script>
    document.getElementById('submitButton').addEventListener('click', function() {
        var submitButton = this;
        var loadingMessage = document.getElementById('loadingMessage');

        // Désactiver le bouton de soumission
        submitButton.disabled = true;

        // Cacher le bouton de soumission
        submitButton.style.display = 'none';

        // Afficher le message de chargement
        loadingMessage.style.display = 'inline-block';

        Optionnel : Simuler un délai de soumission de formulaire
        setTimeout(function() {
            // Réactiver le bouton de soumission
            submitButton.disabled = false;
            // Cacher le message de chargement
            loadingMessage.style.display = 'none';
        }, 3000);
    });
</script> -->

   
    <script src="{{ asset('asset/assetJS/inscription.js') }}"></script>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{ asset('asset/assetInscription/script.js') }}"></script>
   
</body>
</html>


{{-- @endsection --}}