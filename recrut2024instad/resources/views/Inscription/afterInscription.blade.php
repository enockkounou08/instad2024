<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Après Inscription</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/afterInscription.css') }}">
</head>
<body>
    <div class="content-container">

    @if(session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif


@if(session('message'))
    <div class="alert alert-danger">
        {{ session('erreur') }}
    </div>
@endif

        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <p class="highlight">
            Vous devez avoir reçu un mail de validation pour votre inscription. Veuillez cliquer sur le lien contenu dans cet email pour activer votre compte.
        </p>

        <p>
            Ou entrer <a class="btn-custom" href="{{ asset('activation') }}"><b>ici</b></a> le code que vous avez reçu.
        </p>

        <div class="btn-group">
            <a class="btn-primary-custom" href="{{ route('connexion_page') }}"><b>Connexion</b></a>
            <a class="btn-primary-custom" href="{{ route('Accueil_page') }}"><b>Page d'accueil</b></a>
        </div>
    </div>
</body>
</html>
