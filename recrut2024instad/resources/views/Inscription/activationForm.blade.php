<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activation de compte</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/afterInscription.css') }}">
</head>
<body>

<div class="activation-container">
    <h1>Activation de compte</h1>

    @if (session('message'))
        <div class="message">{{ session('message') }}</div>
    @endif

    {{-- @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif --}}

    <form action="{{ route('activateAccount') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="activation_code">Code d'activation:</label>
            <input type="text" class="form-control" id="activation_code" name="activation_code" required>
        </div>
        <button type="submit" class="btn btn-success">Activer</button>
    </form>

    <p class="mt-3">
        Retourner à la page de <a class="btn btn-link" href="{{ route('connexion_page') }}"><b><mark>connexion!!</mark></b></a>
    </p>
</div>

</body>
</html>
