<!DOCTYPE html>
<html>
<head>
	<title>Page de connexion à votre compte</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/assetConnet/css/style.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<img class="wave" src="{{ asset('asset/assetConnet/img/wave.png') }}">
	<div class="container">
		<div class="img">
			<img src="{{ asset('asset/assetConnet/img/bg.svg') }}">
		</div>
		<div class="login-content">


            @if(session('error'))
            <div class="alert alert-danger" style="color:red">
                           {{ session('error') }}
                   </div>
                @endif
 

			<form action="{{asset('pageConnexion')}}" method="POST" >
                @csrf
				<img src="{{ asset('asset/assetHome/img/instad-removebg-preview.png') }}">

				<h2 class="title">Bienvenue</h2>

           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>

           		   <div class="div">
           		   		<h5>Email</h5>
           		   		<input type="email" class="input" name="email" value="{{old('email')}}">
                        @error('email')
                            <p class="error" style=" color: red;">{{ $message }}</p>
                        @enderror
           		   </div>

           		</div>


           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>

           		   <div class="div">
           		    	<h5>Mot de passe</h5>
           		    	<input type="password" class="input" name="password" >
                            @error('password')
                                <p class="error" style=" color: red;">{{ $message }}</p>
                            @enderror
            	   </div>
            	</div>
               
               
                <span>vous n'avez pas de compte? <a href="{{ route('inscription_page') }}" class="link signup-link">S'inscrire!</a></span>
               
                <div class="field button-field">
                    <button type="submit">Connexion</button>
                </div>

            </form>


        </div>
    </div>
    <script type="text/javascript" src="{{ asset('asset/assetConnet/js/main.js') }}"></script>
</body>
</html>
