<!DOCTYPE html>
<!-- Coding by CodingLab | www.codinglabweb.com-->
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Page de connexion à votre compte </title>

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('asset/assetConnexion/css/style.css') }}">
                
        <!-- Boxicons CSS -->
        <link href="{{ asset('https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css') }}" rel='stylesheet'>
                        
    </head>
    <body>
        <!--style>
            .image{
                border: 1px solid black ;
                width: 100px;
                height:100px;

            }
            
            .container{
                border: 1px solid black ;
                margin-top: 200px;
                margin-left: 1000px;
                width: 100px;
                height:100px;
            }


        </style-->
       <section class="image forms">

       </section>
        <section class="container forms">
            <div class="form login">
                <div class="form-content">
                    <header>Connexion</header> 

                    @if(session('error'))
                 <div class="alert alert-danger" style="color:red">
                                {{ session('error') }}
                        </div>
                     @endif
            
                   
                    <form action="{{asset('pageConnexion')}}" method="POST" >
                        @csrf
                        <div class="field input-field">
                            <input name="email" type="email" placeholder="Email" class="input" value="{{old('email')}}" >
                                    <!-- Afficher les messages d'erreur sous le champ d'email -->
                            @error('email')
                                <p class="error" style=" color: red;">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="field input-field">
                            <input name="password" type="password" placeholder="Mot de passe" class="password" >
                            <i class='bx bx-hide eye-icon'></i>
                                      <!-- Afficher les messages d'erreur sous le champ d'email -->
                            @error('password')
                                <p class="error" style=" color: red;" >{{ $message }}</p>
                            @enderror
                        </div><br>

                        <div class="form-link">
                            <a href="{{route('password.request')}}" class="forgot-pass">vous avez oubliez votre mot de passe ?</a>
                        </div>

                        <div class="field button-field">
                            <button type="submit">Connexion</button>
       
                        </div>
                    </form>

                    <div class="form-link">
                        <span>vous n'avez pas de compte? <a href="{{ route('inscription_page') }}" class="link signup-link">S'inscrire!</a></span>
                    </div>
                </div> 

               
            <!-- Signup Form -->

        </section>

        <!-- JavaScript -->
        <script src="{{ asset('asset/assetConnexion/js/script.js') }}"></script>
    </body>
</html>