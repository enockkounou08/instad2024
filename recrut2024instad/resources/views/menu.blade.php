<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="{{ asset('assetHome/img/favicon.ico') }}" rel="icon" name="monlogo"> 

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="{{asset('https://fonts.googleapis.com')}}">
    <link rel="preconnect" href="{{ asset('https://fonts.gstatic.com') }}" crossorigin>
    <link href="{{ asset('https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Inter:wght@700;800&display=swap') }}" rel="stylesheet">
    
    <!-- Icon Font Stylesheet -->
    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css') }}" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('asset/assetHome/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/assetHome/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('/asset/assetHome/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('asset/assetHome/css/style.css') }}" rel="stylesheet">

      <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css" rel="stylesheet">

    
</head>



<style>
   #logoutLink {
    display: none;
}

.user-info {
    position: relative;
    display: inline-block;
}

.user-info:hover .dropdown {
    display: block;
}

.dropdown {
    display: none;
    position: absolute;
    top: 100%;
    left: 0;
    background-color: white;
    min-width: 160px;
    box-shadow: 0px 8px 16px rgba(0,0,0,0.2);
    z-index: 1;
    border: 1px solid #ddd;
}

.dropdown-item {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-item:hover {
    background-color: #f1f1f1;
}

footer {
    background-color: #333;
    color: #fff;
    padding: 20px 0;
}

.footer-container {
    display: flex;
    justify-content: space-around;
    max-width: 1200px;
    margin: 0 auto;
}

.footer-section {
    flex: 1;
    padding: 10px;
}

.footer-section h2 {
    margin-bottom: 10px;
    color: #fff;
}

.footer-section p {
    line-height: 1.6;
}

.footer-section ul {
    list-style: none;
    padding: 0;
}

.footer-section ul li {
    margin-bottom: 8px;
}

.footer-section ul li a {
    color: #fff;
    text-decoration: none;
    transition: color 0.3s;
}

.footer-section ul li a:hover {
    color: #ff9900;
}

.socials a {
    color: #fff;
    margin-right: 10px;
    font-size: 18px;
    text-decoration: none;
    transition: color 0.3s;
}

.socials a:hover {
    color: #ff9900;
}

.footer-bottom {
    text-align: center;
    padding-top: 10px;
    border-top: 1px solid #fff;
    margin-top: 20px;
}

</style>






<body>
         @include('sweetalert::alert')
    
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg bg-white navbar-light shadow sticky-top p-0">
            <a href="{{ route('Accueil_page') }}" class="navbar-brand d-flex align-items-center text-center py-0 px-4 px-lg-5">
                <img src="{{ asset('asset/assetHome/img/instad-removebg-preview.png') }}" alt="" width="75" height="auto">
            </a>
            <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto p-4 p-lg-0">
                     <a href="{{ route('Accueil_page') }}" class="nav-item nav-link {{ Route::currentRouteName() == 'Accueil_page' ? 'active' : '' }}">Accueil</a>
    <a href="{{ route('aproposdenous') }}" class="nav-item nav-link {{ Route::currentRouteName() == 'aproposdenous' ? 'active' : '' }}">A propos</a>
    <a href="{{ route('contactez_nous') }}" class="nav-item nav-link {{ Route::currentRouteName() == 'contactez_nous' ? 'active' : '' }}">Contactez-Nous</a>


        @php
        use App\Models\contact;
        @endphp



        @php
        $user_id = Session::get('user_id');
        $user_email = Session::get('user_email');
        $userNom = Session::get('user_nom');
        $userPrenom = Session::get('user_prenom');

        $userIsValid = false;
        if ($userNom && $userPrenom) {
            $userIsValid = \App\Models\contact::where('id', $user_id)
                                           ->where('email', $user_email)
                                           ->exists();
        }
        @endphp


                @if($userIsValid)




                                    
                                   
                                       
                                     
                                        <div class="user-info">
                        <div class="nav-link active" id="username">{{ Session::get('user_nom') }} - {{ Session::get('user_prenom') }} </div>
                        <div class="dropdown">

                            <a href="{{ route('mes-candidatures') }}" class="dropdown-item">Mes candidatures</a>
                            <a href="{{ route('profileUserView') }}" class="dropdown-item">Mon Profile</a>
                       

                            <a href="#" class="dropdown-item" onclick="confirmLogout(event)">Déconnexion</a>

                            <script>
                                function confirmLogout(event) {
                                    event.preventDefault();  // Empêche la navigation immédiate

                                    // Afficher la boîte de dialogue de confirmation
                                    Swal.fire({
                                        title: 'Êtes-vous sûr?',
                                        text: "Vous allez être déconnecté!",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonText: 'Oui, déconnectez-moi!',
                                        cancelButtonText: 'Annuler',
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            // Si l'utilisateur confirme, rediriger vers la route de déconnexion
                                            window.location.href = '{{ route('logout') }}';
                                        }
                                    });
                                }
                            </script>

                        </div>
                    </div>
                                    @else 


                    <a href="{{ route('connexion_page') }}" 
                    class="btn btn-outline-success me-1 animated slideInLeft {{ Route::currentRouteName() == 'connectez-vous' ? 'active' : '' }}" 
                    style="width: 150px; height: 65px; padding: 5px 10px; margin: 2px 0; display: flex; align-items: center; justify-content: center;">
                        Se Connecter
                    </a>


{{-- 
                     <a href="{{ route('connexion_page', ['role' => 'admin']) }}" 
                        class="btn btn-outline-danger me-1 animated slideInLeft {{ Route::currentRouteName() == 'connectez-vous' ? 'active' : '' }}" 
                        style="width: 150px; height: 65px; padding: 5px 10px; margin: 2px 0; display: flex; align-items: center; justify-content: center;">
                        Se Connecter (Admin)
                     </a> --}}


                    <div class="nav-item ">
                        <a href="{{ route('inscription_page') }}" class="nav-item nav-link" >S'inscrire</a>


                       
                        <div class="rounded-0 m-0">
                            
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </nav>
     <!-- SweetAlert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Votre script -->
    <script>
        @if (session('success'))
            Swal.fire({
                icon: 'success',
                title: 'Succès',
                text: '{{ session('success') }}',
                confirmButtonText: 'OK'
            });
        @endif
        @if (session('error'))
            Swal.fire({
                icon: 'error',
                title: 'Erreur',
                text: '{{ session('error') }}',
                confirmButtonText: 'OK'
            });
        @endif
    </script>

        <div class="container-fluid">
            @yield('content')
        </div>

     @include('sweetalert::alert')


        <footer>
            <div class="footer-container">
                <!-- Section 1 : Informations générales -->
                <div class="footer-section about">
                    <h2>À propos </h2>
                    <p>L'lNStaD coordonne toutes les activités de développement, de production, d'utilisation, de diffusion et d'archivage des statistiques officielles.</p>
                    {{-- <div class="socials">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-linkedin"></i></a>
                    </div> --}}
                </div>
        
                <!-- Section 2 : Liens rapides -->
                <div class="footer-section links">
                    <h2>Liens rapides</h2>
                    <ul>
                        <li><a href="{{ route('politique') }}">Politique d'utilisation</a></li>
                        <li><a href="{{ route('aproposdenous') }}">À propos de nous</a></li>
                        <li><a href="{{ route('contactez_nous') }}">Contactez-nous</a></li>
                        <li><a href="{{ route('services') }}">Services</a></li>
                    </ul>
                </div>
        
                <!-- Section 3 : Contact -->
                <div class="footer-section contact">
                    <h2>Nous contacter</h2>
                    <p><i class="fas fa-map-marker-alt"></i> 01 B.P. 323 COTONOU</p>
                    <p><i class="fas fa-phone"></i> +229 21 30 82 44</p>
                    <p><i class="fas fa-envelope"></i> instad@instad.bj</p>
                </div>
            
        
            <!-- Section 4 : Copyright -->
            <div class="footer-bottom">
                <p>&copy; 2024 Recrutement INStaD | Tous droits réservés</p>
            </div>
        </div>
        </footer>
        
        


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="{{ asset('https://code.jquery.com/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('asset/assetHome/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('asset/assetHome/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('asset/assetHome/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('asset/assetHome/lib/owlcarousel/owl.carousel.min.js') }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('asset/assetHome/js/main.js') }}"></script>

