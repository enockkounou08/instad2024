<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/mes_candidatures.css') }}">
    <title>Mes Candidatures</title>
</head>
<body>
    <div class="container">
        <h1>Mes Candidatures</h1>
        <div class="accueil">
            <a href="{{ route('Accueil_page') }}">Accueil</a>
        </div>
        @if($candidatures->isEmpty())
            <p>Vous n'avez aucune candidature.</p>
        @else
            <table class="table-style">
                <thead>
                    <tr>
                        {{-- <th>ID Enquête</th> --}}
                        <th>Nom Enquête</th>
                        <th>Date de candidature</th>
                        <th>Heure de candidature</th>
                        <th>Mon attestation</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($candidatures as $candidature)
                        <tr>
                            {{-- <td>{{ $candidature->id_enquete }}</td> --}}
                            <td>{{ $candidature->nom_enquete }}</td>
                            <td>{{ $candidature->date_formatted }}</td>
                            <td>{{ $candidature->time_formatted }}</td>
                            <td> Revenez plus-tard! </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
</body>
</html>
