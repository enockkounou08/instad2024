<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon profiles</title>
    <link rel="stylesheet" href="{{ asset('asset/assetInscription/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/afterInscription.css') }}">

      <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css" rel="stylesheet">

</head>
<body>
    <div id="page" class="site">
        <div class="container">
            <div class="form-box">
                <div class="progress">
                   <div class="logo"><a href="{{ route('Accueil_page') }}"><span>Retour</span> page d'Accueil</a></div>
                    <ul class="progress-steps">
                        <li class="step active">
                            <span>1</span>
                            <p>Personnel<br><span>30 secs pour completer</span></p>
                        </li>
                        <li class="step">
                            <span>2</span>
                            <p>Contact<br><span>60 secs pour completer</span></p>
                        </li>
                        <li class="step">
                            <span>3</span>
                            <p>Diplôme<br><span>25 secs pour completer</span></p>
                        </li>
                    </ul>
                </div>
                            
                  
                <form  action="#" method="post" enctype="multipart/form-data">
                    @csrf
                             @if($errors)
                                        @foreach ($errors->all() as $error)
                                                 <li style="color: red;"> {{ $error }}</li>
                                        @endforeach

                                @endif  

                                <div class="form-one form-step active">
                                        <div class="bg-svg"></div>
                                        <h2> Informations Personnelles</h2>
                                        <p>Cliquez dans le vide chaque fois, après avoir fini de renseigner un champs</p>

                                        @error('autreNationaliteInput')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror

                                        <div>
                                            <label>Nom</label>
                                            <span>{{ $contact->personne->nom }}</span>
                                                <div>
                                                  @error('nom')
                                                        <li class="error" style=" color: red;">{{ $message }}</li>
                                                    @enderror
                                                </div> 
                                        </div>

                                        <div>
                                            <label>Prénoms</label>
                                            <span>{{ $contact->personne->prenom }}</span>
                                            @error('prenom')
                                                <li class="error" style="color: red;">{{ $message }}</li>
                                            @enderror
                                        </div>
   		

                                       {{--  <div class="radio">
                                                <label>Sexe</label>

                                                <label>
                                                    <input type="radio" name="gender" value="male" {{ old('gender') == 'male' ? 'checked' : '' }}>
                                                    Masculin
                                                </label>
                                                <label>
                                                    <input type="radio" name="gender" value="female" {{ old('gender') == 'female' ? 'checked' : '' }}>
                                                    Féminin
                                                </label>

                                                @error('gender')
                                                    <li class="error" style=" color: red;">{{ $message }}</li>
                                                @enderror
                                            <span id="passwordMatchMessage" class="error" style="color: red; display: none;">Les mots de passe ne se correspondent pas.</span>

                                            </div> --}}


                                       
                                        {{-- <div class="birth">
                                            <label>Date de naissance</label>
                                            <div class="labelfont" >
                                                <label for="" style="margin-left: 15px;">jour</label>
                                                <label for="" style="margin-left: 52px;">mois</label>
                                                <label for="" style="margin-left: 50px;">année</label>
                                            </div>
                                                <div class="grouping">
                                                    <input type="text" name="day" id="day" value="{{ old('day') }}" placeholder="JJ" maxlength="2">
                                                    <input type="text" name="month" id="month" value="{{ old('month') }}" placeholder="MM" maxlength="2">
                                                    <input type="text" name="year" id="year" value="{{ old('year') }}" placeholder="AAAA" maxlength="4">
                                                </div>
                                                <span id="dayError" class="error" style="display:none; color: red;">Le jour doit être compris entre 1 et 31.</span>
                                                <span id="monthError" class="error" style="display:none; color: red;">Le mois doit être compris entre 1 et 12.</span>
                                                <span id="yearErro" class="error" style="display:none; color: red;">Veuillez entrer une année valide.</span>
                    
                                                <div id="error-messages" style=" color: red;">
                                                    <!-- Les messages d'erreur seront affichés ici -->
                                                </div>
                                                @error('day')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                @error('month')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                @error('year')
                                                    <span class="error" style=" color: red;">{{ $message }}</span>
                                                @enderror
                                                </div> --}}
                                            <div>
                                                <label>Nationalité</label>
                                                <span>{{ $contact->personne->nationalite }}</span>
                                           </div>
                                            <div>
                                                <label>1ère Langue nationale Parlée</label>
                                                    <span>{{ $contact->personne->langue1 }}</span>
                                                @error('langue1')
                                                    <span class="error" style="color: red;">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div>
                                                <label>2ème Langue nationale Parlée</label>
                                                <span>{{ $contact->personne->langue2 }}</span>
                                                @error('langue2')
                                                    <span class="error" style="color: red;">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div>
                                                <label>3ème Langue nationale Parlée</label>
                                                <span>{{ $contact->personne->langue3 }}</span>
                                                @error('langue3')
                                                    <span class="error" style="color: red;">{{ $message }}</span>
                                                @enderror
                                            </div>
                                </div>




                                    <div class="form-two form-step">                      
                                                        <div class="bg-svg"></div>
                                                        <h2>Contact</h2>
                                        <p>Cliquez dans le vide chaque fois, après avoir fini de renseigner un champs</p>

                                        @error('autreDiplomeInput')
                                             <span class="error" style="color: red;">{{ $message }}</span>
                                        @enderror

                                        @error('autreOptionInput')
                                            <span class="error" style="color: red;">{{ $message }}</span>
                                        @enderror

                                                        <div class="telW">
                                                            <label for="telWhatsapp">Numéro Whatshapp</label>
                                                            <div class="telnum">
                                                                <span>{{ $contact->num_whatsapp }}</span>
                                                            </div>
                                                            @error('num_whatsapp')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="telx">
                                                            <label for="tel">Numéro de téléphone</label>
                                                            <div class="telnum">
                                                              <span>{{ $contact->num_tel }}</span>
                                                            </div>
                                                            @error('num_tel')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        

                                                       {{--  <div>
                                                            <label>Email</label>
                                                            <input type="email" name="email" id="email" placeholder="Emai@gmail.com" value="{{ $contact->email }}"  url-emailExist="{{ route('app_exist_email') }}" token="{{ csrf_token() }}">
                                                                    <div id="emailExistResult" style=" color: red;"></div>

                                                            @error('email')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                            <span id="emailError" style="color: red;"></span><br><br>
                                                                @if(isset($message))
                                                                    <div class="alert alert-info">
                                                                        {{ $message }}
                                                                    </div>
                                                                @endif
                                                        </div> --}}


                                                                            {{--                                        
                                                        <div>
                                                            <label>Mot de passe   <li style="color: red;"> Remmettez votre encienne mot de passe si vous ne voulez pas en changer.</li>)</label>
                                                            <div class="password-container">
                                                                <input type="password" name="password" id="password" placeholder="Mot de passe">
                                                                <span class="show_hide fa fa-eye-slash"></span>
                                                            </div>
                                                            <div class="indicator">
                                                                <span class="icon-text">Force : </span><span class="text"></span>
                                                            </div>
                                                            @error('password')
                                                                <span class="error" style="color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                         --}}


                                                        {{-- 
                                                         <div style="position: relative;">
                                                            <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer mot de passe">
                                                            <span id="togglePassword" style="position: absolute; right: 10px; top: 50%; transform: translateY(-50%); cursor: pointer;">
                                                                <i class="fas fa-eye-slash"></i>
                                                            </span>
                                                            @error('password_confirmation')
                                                            <span class="error" style="color: red;">{{ $message }}</span>
                                                            @enderror
                                                            <span id="passwordMatchMessage" class="error" style="color: red; display: none;">Les mots de passe ne correspondent pas.</span>
                                                        </div> --}}



                                                        <div>
                                                            <label>Ville</label>
                                                            <span>{{ $contact->ville }}</span>
                                                            @error('ville')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        <div>
                                                            <label>Numéro IFU</label>
                                                            <span>{{ $contact->ifu }}</span>
                                                            @error('ifu')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                
                                                        <div>
                                                            <label>Type de pièce</label>
                                                            <span>{{ $contact->type_piece }}</span>
                                                            @error('type_piece')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div>
                                                            <label>Numéro de la pièce</label>
                                                            <span>{{ $contact->num_piece }}</span>
                                                            @error('num_piece')
                                                                <span class="error" style=" color: red;">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                    </div>
                                    <div class="form-three form-step">          
                                            <div class="bg-svg"></div>
                                                <h2> Vos information sont il correcte?</h2>
                                                <p>Appuis sur le bouton <strong>J'ai terminer</strong></p>
                                                <div>
                                                    @error('diplome')
                                                        <span class="error" style="color: red;">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                    {{-- <a href="#" class="dropdown-item" onclick="confirmLogout(event)">OUI</a> --}}

                                                 <!-- SweetAlert2 JS -->
                                                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                                           <script>
                                                function confirmLogout(event) {
                                                    event.preventDefault(); // Empêche la navigation immédiate

                                                    // Afficher la boîte de dialogue de confirmation
                                                    Swal.fire({
                                                        title: 'Êtes-vous sûr?',
                                                        text: "De Certifier l'exactitude de vos informations",
                                                        icon: 'success',
                                                        showCancelButton: true,
                                                        confirmButtonText: 'Oui, Mes informations sont correctes',
                                                        cancelButtonText: 'Non, Je veux modifier',
                                                    }).then((result) => {
                                                        if (result.isConfirmed) {
                                                            // Si l'utilisateur confirme, rediriger vers la route d'accueil
                                                            window.location.href = '{{ route('Accueil_page') }}';
                                                        } else if (result.dismiss === Swal.DismissReason.cancel) {
                                                            // Si l'utilisateur clique sur Annuler, rediriger vers la route profileUser
                                                            window.location.href = '{{ route('profileUser') }}';
                                                        }
                                                    });
                                                }
                                            </script>


                                                    <div>
                                                    </div>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn-prev" disabled>Retour</button>
                                        <button type="button" class="btn-next">Suivant</button>
                                        <button onclick="confirmLogout(event)" class="btn-submit" id="submitButton">J'ai terminer</button>
                                        <div id="loadingMessage" class="loading-message" style="display:none;">Loading<span class="dots">...</span></div>
                                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- <script>
    document.getElementById('submitButton').addEventListener('click', function() {
        var submitButton = this;
        var loadingMessage = document.getElementById('loadingMessage');

        // Désactiver le bouton de soumission
        submitButton.disabled = true;

        // Cacher le bouton de soumission
        submitButton.style.display = 'none';

        // Afficher le message de chargement
        loadingMessage.style.display = 'inline-block';

        Optionnel : Simuler un délai de soumission de formulaire
        setTimeout(function() {
            // Réactiver le bouton de soumission
            submitButton.disabled = false;
            // Cacher le message de chargement
            loadingMessage.style.display = 'none';
        }, 3000);
    });
</script> -->

   
    <script src="{{ asset('asset/assetJS/inscription.js') }}"></script>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{ asset('asset/assetInscription/script.js') }}"></script>
     @include('sweetalert::alert')
   
</body>
</html>


{{-- @endsection --}}