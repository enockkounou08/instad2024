
@extends('menu')

@section('title', 'Nos-services')

@section('content')

<div class="container mt-5 mb-5">
    <div class="card shadow-lg">
        <div class="card-header bg-success text-white text-center">
            <h2 class="m-0">Nos Services</h2>
        </div>
        <div class="card-body p-4">
            <p class="lead">
                Découvrez les services que notre plateforme vous offre et vous propose pour vous aider dans vos différentes candidatures et demandes d'emploi.
            </p>

            <h3 class="mt-4">1. Candidater à des enquêtes </h3>
            <p>
                Notre plateforme vous permet de candidater à des enquêtes disponibles au sein de l'institut. A travers cette plateforme, vous pourriez candidater à plusieurs enquêtes, vous disposez de votre historique pour voir toutes vos candidatures effectuées, la date de candidature comme l'heure. Il vous permettra cette plateforme dans l'avenir à vous délivrer une attestation pour l'enquête effectuer.
            </p>

            
            <h3 class="mt-4">2. Sécurité et confidentialité</h3>
            <p>
                Nous offrons des services de sécurité de pointe pour protéger vos données et garantir leur confidentialité. Toutes les informations sont cryptées et sauvegardées régulièrement pour éviter toute perte de données.
            </p>


            <h3 class="mt-4">3. Plateformes pour des offres d'emploi </h3>
            <p>
                
                La platefome outre les services internes offre la possibilité aux utilisateurs de pouvoir visiter d'autres plateformes de récrutements pour des candidatures. Au nombre e celles ci, nous pouvons notifier :
                <ul>
                    <li><a href="https://www.novojob.com/benin" target="_blank">Novajob</a></li>
                    <li><a href="https://www.gouv.bj/opportunites/offres-emploi" target="_blank">Offre d'emploi benin</a></li>
                    <li><a href="https://sica.anpe.bj/login/demandeur" target="_blank">ANPE</a></li>
                    <li><a href="https://www.emploibenin.com" target="_blank">emploibenin.com</a></li>
                    <li><a href="https://wiijob.com/inscription" target="_blank">Wiijob</a></li>
                </ul>
            </p>

        </div>
        
    </div>
</div>
     @include('sweetalert::alert')

@endsection