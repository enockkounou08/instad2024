@extends('menu')

@section('title', 'Politique d\'utilisation')

@section('content')


<div class="container mt-5 mb-5">
    <div class="card shadow-lg">
        <div class="card-header bg-primary text-white text-center">
            <h2 class="m-0">Politique d'Utilisation</h2>
        </div>
        <div class="card-body p-4">
            <p class="lead">
                Bienvenue sur notre plateforme. Cette page présente les règles d'utilisation de nos services. Veuillez lire attentivement notre politique pour comprendre vos droits et obligations lors de l'utilisation de nos services.
            </p>

            <h3 class="mt-4">1. Inscription</h3>
            <p>
                Sur notre plateforme, si vous aimeriez avoir droit d'accès pour postuler à des enquêtes disponibles sur la plateforme, il vous serait indispensable dans un premier temps de vous inscrire à travers le bouton '' S'INSCRIRE '' se trouvant dans le menu tout en haut à l'extrême droit de la page d'accueil. Pour cela, vous vous inscrirez tout en renseignant les informations demandées dans les champs concernés. <br>
                NB : Veillez tenir compte de ses informations : <br>
                <ul>
                    <li>Le champ IFU doit comporter 13 chiffres exactement</li>
                    <li>L'email doit être un email fonctionnel et utilisable pour reception de validation de compte</li>
                    <li>La date d'expiration de votre carte doit être posterieure d'au moins 10 jours </li>
                    <li> Le mot de passe ... </li>
                </ul>

            </p>

            <h3 class="mt-4">2. Connexion </h3>
            <p>
                 Après l'inscription, il faut donc se connecter pour non seulement avoir accès aux contenus d'une enquête spécifique mais aussi si l'on le souhaite candidater pour l'enquête</a>.
            </p>

            <h3 class="mt-4">3. Consulter une enquête</h3>
            <p>
                Vous aviez l'opportunité de consulter une enquête disponible sur la plateforme sans postuler à cette dernière et sans même vous connecter, cela vous permettra de vous renseigner et voir de quoi il est question concernant l'enquête.
            </p>

            <h3 class="mt-4">4. Candidater pour une enquête</h3>
            <p>
                La candidature à une enquête sera possible une fois que vous vous êtes inscrit sur la dite plateforme, vous vous êtes connecté et consulté l'enquête à laquelle vous desirez candidater. Il ne restera donc qu'à cliquer sur le bouton " POSTULER " en bas de la page de détails c'est-à-dire des informations en rapport avec l'enquête et de repondre aux différentes questions posées concernant la dite enquête .
            </p>

        </div>
    
    </div>
</div>

     @include('sweetalert::alert')

@endsection
