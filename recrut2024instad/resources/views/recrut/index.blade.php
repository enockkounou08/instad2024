@extends('menu')

@section('title', 'Recrutement - INStaD')

@section('content')
    <!-- Navbar End -->
    @php
        use App\Models\contact;
        $user_id = Session::get('user_id');
        $user_email = Session::get('user_email');
        $userNom = Session::get('user_nom');
        $userPrenom = Session::get('user_prenom');

        $userIsValid = false;
        if ($userNom && $userPrenom) {
            $userIsValid = \App\Models\contact::where('id', $user_id)
                                           ->where('email', $user_email)
                                           ->exists();
        }
    @endphp



     @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}

         <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Votre script -->
    <script>
      Swal.fire({
                      title: "Good job!",
                      text: "You clicked the button!",
                      icon: "success"
                });
    </script>
    
    </div>

@endif




  <!-- Carousel Start -->
    <div class="container-fluid p-0">
        <div class="owl-carousel header-carousel position-relative">
            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset('asset/assetHome/img/batiment_instad.jpg') }}" alt="" style="width: 12000px; height: 1200px;">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(43, 57, 64, .5);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-3 text-white animated slideInDown mb-4 fs-9">Participez au recensement, façonnez l'avenir de notre communauté avec une contribution significative</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-2">
                                    Le Recensement Général de la Population et de l'Habitat (RGPH) et le Recensement Général des Entreprises (RGE) dévoilent une toile complexe mais essentielle de notre société. Ces recensements nous offrent une compréhension approfondie des dynamiques démographiques, sociales et économiques qui façonnent notre monde. Grâce aux données du RGPH, nous traçons le portrait précis de nos communautés, capturant leurs besoins, leurs évolutions et leurs diversités. D'un autre côté, le RGE nous ouvre les portes du tissu entrepreneurial, révélant les forces et les défis qui animent notre économie.
                                </p>n 
                                @section('connexion')
                                    @if($userIsValid)
                                        <!-- Contenu si l'utilisateur est valide -->
                                    @else
                                        <a href="{{ route('connexion_page') }}" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Connectez-vous</a>
                                    @endif
                                @show
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset('asset/assetHome/img/Nouveau dossier/20210727_113922.jpg') }}" alt="" style="width: 11000px; height: 1200px;">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(43, 57, 64, .5);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-3 text-white animated slideInDown mb-4 fs-9">Explorez les données vitales des entreprises et des individus. Façonnons ensemble un avenir informé et prospère.</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-2">
                                    Embarquez pour un voyage de découverte inédit, où la magie des chiffres rencontre la réalité de nos vies. À travers le Recensement Général de la Population et de l'Habitat (RGPH) et le Recensement Général des Entreprises (RGE), nous révélons des panoramas authentiques et captivants.
                                </p>
                                @yield('connexion')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset('asset/assetHome/img/Nouveau dossier/ben.jfif') }}" alt="" style="width: 1400px; height: 1200px;">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(43, 57, 64, .5);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-3 text-white animated slideInDown mb-4 fs-6.2">Participez à la construction de l'avenir en explorant les données vitales des entreprises et des individus.</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-2">
                                    Explorez un monde de possibilités à travers notre plateforme de recrutement dynamique, reliant les talents aux opportunités professionnelles.
                                </p>
                                @yield('connexion')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Carousel End -->


  <!-- Search Start -->
    <div class="container-fluid bg-primary mb-5 wow fadeIn" data-wow-delay="0.1s" style="padding: 35px;">
        <div class="container">
            <div class="row g-1">
                <div class="col-md-8">
                    <div class="row g-3">
                        <!-- Placeholders for search -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Enquêtes disponibles -->
    <div class="container-xxl py-5">
        <div class="container">
            <h1 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s"> Avis de récrutement </h1>
            <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.3s">
                <ul class="nav nav-pills d-inline-flex justify-content-center border-bottom mb-5"></ul>

                @foreach($contenus as $contenu)
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane fade show p-0 active">
                        <div class="job-item p-4 mb-4">
                            <div class="row g-4">
                                <div class="col-sm-12 col-md-8 d-flex align-items-center">
                                    <img class="flex-shrink-0 img-fluid border rounded" src="{{ asset('asset/assetHome/img/instad-removebg-preview.png') }}" alt="" style="width: 80px; height: 80px;">
                                    <div class="text-start ps-4">
                                        <h5 class="mb-3">{{ $contenu->nom }}</h5>
                                        <span class="text-truncate me-3">
                                            <i class="fa fa-map-marker-alt text-primary me-2"></i>
                                            République du Bénin
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 d-flex flex-column align-items-start align-items-md-end justify-content-center">
                                    <!-- Bouton pour chaque enquête -->
                                    <div class="d-flex mb-3">
                                        <a id="enquete-button-{{ $contenu->id }}" class="btn postuler-button" href="#" disabled>Chargement...</a>
                                    </div>
                                    <small class="text-truncate">
                                        <i class="far fa-calendar-alt text-primary me-2"></i>
                                        Date Limite : {{ \Carbon\Carbon::parse($contenu->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY HH:mm') }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

         @include('sweetalert::alert')


    <!-- JavaScript -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    



    <script>
        const getEnqueteStateUrl = `{{ route('get.enquete.state', ['id' => ':id']) }}`;
        const detailsEnqueteShowUrl = `{{ route('detailsenquete.show', ['id' => ':id']) }}`;
    </script>


    <script src="{{ asset('asset/assetJS/indexAcc.js') }}"></script>

@endsection