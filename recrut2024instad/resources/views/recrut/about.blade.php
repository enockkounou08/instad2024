@extends('menu')

@section('title', 'A propos de nous !')

@section('content')
        <!-- Header End -->
        <div class="container-xxl py-5 bg-dark page-header mb-5">
            <div class="container my-5 pt-5 pb-4">
                <h1 class="display-3 text-white mb-3 animated slideInDown">À propos de nous</h1>
            </div>
        </div>
        <!-- Header End -->


        <!-- About Start -->                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       n 
         
         <div class="container-xxl py-5">
            <div class="container">
                <div class="row g-5 align-items-center">
                    <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                        <div class="row g-0 about-bg rounded overflow-hidden">
                            <div class="col-6 text-start">
                                <img class="img-fluid w-100" src="{{ asset('asset/assetHome/img/pers_instad.jpg') }}">
                            </div>
                            <div class="col-6 text-start">
                                <img class="img-fluid" src="{{ asset('asset/assetHome/img/pers_instad1.jpg') }}" style="width: 85%; margin-top: 15%;">
                            </div>
                            <div class="col-6 text-end">
                                <img class="img-fluid" src="{{ asset('asset/assetHome/img/pers_instad2.jpg') }}" style="width: 85%;">
                            </div>
                            <div class="col-6 text-end">
                                <img class="img-fluid w-100" src="{{ asset('asset/assetHome/img/benin.jpg') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                        <h1 class="mb-4">À propos de l'INStaD au Bénin</h1>
                        <p class="mb-4">Bienvenue sur le site officiel de l'Institut National de la Statistique et de la Démographie (INStaD) au Bénin. Notre institution est le pilier central de la collecte, de l'analyse et de la diffusion de données statistiques et démographiques fiables dans le pays.</p>
                        <p><i class="fa fa-check text-primary me-3"></i>Notre Mission

                            À l'INStaD, est de soutenir le développement socio-économique du Bénin en fournissant des informations statistiques de haute qualité. Nous sommes engagés à collecter des données précises et à les rendre accessibles à tous les citoyens, aux chercheurs, aux décideurs politiques et aux organisations internationales.</p>
                        <p><i class="fa fa-check text-primary me-3"></i>Notre Engagement envers la Transparence : À l'INStaD, nous croyons en la transparence et l'ouverture. Nous partageons nos méthodologies, nos sources de données et nos rapports publics pour permettre à chacun de comprendre notre travail et d'utiliser nos données de manière responsable.</p>
                        <p><i class="fa fa-check text-primary me-3"></i>Ce que Vous Trouverez ici : Dans notre section "À Propos", vous découvrirez notre engagement envers l'intégrité des données, notre processus de collecte et d'analyse, ainsi que notre équipe d'experts dévoués. Nous mettons également en lumière nos projets en cours, nos partenariats avec d'autres institutions et notre contribution à l'avancement de la société béninoise.</p>
                        <a class="btn btn-primary py-3 px-5 mt-3" target="blank" href="https://instad.bj/">Lire plus</a>
                    </div>
                </div>
            </div>
        <!-- About End -->

     @include('sweetalert::alert')

      @endsection