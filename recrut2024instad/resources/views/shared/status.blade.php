@php
    $status ??= 0;
    $true  ??=  'Actif';
    $trueColor  ??=  'success';
    $false  ??=  'Inactif';
    $falseColor  ??=  'danger';
    $null  ??=  'Indéfini';
    $nullColor  ??=  'secondary';
@endphp

@if ($status == 1)
    <div class="badge bg-{{ $trueColor }}">{{ $true }}</div>
@elseif($status == 0)
    <div class="badge bg-{{ $falseColor }}">{{ $false }}</div>
@else
    <div class="badge bg-{{ $nullColor }}">{{ $null }}</div>
@endif
