@php
    $label ??= null;
    $name ??= null;
    $id ??= null;
    $class ??= null;
    $required ??= '';
    $onclick ??= '';
    $value ??= 0; // La valeur de ce champ
@endphp

<div @class(['form-check form-switch', $class])>
    <input
        type="radio"
        name="{{ $name }}"
        value="{{ $value }}"
        id="{{ $id }}"
        {{ $required }}
        {{ $onclick }}
        class="form-check-input @error($name) is-invalid @enderror"
        role="switch"
        @if(old($name) == $value) checked @endif>

    <label for="{{ $id }}" class="form-check-label">
        {{ $label }}
        @if($required !== '')
            <span class="text-danger">*</span>
        @endif
    </label>

    @error($name)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
