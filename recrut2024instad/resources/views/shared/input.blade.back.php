@php
    $type ??= 'text';
    $class ??= null;
    $input_class ??= null;
    $name ??= '';
    $value ??= '';
    $placeholder ??= '';
    $label ??= '';
    $id ??= $name;
    $group_id ??= '';
    $indication ??= '';
    $required ??= '';
    $maxlength ??= '';
    $disabled ??= '';
    $readonly ??= '';
    $accept ??= '';
    $data ??= [];
    $oninput ??= '';
    $onclick ??= '';
    $options ??= [];

    // Récupération de la valeur à pré-remplir
    $oldValue = old($name, $value);
    // Si le champ a une valeur spécifiée ou s'il y a des données existantes pour ce champ, utilisez cette valeur pour pré-remplir le champ
    $preFilledValue = ($oldValue != '' || count((array)$data) > 0) ? $oldValue : '';
@endphp

<div @class(["form-group", $class]) id="{{$group_id}}">
    @if($label != '')
        <label
            for="{{ $id }}"
            class="form-label">
            {{ $label }}
            @if($required !== '')
                <span class="text-danger">*</span>
            @endif
        </label>
    @endif

    @if ($type === 'textarea')
        <textarea
            {{ $readonly }}
            {{ $disabled }}
            placeholder="{{ $placeholder }}"
            rows="6"
            id="{{ $id }}"
            name="{{ $name }}"
            class="form-control {{ $input_class }} @error($name) is-invalid @enderror"
            {{ $required }} {{ $oninput }}
            {{ $maxlength }} >@if($value != ''){{ old($name, $value) }}@elseif(count((array)$data) > 0){{ old($name, $data[$name]) }}@endif</textarea>
    @elseif($type === 'submit')
        <input
            {{ $readonly }}
            {{ $disabled }}
            type="{{ $type }}"
            id="{{ $id }}"
            name="{{ $name }}"
            {{ $onclick }}
            class="btn {{ $input_class }}"
            value="{{ $value }}"
        >
    @else
        <input
            @if($accept) accept="{{ $accept }}" @endif
            @if(count($options) > 0)
                @foreach($options as $attribut => $v)
                    {{ $attribut .' = '. $v }}
                @endforeach
            @endif
            {{ $readonly }}
            {{ $disabled }}
            {{ $oninput }}
            placeholder="{{ $placeholder }}"
            type="{{ $type }}"
            id="{{ $id }}"
            name="{{ $name }}"
            class="form-control {{ $input_class }} @error($name) is-invalid @enderror"

            @if($value !== '')
                value="{{ old($name, $value) }}"
            @elseif(count((array)$data) > 0)
                value="{{ old($name, $data[$name]) }}"
            @endif
            {{ $required }}
            {{ $maxlength }}
        >
    @endif
    @if($indication != '')
        <span class="is-indication text-left" id="ind_{{$id}}">{!! $indication !!}</span>
    @endif

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
