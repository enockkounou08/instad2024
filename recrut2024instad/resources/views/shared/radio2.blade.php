@php
    $label ??= null;
    $name ??= null;
    $id ??= null;
    $class ??= null;
    $required ??= '';
    $onclick ??= '';
    $value ??= 0;
@endphp

<div @class(['form-check form-switch', $class])>
    <input
        @checked(old($name, $value ?? false))
        type="radio"
        value="{{$value}}"
        name="{{ $name }}"
        {{$required}}
        {{$onclick}}
        class="form-check-input  @error($name) is-invalid @enderror"
        role="switch"
        id="{{ $id }}"
    >
    <label for="{{ $name }}" class="form-check-label">{{ $label }} @if($required !== '')
            <span class="text-danger">*</span>
        @endif </label>
    @error($name)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
