@php
    $class ??= '';
    $name ??= '';
    $value ??= '';
    $placeholder ??= 'Choisissez une option';
    $label ??= '';
    $data ??= [];
    $multiple ??= '';
    $id ??= $name;
    $champ ??= 'name';
    $required ??= '';
    $disabled ??= '';
    $readonly ??= '';
    $input_class ??= '';
    $value_key ??= '';
    $indication ??= '';
    $selectedValues ??= [];

     // Récupération de la valeur à pré-remplir
    $oldValue = old($name, $value);
    // Si le champ a une valeur spécifiée ou s'il y a des données existantes pour ce champ, utilisez cette valeur pour pré-remplir le champ
    $preFilledValue = ($oldValue != '' || count((array)$data) > 0) ? $oldValue : '';
@endphp

<div @class(["form-group", $class])>
    @if($label != '')
        <label
            for="{{ $id }}"
            class="form-label">{{ $label }}
            @if($required !== '')
                <span class="text-danger">*</span>
            @endif
        </label>
    @endif

    @if ($multiple !== '')
        <select
            {{ $readonly }}
            {{ $disabled }}
            name="{{ $name }}[]"
            id="{{ $id }}"
            placeholder="{{ $placeholder }}"
            {{$multiple}} {{$required}}
            class="form-control {{  $input_class }}
            @error($name) is-invalid @enderror"
        >
    @else
        <select
            {{ $readonly }}
            {{ $disabled }}
            name="{{ $name }}"
            id="{{ $id }}"
            placeholder="{{ $placeholder }}"
            {{$required}}
            class="form-control {{ $input_class }}
            @error($name) is-invalid @enderror"
        >
    @endif
            <option value=""></option> <!-- Option vide ajoutée -->
        @foreach($data as $k => $v)
                @if(isset($v) && $value_key != '')
                    <option
                        @if(gettype($selectedValues) == 'string' && $v->$value_key == $selectedValues) selected @endif
                        @if(gettype($selectedValues) == 'array' && in_array($v->$value_key,$selectedValues)) selected @endif
                        @if($preFilledValue == $v->$value_key) selected @endif value="{{ $v->$value_key }}"
                    >
                        {{ $v->$champ }}
                    </option>
                @elseif (isset($v->id))
                    <option
                        @if(gettype($selectedValues) == 'string' && $v->id == $selectedValues) selected  @endif
                        @if(gettype($selectedValues) == 'array' && in_array($v->id,$selectedValues)) selected @endif
                        @if($preFilledValue == $v->id) selected @endif
                        value="{{ $v->id }}"
                    >
                        {{ $v->$champ }}
                    </option>
                @else
                    <option @if(gettype($selectedValues) == 'string' && $k == $selectedValues) selected  @endif
                    @if(gettype($selectedValues) == 'array' && in_array($k, $selectedValues)) selected @endif
                    @if($preFilledValue == $k) selected @endif
                    value="{{ $k }}">
                        {{ $v }}
                    </option>
                @endif
        @endforeach
    </select>
    @if($indication != '')
        <span class="is-indication text-left" id="ind_{{$id}}">{!! $indication !!}</span>
    @endif

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
