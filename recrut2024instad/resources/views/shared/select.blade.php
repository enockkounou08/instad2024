@php
    // Valeurs par défaut pour éviter les erreurs si certaines variables sont manquantes
    $name ??= '';
    $label ??= '';
    $required ??= '';
    $multiple ??= '';
    $input_class ??= '';
    $data ??= [];
    $champ ??= 'name';  // Le champ qui va contenir le texte affiché (par défaut 'name')
    $selectedValues ??= old($name, []);  // Valeur sélectionnée ou ancienne valeur
    $placeholder ??= 'Choisissez une option';
    $id ??= $name;
@endphp

<div class="form-group">
    @if ($label)
        <label for="{{ $id }}" class="form-label">
            {{ $label }}
            @if ($required) <span class="text-danger">*</span> @endif
        </label>
    @endif

    <select
        name="{{ $name }}{{ $multiple ? '[]' : '' }}"
        id="{{ $id }}"
        class="form-control {{ $input_class }} @error($name) is-invalid @enderror"
        {{ $multiple }}
        {{ $required }}
    >
        <option value="">{{ $placeholder }}</option>
        
        @foreach ($data as $k => $v)
            @php
                // Vérifier la valeur à comparer pour savoir si cette option est sélectionnée
                $valueToCompare = isset($v->id) ? $v->id : (is_object($v) ? $v->{$champ} : $k);
                $isSelected = in_array($valueToCompare, (array) $selectedValues);
            @endphp

            <option value="{{ $valueToCompare }}" {{ $isSelected ? 'selected' : '' }}>
                {{ isset($v->$champ) ? $v->$champ : $v }}
            </option>
        @endforeach
    </select>

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>

