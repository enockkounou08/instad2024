@php
    $type ??= 'text';
    $class ??= null;
    $input_class ??= null;
    $name ??= '';
    $value ??= '';
    $placeholder ??= '';
    $label ??= '';
    $id ??= $name;
    $group_id ??= '';
    $indication ??= '';
    $required ??= '';
    $maxlength ??= '';
    $disabled ??= '';
    $readonly ??= '';
    $accept ??= '';
    $data ??= [];
    $oninput ??= '';
    $onclick ??= '';
    $label_id ??= '';
    $options ??= [];
    $autocomplete ??= 'off';

    $oldValue = old($name, $value);
    if (isset($value) && $value !== '') {
        $preFilledValue = $value;
    } else {
        $preFilledValue = isset($data->{$name}) ? $data->{$name} : old($name, $value);
    }
@endphp

<div @class(["form-group", $class]) id="{{$group_id}}">
    @if($label != '')
        <label
            for="{{ $id }}"
            class="form-label"
            id="{{ $label_id }}">
            {!! $label !!}
            @if($required !== '')
                <span class="text-danger">*</span>
            @endif
        </label>
    @endif

    @if ($type === 'textarea')
        <textarea
            {{ $readonly }}
            {{ $disabled }}
            placeholder="{{ $placeholder }}"
            rows="6"
            id="{{ $id }}"
            name="{{ $name }}"
            class="form-control {{ $input_class }} @error($name) is-invalid @enderror"
            {{ $required }} {{ $oninput }}
            {{ $maxlength }} autocomplete="{{ $autocomplete }}">{{ $preFilledValue }}</textarea>
    @elseif($type === 'submit')
        <input
            {{ $readonly }}
            {{ $disabled }}
            type="{{ $type }}"
            id="{{ $id }}"
            name="{{ $name }}"
            {{ $onclick }}
            class="btn {{ $input_class }}"
            value="{{ $value }}"
        >
    @else
        <input
            @if($accept) accept="{{ $accept }}" @endif
            @if(count($options) > 0)
                @foreach($options as $attribut => $v)
                    {{ $attribut .' = '. $v }}
                @endforeach
            @endif
            {{ $readonly }}
            {{ $disabled }}
            {{ $oninput }}
            placeholder="{{ $placeholder }}"
            type="{{ $type }}"
            id="{{ $id }}"
            name="{{ $name }}"
            class="form-control {{ $input_class }} @error($name) is-invalid @enderror"
            value="{{ $preFilledValue }}"
            {{ $required }}
            {{ $maxlength }}
            autocomplete="{{ $autocomplete }}"
        >
    @endif
    @if($indication != '')
        <span class="is-indication text-left" id="ind_{{$id}}">{!! $indication !!}</span>
    @endif

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
