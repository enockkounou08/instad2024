@php
    $label ??= null;
    $name ??= null;
    $id ??= $name;
    $class ??= null;
    $required ??= '';
    $disabled ??= '';
    $onclick ??= '';
    $value ??= 0;
@endphp

<div @class(['form-check form-switch', $class])>
    <input type="hidden" value="0" name="{{ $name }}">
    <input @checked(old($name, $value ?? false)) type="checkbox" value="1" name="{{ $name }}" {{$required}} {{$disabled}} {{$onclick}}
    class="form-check-input  @error($name) is-invalid @enderror" role="switch" id="{{ $id }}">
    <label for="{{ $name }}" class="form-check-label">{{ $label }} @if($required !== '')
            <span class="text-danger">*</span>
        @endif </label>
    @error($name)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
