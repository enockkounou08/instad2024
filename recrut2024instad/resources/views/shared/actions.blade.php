@php
    $data ??= '';
    $name ??= '';
    $edit ??= true;
    $editName ??= __('Edit');
    $status ??= true;
    $statusName ??= 'status';
    $delete ??= true;
    $statusLabelIfActive ??= __('Disable');
    $statusLabelIfNotActive ??= __('Enable');
    $editRoute ??= route('user.'.$name.'.edit', $data);
    $dataStatus ??= $data->status;
    $dataId ??= $data->id;

    //Si option spécifique
    $specificsLinks ??= [];
@endphp

<div class="dropdown">
    <a class="dropdown-toggle text-muted" href="#" data-bs-toggle="dropdown" aria-expanded="false">
        Action
    </a>
    <ul class="dropdown-menu">

        @if(count($specificsLinks) > 0)
            @foreach($specificsLinks as $link => $sName)
                <li>
                    <a class="dropdown-item" href="{{ $link }}">{{ $sName }}</a>
                </li>
            @endforeach
        @endif

            @if($edit)
                @if(getUserRoleData('actions_modifier', Auth::id()))
                    <li><a class="dropdown-item" href="{{ $editRoute }}">{{ $editName }}</a></li>
                @endif
            @endif


        @if($status)
                @if(getUserRoleData('actions_activer', Auth::id()))
                    <li>
                        <a href="#" class="dropdown-item update-status"
                           data-id="{{ $data->id }}" data-status="{{ $data->$statusName }}" data-method="post">
                            {{ $data->$statusName == 1 ? __('Disable') : __('Enable') }}
                        </a>
                    </li>
                @endif
            @endif

        @if($delete)
            @if(getUserRoleData('actions_supprimer', Auth::id()))
            <li>
                <a href="#" class="dropdown-item update-status" data-id="{{ $dataId }}" data-status="{{ $dataStatus }}" data-method="delete">
                    {{ __('Delete') }}
                </a>
            </li>
            @endif
        @endif
    </ul>
</div>
