@php
    $data ??= '';
    $name ??= '';
    $edit ??= true;
    $status ??= true;
    $statusName ??= 'status';
    $delete ??= true;
    $class ??= 'update-status';
@endphp

<div class="dropdown">
    <a class="dropdown-toggle text-muted" href="#" data-bs-toggle="dropdown" aria-expanded="false">
        Action
    </a>
    <ul class="dropdown-menu">
        @if($edit)
            @if(getUserRoleData('actions_modifier', Auth::id()))
                <li><a class="dropdown-item" href="{{ route('admin.'.$name.'.edit', $data) }}">{{ __('Edit') }}</a></li>
            @endif
        @endif
        @if($status)
            @if(getUserRoleData('actions_activer', Auth::id()))
                <li>
                    <a href="#" class="dropdown-item {{ $class }}"
                       data-id="{{ $data->id }}" data-status="{{ $data->$statusName }}" data-method="post">
                        {{ $data->$statusName == 1 ? __('Disable') : __('Enable') }}
                    </a>
                </li>
            @endif
        @endif
        @if($delete)
            @if(getUserRoleData('actions_supprimer', Auth::id()))
                <li>
                    <a href="#" class="dropdown-item {{ $class }}"
                       data-id="{{ $data->id }}" data-status="{{ $data->status }}" data-method="delete">
                        {{ __('Delete') }}
                    </a>
                </li>
            @endif
        @endif
    </ul>
</div>
