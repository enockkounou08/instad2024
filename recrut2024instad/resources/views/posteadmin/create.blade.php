<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Créer un Poste Admin</title>
    <style>
        /* Styles basiques pour le formulaire */
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        form {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        input[type="text"], select {
            width: 100%;
            padding: 8px;
            margin: 10px 0;
            border: 1px solid #ced4da;
            border-radius: 4px;
        }
        button {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 10px 15px;
            border-radius: 4px;
            cursor: pointer;
        }
        button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>

@if(session('success'))
    <div style="color: green; margin-bottom: 20px;">
        {{ session('success') }}
    </div>
@endif

@if ($errors->any())
    <div style="color: red; margin-bottom: 20px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('posteadmin.store') }}" method="POST">
    @csrf
    <label for="nom_poste">Nom du Poste :</label>
    <input type="text" id="nom_poste" name="nom_poste" value="{{ old('nom_poste') }}">

    <label for="roleadmin_id">Rôle :</label>
    <select id="roleadmin_id" name="roleadmin_id">
        <option value="">Sélectionnez un rôle</option>
        @foreach($roleadmins as $roleadmin)
            <option value="{{ $roleadmin->id }}">{{ $roleadmin->nom_role }}</option>
        @endforeach
    </select>

    <button type="submit">Enregistrer</button>
</form>

</body>
</html>
