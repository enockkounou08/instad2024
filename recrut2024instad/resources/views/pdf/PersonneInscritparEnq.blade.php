<!DOCTYPE html>
<html>
<head>
    <title>Export PDF</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
            text-align: left;
            word-wrap: break-word; /* This will make long text wrap to the next line */
        }
        th {
            background-color: #f2f2f2;
        }
        .container {
            width: 100%;
            overflow-x: auto;
        }
    </style>
</head>
<body>
    <h1 style="font-size: 14px;">{{ $enque->nom }}</h1>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th style="width: 10%;">Nom</th>
                    <th style="width: 10%;">Prénoms</th>
                    <th style="width: 30%;">Email</th> <!-- Deux fois plus large -->
                    <th style="width: 10%;">Date de Naissance</th>
                    <th style="width: 10%;">Nationalité</th>
                    <th style="width: 15%;">Numéro WhatsApp</th>
                    <th style="width: 15%;">Numéro Téléphone</th>
                    <th style="width: 10%;">Type de Pièce</th>
                    <th style="width: 10%;">Numéro de Pièce</th>
                    <th style="width: 10%;">Numéro IFU</th>
                    <th style="width: 10%;">Langue 1</th>
                    <th style="width: 10%;">Langue 2</th>
                    <th style="width: 10%;">Langue 3</th>
                    <th style="width: 10%;">Diplôme</th>
                </tr>
            </thead>
            <tbody>
                @if($candidatures->isEmpty())
                    <tr>
                        <td colspan="13" style="text-align: center;">Pas de postulant pour le moment.</td>
                    </tr>
                @else
                    @foreach($candidatures as $candidature)
                        <tr>
                            <td>{{ $candidature->nom ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->prenom ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->email ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->date_of_birth ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->nationalite ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->num_whatsapp ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->num_tel ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->type_piece ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->num_piece ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->ifu ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->langue1 ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->langue2 ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->langue3 ?? 'Non disponible' }}</td>
                            <td>{{ $candidature->diplome ?? 'Non disponible' }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>
