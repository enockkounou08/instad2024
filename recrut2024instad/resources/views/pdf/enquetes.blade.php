<!DOCTYPE html>
<html>
<head>
    <title>Liste des Enquêtes</title>
</head>
<body>
    <h1>Liste des Enquêtes</h1>
    <table border="1" style="width:100%; border-collapse: collapse;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Date d'expiration</th>
                <th>État</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($enquetes as $enquete)
                <tr>
                    <td>{{ $enquete->id }}</td>
                    <td>{{ $enquete->nom }}</td>
                    <td>{{ $enquete->info }}</td>
                    <td>{{ $enquete->date_expiration }}</td>
                    <td>{{ $enquete->etat_enquete }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
