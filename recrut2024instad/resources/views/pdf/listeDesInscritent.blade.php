<!DOCTYPE html>
<html>
<head>
    <title>Liste des Utilisateurs</title>
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
            font-size: 10px; /* Réduire la taille de la police pour gagner de l'espace */
        }
        table {
            width: 100%;
            border-collapse: collapse;
            table-layout: auto; /* Permet aux colonnes de s'ajuster automatiquement */
        }
        table, th, td {
            border: 1px solid black;
        }
        th, td {
            padding: 5px; /* Réduire le padding pour plus de contenu */
            text-align: left;
            word-wrap: break-word; /* Permet les retours à la ligne automatiques */
        }
        th {
            background-color: #f2f2f2;
        }
        .nom-col, .prenom-col, .date-col {
            width: 15%;
        }
        
        .email-col {
            width: 20%;
        }
        
        .tel-col, .whatsapp-col, .langue-col, .nationalite-col  {
            width: 10%;
        }
    </style>
</head>
<body>
    <h1>Liste des Utilisateurs</h1>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th class="nom-col">Nom</th>
                <th class="prenom-col">Prénoms</th> 
                <th class="email-col">Email</th>
                <th class="date-col">Date de Naissance</th>
                <th class="nationalite-col">Nationalité</th>
                <th class="langue-col">Langue 1</th>
                <th class="langue-col">Langue 2</th>
                <th class="langue-col">Langue 3</th>
                <th class="typepiece-col">Type de pièce</th>
                <th class="numpiece-col">Numéro de pièce</th>
                <th class="numifu-col">Numéro IFU</th>
                <th class="tel-col">Numéro de téléphone</th>
                <th class="whatsapp-col">Numéro Whatshapp</th>
                <th class="diplome-col">Diplôme</th>
            </tr>

        </thead>
        <tbody>
            @foreach($results as $result)
                <tr>
                    <td class="nom-col">{{ $result->nom }}</td>
                    <td class="prenom-col">{{ $result->prenom }}</td>
                    <td class="email-col">{{ $result->email }}</td>
                    <td class="date-col">{{ $result->date_of_birth }}</td>
                    <td class="nationalite-col">{{ $result->nationalite }}</td>
                    <td class="langue1-col">{{ $result->langue1 }}</td>
                    <td class="langue2-col">{{ $result->langue2 }}</td>
                    <td class="langue3-col">{{ $result->langue3 }}</td>s
                    <td class="type_piece-col">{{ $result->type_piece }}</td>
                    <td class="num_piece-col">{{ $result->num_piece }}</td>
                    <td class="ifu-col">{{ $result->ifu }}</td>
                    <td class="tel-col">{{ $result->num_tel }}</td>
                    <td class="whatsapp-col">{{ $result->num_whatsapp }}</td>
                    <td class="diplome-col">{{ $result->diplome }}</td>
            @endforeach
        </tbody>
    </table>
    {{-- @foreach( $data as dataa)
    {{ $dataa->date }}
    @endforeach --}}

</html>


