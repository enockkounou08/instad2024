<!-- resources/views/pdf/invoice.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        body {
            font-family: sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>{{ $title }}</h1>
    <p>Date: {{ $date }}</p>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Info</th>
                <th>Date d'expiration</th>
                <th>Occurrences</th>
                <th>Personnes Candidatées</th>
            </tr>
        </thead>
        <tbody>
            @foreach($enquetes as $enquete)
                <tr>
                    <td>{{ $enquete->id }}</td>
                    <td>{{ $enquete->nom }}</td>
                    <td>{{ $enquete->info }}</td>
                    <td>{{ $enquete->date_expiration }}</td>
                    <td>{{ $enquete->occurrences }}</td>
                    <td>{{ $enquete->personnes_associées }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
