@extends('menu')

<link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/mes_candidatures.css') }}">

@section('title', 'Details enquêtes')

@section('content')



  @php
        use App\Models\contact;
        @endphp



        @php
        $user_id = Session::get('user_id');
        $user_email = Session::get('user_email');
        $userNom = Session::get('user_nom');
        $userPrenom = Session::get('user_prenom');

        $userIsValid = false;
        if ($userNom && $userPrenom) {
            $userIsValid = \App\Models\contact::where('id', $user_id)
                                           ->where('email', $user_email)
                                           ->exists();
        }
        @endphp


<div class="flex justify-between bg-blue-500 p-4 rounded">
  <hr>
  <h1 class="flex justify-between bg-blue-500 p-4 text-center rounded">INFORMATION SUR L'ENQUETE</h1>
  <hr>

  <div>
    <label for="Nom" style="font-weight: bold; font-size:25px; text-decoration:underline;" class="form-label">Nom de l'Enquête:</label>
    <label for="" style="font-family: inherit; font-size:28px; color:blue;" class="font-bold text-3">{{ $contenus->nom }}</label>
  </div>
  
  <input type="hidden" name="nom_enquete" value="{{ $nomEnquete }}">

  <div>
    <label for="" style="font-weight: bold; font-size:25px; text-decoration:underline;">Information sur l'enquête: </label>
    <label for="">{{ $contenus->info }}</label>
    <div>
      Date Limite : {{ \Carbon\Carbon::parse($contenus->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY HH:mm') }}
    </div>
  </div>
  
  <br>
  <div class="actions">
    @if($userIsValid)
    <!--p>Utilisateur connecté : {{ $user_nom }} {{ $user_prenom }}  - {{ Session::get('user_email') }}</p-->
      <p>Utilisateur connecté : {{ $user_nom }} {{ $user_prenom }}  </p>

      @if($dateLimiteDepassee)
        <p style="color:red; font-size:20px">Date limite dépassée</p>
      @else


      @if ($aDejaPostule)
                <p style="color: rgb(230, 230, 5); font-size:25px">Votre candidature a été déjà prise en compte, vous ne pouvez le faire une seconde fois, Merci !</p>
                 @else
   

               <!--  <a 
                href="{{ route('questions.dispo.par.enquete', ['id' => $contenus->id]) }} " style="font-size:30px">

                Postulez ici

                </a> -->


<a id="postulez-link" href="#" style="font-size:30px">Chargement...</a>


                <!-- Inclure jQuery si ce n'est pas déjà fait -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>



{{-- ce bout de script va permettre la conversion ou le passage de blade à javascript  --}}
 <script>
  const enquêteId = {{ $contenus->id }};
  const url = `{{ route('get.enquete.state', ['id' => ':id']) }}`.replace(':id', enquêteId);
  const questionsRoute = `{{ route('questions.dispo.par.enquete', ['id' => ':id']) }}`;
</script> 
    
      @endif
  
        
      @endif
    @else
      <center style="font-size:32px">
        <a href="{{ route('connexion_page') }}" class="border border-white rounded font-bold bg-blue">Connectez-vous pour postuler</a>
      </center>
    @endif
</div>

<script src="{{ asset('asset/assetJS/infoEnquette.js') }}"></script>
  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</div>
@endsection
