<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails de l'enquête</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/assetAutresCSS/questionEnquette.css') }}">
     <!-- Inclure vos fichiers CSS si nécessaire -->
    
   
</head>

     @include('sweetalert::alert')

<body>


      @php
        use App\Models\contact;
        @endphp

        @php
        $user_id = Session::get('user_id');
        $user_email = Session::get('user_email');
        $userNom = Session::get('user_nom');
        $userPrenom = Session::get('user_prenom');

        $userIsValid = false;
        if ($userNom && $userPrenom) {
            $userIsValid = \App\Models\contact::where('id', $user_id)
                                           ->where('email', $user_email)
                                           ->exists();
        }
        @endphp


    <div class="centered-content">
        <div class="card">


            <a href="{{ route('Accueil_page') }}" class="btn-return">
                <span>Retour à la page d'accueil</span>
            </a>

           
@if($userIsValid)

                                                <h3>Utilisateur connecté</h3>
                                                <p>Nom: {{ $user_nom }}</p>
                                                <p>Prénom: {{ $user_prenom }}</p>
                                                <p>email: {{ $user_email }}</p>

                                               
                                        
                                                <h3>Détails de l'enquête</h3>
                                                <p>Nom de l'enquête: {{ $nomEnquete }}</p>
                                                <p>Date d'expiration: {{ $contenus->date_expiration }}</p>
                                            </div>
                                        </div>



                                        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    const enquêteId = {{ $contenus->id }};
    const url = `{{ route('get.enquete.state', ['id' => ':id']) }}`.replace(':id', enquêteId);
</script>
 



                                        @if ($aDejaPostule)
                                                    <p class="success-message">Votre candidature a été soumise avec succès, Merci !</p>
                                                     @else
                                        <div id="maintenanceMessage" style="display:none; color: red;">
                                        Nous sommes en maintenance.
                                    </div>

 <form id="surveyForm" action="{{ route('reponseCandidature', ['idEnquete' => $contenus->id]) }}" method="POST" onsubmit="return validateForm()" enctype="multipart/form-data">
                                                    @csrf

                                                    <div>
                                                        <h3>Veuillez répondre aux questions ci-dessous</h3>
                                                        <!-- Liste des questions -->
                                                        @foreach($questions as $question)
                                                        <div>
                                                            <p>{{ $question->question }}</p>
                                                            <input type="hidden" name="idEnquete" value="{{ $question->idEnquete }}">

                                                            <!-- Affichage dynamique des champs de réponse -->
                                                            @if($question->typeReponse == 'input')
                                                                <input type="text" name="response_{{ $question->id }}" placeholder="{{ $user_email }}" value="{{ $user_email }}" required>

            @elseif($question->typeReponse == 'email')
            <div class="mail">
                <input type="email" name="response_{{ $question->id }}" placeholder="Votre email" required>
            </div>

                                                            @elseif($question->typeReponse == 'textarea')
                                                                <textarea name="response_{{ $question->id }}" placeholder="Votre réponse ici" required></textarea>

            @elseif($question->typeReponse == 'numericTel')
                <div class="numeric-field">
                    <div class="phone-field">
                    <span class="prefix">+229</span>
                    <input type="text" name="response_{{ $question->id }}" pattern="\d*" maxlength="8" placeholder="" inputmode="numeric" oninput="validateNumericInput(this)" required>
                </div>
                </div>

                                                            @elseif($question->typeReponse == 'numericAge')
                                                                <div class="numeric-field">
                                                                    <input type="text" name="response_{{ $question->id }}" pattern="\d*" maxlength="3" placeholder="Votre âge" inputmode="numeric" oninput="validateNumericInput(this)" required>
                                                                    <span class="suffix">ans</span>
                                                                </div>

                                                            @elseif($question->typeReponse == 'date')
                                                                <input type="date" name="response_{{ $question->id }}" required>

                                                            @elseif($question->typeReponse == 'ifu')
                                                                <input type="text" name="response_{{ $question->id }}" pattern="[0-9]{13}" placeholder="Entrez votre IFU (13 chiffres)" required>

                                                            @elseif($question->typeReponse == 'file')
                                                               <input type="file" name="response_{{ $question->id }}" accept=".jpg,.jpeg,.png,.pdf" id="fileInput" required>
                                                <div id="error-message" style="color: red; display: none;"></div> <!-- Zone pour afficher le message d'erreur -->

                                               
                                                            @elseif($question->typeReponse == 'listeDep')
                                                                <select name="response_{{ $question->id }}" required>
                                                                    <option value="">Sélectionnez un département</option>
                                                                    <option value="Alibori">Alibori</option>
                                                                    <option value="Atacora">Atacora</option>
                                                                    <option value="Atlantique">Atlantique</option>
                                                                    <option value="Borgou">Borgou</option>
                                                                    <option value="Collines">Collines</option>
                                                                    <option value="Couffo">Couffo</option>
                                                                    <option value="Donga">Donga</option>
                                                                    <option value="Littoral">Littoral</option>
                                                                    <option value="Mono">Mono</option>
                                                                    <option value="Ouémé">Ouémé</option>
                                                                    <option value="Plateau">Plateau</option>
                                                                    <option value="Zou">Zou</option>
                                                                </select>

                                                            @elseif($question->typeReponse == 'langue')
                                                                <select name="response_{{ $question->id }}" required>
                                                                    <option value="">Sélectionnez une langue</option>
                                                                    <option value="Français">Français</option>
                                                                    <option value="Anglais">Anglais</option>
                                                                    <option value="Minan">Minan</option>
                                                                    <option value="Sahouè">Sahouè</option>
                                                                    <option value="Fon">Fon</option>
                                                                    <option value="Yoruba">Yoruba</option>
                                                                    <option value="Adja">Adja</option>
                                                                    <option value="Bariba">Bariba</option>
                                                                    <option value="Dendi">Dendi</option>
                                                                    <option value="Goun">Goun</option>
                                                                    <option value="Mahi">Mahi</option>
                                                                    <option value="Peulh">Peulh</option>
                                                                    <option value="Bètamaribè">Bètamaribè</option>
                                                                    <option value="Ditammari">Ditammari</option>
                                                                    <option value="Yom">Yom</option>
                                                                    <option value="Anii">Anii</option>
                                                                </select>

                                                            @elseif($question->typeReponse == 'expInstad')
                                                                <select name="response_{{ $question->id }}" required>
                                                                    <option value="">Sélectionnez une expérience</option>
                                                                    <option value="Experience 1">Experience 1</option>
                                                                    <option value="Experience 2">Experience 2</option>
                                                                    <option value="Experience 3">Experience 3</option>
                                                                </select>

                                                            @elseif(in_array($question->typeReponse, ['radio', 'checkbox', 'select']))
                                                                @php
                                                                    $options = [];
                                                                    for ($i = 1; $i <= $question->nbrOption; $i++) {
                                                                        $optionField = "option{$i}";
                                                                        if (!empty($question->$optionField)) {
                                                                            $options[] = $question->$optionField;
                                                                        }
                                                                    }
                                                                @endphp
                                                                @if($question->typeReponse == 'radio')
                                                                    @foreach($options as $option)
                                                                        <div>
                                                                            <input type="radio" name="response_{{ $question->id }}" value="{{ $option }}" required>
                                                                            <label>{{ $option }}</label>
                                                                        </div>
                                                                    @endforeach

                                                                @elseif($question->typeReponse == 'checkbox')
                                                                    @foreach($options as $option)
                                                                        <div>
                                                                            <input type="checkbox" name="response_{{ $question->id }}[]" value="{{ $option }}">
                                                                            <label>{{ $option }}</label>
                                                                        </div>
                                                                    @endforeach

                                                                @elseif($question->typeReponse == 'select')
                                                                    <select name="response_{{ $question->id }}" required>
                                                                        <option value="">Choisissez une option</option>
                                                                        @foreach($options as $option)
                                                                            <option value="{{ $option }}">{{ $option }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                @endif
                                                            @endif

                                                            <!-- Afficher les messages d'erreur -->
                                                            <div class="error-message" id="error_{{ $question->id }}"></div>
                                                        </div>
                                                        @endforeach
                                                    </div>

                                                    <button type="submit">Soumettre</button>
</form>

                                        @endif

    <script src="{{ asset('asset/assetJS/questionEnquette.js') }}"></script>
     @else


     @endif 


</body>
</html>
