<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Session;
use App\Models\contact;


class VerifUserSessionActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {



        if (!Session::has('user_id') || !Session::has('user_email') || 
            !contact::where('id', Session::get('user_id'))
                               ->where('email', Session::get('user_email'))
                               ->exists()) {
             return redirect()->route('connexion_page')->with('error', 'Connectez-vous avant d\'accéder à cette page');
           // return redirect()->route('connexion_page');
        }

        // Vérifier si une session utilisateur est active
        // if (!Session::has('user_id')) {
        //     // Rediriger l'utilisateur vers la page de connexion s'il n'est pas connecté
        //     return redirect()->route('connexion_page')->with('error', 'Connectez-vous avant d\'accéder à cette page');
        // }
        
        return $next($request);
    }
}
