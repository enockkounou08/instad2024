<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
               // Vérifier si l'utilisateur est connecté et est un administrateur
               if (Auth::check() && Auth::user()->role == 1) {
                return $next($request);
            }
    
            // Rediriger vers la page d'accueil si l'utilisateur n'est pas un administrateur
            return redirect('/pageAccueil')->with('error', 'Vous n\'avez pas l\'autorisation d\'accéder à cette page.');
        
    }
}

