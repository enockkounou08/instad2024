<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Session;


class CheckUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {



          // Vérifie si la session ne contient pas 'user_id'
          if (!Session::has('user_id')) {
            // Redirige vers la page de connexion avec un message d'erreur si 'user_id' n'est pas présent
            return redirect('/pageConnexionQuestion')->with('error', 'Veuillez vous connecter pour accéder à cette page.');
        }

        // Si 'user_id' est présent, continue vers la prochaine requête

        
        return $next($request);
    }
}
