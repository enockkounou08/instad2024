<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckDisabledClass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
     public function handle(Request $request, Closure $next)
    {
        $user_id = session('user_id');
        $user = \App\Models\admin_action_model::find($user_id);

        $rolesIds = json_decode(optional($user->poste)->role_id, true);
        $roles = is_array($rolesIds)
            ? \App\Models\Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
            : collect();

        $disabledClasses = $roles->pluck('classe_desactive')->toArray();

        // Vérifiez si l'URL demandée est associée à une classe désactivée
        foreach ($disabledClasses as $class) {
            if (strpos($request->url(), $class) !== false) {
                // Redirigez ou affichez un message d'erreur
                return redirect()->route('error.page')->with('error', 'Vous n\'êtes pas autorisé à accéder à ce lien.');
            }
        }

        return $next($request);
    }
}
