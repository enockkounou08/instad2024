<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */

    public function handle(Request $request, Closure $next): Response
    {
         if (Auth::user() && Auth::user()->role == 'super_admin') {
        return $next($request);
    }

    return redirect('/'); // Rediriger vers la page d'accueil ou une autre page selon vos besoins

        //return $next($request);


        if (Auth::guard('admin')->check()) {
            return $next($request);
        }

        return redirect('/'); // Rediriger vers la page d'accueil ou une autre page selon vos besoins
    
    }

 /*   public function handle($request, Closure $next)
{
    if (Auth::user() && Auth::user()->role == 'super_admin') {
        return $next($request);
    }

    return redirect('/'); // Rediriger vers la page d'accueil ou une autre page selon vos besoins
}*/

}
