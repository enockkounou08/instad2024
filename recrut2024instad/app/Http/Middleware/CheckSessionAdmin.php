<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Session; // Importation de la classe Session
use App\Models\admin_action_model; // Assurez-vous d'importer également le modèle admin_action_model


class CheckSessionAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
    //     if (!Session::has('user_email') || !admin_action_model::where('email', Session::get('user_email'))->exists()) {
    //     return redirect()->route('admin.profile.connexion');
    // }

        if (!Session::has('user_id') || !Session::has('user_email') || 
            !admin_action_model::where('id', Session::get('user_id'))
                               ->where('email', Session::get('user_email'))
                               ->exists()) {
             return redirect()->route('admin.profile.connexion');
           // return redirect()->route('connexion_page');
        }
        return $next($request);
    }




}
