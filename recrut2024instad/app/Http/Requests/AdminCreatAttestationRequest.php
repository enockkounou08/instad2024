<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCreatAttestationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'titre' => 'required|string|max:255',
            'contenu' => 'required|string',
            'enquete_id' => 'required|exists:super_admin_enquette_models,id', // Vérifie que l'enquête existe
            // 'type_attestation' => 'required|string',
            'type_attestation_id' => 'required|exists:admin_creat_type_attestation,id', // Vérifie si le type existe dans la table
           


 
        ];
    }



    /**
     * Messages personnalisés pour les erreurs de validation.
     */
    public function messages(): array
    {
        return [
            'titre.required' => 'Le titre est obligatoire.',
            'titre.max' => 'Le titre ne peut pas dépasser 255 caractères.',
            'contenu.required' => 'Le contenu est obligatoire.',
            'type_attestation_id.required' => 'Le Type d\' attestation est obligatoire.',
            'enquete_id.required' => 'L\'ID de l\'enquête est requis.', // Message personnalisé si l'ID est absent
            'enquete_id.exists' => 'L\'enquête sélectionnée est invalide.', // Message si l'ID ne correspond à aucune enquête
  
        ];
    }

    
}
