<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class connexionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //
           // 'nom' =>'required|min:3',
            'email' =>'required|email',
            'password' =>'required|password',
          ];
    }

    public function messages()
    {
        return [
            'email.required ' => ' le champ email est requis',
            'password.required ' => ' le champ mot de passe est requis',
           // 'email.min ' => ' le champ email est requis',

        ];
    }
}
