<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\candidatureModel;
use App\Models\admin_action_model;
use Illuminate\Support\Facades\DB;
use App\Models\EnqueteQuestionModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\SuperAdminEnquetteModel;
use App\Models\Roleadmin;
use Illuminate\Support\Facades\Session;



class AdminProfileController extends Controller
{
    //



    public function showAdminProfile()
    {

        $user_id = Session::get('user_id');
        $user_nom = Session::get('user_nom');
        $user_prenom = Session::get('user_prenom');
        $user_email = Session::get('user_email');

    $actions = admin_action_model::all();

        // Vérifiez si les informations existent dans la session
        if(!$user_nom || !$user_email) {
            // Redirigez l'utilisateur si les informations sont manquantes ou affichez un message d'erreur
            return redirect()->route('connexion_page')->with('error', 'Veuillez vous connecter pour accéder à votre profil.');
        }


$user_id = Session::get('user_id');
 $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();

     $currentPageClasses = ['adminProfil'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }

        // Passer les informations de l'utilisateur à la vue
        return view('SuperAdmin.Admin.ProfileAdmin', compact('actions','user_id','user_nom', 'user_prenom', 'user_email','disabledClasses'));
    }





 public function submitForm(Request $request)
{


      // Validate the input data
    $validatedData = $request->validate([
        'nom' => 'required|string|max:255',
        'prenom' => 'required|string|max:255',
        'email' => 'required|email|max:255',
        // 'password' => ['required', 'string', 'min:8'],
         // Corrected the password validation
        // 'password' => 'required|min:6',
     'password' => 'required', // Validation du mot de passe

       
    ]);
// dd( $validatedData);
    // Save the data to the database
     $personne = new admin_action_model();
   $personne->prenom = $request->prenom;
   
   $personne->nom = $request->nom;
   $personne->email = $request->email;
   $personne->password = bcrypt($request->password);
   $personne->poste_id = null;

  

 
$personne->save();
    // Redirect back with a success message
    return redirect()->route('admin.profile')->with('success', 'Données mises à jour avec succès !');
}



public function update(Request $request, $id)
{
    // Validate the incoming request
    $request->validate([
        'nom' => 'required|string|max:255',
        'prenom' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6', // Password is required and must be at least 6 characters
      
    ]);

    // Update the record directly in the database
    DB::table('admin_action_models')
        ->where('id', $id)
        ->update([
            'nom' => $request->input('nom'),
            'prenom' => $request->input('prenom'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')), // Encrypt and update the password
           
            'updated_at' => now(),
        ]);

    // Redirect back with a success message
    return redirect()->back()->with('success', 'Action mise à jour avec succès!');
}

   

     public function destroy($id)
    {
        // Trouver l'action par son ID
        $action = admin_action_model::findOrFail($id);

        // Supprimer l'action
        $action->delete();

        // Rediriger avec un message de succès
        return redirect()->route('admin.profile')->with('success', 'Action supprimée avec succès !');
    }


   public function showLoginForm()
    {
        return view('SuperAdmin.Admin.ProfileAdminConnexion'); // Return the view for the login form
    }




public function verifySession(Request $request)
{
    // Vérifier si la session contient les informations nécessaires
    if (!Session::has('user_email')) {
        // Si la session n'est pas valide, rediriger vers la page de connexion
        return redirect()->route('connexion_page');
    }

    // Vous pouvez ajouter d'autres vérifications si nécessaire, par exemple :
    // Vérifier si l'email de l'utilisateur dans la session correspond à un enregistrement dans la base de données
    //$user = admin_action_model::where('email', Session::get('user_email'))->first();

// Vérifier l'email et l'ID de l'utilisateur dans la base de données
    $user = admin_action_model::where('email', Session::get('user_email'))
                              ->where('id', Session::get('user_id'))
                              ->first();
    if (!$user) {
        // Si l'utilisateur n'est pas trouvé, rediriger vers la page de connexion
        return redirect()->route('connexion_page');
    }



    // Si tout est correct, autoriser l'accès à la page
    return redirect()->route('accueiladmin_page');
}


public function showLoginFormLog(Request $request)
{
    // Validation des entrées du formulaire avec des messages d'erreur personnalisés
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
    ], [
        'email.required' => 'L\'adresse email est obligatoire.',
        'email.email' => 'Veuillez entrer une adresse email valide.',
        'password.required' => 'Le mot de passe est obligatoire.',
    ]);

    $user = admin_action_model::where('email', $request->email)->first();

    // Tentative de connexion
    if ($user && Hash::check($request->password, $user->password)) {
        // Le mot de passe correspond, connecter l'utilisateur
        Auth::login($user);
                
        // Récupérer l'id de l'utilisateur connecté
        $userId = Auth::id();
        $request->session()->regenerate();

        // Mettre les informations de l'utilisateur dans la session
        Session::put('user_id', $userId);
        Session::put('user_nom', $user->nom);
        Session::put('user_prenom', $user->prenom);
        Session::put('user_email', $user->email);  // Ajouter l'email de l'utilisateur dans la session

        // Rediriger l'utilisateur vers la page d'accueil après vérification de la session
        return $this->verifySession($request);
    } else {
        // Authentification échouée

        // Déboguer les credentials utilisés pour la connexion
        
        return redirect()->back()->with('error', 'Addresse email ou mot de passe incorrect');

    }
}

   
   // EnqueteController.php
public function updateState(Request $request, $id)
{
    // Trouver l'enquête par son ID ou lancer une exception si elle n'existe pas
    $enquete = SuperAdminEnquetteModel::findOrFail($id);

    // Mettre à jour le champ 'etat_enquete' en fonction de la valeur envoyée dans la requête
    $enquete->etat_enquete = $request->input('state') === 'activé' ? 'activé' : 'désactivé';

    // Sauvegarder les modifications dans la base de données
    $enquete->save();

    // Retourner une réponse JSON pour indiquer que la mise à jour a réussi
    return response()->json(['success' => true]);
}
// AdminProfileController.php
public function getState($id)
{
    // Rechercher l'enquête par son ID dans la table 'super_admin_enquette_models'
    $enquete = SuperAdminEnquetteModel::find($id);

    // Si l'enquête existe, renvoyer son état
    if ($enquete) {
        return response()->json(['etat_enquete' => $enquete->etat_enquete]);
    }

    // Si l'enquête n'est pas trouvée, renvoyer une erreur 404
    return response()->json(['error' => 'Enquête non trouvée'], 404);
}




// EnqueteController.php
public function getEnqueteState($id)
{
    // Recherchez l'enquête par son ID
    $enquete = SuperAdminEnquetteModel::find($id);

    // Si l'enquête existe, renvoyer son état sous forme de JSON
    if ($enquete) {
        return response()->json(['etat_enquete' => $enquete->etat_enquete]);
    }

    // Sinon, renvoyer une erreur
    return response()->json(['message' => 'Enquête non trouvée'], 404);
}





}
