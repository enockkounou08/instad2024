<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\Contact; // Assurez-vous d'importer le modèle Contact
use Illuminate\Support\Str;

class MotDePassOublie extends Controller
{
    //
    public function forgotPassword (){

        return view ('MotDePasse.UpdatePassWord');
    }

    public function showRequestForm()
    {
        return view('MotDePasse.UpdatePassWord');
    }

    // public function sendResetLink(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required|email|exists:contacts,email',
    //     ]);

    //     $token = Str::random(60);

    //     // Sauvegarder le token dans la base de données
    //     DB::table('password_resets')->insert([
    //         'email' => $request->email,
    //         'token' => $token,
    //         'created_at' => now(),
    //     ]);

    //     $link = url('/reset-password/' . $token);

    //     // Envoyer l'e-mail de réinitialisation de mot de passe
    //     Mail::to($request->email)->send(new \App\Mail\ResetPasswordMail($link));

    //     return back()->with('status', 'Nous avons envoyé votre lien de réinitialisation de mot de passe par e-mail !');
    // }

    // Envoyer le lien de réinitialisation de mot de passe
    public function sendResetLink(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:contacts,email',
        ]);

        $token = Str::random(60);

        // Sauvegarder le token dans la base de données
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => now(),
        ]);

        $link = url('/reset-password/' . $token);

        // Envoyer l'e-mail de réinitialisation de mot de passe
        Mail::to($request->email)->send(new \App\Mail\ResetPasswordMail($link));

        return back()->with('status', 'Nous avons envoyé votre lien de réinitialisation de mot de passe par e-mail !');
    }



    // public function showResetForm($token)
    // {
    //     return view('MotDePasse.ResetPassword', ['token' => $token]);
    // }

      // Afficher le formulaire de réinitialisation de mot de passe
      public function showResetForm($token)
      {
          return view('MotDePasse.ResetPassword', ['token' => $token]);
      }



    // public function resetPassword(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required|email|exists:contacts,email',
    //         'password' => 'required|confirmed|min:8',
    //         'password_confirmation' => 'required',
    //         'token' => 'required'
    //     ]);

    //     // Vérifiez le token
    //     $reset = DB::table('password_resets')
    //                 ->where('email', $request->email)
    //                 ->where('token', $request->token)
    //                 ->first();

    //     if (!$reset) {
    //         return back()->withErrors(['email' => 'Token de réinitialisation invalide.']);
    //     }

    //     // Mettre à jour le mot de passe
    //     $contact = Contact::where('email', $request->email)->first();
    //     $contact->password = Hash::make($request->password);
    //     $contact->save();

    //     // Supprimer le token
    //     DB::table('password_resets')->where('email', $request->email)->delete();

    //     return redirect('/login')->with('status', 'Votre mot de passe a été réinitialisé avec succès !');
    // }




    // Réinitialiser le mot de passe
    public function resetPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required',
            'token' => 'required'
        ]);

        // Vérifiez le token
        $reset = DB::table('password_resets')
                    ->where('token', $request->token)
                    ->first();

        if (!$reset) {
            return back()->withErrors(['token' => 'Token de réinitialisation invalide.']);
        }

        // Mettre à jour le mot de passe
        $contact = Contact::where('email', $reset->email)->first();
        $contact->password = Hash::make($request->password);
        $contact->save();

        // Supprimer le token
        DB::table('password_resets')->where('email', $reset->email)->delete();

        return redirect('/pageConnexion')->with('status', 'Votre mot de passe a été réinitialisé avec succès !');
    }


}
