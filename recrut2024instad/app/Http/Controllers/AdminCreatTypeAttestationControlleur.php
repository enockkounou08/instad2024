<?php

namespace App\Http\Controllers;

use App\Models\admin_creat_type_attestation;
use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Http\Request;

class AdminCreatTypeAttestationControlleur extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
          return view('SuperAdmin.Admin.AdminCreatTypeAttestation');

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     //
    // }


      // Enregistrer un nouveau type d'attestation
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required|string|max:255',
        ]);

        admin_creat_type_attestation::create([
            'nom' => $request->nom,
        ]);
        Alert::toast('Attestation effectuée','success');


        return redirect()->route('attestationType.index')->with('success', 'Type d\'attestation créé avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
