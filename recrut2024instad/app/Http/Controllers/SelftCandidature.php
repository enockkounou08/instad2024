<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SelftCandidature extends Controller
{
    //

   




public function showCandidatures()
{
    // Récupérer l'ID de l'utilisateur connecté
    $userId = Session::get('user_id');

    // Récupérer les candidatures de l'utilisateur en ordre décroissant de création
    $candidatures = DB::table('candidature_models')
                    ->where('id_personne', $userId)
                    ->orderBy('created_at', 'desc')
                    ->select('id_enquete', 'nom_enquete', 'created_at')
                    ->get();

    // Formater les dates et heures
    $candidatures->transform(function($candidature) {
        $candidature->date_formatted = Carbon::parse($candidature->created_at)->format('d F Y');
        $candidature->time_formatted = Carbon::parse($candidature->created_at)->format('H\hi');
        return $candidature;
    });

    // Passer les candidatures à la vue
    return view('recrut.mes_candidatures', ['candidatures' => $candidatures]);
}

}
