<?php

namespace App\Http\Controllers\test;

use App\Mail\TestMail;
use App\Models\contact;
use App\Models\diplome;
use App\Models\personne;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\test\testModel;
use App\Services\EmailService;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\connexionRequest;
use Illuminate\Support\Facades\Session;


use Illuminate\Support\Facades\Validator;


class testcontroller extends Controller
{
    // Déclarez une variable pour stocker la requête
    protected $request;

    // Ajoutez le paramètre $request dans votre constructeur
    function __construct(Request $request){
        // Injectez la requête dans la variable $request
        $this->request = $request;
    }

    public function testpage(){
        return view('test.test');
    }

    public function existEmails(){
        // Utilisez $this->request pour accéder à la requête
        $email = $this->request->input('email');
        $user = contact::where('email', $email) -> first();

        $response = "";
        ($user) ? $response = "exist" : $response = "not exit";

        return response()->json([
            'code' => 200,
            'response' => $response
        ]);
    }

    public function existEmail(Request $request){
        $email = $request->input('email'); // Récupérer l'e-mail de la requête
        $user = contact::where('email', $email)->first();

        $response = ($user) ? "exist" : "not exist"; // Correction de la syntaxe

        return response()->json([
            'code' => 200,
            'response' => $response
        ]);
    }



    public function tokenM(Request $request){
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string',
            'email' => 'required|email', // Règle de validation pour l'e-mail
        ]);
    
        $messages = [
            'nom.required' => 'Le champs nom est obligatoire.',
            'email.required' => 'L\'adresse e-mail est obligatoire.',
            'email.email' => 'Veuillez saisir une adresse e-mail valide.',
        ];
    
        // Application des messages personnalisés
        $validator->setCustomMessages($messages);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
    
        // Générer un code d'activation aléatoire
        $activation_code ="";
        $length_code = 5;
    
        for($i = 0; $i < $length_code; $i++) {
            $activation_code .= mt_rand(0,9);
        }
    
        // Générer un jeton d'activation sécurisé
        $activation_token = Hash::make(uniqid() . $request->email);

       
        
        // Créer une nouvelle instance de testModel et enregistrer les données
        $diplome = new testModel();
        $diplome->nom = $request->nom;
        $diplome->email = $request->email;
        $diplome->activation_code = $activation_code;
        $diplome->activation_token = $activation_token;
        $diplome->save();

        $toEmail = $request->email;
        $message = 'Salut Votre compte viens d\'être créer .veuillez l\'activer avec ce code '. $activation_code . ' merci'. ' ou cliquer sur le lien : <a href="'.$activation_token. '"> lien</a> pour activer le compte ' ;
        $subject = 'signet DSIBD';

        $response = Mail::to($toEmail)->send(new TestMail ($message, $subject));
    
        return view ('Inscription.afterInscription');
    }



    public function sendEmail()
    {
        $details = [
            'title' => 'Mail from Laravel',
            'body' => 'This is for testing email using smtp'
        ];

        Mail::to('enocksagbokounnou@gmail.com')->send(new \App\Mail\TestMail($details));

        return 'Email sent successfully!';
    }



    public function handleForm(Request $request)
    {
        // Validation des données du formulaire
        $validatedData = $request->validate([
            'nom' => 'required|string|max:255',
            'email' => 'required|email|max:255',
        ]);

        // Préparer les détails pour l'email
        $details = [
            'title' => 'Mail from Laravel',
            'body' => 'This is a test email sent from Laravel. Here are your details:',
            'nom' => $validatedData['nom'],
            'email' => $validatedData['email'],
        ];

        // Envoyer l'email
        Mail::to($validatedData['email'])->send(new TestMail($details));

        return back()->with('success', 'Email sent successfully!');
    }


    public function sendWelcomeEmail(){


        $toEmail = 'enocksagbokounnou@gmail.com';
        $message = 'Salut Enock je suis le mail de
         teste que tu es entrain de créer ';
        $subject = 'Bienvenu dans le message de mail de Laravel';

        $response = Mail::to($toEmail)->send(new TestMail ($message, $subject));
        dd( $response);
    }




    public function register(Request $request)
    {
        $activation_code = Str::random(60);
        $activation_token = Str::random(60);

        $user = testModel::create([
            'nom' => $request->input('nom'),
            'email' => $request->input('email'),
            'activation_code' => $activation_code,
            'activation_token' => $activation_token,
        ]);

        $message = "Hello " . $user->nom . ", please click the link below to activate your account.";
        $subject = "Activation Email";

        Mail::to($user->email)->send(new TestMail($message, $subject, $activation_token));

        return response()->json(['message' => 'Registration successful. Please check your email for activation link.']);
    }

    public function verify($token)
    {
        $user = contact::where('activation_token', $token)->first();

        if ($user) {
            $user->is_verified = 1;
            $user->activation_token = null;
            $user->save();

            // return response()->json(['message' => 'Votre compte a été activé avec success.']);

            //    return view('Inscription.activationForm') ->with('message', 'Votre compte a été activé avec succès.');

               return redirect('activation') ->with('message', 'Votre compte a été activé avec succès !');
        }

        // return response()->json(['message' => 'Votre lien d\'activation est invalide.'], 400);

        return view('Inscription.afterInscription') ->with('erreur', 'L\'activation a echoué');
    }
    
}
