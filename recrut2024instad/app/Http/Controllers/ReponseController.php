<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionModel;
use App\Models\ReponseModel;
use App\Models\EvaluationModel;
use App\Models\Evaluation;

class ReponseController extends Controller
{
                //

    // public function handleFormSubmission(Request $request)
    //   {
    //                 // Récupération des réponses sélectionnées depuis le formulaire
    //                 $selectedAnswers = $request->input('choix');
            
    //                 // Initialisation du total des points
    //                 $totalPoints = 0;
            
    //                 // Vérification des réponses et calcul des points
    //                 foreach($selectedAnswers as $questionId => $selectedAnswer) {
    //                     // Récupération de la réponse correcte depuis la base de données
    //                     $correctAnswer = ReponseModel::where('id_question', $questionId)->value('reponse');
            
    //                     // Vérification si la réponse sélectionnée correspond à la réponse correcte
    //                     if($selectedAnswer === $correctAnswer) {
    //                         $totalPoints += 1; // Ajout de 1 point si la réponse est correcte
    //                     }
    //                 }
            
    //                 // Retourne la vue des résultats avec le total des points
    //                 return view('resultats', ['totalPoints' => $totalPoints]);
    //     }



    //    public function handleFormSubmission(Request $request)
    // {
    //     // Récupération des réponses sélectionnées depuis le formulaire
    //     $selectedAnswers = $request->input('choix');

    //     // Initialisation du total des points
    //     $totalPoints = 0;

    //     // Vérification des réponses et calcul des points
    //     foreach($selectedAnswers as $questionId => $selectedAnswer) {
    //         // Récupération de la réponse correcte depuis la base de données
    //         $correctAnswer = Reponse::where('id_question', $questionId)->value('solution');

    //         // Vérification si la réponse sélectionnée correspond à la réponse correcte
    //         if($selectedAnswer === $correctAnswer) {
    //             $totalPoints += 1; // Ajout de 1 point si la réponse est correcte
    //         }
    //     }

    //     // Retourne la vue des résultats avec le total des points
    //     return view('resultats', ['totalPoints' => $totalPoints]);
    // }



//     public function handleFormSubmission(Request $request)
// {
//     // Récupération des réponses sélectionnées depuis le formulaire
//     $selectedAnswers = $request->input('choix');

//     // Initialisation du total des points
//     $totalPoints = 0;

//     // Vérification des réponses et calcul des points
//     foreach ($selectedAnswers as $questionId => $selectedAnswer) {
//         // Récupération de la réponse correcte depuis la base de données
//         $correctAnswer = Reponse::where('id_question', $questionId)->value('solution');

//         // Vérification si la réponse sélectionnée correspond à la réponse correcte
//         if ($selectedAnswer === $correctAnswer) {
//             $totalPoints += 1; // Ajout de 1 point si la réponse est correcte
//         }
//     }

//     // Une fois que toutes les réponses ont été vérifiées et que les points ont été calculés,
//     // vous pouvez effectuer d'autres actions si nécessaire, comme sauvegarder les réponses dans la base de données
//     // ou renvoyer d'autres informations à la vue.

//     // Par exemple, si vous souhaitez enregistrer les réponses de l'utilisateur dans la base de données,
//     // vous pouvez le faire ici

//     // Retourne la vue des résultats avec le total des points
//     return view('resultats', ['totalPoints' => $totalPoints]);
// }


// public function handleFormSubmission(Request $request)
// {
//     // Récupération des réponses sélectionnées depuis le formulaire
//     $selectedAnswers = $request->input('choix');

//     // Initialisation du total des points
//     $totalPoints = 0;

//     // Vérification des réponses et calcul des points
//     foreach ($selectedAnswers as $questionId => $selectedAnswer) {
//         // Récupération de la réponse correcte depuis la base de données
//         $correctAnswer = ReponseModel::where('id_question', $questionId)->value('solution');

//         // Vérification si la réponse sélectionnée correspond à la réponse correcte
//         if ($selectedAnswer === $correctAnswer) {
//             $totalPoints += 1; // Ajout de 1 point si la réponse est correcte
//         }
//     }

//     // Stocker le total des points dans la session flash
//     $request->session()->flash('totalPoints', $totalPoints);

//     // Redirection arrière avec un message flash
//     return redirect()->back()->with('success', 'Réponses soumises avec succès!');
// }




// public function pageQuestion(Request $request)
// {
//     // Récupérer les réponses du formulaire
//     $reponses = $request->input('choix');

//     // Variable pour stocker le total des points
//     $totalPoints = 0;

//     // Parcourir chaque réponse
//     foreach ($reponses as $index => $reponse) {
//         // Récupérer l'ID de la question correspondante
//         $idQuestion = $request->input('id_question')[$index];

//         // Vérifier si l'ID de la question est trouvé dans la table "reponses"
//         $reponseTrouvee = ReponseModel::where('id_question', $idQuestion)->first();

//         if ($reponseTrouvee) {
//             // Vérifier si la réponse sélectionnée correspond à la solution exacte
//             if ($reponseTrouvee->solution === $reponse) {
//                 // Ajouter 1 point si la réponse est correcte
//                 $totalPoints += 1;
//             }
//         }
//     }

//     // Maintenant vous avez le total des points obtenus
//     // Vous pouvez faire ce que vous voulez avec cette valeur, comme l'afficher dans une vue, la sauvegarder dans la base de données, etc.

//     return view('Question.index')->with('totalPoints', $totalPoints);
// }



// public function pageQuestion(Request $request)
// {
//     // Récupérer les réponses du formulaire
//     $reponses = $request->input('choix');

//     // Variable pour stocker le total des points
//     $totalPoints = 0;

//     // Parcourir chaque réponse
//     foreach ($reponses as $index => $reponse) {
//         // Récupérer l'ID de la question correspondante
//         $idQuestion = $request->input('id_question')[$index];

//         // Vérifier si l'ID de la question est trouvé dans la table "reponses"
//         $reponseTrouvee = ReponseModel::where('id_question', $idQuestion)->first();

//         if ($reponseTrouvee) {
//             // Vérifier si la réponse sélectionnée correspond à la solution exacte
//             if ($reponseTrouvee->solution === $reponse) {
//                 // Ajouter 1 point si la réponse est correcte
//                 $totalPoints += 1;
//             }
//         }
//     }

//     // Récupération de tous les contenus/questions
//     $contenus = QuestionModel::all();

//     // Retourner la vue avec les contenus/questions et le total des points
//     return view('Question.index', compact('contenus', 'totalPoints'));
// }


public function pageQuestion(Request $request)
{
    // Récupérer l'identifiant de l'utilisateur connecté à partir de la session
    $idUser = session('user_id');

     // Vérifier si l'utilisateur a déjà une évaluation enregistrée
     $evaluationExistante = EvaluationModel::where('id_uitisateur', $idUser)->first();

     if ($evaluationExistante) {
        // Rediriger l'utilisateur vers la page d'évaluation existante
        return redirect('/pageEvaluation');
    }else {
                            // Récupérer les réponses du formulaire
                $reponses = $request->input('choix');

                // Variable pour stocker le total des points
                $totalPoints = 0;

                // Tableau pour stocker les identifiants des questions avec des réponses incorrectes
                $questionsIncorrectes = [];

                // Parcourir chaque réponse
                foreach ($reponses as $index => $reponse) {
                    // Récupérer l'ID de la question correspondante
                    $idQuestion = $request->input('id_question')[$index];

                    // Vérifier si l'ID de la question est trouvé dans la table "reponses"
                    $reponseTrouvee = ReponseModel::where('id_question', $idQuestion)->first();

                    if ($reponseTrouvee) {
                        // Vérifier si la réponse sélectionnée correspond à la solution exacte
                        if ($reponseTrouvee->solution === $reponse) {
                            // Ajouter 1 point si la réponse est correcte
                            $totalPoints += 1;
                        } else {
                            // Si la réponse est incorrecte, ajouter l'identifiant de la question au tableau
                            $questionsIncorrectes[] = $idQuestion;
                        }
                    }
                }

                // Enregistrement des données dans la table d'évaluation
                $evaluation = new EvaluationModel();
                $evaluation->id_uitisateur = $idUser;
                $evaluation->nbr_repnse_trouver = $totalPoints;
                $evaluation->id_repnse_incorrecte = implode(',', $questionsIncorrectes); // Convertir le tableau en chaîne de caractères
                $evaluation->note = $totalPoints;
                $evaluation->save();

                // Récupération de tous les contenus/questions
                $contenus = QuestionModel::all();

                // // Retourner la vue avec les contenus/questions et le total des points
                // return view('Question.index', compact('contenus', 'totalPoints'));

                // Retourner la vue avec un message de succès
            return view('Question.index', compact('contenus', 'totalPoints'))->with('success', 'Votre évaluation a été enregistrée avec succès.');

    }
   
}



// public function question(Request $request)
// {
//     // Récupérer l'identifiant de l'utilisateur connecté à partir de la session
//     $idUser = session('user_id');

//     // Vérifier si l'utilisateur a déjà une évaluation enregistrée
//     $evaluationExistante = EvaluationModel::where('id_uitisateur', $idUser)->first();

//     // Déclaration de la variable pour stocker la note de l'utilisateur
//     $noteUtilisateur = null;

//     if ($evaluationExistante) {
//         // Récupérer la note de l'utilisateur si une évaluation existe
//         $noteUtilisateur = $evaluationExistante->note;
        
//         // Rediriger l'utilisateur vers la page d'évaluation existante
//         return redirect('/pageEvaluation')->with('note', $noteUtilisateur);
//     }

//     // Si $selectedAnswers est null, initialise $totalPoints à 0
//     $totalPoints = 0;

//     // Récupération de tous les contenus/questions
//     $contenus = QuestionModel::all();

//     // Retourne la vue avec les contenus/questions, le total des points et la note de l'utilisateur
//     return view('Question.index', compact('contenus', 'totalPoints', 'noteUtilisateur'));
// }




// public function question(Request $request)
// {
//     // Récupérer l'identifiant de l'utilisateur connecté à partir de la session
//     $idUser = session('user_id');

//     // Vérifier si l'utilisateur a déjà une évaluation enregistrée
//     $evaluationExistante = EvaluationModel::where('id_uitisateur', $idUser)->first();

//     // Déclaration de la variable pour stocker la note de l'utilisateur
//     $noteUtilisateur = null;

//     if ($evaluationExistante) {
//         // Récupérer la note de l'utilisateur si une évaluation existe
//         $noteUtilisateur = $evaluationExistante->note;
        
//         // Rediriger l'utilisateur vers la page d'évaluation existante
//         return redirect('/pageEvaluation')->with('note', $noteUtilisateur);
//     }

//     // Si $selectedAnswers est null, initialise $totalPoints à 0
//     $totalPoints = 0;

//     // Récupération de tous les contenus/questions
//     $contenus = QuestionModel::all();

//     // Retourne la vue avec les contenus/questions, le total des points et la note de l'utilisateur
//     return view('Question.index', compact('contenus', 'totalPoints', 'noteUtilisateur'));
// }

}
