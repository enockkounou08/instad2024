<?php

namespace App\Http\Controllers;

use App\Models\Contact_us;
use App\Models\VilleModel;
use Illuminate\Http\Request;

use App\Models\LangueParleeModel;
use App\Models\admin_action_model;
use App\Models\PosteadminModel;
use App\Models\Roleadmin;
use RealRashid\SweetAlert\Facades\Alert;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return view ('contact_us.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('contact_us.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        //
        return redirect('contact_us');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $contact_us = Contact_us::find($id);
        return view('contact_us.show')->with('contact_us', $contact_us);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $contact_us = Contact_us::find($id);
        return view('contact_us.edit')->with('contact_us', $contact_us);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $contact_us = Contact_us::find($id);
        $input = $request->all();
        $contact_us->update($input);
        return redirect('contact_us')->with('flash_message', 'contact_us Updated!'); 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        Contact_us::destroy($id);
        return redirect('contact_us')->with('flash_message', 'contact_us deleted!');
    }

    // Controlleur concernant l'ajout d'une nouvelle langue depuis la page admin
    // Affiche le formulaire
    public function AffichageFormLangue()
    {
        return view('SuperAdmin/questions.langueParlee');
    }


    // Gère la soumission du formulaire
    public function SoumissionFormLangue(Request $request)
    {
        // Validation des données du formulaire
        $validatedData = $request->validate([
            'nom_langue' => 'required|string|max:255',
        ]);

        // Enregistrer la langue 
        LangueParleeModel::create($validatedData);

        
        return redirect()->back()->with('success', 'La langue a été ajoutée avec succès !');
    }
       



    // Affiche le formulaire
    public function ShowFormCity()
    {
        return view('SuperAdmin/questions.ville');
    }


    // Gère la soumission du formulaire
    public function SubmitFormCity(Request $request)
    {
        // Validation des données du formulaire
            $validatedData = $request->validate([
            'ville' => 'required|string|max:255',
        ]);

       
        // Enregistrer la ville
         VilleModel::create($validatedData);
         return back()->with('success', 'La ville a été ajoutée avec succès !');
    }



    public function affecter_role_form()
    {
        $admins = admin_action_model::all();
        $postes = PosteadminModel::all();

        $user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();

    $currentPageClasses = ['affecter-un-poste'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }
        return view('SuperAdmin/Admin/affecterRoleAdmin', compact('admins', 'postes','disabledClasses'));
    }

    public function affecter_role(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'administrateur' => 'required|exists:admin_action_models,id', // Vérifie que l'administrateur existe
            'poste' => 'required', // Chaque rôle doit exister dans la table roles
        ]);


        $admin = admin_action_model::find($validatedData['administrateur']);
       
        $admin->poste_id = $validatedData['poste'];

        
        $admin->save();

        //dd($admin);
        Alert::toast('Rôle affecté avec succès !.', 'success');

        return redirect()->back();
    }

}
