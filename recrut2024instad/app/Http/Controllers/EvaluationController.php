<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EvaluationModel;

class EvaluationController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function Evalution() { 

       
    //         return view ('Evaluation.index' );
    //      }

   

public function Evalution()
{
    // Récupérer l'identifiant de l'utilisateur connecté à partir de la session
    $idUser = session('user_id');

    // Récupérer la note de l'utilisateur
    $noteUtilisateur = EvaluationModel::where('id_uitisateur', $idUser)->value('note');

    // Retourner la vue avec la note de l'utilisateur
    return view('Evaluation.index', compact('noteUtilisateur'));
}

}
