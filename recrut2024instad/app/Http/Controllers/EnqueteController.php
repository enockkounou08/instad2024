<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Models\personne;
use App\Models\diplome;
use App\Models\contact;
use App\Models\SuperAdminEnquetteModel;
use App\Models\EnqueteQuestionModel;
use RealRashid\SweetAlert\Facades\Alert;

class EnqueteController extends Controller
{
  user
    //

    public function postuler($id) {
        $user = Auth::user(); // Récupérer l'utilisateur connecté

        // Vérifiez si l'utilisateur est connecté
        if (Auth::check()) {
            $user = Auth::user(); // Récupérer l'utilisateur connecté

            // Enregistrez l'ID de l'utilisateur connecté dans le champ id_postulant de la table
            SuperAdminEnquetteModel::where('id', $id)->update(['id_postulant' => Auth::id()]);
            // Redirigez l'utilisateur vers une page de confirmation ou toute autre page appropriée
            return redirect()->back()->with('success', 'Vous avez postulé avec succès !');
        } else {
            // Redirigez l'utilisateur vers la page de connexion s'il n'est pas connecté
            return redirect()->route('login')->with('error', 'Veuillez vous connecter pour postuler.');
        }
    }



    public function getQuestion($id)
{
    $question = EnqueteQuestionModel::find($id);
    return response()->json($question);
}


// public function editQuestionee($id)
// {
//     $question = EnqueteQuestionModel::findOrFail($id);
//     return view('SuperAdmin.questions.ModifierEnqQuestion', compact('question'));
// }


// public function updateQuestion(Request $request, $id)
// {
//     $request->validate([
//         'question' => 'required|string|max:255',
//         'typeReponse' => 'required|string',
//         // Validez également les options si nécessaire
//     ]);

//     $question = EnqueteQuestionModel::findOrFail($id);
//     $question->question = $request->input('question');
//     $question->typeReponse = $request->input('typeReponse');
    
//     // Mise à jour des options en fonction du nombre d'options disponibles
//     for ($i = 1; $i <= 6; $i++) {
//         $optionField = 'option' . $i;
//         $question->$optionField = $request->input($optionField);
//     }
    
//     $question->save();

//     return redirect()->route('admin.questions.list')->with('success', 'Question mise à jour avec succès.');
// }



 public function edit($id)
    {
        // Récupérer la question par ID
        $question = EnqueteQuestionModel::findOrFail($id);
        
        // Retourner la vue d'édition avec la question
        return view('questions.edit', compact('question'));
    }


    public function update(Request $request, $id)
{
    // Validation des données
    $request->validate([
        'question' => 'required|string|max:255',
        'typeReponse' => 'nullable|string',
        // Ajoutez d'autres règles de validation si nécessaire
    ]);

    // Trouver la question et mettre à jour
    $question = EnqueteQuestionModel::findOrFail($id);
    $question->update($request->all());

    // Rediriger vers une page de succès ou afficher un message
    return redirect()->route('admin.edit.question', $id)->with('success', 'Question mise à jour avec succès !');
}





 // Afficher le formulaire d'édition d'une question
    public function editQuestion($enqueteId, $questionId)
    {
        $question = EnqueteQuestionModel::find($questionId);
        if (!$question) {
            return redirect()->back()->with('error', 'Question non trouvée.');
        }
        return view('SuperAdmin.questions.EditQuestionEnquete', compact('question', 'enqueteId'));
    }


    public function deletePage($enqueteId, $questionId)
{
    // Tu peux ici demander une confirmation avant de supprimer la question
    $question = DB::table('enquete_question')->where('id', $questionId)->first();
    
    return view('SuperAdmin.questions.DelateQuestionEnquete', compact('enqueteId', 'questionId', 'question'));
}





// public function updateQuestion(Request $request, $enqueteId, $questionId)
// {
//     $question = EnqueteQuestionModel::find($questionId);
//     if (!$question) {
//         return redirect()->back()->with('error', 'Question non trouvée.');
//     }

//     // Mise à jour des champs de la question
//     $question->question = $request->input('question');
//     $question->typeReponse = $request->input('typeReponse');
//     $question->nbrOption = $request->input('nbrOption');
//     for ($i = 1; $i <= 6; $i++) {
//         $question->{'option' . $i} = $request->input('option' . $i);
//     }

//     $question->save();

//     return redirect()->route('accueiladmin_page')->with('success', 'Question mise à jour avec succès.');
// }

// public function updateQuestion(Request $request, $enqueteId, $questionId)
// {
//     $request->validate([
//         'question' => 'required|string|max:255',
//         'typeReponse' => 'required|string',
//         'nbrOption' => 'nullable|integer|min:0',
//         // Validation conditionnelle des options
//         'option1' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//         'option2' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//         'option3' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//         'option4' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//         'option5' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//         'option6' => 'nullable|required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
//     ]);

//     $question = EnqueteQuestionModel::find($questionId);
//     if (!$question) {
//         return redirect()->back()->with('error', 'Question non trouvée.');
//     }

//     // Mise à jour des champs de la question
//     $question->question = $request->input('question');
//     $question->typeReponse = $request->input('typeReponse');
//     $question->nbrOption = $request->input('nbrOption');
//     for ($i = 1; $i <= 6; $i++) {
//         $question->{'option' . $i} = $request->input('option' . $i);
//     }

//     $question->save();

//     return redirect()->route('accueiladmin_page')->with('success', 'Question mise à jour avec succès.');
// }



public function updateQuestion(Request $request, $enqueteId, $questionId)
{
    // Validation générale
    $validationRules = [
        'question' => 'required|string|max:255',
        'typeReponse' => 'required|string',
        'nbrOption' => 'nullable|integer|min:0',
    ];

    // Ajouter des règles de validation conditionnelles pour les options
    if ($request->input('nbrOption') > 0) {
        $validationRules += [
            'option1' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
            'option2' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
            'option3' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
            'option4' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
            'option5' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
            'option6' => 'required_if:typeReponse,radio|required_if:typeReponse,checkbox|required_if:typeReponse,select|string',
        ];
    }

    // Appliquer la validation
    $request->validate($validationRules);

    $question = EnqueteQuestionModel::find($questionId);
    if (!$question) {
        return redirect()->back()->with('error', 'Question non trouvée.');
    }

    // Mise à jour des champs de la question
    $question->question = $request->input('question');
    $question->typeReponse = $request->input('typeReponse');
    $question->nbrOption = $request->input('nbrOption');
    for ($i = 1; $i <= 6; $i++) {
        $question->{'option' . $i} = $request->input('option' . $i);
    }

    $question->save();

    return redirect()->route('accueiladmin_page')->with('success', 'Question mise à jour avec succès.');
}


public function deleteQuestion($enqueteId, $questionId)
{
    $question = EnqueteQuestionModel::find($questionId);
    if ($question) {
        $question->delete();
        return redirect()->route('accueiladmin_page')->with('success', 'Question supprimée avec succès.');
    }

    return redirect()->back()->with('error', 'Question non trouvée.');
}





}
