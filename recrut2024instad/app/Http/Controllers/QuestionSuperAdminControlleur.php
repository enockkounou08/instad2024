<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EnqueteQuestionModel;
use Illuminate\Support\Facades\DB;

class QuestionSuperAdminControlleur extends Controller
{
  


//     public function saveQuestions(Request $request)
// {
//     $questions = $request->input('questions');
//     $enqueteId = $request->input('enqueteId');

//     foreach ($questions as $questionData) {
//         // Vérifiez si les clés attendues existent
//         if (!isset($questionData['question']) || !isset($questionData['typeReponse'])) {
//             continue; // Ignorer cette question si les données essentielles sont manquantes
//         }

//         $question = new EnqueteQuestionModel();
//         $question->question = $questionData['question'];
//         $question->typeReponse = $questionData['typeReponse'];
//         $question->nbrOption = $questionData['nbrOption'] ?? null; // Utiliser null si 'nbrOption' est manquant
//         $question->idEnquete = $enqueteId;
//         $question->nomEnquete = "Nom de l'enquete"; // Assurez-vous de remplir ce champ si nécessaire

//         // Stocker les options
//         $options = $questionData['options'] ?? [];
//         $question->option1 = $options[0] ?? null;
//         $question->option2 = $options[1] ?? null;
//         $question->option3 = $options[2] ?? null;
//         $question->option4 = $options[3] ?? null;
//         $question->option5 = $options[4] ?? null;
//         $question->option6 = $options[5] ?? null;

//         $question->save();
//     }

//     return response()->json(['message' => 'Questions enregistrées avec succès!'], 200);
// }



//  public function handleFormSubmission(Request $request)
//     {
//         $questions = $request->input('questions');
//         return view('q', ['questions' => $questions]);
//     }

// // QuestionController.php
// public function saveQuestions(Request $request)
// {

    
//     // Validation des données reçues
//     $validated = $request->validate([
//         'questions.*.question' => 'required|string',
//         'questions.*.typeReponse' => 'required|string',
//         'questions.*.nbrOption' => 'nullable|integer',
//         'questions.*.options' => 'array',
//         'questions.*.options.*' => 'string|nullable',
//     ]);

//     // Traitement des questions
//     foreach ($validated['questions'] as $questionData) {
//         $question = new EnqueteQuestion();
//         $question->question = $questionData['question'];
//         $question->typeReponse = $questionData['typeReponse'];
//         $question->nbrOption = $questionData['nbrOption'];
        
//         $question->option1 = $questionData['options'][0] ?? null;
//         $question->option2 = $questionData['options'][1] ?? null;
//         $question->option3 = $questionData['options'][2] ?? null;
//         $question->option4 = $questionData['options'][3] ?? null;
//         $question->option5 = $questionData['options'][4] ?? null;
//         $question->option6 = $questionData['options'][5] ?? null;
//         $question->idEnquete = $request->idEnquete ?? null;
//         $question->nomEnquete = $request->nomEnquete ?? null;

//         $question->save();
//     }

//     return response()->json(['message' => 'Questions enregistrées avec succès !'], 200);
// }




// // YourController.php
// public function getQuestionsByEnqueteId(Request $request)
// {
//     $idEnquete = $request->input('idEnquete');
//     $questions = DB::table('enquete_question')
//         ->where('idEnquete', $idEnquete)
//         ->get();

//     return response()->json($questions);
// }



public function question(Request $request)
{
    $idEnquete = $request->input('idEnquete');

    $questions = DB::table('enquete_question')
        ->where('idEnquete', $idEnquete)
        ->get();

    return response()->json($questions);
}


public function showEnqueteQuestions($id)
{
    $questions = DB::table('enquete_question')->where('idEnquete', $id)->get();
   return response()->json($questions);
}


}
