<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Models\AdminCreatAttestationModel;
use App\Models\admin_creat_type_attestation;
use App\Models\SuperAdminEnquetteModel;
use Illuminate\Http\Request;
use App\Http\Requests\AdminCreatAttestationRequest;
use Illuminate\Support\Facades\Log;
use App\Exports\EnqueteExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;


class AdminCreatAttestationControlleur extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
        {
        //
        // return view('SuperAdmin.Admin.AdminCreatAttestation');

        }

    // Méthode privée pour effectuer la recherche
    private function performSearch($searchTerm, $model)
        {
            // Vérifier si le modèle est valide
            if (!class_exists($model)) {
                throw new \InvalidArgumentException("Le modèle spécifié n'existe pas : {$model}");
            }

            // Instancier le modèle
            $modelInstance = new $model();

            // Vérifier que le modèle a des champs "fillable"
            if (!method_exists($modelInstance, 'getFillable')) {
                throw new \InvalidArgumentException("Le modèle spécifié n'a pas de champs remplissables : {$model}");
            }

            // Récupérer les champs remplissables
            $searchableFields = $modelInstance->getFillable();

            // Effectuer la recherche sur le modèle donné
            return $model::query()
                ->where(function ($query) use ($searchTerm, $searchableFields) {
                    foreach ($searchableFields as $field) {
                        $query->orWhere($field, 'like', '%' . $searchTerm . '%');
                    }
                })
                ->get();
        }


    


    public function create(Request $request)
        {
            $enquetes = [];

            // Récupérer les paramètres de recherche et le modèle
            $searchTerm = $request->input('search');
            $modelName =  SuperAdminEnquetteModel::class; // Par défaut : SuperAdminEnquetteModel

            // Si une recherche est effectuée
            if ($searchTerm) {
                try {
                    $enquetes = $this->performSearch($searchTerm, $modelName);
                } catch (\Exception $e) {
                    // Gérer les erreurs (par exemple : afficher un message dans la vue)
                    \Illuminate\Support\Facades\Log::error($e->getMessage());
                }
            }

            // Retourner la vue avec les résultats
            return view('SuperAdmin.Admin.AdminCreatAttestation', compact('enquetes'));
        }


    

    public function store(AdminCreatAttestationRequest $request)
        {
            // Validation des données de la requête
            $validatedData = $request->validated();

            // Vérification si l'attestation existe déjà avec le même enquete_id et type_attestation_id
            $existingAttestation = AdminCreatAttestationModel::where('enquete_id', $validatedData['enquete_id'])
                                                             ->where('type_attestation_id', $validatedData['type_attestation_id'])
                                                             ->first();

            // Si une attestation existe déjà, retourner une erreur
            if ($existingAttestation) {
                Alert::toast( 'Une enquête ne peut pas avoir deux fois le même type d\'attestation.', 'error',);
                return redirect()->back()->with('error', 'Une enquête ne peut pas avoir deux fois le même type d\'attestation.');
            }

            // Créer l'attestation avec l'ID de l'enquête
            $attestation = new AdminCreatAttestationModel();

            $attestation->titre = $validatedData['titre'];
            $attestation->contenu = $validatedData['contenu'];

            // Vérifier si un ID d'enquête est fourni dans la requête
            if ($request->has('enquete_id')) {
                $attestation->enquete_id = $validatedData['enquete_id'];
            } else {
                // Message de succès
                Alert::error('Erreur', 'Veuillez choisir une enquête existante.');
                // Redirection avec message de succès
                return redirect()->back()->with('error', 'Veuillez choisir une enquête existante.');
            }

            // Ajouter le type d'attestation
            $attestation->type_attestation_id = $validatedData['type_attestation_id'];

            // Sauvegarder l'attestation dans la base de données
            $attestation->save();

            // Message de succès
            Alert::toast( 'Attestation effectuée', 'success',);

            // Redirection avec message de succès
            return redirect()->back()->with('success', 'Attestation créée avec succès !');
        }



    public function searchEnquete(Request $request)
        {
            // Valider la requête de recherche
            $request->validate([
                'search' => 'required|string|max:255',
            ]);

            // Récupérer les champs à rechercher depuis le modèle
            $searchableFields = (new SuperAdminEnquetteModel)->getFillable();

            // Construire une requête pour rechercher dans les champs définis
            $enquetes = SuperAdminEnquetteModel::query()
                ->where(function ($query) use ($request, $searchableFields) {
                    foreach ($searchableFields as $field) {
                        $query->orWhere($field, 'like', '%' . $request->search . '%');
                    }
                })
                ->get();

            // Retourner la vue avec les résultats de la recherche
            return view('SuperAdmin.Admin.AdminCreatAttestation', compact('enquetes'));
        }


   
    public function show($id)
        {
          try {
            $enquete = SuperAdminEnquetteModel::findOrFail($id); // Trouver l'enquête par ID
            
            $typesAttestation = admin_creat_type_attestation::all(); // Récupérer tous les types d'attestation
            
                $typesAttestationexit = AdminCreatAttestationModel::with('type')
                ->where('enquete_id',$enquete->id )
                ->get(); // Récupérer tous les types d'attestation
               // dd($typesAttestationexit );
            
          
                // return view('SuperAdmin.Admin.EnqueteDetails', compact('enquete', 'typesAttestation'));
                return view('SuperAdmin.Admin.EnqueteDetails', compact('enquete', 'typesAttestation', 'typesAttestationexit'));

            } catch (\Exception $e) {
                // Gérer l'erreur si l'enquête n'existe pas
                \Illuminate\Support\Facades\Log::error($e->getMessage());
                return redirect()->route('attestation.create')->with('error', 'Enquête non trouvée.');
            }
        }






    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
         try {
              $Attestationexit = AdminCreatAttestationModel::with('type')
                    ->where('id',$id )
                    ->get(); 
// dd ($Attestationexit );
                // Retourner la vue avec les résultats de la recherche
                 return view('SuperAdmin.Admin.AdminVoirAttestationCreat', compact('Attestationexit'));

            } catch (\Exception $e) {
                // Gérer l'erreur si l'enquête n'existe pas
                \Illuminate\Support\Facades\Log::error($e->getMessage());
                return redirect()->route('attestation.create')->with('error', 'Enquête non trouvée.');
            }

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

        // Méthode
    // public function exportExcel()
    // {
    //     return Excel::download(new EnqueteExport, 'enquetes.xlsx');
    // }


    public function exportExcel(Request $request)
        {
            // Récupérer le terme de recherche et effectuer la recherche
            $searchTerm = $request->input('search');
            $enquetes = [];

            if ($searchTerm) {
                try {
                    $enquetes = $this->performSearch($searchTerm, SuperAdminEnquetteModel::class);
                } catch (\Exception $e) {
                    \Illuminate\Support\Facades\Log::error($e->getMessage());
                }
            }

            // Retourner l'export Excel
            return Excel::download(new EnqueteExport($enquetes), 'enquetes.xlsx');
        }


    public function exportPDF(Request $request)
        {
            // Récupérer le terme de recherche et effectuer la recherche
            $searchTerm = $request->input('search');
            $enquetes = [];

            if ($searchTerm) {
                try {
                    $enquetes = $this->performSearch($searchTerm, SuperAdminEnquetteModel::class);
                } catch (\Exception $e) {
                    \Illuminate\Support\Facades\Log::error($e->getMessage());
                }
            }

            // Charger la vue pour le PDF
            $pdf = PDF::loadView('pdf.enquetes', compact('enquetes'));

            // Retourner le PDF généré
            return $pdf->download('enquetes.pdf');
        }

}
