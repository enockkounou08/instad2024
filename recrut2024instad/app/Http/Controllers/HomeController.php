<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\contact; 
use App\Models\diplome;
use App\Models\personne;
use App\Models\Contact_us;
use Illuminate\Http\Request;
use App\Models\enquetedisponible;
use Illuminate\Support\Facades\Auth;
use App\Models\SuperAdminEnquetteModel;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    //
  

    // public function index()
    //     {
    //         $contenus = SuperAdminEnquetteModel::all();
    //      //   $contenus = enquetedisponible::all();
    //         $user = Auth::user(); // Récupérer l'utilisateur connecté

          


    //         return view('recrut.index', compact('contenus', 'user'));
    //     }


    public function index()
    {
        // Récupérer les enquêtes triées par date de création (du plus récent au plus ancien)
        $contenus = SuperAdminEnquetteModel::orderBy('created_at', 'desc')->get();
        
        // Récupérer l'utilisateur connecté
        $user = Auth::user();
         //    Alert::success('Succès', 'Bienvenu');

        return view('recrut.index', compact('contenus', 'user'));
    }


 
      
    public function nav()
    {
        $contenus = SuperAdminEnquetteModel::all();
        $user = Auth::user(); // Récupérer l'utilisateur connecté

        return view('navigate.navigate', compact('contenus', 'user'));
    }
// la methode pour l'affichage de la page à propos de nous 
   public function about(){
        return view('recrut.about');
   }

   // la methode pour l'affichage des services de la plateforme
   public function service(){
    return view('recrut.service');
}

// la methode pour l'affichage de la politique de confidentialité de la plateforme
public function politique(){
    return view('recrut.politique');
}

   public function contact(){
        return view('contact_us.contact');
   }

   public function sendInformation(Contact_us $requestModel, Request $request){
     // validation des champs avec les messages d'erreurs 
     $ErrorMessage = [
         'nom.required' => 'Le champs de nom est requis ',
         'email.required' => 'Le champs email est requis',
         'tel.required' => 'Le champs téléphone est requis', 
         'message.required' => 'Le champs message est requis',

     ];

     $request->validate([
         'nom' => ['required'],
         'email' => ['required'],
         'tel' => ['required'],
         'message' => ['required'],
     ], $ErrorMessage);

     // Enregistremnet des données dans la base 
     $requestModel->nomcomplet = $request->nom;
     $requestModel->Email = $request->email;
     $requestModel->Telephone = $request->tel;
     $requestModel->content = $request->message;
     $requestModel->save();
    
     return redirect('/contact')->with('flash_message', 'votre commentaire a bien été envoyé');
 } 


 public function profileUserView()
 {
     // $user = Auth::user();
        $user_id = Session::get('user_id');
       // $contacts = contact::where('id',$user_id);

        $contact = contact::find($user_id); // Trouver un contact avec un ID spécifique
        $personne = $contact->personne; // Récupérer la personne associée

      // dd($contact);

        return view('recrut.profileView', compact('contact'));
 }
 public function profileUser()
 {
     // $user = Auth::user();
        $user_id = Session::get('user_id');
       // $contacts = contact::where('id',$user_id);

        $contact = contact::find($user_id); // Trouver un contact avec un ID spécifique
        $personne = $contact->personne; // Récupérer la personne associée

      // dd($contact);

        return view('recrut.profile', compact('contact'));
 }

 public function profileUserPost(Request $request)
 {

       // Validation des données du formulaire
          $validator = Validator::make($request->all(), [
            'prenom' => 'required|string',
            'nom' => 'required|string',
         // 'gender' => 'required|in:male,female', // Ajout de la règle de validation pour le champ gender
            
         //    'day' => 'required|numeric|min:1|max:31',
         //    'month' => 'required|numeric|min:1|max:12',

        
         //       'year' => ['required', 'numeric', function ($attribute, $value, $fail) {
         //        $currentYear = date('Y');
         //        $age = $currentYear - $value;
    
         //        if ($age < 18) {
         //            $fail('Vous êtes trop jeune pour postuler.');
         //        }
    
         //        if ($age > 70) {
         //            $fail('Vous êtes trop âgé pour postuler.');
         //        }
         //    }],

            'langue1' => 'required|not_in:0', // Règle de validation pour la 1ère langue
            'langue2' => 'required|not_in:0', // Règle de validation pour la 1ère langue
            'langue3' => 'required|not_in:0', // Règle de validation pour la 1ère langue
     
            'nationalite' => 'required', // Règle de validation pour la nationalité
            'autreNationaliteInput' => 'required_if:nationalite,autre', // Règle de validation pour l'autre nationalité
       
            'num_tel' => 'required|numeric|digits:10', // Règle de validation pour le numéro de téléphone
            'num_whatsapp' => 'required|numeric|digits:10', // Règle de validation pour le numéro de téléphone WhatsApp
         //   'email' => 'required|email', // Règle de validation pour l'e-mail
               
            'password' => 'required|min:6|confirmed', // Validation du mot de passe
            'password_confirmation' => 'required', // Validation du mot de passe
            'ville' => 'required',
            'type_piece' => 'required',
            'num_piece' => 'required|numeric',
            'ifu' => 'required|numeric|digits:13',


'exp_day' => 'required|numeric|min:1|max:31',
'exp_month' => 'required|numeric|min:1|max:12',
'exp_year' => ['required', 'numeric', function ($attribute, $value, $fail) {
    $currentYear = date('Y');
    $today = Carbon::today();
    $expirationDate = Carbon::create($value, request('exp_month'), request('exp_day'));

    if ($expirationDate->isToday()) {
        $fail('Votre pièce expire aujourd\'hui, Veuillez utiliser une autre.');
    }

    if ($expirationDate->lt($today)) {
        $fail('Votre pièce a déjà expiré.');
    }
}],

 
        'diplome' => 'required', // Règle de validation pour la nationalité
        'autreDiplomeInput' => 'required_if:diplome,autre', // Règle de validation pour l'autre nationalité
   
  
      ]);


           // Personnalisation des messages d'erreur
        $messages = [
            'prenom.required' => 'Le champ prenoms est obligatoire.',
            'nom.required' => 'Le champ nom est obligatoire.',
            // 'gender.required' => 'Veuillez sélectionner un genre.', // Message d'erreur personnalisé pour le champ gender
        
            // 'day.required' => 'Le champ jour est obligatoire.',
            // 'day.numeric' => 'Le champ jour doit être un entier.',
            // 'day.min' => 'Le champ jour doit être compris entre 1 et 31.',
            // 'day.max' => 'Le champ jour doit être compris entre 1 et 31.',
            // 'month.required' => 'Le champ mois est obligatoire.',
            // 'month.numeric' => 'Le champ mois doit être un entier.',
            // 'month.min' => 'Le champ mois doit être compris entre 1 et 12.',
            // 'month.max' => 'Le champ mois doit être compris entre 1 et 12.',
            // 'year.required' => 'Le champ année est obligatoire.',
            // 'year.numeric' => 'Le champ année ne doit comporter que des entier.',
            // 'year.min' => 'Le champ année doit être supérieure à 1900.',
            // 'year.max' => 'Le champ année doit être inférieure ou égale à '.date('Y').'.',
         
            'nationalite.required' => 'Veuillez sélectionner une nationalité.', // Message d'erreur si la nationalité n'est pas sélectionnée
            'autreNationaliteInput.required_if' => 'Veuillez spécifier l\'autre nationalité.', // Message d'erreur si "autre" est sélectionné mais l'autre nationalité n'est pas spécifiée
      
            'langue1.required' => 'Veuillez sélectionner la 1ère langue parlée.',
            'langue1.not_in' => 'Veuillez sélectionner la 1ère langue parlée.', // Message d'erreur personnalisé si la valeur est vide

            'langue2.required' => 'La deuxième langue parlée est obligatoire.',
            'langue2.not_in' => 'Veuillez sélectionner la deuxième langue parlée.', // Message d'erreur personnalisé si la valeur est vide

            'langue3.required' => 'La troisième langue parlée est obligatoire.',
            'langue3.not_in' => 'Veuillez sélectionner la troisième langue parlée.', // Message d'erreur personnalisé si la valeur est vide

            'num_tel.required' => 'Le numéro de téléphone est obligatoire.',
            'num_tel.numeric' => 'Le numéro de téléphone doit ne doit comporter que des entiers.',
            'num_tel.digits' => 'Le numéro de téléphone doit être de exactement 10 caractères .',
            'num_whatsapp.required' => 'Le numéro WhatsApp est obligatoire.',
            'num_whatsapp.numeric' => 'Le numéro WhatsApp ne doit comporter que des entiers.',
            'num_whatsapp.digits' => 'Le numéro WhatsApp doit être de exactement 10 caractères.',
            // 'email.required' => 'L\'adresse email est obligatoire.',
            // 'email.email' => 'Veuillez saisir une adresse e-mail valide.',
            // // 'email.unique' => 'Cette adresse email est déjà utilisée.',
        

            'password.required' => 'Le champ mot de passe est obligatoire.',
            'password.min' => 'Le mot de passe doit avoir au moins :min caractères.',
            'password.confirmed' => 'Les mots de passe ne correspondent pas à celui du champ confirmation.',
            'password_confirmation.required' => 'Le champ de confirmation du mot de passe est requis.',

            'ville.required' => 'Le champ ville est obligatoire.',
            'type_piece.required' => 'Veuillez sélectionner un type de pièce.',
            'num_piece.required' => 'Le champ numéro de pièce est obligatoire.',
            'num_piece.numeric' => 'Le champ numéro ne doit contenir que des entiers.',
            
            'ifu.required' => 'Le champ ifu est obligatoire.',
            'ifu.numeric' => 'Le champ ifu ne doit contenir que des nombres entiers.',
            'ifu.digits' => 'Le champ ifu doit obligatoirement comporter 13 chiffres .',
            
            'exp_day.required' => 'Le champ jour d\'expiration est obligatoire.',
            'exp_day.numeric' => 'Le champ jour doit être un entier.',
            'exp_day.min' => 'Le champ jour doit être compris entre 1 et 31.',
            'exp_day.max' => 'Le champ jour doit être compris entre 1 et 31.',
            'exp_month.required' => 'Le champ mois d\'expiration est obligatoire.',
            'exp_month.min' => 'Le champ mois doit être compris entre 1 et 12.',
            'exp_month.max' => 'Le champ mois doit être compris entre 1 et 12.',
            'exp_month.numeric' => 'Le champ mois doit être un entier.',
            'exp_year.required' => 'Le champ année d\'expiration est obligatoire.',
            'exp_year.numeric' => 'Le champ année doit être un entier.',


            // 'depart_residence.required' => 'Veuillez sélectionner un département de résidence.',
            // 'commune_residence.required' => 'Veuillez sélectionner une commune de résidence.',
           
            'diplome.required' => 'Veuillez sélectionez votre dernier diplôme.', // Message d'erreur si la nationalité n'est pas sélectionnée
            'autreDiplomeInput.required_if' => 'Veuillez indiquer votre diplôme.', // Message d'erreur si "autre" est sélectionné mais l'autre nationalité n'est pas spécifiée
      
            // 'diplome.required' => 'Veuillez sélectionner votre dernier diplôme au format PDF.',
            // 'diplome.file' => 'Le champ diplôme doit être un fichier.',
            // 'diplome.mimes' => 'Le format du fichier doit être PDF.',
            // 'cv.required' => 'Veuillez sélectionner votre curriculum vitae au format PDF.',
            // 'cv.file' => 'Le champ CV doit être un fichier.',
            // 'cv.mimes' => 'Le format du fichier doit être PDF.',
        
    
      ];
        $user_id = Session::get('user_id');

        $contact = contact::find($user_id); // Trouver un contact avec un ID spécifique
        $personne = $contact->personne; // Récupérer la personne associée
// dd($contact);

      // Application des messages personnalisés
        $validator->setCustomMessages($messages);
 
        // Vérification de la validation
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
                        // Insertion des données dans la base de données

                    // Insertion dans la table 'personnes'
                     if ($personne){
                    $personne->prenom = $request->prenom;
                    $personne->nom = $request->nom;
                 //   $personne->gender = $request->gender;
                  //  $personne->date_of_birth = $request->year . '-' . $request->month . '-' . $request->day;
                    $personne->nationalite = $request->nationalite;
                    $personne->autre_nationalite = $request->autreNationaliteInput;
                    $personne->langue1 = $request->langue1;
                    $personne->langue2 = $request->langue2;
                    $personne->langue3 = $request->langue3;
                    $personne->save();
                       }
                    // Insertion dans la table 'contacts'
                        if ($contact){
                    $contact->num_tel = $request->num_tel;
                    $contact->num_whatsapp = $request->num_whatsapp;
               //     $contact->email = $request->email;
                    $contact->password = bcrypt($request->password); // Assurez-vous de hasher le mot de passe avant de l'enregistrer
                    $contact->password_confirmation = bcrypt($request->password_confirmation);
                    $contact->ville = $request->ville;
                    $contact->type_piece = $request->type_piece;
                    $contact->num_piece = $request->num_piece;
                    $contact->ifu = $request->ifu;
                    $contact->exp_day = $request->exp_day;
                    $contact->exp_month = $request->exp_month;
                    $contact->exp_year = $request->exp_year;
                    $contact->save();
                }

                    
                    // Insertion dans la table 'diplomes'
                    $diplome = new Diplome();
                    // $diplome->diplome = $request->diplome;
                    // $diplome->cv = $request->cv;

                    $diplome->diplome = $request->diplome;
                    $diplome->autrediplome = $request->autreDiplomeInput;
                   
                    
                  //  $diplome->autrediplome = $request->autreDiplomeInput;

                 //   $diplome->save();

     Alert::toast('vos informations ont été modifié', 'success');

      Auth::logout(); // Déconnexion de l'utilisateur

    // Effacer toutes les données de session
    Session::flush();
     return redirect()->route('Accueil_page');
 }
    
}
