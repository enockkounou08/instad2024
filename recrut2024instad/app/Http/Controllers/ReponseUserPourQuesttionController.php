<?php

namespace App\Http\Controllers;
use App\Models\ReponseUtilisateurPourEnquete;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\EnqueteQuestionModel;
use Illuminate\Support\Facades\DB;
use App\Models\candidatureModel;
use App\Models\SuperAdminEnquetteModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\PostulerMail;
use RealRashid\SweetAlert\Facades\Alert;


class ReponseUserPourQuesttionController extends Controller
{
    //





    public function storeUserResponses(Request $request)
{

    // Obtenez les données de l'utilisateur connecté
    $user_id = $request->session()->get('user_id');
    if (!$user_id) {

        return redirect()->route('login')->withErrors(['error' => 'Utilisateur non authentifié']);
    }

    $enquete_id = $request->input('idEnquete');
    
      dd($enquete_id);

    // Parcourir les questions soumises
    foreach ($request->all() as $key => $value) {
        if (strpos($key, 'response_') === 0) {
            $question_id = str_replace('response_', '', $key);
            $question_type = $request->input('type_reponse_' . $question_id);

            // Vérifiez si type_reponse est défini
            if (is_null($question_type)) {
                return redirect()->back()->withErrors(['error' => 'Type de réponse manquant pour la question ID: ' . $question_id]);
            }

            // Enregistrer la réponse de l'utilisateur
            ReponseUtilisateurPourEnquete::create([
                'user_id' => $user_id,
                'enquete_id' => $enquete_id,
                'question_id' => $question_id,
                'type_reponse' => $question_type,
                'reponse' => is_array($value) ? json_encode($value) : $value,
            ]);
        }
    }

    return redirect()->route('Accueil_page')->with('success', 'Vos réponses ont été enregistrées avec succès.');
}









public function submitSurvey(Request $request)
{
    // Retrieve the necessary data from the request
  $userId = $request->session()->get('user_id');

    $surveyId = $request->input('idEnquete');
    $responses = $request->all(); // Get all responses from the request

    // Initialize an array to store the data
    $questionsAndResponses = [];

    // Loop through each response
    foreach ($responses as $key => $response) {
        if (str_starts_with($key, 'response_')) {
            // Extract the question ID from the key
            $questionId = explode('_', $key)[1];

            // Retrieve the question text from your `enquete_question` table (assuming it's available)
            $question = DB::table('enquete_question')->where('id', $questionId)->first();

            // Add the question and response to the array
            $questionsAndResponses[] = [
                'question_id' => $questionId,
                'question_texte' => $question->question,
                'reponse' => is_array($response) ? implode(', ', $response) : $response,
            ];
        }
    }

    // Calculate the total number of questions and responses
    $totalQuestions = count($questionsAndResponses);
    $totalResponses = count(array_filter($responses));

    // Insert the data into the table
    foreach ($questionsAndResponses as $data) {
        DB::table('reponse_utilisateur_pour_enquetes')->insert([
            'user_id' => $userId,
            'enquete_id' => $surveyId,
            'nbr_question' => $totalQuestions,
            'nbr_reponse' => $totalResponses,
            'question_id' => $data['question_id'],
            'question_texte' => $data['question_texte'],
            'reponse' => $data['reponse'],
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    return redirect()->back()->with('success', 'Survey responses saved successfully!');
}






public function reponseCandidature(Request $request)
{
       // Alert::success('Succès', 'Candidature success');

    // Récupérer les données de session
    $userId = $request->session()->get('user_id');
    $userNom = $request->session()->get('user_nom');
    $userPrenom = $request->session()->get('user_prenom');
    $userEmail = $request->session()->get('user_email');

    // Vérifier si l'utilisateur est connecté
    if (!$userId || !$userEmail) {
        return redirect()->route('login')->with('error', 'Veuillez vous reconnecter.');
    }

    // Récupérer les données de l'enquête
    $surveyId = $request->input('idEnquete');
   
    // Vérifier si l'utilisateur a déjà postulé
    $candidatureExists = DB::table('candidature_models')
        ->where('id_personne', $userId)
        ->where('email_personne', $userEmail)
        ->where('id_enquete', $surveyId)
        ->exists();
    
    if ($candidatureExists) {
        return redirect()->back()
            ->with('error', 'Vous avez déjà une candidature.')
            ->with([
                'user_id' => $userId,
                'user_nom' => $userNom,
                'user_prenom' => $userPrenom,
                'user_email' => $userEmail,
            ]);
    } else {
        // Traiter les réponses de l'utilisateur
        $responses = $request->except('_token');  // Ne pas inclure le token CSRF
        $questionsAndResponses = [];
        foreach ($responses as $key => $response) {
            if (str_starts_with($key, 'response_')) {
                $questionId = explode('_', $key)[1];
                $question = DB::table('enquete_question')->where('id', $questionId)->first();
                $questionsAndResponses[] = [
                    'question_id' => $questionId,
                    'question_texte' => $question->question,
                    'reponse' => is_array($response) ? implode(', ', $response) : $response,
                ];
            }
        }

        // Insérer les réponses dans la table
        foreach ($questionsAndResponses as $data) {
            DB::table('reponse_utilisateur_pour_enquetes')->insert([
                'user_id' => $userId,
                'enquete_id' => $surveyId,
                'nbr_question' => count($questionsAndResponses),
                'nbr_reponse' => count(array_filter($responses)),
                'question_id' => $data['question_id'],
                'question_texte' => $data['question_texte'],
                'reponse' => $data['reponse'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        // Créer une nouvelle candidature
        $enquete = SuperAdminEnquetteModel::find($surveyId);
        if ($enquete) {
            $candidature = new CandidatureModel;
            $candidature->id_personne = $userId;
            $candidature->nom_personne = $userNom;
            $candidature->email_personne = $userEmail;
            $candidature->id_enquete = $surveyId;
            $candidature->nom_enquete = $enquete->nom;

            // // Envoyer un email de confirmation
            // $message = "Salut " . $userNom . ", vous avez postulé pour l'enquête : " . $enquete->nom;
            // $subject = "L'Institut National de la Statistique vous remercie";
            // Mail::to($userEmail)->send(new PostulerMail($message, $subject));

            $candidature->save();
        Alert::success('Succès', 'Vos réponses ont été enregistrées avec succès.');

 

   // return redirect()->back()->with('success', 'Vos réponses ont été enregistrées avec succès.');
       //  return view('InfoEnquette.afterReponse')->with('success', 'Vos réponses ont été enregistrées avec succès.');

   return redirect()->route('Accueil_page')->with('success', 'Vos réponses ont été enregistrées avec succès.');


        } else {
            return redirect()->back()
                ->with('error', 'L’enquête sélectionnée est introuvable.')
                ->with([
                    'user_id' => $userId,
                    'user_nom' => $userNom,
                    'user_prenom' => $userPrenom,
                    'user_email' => $userEmail,
                ]);
        }
    }
}



public function afterReponse()
        {
        Alert::success('Succès', 'Candidature success');
           

            return view('InfoEnquette.afterReponse');
        }


public function checkUserCandidature(Request $request)
    {
        $userId = $request->session()->get('user_id');
        $userEmail = $request->session()->get('email');
        $surveyId = $request->input('survey_id'); // Assuming survey_id is being passed from the request

        $candidatureExists = DB::table('candidature_model')
            ->where('id_personne', $userId)
            ->where('email_personne', $userEmail)
            ->where('id_enquete', $surveyId)
            ->exists();

        return response()->json(['candidatureExists' => $candidatureExists]);
    }




public function showApplicationForm($surveyId)
        {
            $user = session()->get('user');
            $userId = $user['id'];
            $userEmail = $user['email'];

            $hasApplied = DB::table('candidature_model')
                ->where('id_personne', $userId)
                ->where('email_personne', $userEmail)
                ->where('id_enquete', $surveyId)
                ->exists();

            return view('application_form', [
                'surveyId' => $surveyId,
                'hasApplied' => $hasApplied
            ]);
        }


public function checkApplicationStatus($id)
{
    $user = auth()->user();
    $alreadyApplied = DB::table('candidature_model')
                        ->where('id_personne', $user->id)
                        ->where('email_personne', $user->email)
                        ->where('id_enquete', $id)
                        ->exists();

    return response()->json([
        'alreadyApplied' => $alreadyApplied,
    ]);
}


}
