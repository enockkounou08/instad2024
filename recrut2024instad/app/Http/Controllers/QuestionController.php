<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionModel;
use App\Models\ReponseModel;

use App\Http\Requests\connexionRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\personne;
use App\Models\EvaluationModel;
use App\Models\diplome;
use App\Models\ConnexionAdminModel;
use App\Models\contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;



class QuestionController extends Controller
{
    //

    // public function question() { 

    //     $contenus = QuestionModel::all();

    //     return view ('Question.index',compact('contenus'));
    // }

//     public function question(Request $request)
// {
//     // Récupération des réponses sélectionnées depuis le formulaire
//     $selectedAnswers = $request->input('choix');

//     // Initialisation du total des points
//     $totalPoints = 0;

//     // Vérification des réponses et calcul des points
//     foreach ($selectedAnswers as $questionId => $selectedAnswer) {
//         // Récupération de la réponse correcte depuis la base de données
//         $correctAnswer = Reponse::where('id_question', $questionId)->value('solution');

//         // Vérification si la réponse sélectionnée correspond à la réponse correcte
//         if ($selectedAnswer === $correctAnswer) {
//             $totalPoints += 1; // Ajout de 1 point si la réponse est correcte
//         }
//     }

//     // Récupération de tous les contenus/questions
//     $contenus = QuestionModel::all();

//     // Retourne la vue avec les contenus/questions et le total des points
//     return view('Question.index', compact('contenus', 'totalPoints'));
// }




// public function question(Request $request)
// {
   
//         // Si $selectedAnswers est null, initialise $totalPoints à 0
//         $totalPoints = 0;
    

//     // Récupération de tous les contenus/questions
//     $contenus = QuestionModel::all();
  

//     // Retourne la vue avec les contenus/questions et le total des points
//     return view('Question.index', compact('contenus', 'totalPoints'));
// }


public function question(Request $request)
{
    // Récupérer l'identifiant de l'utilisateur connecté à partir de la session
    $idUser = session('user_id');

    // Vérifier si l'utilisateur a déjà une évaluation enregistrée
    $evaluationExistante = EvaluationModel::where('id_uitisateur', $idUser)->first();

    if ($evaluationExistante) {
        // Rediriger l'utilisateur vers la page d'évaluation existante
        return redirect('/pageEvaluation');
    }else {
                // Si $selectedAnswers est null, initialise $totalPoints à 0
            $totalPoints = 0;

            // Récupération de tous les contenus/questions
            $contenus = QuestionModel::all();

            // Retourne la vue avec les contenus/questions et le total des points
            return view('Question.index', compact('contenus', 'totalPoints'));

    }

    }




    public function connexionquestion() {    

        return view ('Question.PageConnexion');
    }

    public function connexionquestionTraitement(Request $request) {    

        // Validation des données du formulaire
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
         ], [
            // Personnalisation des messages d'erreur
            'email.required' => 'L\'adresse email est obligatoire.',
            'email.email' => 'Veuillez saisir une adresse email valide.',
            'password.required' => 'Le mot de passe est obligatoire.',
            'password.min' => 'Le mot de passe doit contenir au moins :min caractères.',
         ]);
     
         // Vérifier si la validation a échoué
         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
             // Cette ligne redirige l'utilisateur vers la page précédente avec les erreurs de validation et les données saisies précédemment
         }

          // Si la validation réussit, tu peux continuer avec la logique de connexion ici
    
         // Vérification de l'utilisateur
         $user = contact::where('email', $request->email)->first();
   // dd($user);
         if ($user && Hash::check($request->password, $user->password)) {
             // Le mot de passe correspond
             // Connecte l'utilisateur
             Auth::login($user);
            
             //récupérer l'id de l'utilisateur connecter
             $userId = Auth::id();
             $request->session()->regenerate();
             // Comparaison avec l'ID d'une autre table
   // dd($userId);
             


             $otherTableEntry = personne::where('id', $userId)->first();
  //  dd($otherTableEntry->id);

             Session::put('user_id', $userId);

             Session::put('user_nom', $otherTableEntry->nom);
             Session::put('user_prenom', $otherTableEntry->prenom);
             Session::put('userid', $otherTableEntry->id);
 //dd($a);
 //dd(Session::all());

             // Redirige l'utilisateur vers la page d'accueil
             return redirect('/pageQuestion');
            
         }  else {
            // Rediriger avec un message d'erreur si aucune réponse n'a été soumise
            return redirect()->back()->with('error', 'Addresse email ou mot de passe incorrecre');
        }
    }

}
