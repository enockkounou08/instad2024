<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Models\Contact;
use App\Models\Diplome;
use App\Models\Personne;
use App\Models\Roleadmin;
use App\Models\admin_action_model;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\PhpWord;
use App\Exports\EnquetesExport;
use App\Models\PosteadminModel;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\candidatureModel;
use PhpOffice\PhpWord\IOFactory;
use App\Models\enquetedisponible;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\Style\Table;
use App\Exports\CandidaturesExport;
use App\Exports\UtilisateursExport;
use App\Models\EnqueteQuestionModel;
use Maatwebsite\Excel\Facades\Excel;
// use PhpOffice\PhpWord\PhpWord;
// use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\SimpleType\Jc;
// use Illuminate\Support\Facades\DB;
// use Carbon\Carbon;
use App\Exports\UtilisateursWordExport;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Session;
use App\Models\SuperAdminEnquetteModel;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Support\Facades\DB;
// use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class SuperAdminControlleur extends Controller 
{

//  public function pageAccueil() {

//     // Récupérer tous les identifiants de SuperAdminEnquetteModel
//     $ids = SuperAdminEnquetteModel::pluck('id');

//     // Initialiser un tableau pour stocker les résultats
//     $nombreOccurrences = [];

//     // Pour chaque identifiant, compter le nombre d'occurrences dans CandidatureModel
//     foreach ($ids as $id) {
//         $occurrences = CandidatureModel::where('id_enquete', $id)->count();
//         $nombreOccurrences[$id] = $occurrences;
//     }

//     // Récupérer les id_personne où id_enquete est égal à 1
//     $id_personnes_enquete_1 = CandidatureModel::where('id_enquete', 1)->pluck('id_personne');

//     // Récupérer les autres contenus si nécessaire
//     $contenus = SuperAdminEnquetteModel::all();
    
//     //j'affiche leurs nom et prénoms  
//     // Récupérer toutes les enquêtes avec leurs états
//     $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
//         ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
//         ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', 'super_admin_enquette_models.etat_enquete', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
//         ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', 'super_admin_enquette_models.etat_enquete')
//         ->paginate(5);  // Remplacer 'get()' par 'paginate(5)' pour 5 enquêtes par page
//     // Passer les résultats à la vue
//     return view('SuperAdmin.index2', compact( 'nombreOccurrences','enquetes','contenus'));
// }



 public function pageAccueil() {

    $user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
    
    // Récupérer tous les identifiants de SuperAdminEnquetteModel
    $ids = SuperAdminEnquetteModel::pluck('id');

    // Initialiser un tableau pour stocker les résultats
    $nombreOccurrences = [];

    // Pour chaque identifiant, compter le nombre d'occurrences dans CandidatureModel
    foreach ($ids as $id) {
        $occurrences = CandidatureModel::where('id_enquete', $id)->count();
        $nombreOccurrences[$id] = $occurrences;
    }

    // Récupérer les id_personne où id_enquete est égal à 1
    $id_personnes_enquete_1 = CandidatureModel::where('id_enquete', 1)->pluck('id_personne');

    // Récupérer les autres contenus si nécessaire
    $contenus = SuperAdminEnquetteModel::all();
    
    //j'affiche leurs nom et prénoms
    // Récupérer toutes les enquêtes avec leurs états
    $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', 'super_admin_enquette_models.etat_enquete', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', 'super_admin_enquette_models.etat_enquete')
        ->paginate(5);  // Remplacer 'get()' par 'paginate(5)' pour 5 enquêtes par page
        //  $user = admin_action_model::find($user_id);
        // dd($user->poste->role_id);



    //       // Trouver l'utilisateur
    // $user = admin_action_model::find($user_id);

    // // Décoder le champ `role_id` en tableau
    // $rolesIds = json_decode($user->poste->role_id, true);

    // // Vérifier que le décodage a fonctionné
    // if (is_array($rolesIds)) {
    //     // Récupérer tous les rôles correspondants
    //     $roles = Roleadmin::whereIn('id', $rolesIds)
    //     ->select('classe_desactive')
    //     ->get();
    // } else {
    //     // Si le champ `role_id` n'est pas un tableau valide
    //     $roles = collect();
    // }

        try {
    $user = admin_action_model::find($user_id);
    $rolesIds = json_decode(optional($user->poste)->role_id, true);
    $roles = is_array($rolesIds)
        ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
        : collect();

      //  dd( );
} catch (\Exception $e) {
    $roles = collect();
    $error = "Impossible de récupérer les informations utilisateur.";
}


    // dd($roles);
    $disabledClasses = $roles->pluck('classe_desactive')->toArray();

     // Vérifier si l'utilisateur a une classe désactivée pour cette page
    // $currentPageClass = 'dashboard'; // La classe à vérifier pour cette page
    // if (in_array($currentPageClass, $disabledClasses)) {
    //     // Rediriger vers une autre page
    //     // return redirect()->route('page_interdite')->with('error', 'Vous n\'êtes pas autorisé à accéder à cette page.');
    //      // Lancer une erreur 404
    //     abort(404);
    // }
    // dd();
          $currentPageClasses = ['dashboard','sidebar_admins'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }
    // Passer les résultats à la vue
    return view('SuperAdmin.index2', compact( 'nombreOccurrences','enquetes','contenus', 'user', 'roles','disabledClasses'));
}

public function exportToWord() {
    // Récupérer les enquêtes
    $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();

    // Créer un nouveau document Word
    $phpWord = new PhpWord();
    $section = $phpWord->addSection();

    // Ajouter un titre
    $section->addTitle("Liste des Enquêtes", 1);

    // Définir le style du tableau
    $tableStyle = [
        'borderSize' => 6,
        'borderColor' => '000000',
        'cellMargin' => 50
    ];
    $firstRowStyle = ['bgColor' => 'CCCCCC'];
    $phpWord->addTableStyle('Enquetes Table', $tableStyle, $firstRowStyle);

    // Ajouter un tableau avec le style défini
    $table = $section->addTable('Enquetes Table');

    // Ajouter l'en-tête du tableau
    $table->addRow();
    $table->addCell(2000)->addText("N°");
    $table->addCell(4000)->addText("Nom de l'enquête");
    $table->addCell(8000)->addText("Contenu de l'enquête");
    $table->addCell(4000)->addText("Date d'expiration");
    
    // Ajouter les données du tableau
    $ide = 1;
    foreach ($enquetes as $enquete) {
        $table->addRow();
        $table->addCell(2000)->addText($ide);
        $table->addCell(4000)->addText($enquete->nom);
        $table->addCell(8000)->addText($enquete->info);
        $table->addCell(4000)->addText(\Carbon\Carbon::parse($enquete->date_expiration)->locale('fr')->isoFormat('D MMMM YYYY HH:mm'));
        $ide++;
    }

    // Enregistrer le document Word
    $fileName = 'enquetes.docx';
    $filePath = storage_path($fileName);
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save($filePath);

    // Retourner le fichier Word en téléchargement
    return response()->download($filePath)->deleteFileAfterSend(true);
}

public function exportPdf()
{
    // Récupérer les données nécessaires pour le PDF
    $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();

    // Générer le PDF avec les données récupérées
    $data = [
        'title' => 'Liste des enquêtes disponibles',
        'date' => date('d/m/y'),
        'enquetes' => $enquetes
    ];
    $pdf = Pdf::loadView('pdf.AceuilAdminInvoice', $data);
    return $pdf->download('EnquêteDisponible.pdf');
}
               public function createEnquette(SuperAdminEnquetteModel $enquetteModel, enquetedisponible $enquetedisponible, Request $request) {

                    // Valider les champs avec les messages d'erreur personnalisés
                    $customMessages = [
                        'nom.required' => 'Le champ nom est requis.',
                        'contenu.required' => 'Le champ contenu est requis.',
                        'date_expiration.required' => 'La date d\'expiration est obligatoire.',
                        'date_expiration.after_or_equal' => 'La date d\'expiration doit être supérieure à la date actuelle.',
                        'heure_expiration.required' => 'L\'heure d\'expiration est obligatoire.',
                    ];

                    $request->validate([
                        'nom' => ['required'],
                        'contenu' => ['required'],
                        'date_expiration' => ['required', 'date', 'after_or_equal:' . Carbon::now()->toDateString()],
                        'heure_expiration' => ['required', 'date_format:H:i'],
                    ], $customMessages);

                    // Combiner la date et l'heure d'expiration
                    $dateExpiration = $request->input('date_expiration');
                    $heureExpiration = $request->input('heure_expiration');
                    $expiration = Carbon::createFromFormat('Y-m-d H:i', "$dateExpiration $heureExpiration");

                    // Enregistrer l'enquête si la validation passe
                    $enquetteModel->nom = $request->nom;
                    $enquetteModel->info = $request->contenu;
                    $enquetteModel->date_expiration = $expiration;
                    $enquetteModel->save();

                    // Formater la date d'expiration pour l'affichage
                    $formatted_date_expiration = $expiration->isoFormat('DD MMMM YYYY HH:mm');

                    return redirect('/pageUserAdmin')->with('creationEnquette', 'Enquête créée avec succès. La date d\'expiration est ' . $formatted_date_expiration . '.');
              }

    public function updateEnquette ($id){

        $contenus = SuperAdminEnquetteModel::find($id);
            if (!$contenus) {
        // Rediriger avec un message d'erreur
     Alert::error('Error Systhème', 'Veuillez contactez L\'administrateur ');

        return redirect()->back()->with('error', 'nous retrouvons pas d\'enquetes avec ces correspondances.');
    }

        return view('SuperAdmin.modifierEnquette', compact('contenus'));
    }
    public function updateEnquette_traitement(Request $request) {
        $request->validate([
            'nom' => 'required',
            'contenu' => 'required',
            'expiration_date' => 'required|date',
            'expiration_time' => 'required|date_format:H:i',
        ]);
    
        $contenu = SuperAdminEnquetteModel::find($request->id);
          // Vérifier si l'enquête n'existe pas
    if (!$contenu) {
        Alert::error('Error', 'Error');

        // Rediriger avec un message d'erreur
        return redirect()->back()->with('error', 'nous retrouvons pas d\'enquetes avec ces correspondances.');
    }
        $contenu->nom = $request->nom;
        $contenu->info = $request->contenu;
        
        // Combine date and time into a single datetime value
        $expirationDateTime = $request->expiration_date . ' ' . $request->expiration_time;
        $contenu->date_expiration = \Carbon\Carbon::createFromFormat('Y-m-d H:i', $expirationDateTime);
        
        $contenu->update();

        // try {
        // Alert::success('Succès', 'Candidature success');

            
        // } catch (Exception $e) {
        //     //log($e)
        //  \Illuminate\Support\Facades\Log::info($e);

            
        // }
                   Alert::success('Succès', 'Modification effectuée');

           return redirect()->route('accueiladmin_page')->with('status', 'Modification effectuée avec succès');
    }
    

    public function delateEnquette ($id){
        $contenus = SuperAdminEnquetteModel::find($id);

        return view('SuperAdmin.suprimerEnquette', compact('contenus'));
    }

    public function delateEnquetteTraitement ($id){

        $contenu= SuperAdminEnquetteModel::find($id);
    
        if ($contenu) {
            $contenu->delete();
           Alert::success('Succès', 'Suppression effectué avec succèss');

            return  redirect('/pageUserAdmin')->with('statusSup','Suppression effectué avec succèss');
        }
     Alert::error('Error Systhème', 'Veuillez contactez L\'administrateur ');
        return  redirect('/pageUserAdmin')->with('status','L\'enregistrement n\'a pas été trouvé');
    
    }
public function candidaturesByEnqu($id) {
    // Récupérer l'enquête spécifique
    $enque = SuperAdminEnquetteModel::find($id);
    
    // Récupérer toutes les candidatures associées à cette enquête avec les informations de contacts et diplômes necessaires
    $candidatures = CandidatureModel::where('id_enquete', $id)
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
        ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id') // Ajout de la jointure avec la table diplômes
        ->select('candidature_models.*', 'personnes.*', 'contacts.*', 'diplomes.*')
        ->paginate(25);

    $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();
    
    $user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();


    //  // Vérifier si l'utilisateur a une classe désactivée pour cette page
    // $currentPageClass = 'enqueteRegister'; // La classe à vérifier pour cette page
    // if (in_array($currentPageClass, $disabledClasses)) {
    //     // Rediriger vers une autre page
    //     // return redirect()->route('page_interdite')->with('error', 'Vous n\'êtes pas autorisé à accéder à cette page.');
    //      // Lancer une erreur 404
    //     abort(404);
    // }

      $currentPageClasses = ['enqueteRegister','sidebar_admin'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }

    // Passer les résultats à la vue
    return view('SuperAdmin.CandidatByEnq', compact('enque', 'candidatures', 'enquetes','disabledClasses'));
}




public function reponseByEnqu($id) {
    // Récupérer l'enquête spécifique
    $enque = SuperAdminEnquetteModel::find($id);
    
    // Récupérer toutes les candidatures associées à cette enquête
    $candidatures = CandidatureModel::where('id_enquete', $id)
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
        ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('candidature_models.*', 'personnes.*', 'contacts.*', 'diplomes.*')
        ->orderBy('id_personne') // Ajoutez un ordre si nécessaire
        ->paginate(25);
        // ->groupBy('id_personne');  // Grouper par ID de la personne

          $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();

    // Récupérer toutes les questions associées à l'enquête
    $questions = DB::table('enquete_question')
        ->where('idEnquete', $id)
        ->select('id', 'question')
        ->get();

    // Récupérer toutes les réponses associées aux utilisateurs pour cette enquête
    $reponses = DB::table('reponse_utilisateur_pour_enquetes')
        ->where('enquete_id', $id)
        ->select('user_id', 'question_id', 'reponse')
        ->get();


$user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();

    //  // Vérifier si l'utilisateur a une classe désactivée pour cette page
    // $currentPageClass = 'ansUser'; // La classe à vérifier pour cette page
    // if (in_array($currentPageClass, $disabledClasses)) {
    //     // Rediriger vers une autre page
    //     // return redirect()->route('page_interdite')->with('error', 'Vous n\'êtes pas autorisé à accéder à cette page.');
    //      // Lancer une erreur 404
    //     abort(404);
    // }

     $currentPageClasses = ['ansUser','sidebar_admin'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }
    // Passer les résultats à la vue
    return view('SuperAdmin.ReponseByEnq', compact('enque', 'candidatures', 'questions', 'reponses', 'enquetes','disabledClasses'));
}





public function candidatPDF($id) {
    // Récupérer l'enquête spécifique
    $enque = SuperAdminEnquetteModel::find($id);
    
    // Récupérer toutes les candidatures associées à cette enquête avec les informations de contacts et diplômes nécessaires
    $candidatures = CandidatureModel::where('id_enquete', $id)
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
        ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id') // Ajout de la jointure avec la table diplômes
        ->select('candidature_models.*', 'personnes.*', 'contacts.*', 'diplomes.*')
        ->get();

    $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();
    
    // Charger la vue et passer les données
    $pdf = PDF::loadView('pdf.PersonneInscritparEnq', compact('enque', 'candidatures', 'enquetes'));

    // Télécharger le fichier PDF
    return $pdf->download('candidatures.pdf');
}



public function exportToExcelCandidat($id) {
    return Excel::download(new CandidaturesExport($id), 'candidaturesp.xlsx');
  
}


public function exportToWordCandidat($id) {
    // Récupérer les données à afficher dans votre page
    $enquete = SuperAdminEnquetteModel::find($id);
    $candidatures = CandidatureModel::where('id_enquete', $id)
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
        ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('candidature_models.*', 'personnes.*', 'contacts.*', 'diplomes.*')
        ->get();

    // Générer le contenu HTML de la page
    $html = view('word.candidature', compact('enquete', 'candidatures'))->render();

    // Initialiser Dompdf
    $dompdf = new Dompdf();

    // Charger le contenu HTML dans Dompdf
    $dompdf->loadHtml($html);

    // Rendre le document
    $dompdf->render();

    // Nom du fichier Word
    $fileName = 'enquete_' . $enquete->id . '.docx';

    // Télécharger le fichier Word
    return $dompdf->stream($fileName);
}


public function exportToWordCandidature($id) {
    // Récupérer l'enquête spécifique
    $enque = SuperAdminEnquetteModel::find($id);
    
    // Récupérer toutes les candidatures associées à cette enquête avec les informations de contacts et diplômes necessaires
    $candidatures = CandidatureModel::where('id_enquete', $id)
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
        ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('candidature_models.*', 'personnes.*', 'contacts.*', 'diplomes.*')
        ->get();

    $phpWord = new PhpWord();
    $section = $phpWord->addSection();

    $table = $section->addTable();

    // Add table headers
    $table->addRow();
    $table->addCell()->addText('Nom postulant');
    $table->addCell()->addText('Prénoms postulant');
    $table->addCell()->addText('Date de naissance postulant');
    $table->addCell()->addText('Nationalité postulant');
    $table->addCell()->addText('Numéro WhatsApp postulant');
    $table->addCell()->addText('Téléphone du postulant');
    $table->addCell()->addText('Email postulant');
    $table->addCell()->addText('Type de pièce');
    $table->addCell()->addText('Numéro pièce');
    $table->addCell()->addText('1ère langue parlée');
    $table->addCell()->addText('2ème langue parlée');
    $table->addCell()->addText('3ème langue parlée');
    $table->addCell()->addText('Diplôme');

    // Add data rows
    foreach ($candidatures as $candidature) {
        $table->addRow();
        $table->addCell()->addText($candidature->nom ?? 'Non disponible');
        $table->addCell()->addText($candidature->prenom ?? 'Non disponible');
        $table->addCell()->addText($candidature->date_of_birth ?? 'Non disponible');
        $table->addCell()->addText($candidature->nationalite ?? 'Non disponible');
        $table->addCell()->addText($candidature->num_whatsapp ?? 'Non disponible');
        $table->addCell()->addText($candidature->num_tel ?? 'Non disponible');
        $table->addCell()->addText($candidature->email ?? 'Non disponible');
        $table->addCell()->addText($candidature->type_piece ?? 'Non disponible');
        $table->addCell()->addText($candidature->num_piece ?? 'Non disponible');
        $table->addCell()->addText($candidature->langue1 ?? 'Non disponible');
        $table->addCell()->addText($candidature->langue2 ?? 'Non disponible');
        $table->addCell()->addText($candidature->langue3 ?? 'Non disponible');
        $table->addCell()->addText($candidature->diplome ?? 'Non disponible');
    }

    $fileName = 'candidatures.docx';
    $tempFile = tempnam(sys_get_temp_dir(), $fileName);

    $phpWord->save($tempFile, 'Word2007');

    return response()->download($tempFile, $fileName)->deleteFileAfterSend(true);
}


public function utilisateurInscrit(){
      // Requête pour joindre les tables et sélectionner les colonnes souhaitées
      $results = Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
      ->join('diplomes', 'personnes.id', '=', 'diplomes.id')
      ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1','personnes.langue2', 'personnes.langue3', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp', 'contacts.type_piece', 'contacts.num_piece', 'contacts.ifu', 'diplomes.diplome')
      ->paginate(50); // Pagine les résultats avec 10 éléments par page

    //   $enquete = SuperAdminEnquetteModel::all();

      $enquetes = SuperAdminEnquetteModel::leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
      ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
      ->select('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration', DB::raw('COUNT(candidature_models.id) as occurrences'), DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées'))
      ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
      ->get();

    $user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();
// dd($disabledClasses);

    //   // Vérifier si l'utilisateur a une classe désactivée pour cette page
    // $currentPageClass = ['userRegister']; // La classe à vérifier pour cette page
    // if (in_array($currentPageClass, $disabledClasses)) {
    //     // Rediriger vers une autre page
    //     // return redirect()->route('page_interdite')->with('error', 'Vous n\'êtes pas autorisé à accéder à cette page.');
    //      // Lancer une erreur 404
    //     abort(404);
    // }

    $currentPageClasses = ['userRegister','sidebar_admin'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }
        // Retourner les résultats à une vue (vous pouvez créer une vue pour afficher les résultats)
        return view('SuperAdmin.affichageUtilisateur', compact('results','enquetes','disabledClasses'));
} 


public function pdfPreview() {
    // Récupérer les données des utilisateurs
    $results = Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
        ->join('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1', 'personnes.langue2', 'personnes.langue3', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp','contacts.type_piece', 'contacts.num_piece', 'contacts.ifu', 'diplomes.diplome')
        ->get();

    // Créer une vue pour le PDF
    $html = view('pdf.listeDesInscritent', compact('results'))->render();

    // Initialiser Dompdf
    $options = new Options();
    $options->set('isHtml5ParserEnabled', true);
    $options->set('isRemoteEnabled', true);
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);

    // (Facultatif) Définir la taille et l'orientation du papier
    $dompdf->setPaper('A4', 'landscape');

    // Rendre le PDF
    $dompdf->render();

    // Enregistrer le fichier PDF
    $fileName = 'utilisateurs_inscrits.pdf';
    $filePath = storage_path($fileName);
    file_put_contents($filePath, $dompdf->output());

    // Retourner le fichier PDF en téléchargement
    return response()->download($filePath)->deleteFileAfterSend(true);
}

public function exportExcel()
{
    return Excel::download(new UtilisateursExport, 'utilisateurs.xlsx');
}
 




public function lesInscritExportToWord() {
    // Récupérer les données des utilisateurs
    $results = Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
        ->join('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1', 'personnes.langue2', 'personnes.langue3', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp', 'contacts.type_piece', 'contacts.num_piece', 'contacts.ifu', 'diplomes.diplome')
        ->get();

    // Créer un nouveau document Word
    $phpWord = new PhpWord();
    $section = $phpWord->addSection();

    // Ajouter un titre
    $titleStyle = array('bold' => true, 'size' => 16, 'alignment' => Jc::CENTER);
    $section->addText("Liste des Utilisateurs Inscrits", $titleStyle);

    // Définir le style du tableau
    $tableStyle = [
        'borderSize' => 6,
        'borderColor' => '000000',
        'cellMargin' => 50,
        'alignment' => Jc::CENTER
    ];
    $firstRowStyle = ['bgColor' => 'CCCCCC'];
    $phpWord->addTableStyle('Utilisateur Table', $tableStyle, $firstRowStyle);

    // Ajouter un tableau avec le style défini
    $table = $section->addTable('Utilisateur Table');

    // Ajouter l'en-tête du tableau
    $headerFontStyle = ['bold' => true];
    $table->addRow();
    $table->addCell(2000)->addText("Nom", $headerFontStyle);
    $table->addCell(2000)->addText("Prénoms", $headerFontStyle);
    $table->addCell(3000)->addText("Email", $headerFontStyle);
    $table->addCell(2000)->addText("Date de Naissance", $headerFontStyle);
    $table->addCell(2000)->addText("Nationalité", $headerFontStyle);
    $table->addCell(2000)->addText("Langue 1", $headerFontStyle);
    $table->addCell(2000)->addText("Langue 2", $headerFontStyle);
    $table->addCell(2000)->addText("Langue 3", $headerFontStyle);
    $table->addCell(2000)->addText("Numéro Téléphone", $headerFontStyle);
    $table->addCell(2000)->addText("Numéro WhatsApp", $headerFontStyle);
    $table->addCell(2000)->addText("Diplôme", $headerFontStyle);

    // Ajouter les données du tableau
    foreach ($results as $result) {
        $table->addRow();
        $table->addCell(2000)->addText($result->nom);
        $table->addCell(2000)->addText($result->prenom);
        $table->addCell(3000)->addText($result->email);
        $table->addCell(2000)->addText($result->date_of_birth);
        $table->addCell(2000)->addText($result->nationalite);
        $table->addCell(2000)->addText($result->langue1);
        $table->addCell(2000)->addText($result->langue2);
        $table->addCell(2000)->addText($result->langue3);
        $table->addCell(2000)->addText($result->num_tel);
        $table->addCell(2000)->addText($result->num_whatsapp);
        $table->addCell(2000)->addText($result->diplome);
    }

    // Enregistrer le document Word
    $fileName = 'utilisateurs_inscrits.docx';
    $filePath = storage_path($fileName);
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save($filePath);

    // Retourner le fichier Word en téléchargement
    return response()->download($filePath)->deleteFileAfterSend(true);
}

public function EnqueteDisponible()
{
   return Excel::download(new EnquetesExport, 'ListeEnquetesDisponible.xlsx');

}


        public function af(){
            return view('SuperAdmin.home');
        }




        public function storeQuestions(Request $request)
{


    $data = $request->validate([
        'questions.*.question' => 'required|string',
        'questions.*.typeReponse' => 'required|string',
        'questions.*.nbrOption' => 'nullable|integer|min:1|max:6',
        'questions.*.options' => 'nullable|array',
        'questions.*.options.*' => 'required_with:questions.*.nbrOption|string'
    ]);

    dd( $data);

    foreach ($data['questions'] as $questionData) {
        $question = new EnqueteQuestionModel();
        $question->question = $questionData['question'];
        $question->typeReponse = $questionData['typeReponse'];
        $question->nbrOption = $questionData['nbrOption'] ?? 0;

        if ($question->nbrOption) {
            $options = $questionData['options'] ?? [];
            $question->option1 = $options[0] ?? null;
            $question->option2 = $options[1] ?? null;
            $question->option3 = $options[2] ?? null;
            $question->option4 = $options[3] ?? null;
            $question->option5 = $options[4] ?? null;
            $question->option6 = $options[5] ?? null;
        }

        $question->idEnquete = $request->idEnquete;
        $question->nomEnquete = $request->nomEnquete;
        $question->save();
    }

    return response()->json(['message' => 'Questions saved successfully.']);
}
public function question(Request $request)
{
    $questions = $request->questions;

    foreach ($questions as $question) {
        // Préparer les données pour insertion
        $data = [
            'question' => $question['question'],
            'typeReponse' => $question['typeReponse'],
            'nbrOption' => $question['nbrOption'] ?? 0,
            'idEnquete' => $question['idEnquete'],
            'nomEnquete' => $question['nomEnquete'],
            'created_at' => now(),
            'updated_at' => now(),
        ];

        // Ajouter les options si elles existent
        for ($i = 1; $i <= 6; $i++) {
            $data["option$i"] = $question["option$i"] ?? null;
        }

        // Insérer les données dans la table
        DB::table('enquete_question')->insert($data);
    }

    return response()->json(['success' => 'Questions enregistrées avec succès !']);
}

        public function getQuestions($id)
            {
                $questions = EnqueteQuestionModel::where('idEnquete', $id)->get();
                return response()->json($questions);
            }

            public function getQuestionsByEnquete($idEnquete)
{
    $questions = EnqueteQuestionModel::where('idEnquete', $idEnquete)->get();
    return response()->json($questions);
}





public function fetchQuestions($id)
{
    $questions = EnqueteQuestionModel::where('idEnquete', $id)->get();
    return response()->json($questions);
}
public function showQuestionDetails($id)
{
    $question = EnqueteQuestionModel::find($id);
    if ($question) {
        return view('SuperAdmin.questions.questionDetails', ['question' => $question]);
    }
    return redirect()->route('accueiladmin_page')->with('error', 'erreur la question n\'a pas été trouvé');
}

public function editQuestion(Request $request, $id)
{
    $question = EnqueteQuestionModel::find($id);
    if ($question) {
        $question->question = $request->input('question');
        $question->typeReponse = $request->input('typeReponse');
        $question->save();
        return redirect()->route('question.details', ['id' => $id])->with('success', 'Question modifiée avec  succèss');
    }
    return redirect()->route('accueiladmin_page')->with('error', 'erreur la question n\'a pas été trouvé');
}

public function deleteQuestion($id)
{
    $question = EnqueteQuestionModel::find($id);
    if ($question) {
        $question->delete();
        return redirect()->route('accueiladmin_page')->with('success', 'Question suprimée avec succèss');
    }
    return redirect()->route('accueiladmin_page')->with('error', 'erreur la question n\'a pas été trouvé');
}


public function pageModifierQuestion($id)
{
    $question = EnqueteQuestionModel::find($id);
    if ($question) {
        return view('SuperAdmin.questions.questionModifier', ['question' => $question]);
    }
    return redirect()->route('accueiladmin_page')->with('error', 'erreur la question n\'a pas été trouvé');
}


public function DisponibleQuestions(){


    return view('InfoEnquette.QestionEnquete');

}

public function questionsDispoParEnquete($nom_enquete, $date_expiration, $info)
{
    return view('InfoEnquette.QestionEnquete', compact('nom_enquete', 'date_expiration', 'info'));
}

    // Controlleur en rapport avec l'ajout d'un nouveau rôle admin
    // Affiche le formulaire
    public function ShowFormAdminAddRole()
    {
        return view('SuperAdmin/Admin.roleAdmin');
    }


    // Gère la soumission du formulaire
    public function SubmitFormAdminAddRole(Request $request)
    {
       // Validation des données du formulaire
       $validatedData = $request->validate([
        'nom_role' => 'required|string|max:255',
        'attribut' => 'required|string|max:255',
    ]);

        // Enregistrer le roleadmin
        Roleadmin::create($validatedData);
        return redirect()->back()->with('success', 'Le rôle a été ajouté avec succès !');
        

    }



    // // Partie du controlleur du poste administrateur
    // // Affiche le formulaire
    // public function ShowFormAdminAddPoste()
    // {
    //     return view('SuperAdmin/Admin.posteAdmin');
    // }


    // Partie du controlleur du poste administrateur
    // Affiche le formulaire
    public function ShowFormAdminAddPoste()
    {
        $roles = Roleadmin::all();

// dd($roles );

        $user_id = Session::get('user_id');
    $user = admin_action_model::find($user_id);
  try {
            $user = admin_action_model::find($user_id);
            $rolesIds = json_decode(optional($user->poste)->role_id, true);
            $roles = is_array($rolesIds)
                ? Roleadmin::whereIn('id', $rolesIds)->select('classe_desactive')->get()
                : collect();

              //  dd( );
        } catch (\Exception $e) {
            $roles = collect();
            $error = "Impossible de récupérer les informations utilisateur.";
        }

    $disabledClasses = $roles->pluck('classe_desactive')->toArray();

    $currentPageClasses = ['ajouter-poste'];

    // Vérifier si l'une des classes du tableau est désactivée
    if (array_intersect($currentPageClasses, $disabledClasses)) {
        abort(404); // Redirige vers la page 404 si l'une des classes est désactivée
    }
        return view('SuperAdmin/Admin.posteAdmin', compact('roles','disabledClasses'));
    }



    // Gère la soumission du formulaire
    // public function SubmitFormAdminAddPoste(Request $request)
    // {
    //     // Validation des données du formulaire
    //     $validatedData = $request->validate([
    //         'nom_poste' => 'required|string|max:255',
    //         'roleadmin_id' => 'required' 
    //     ]);

    //      // Ajouter une valeur par défaut pour roleadmin_id
    //     $validatedData['roleadmin_id'] ;

    //     // Enregistrer le poste
    //     PosteadminModel::create($validatedData);
    //     return back()->with('success', 'Le poste a été ajouté avec succès !');
    // }

   // Gère la soumission du formulaire
    public function SubmitFormAdminAddPoste(Request $request)
    {
        // dd($request->all());
        // Validation des données du formulaire
        $validatedData = $request->validate([
            'nom_poste' => 'required|string|max:255',
            'role_id' => 'required|array', // Vérifie que roles est un tableau
            'role_id.*' => 'exists:roleadmin,id',
        ]);

        // Ajouter une valeur par défaut pour roleadmin_id
        // $validatedData['roleadmin_id'] = 0;

        // Enregistrer le poste
        $poste = PosteadminModel::create($validatedData);

        $poste->update([
            'role_id' => json_encode($validatedData['role_id'])
        ]);

        Alert::toast('Rôle attribué avec succès au poste!.', 'success');
         return back()->with('success', 'Le poste a été ajouté avec succès !');


        // return redirect()->back();
    }




    // public function SubmitFormAdminAddPoste(Request $request)
    // {
    //     // Validation des données du formulaire
    //     $validatedData = $request->validate([
    //         'nom_poste' => 'required|string|max:255',
    //         'roles' => 'required|array', // 'roles' doit être un tableau
    //         'roles.*' => 'exists:roleadmin,id', // Chaque rôle doit exister dans la table 'roleadmin'
    //     ]);

    //     // Création du poste
    //     $poste = PosteadminModel::create([
    //         'nom_poste' => $validatedData['nom_poste'],
    //     ]);

    //     // Association des rôles au poste via la table pivot
    //     $poste->roleadmins()->attach($validatedData['roles']); // Utilisation de attach() pour associer les rôles

    //     // Message de succès
    //     Alert::toast('Rôle(s) attribué(s) avec succès au poste!', 'success');
    //     return back()->with('success', 'Le poste a été ajouté avec succès !');
    // }



// public function SubmitFormAdminAddPoste(Request $request)
// {
    
//     // Validation des données du formulaire
//     $validatedData = $request->validate([
//         'nom_poste' => 'required|string|max:255',
//         'roles' => 'required', // 'roles' est une chaîne contenant les IDs séparés par des virgules
//     ]);

//     // Création du poste
//     $poste = PosteadminModel::create([
//         'nom_poste' => $validatedData['nom_poste'],
//     ]);

//     // Convertir la chaîne en tableau d'IDs
//     $rolesArray = explode(',', $validatedData['roles']);

//     // Association des rôles au poste via la table pivot
//     $poste->roleadmins()->attach($rolesArray); // Utilisation de attach() avec un tableau d'IDs

//     // Message de succès
//     Alert::toast('Rôle(s) attribué(s) avec succès au poste!', 'success');
//     return back()->with('success', 'Le poste a été ajouté avec succès !');
// }


}
