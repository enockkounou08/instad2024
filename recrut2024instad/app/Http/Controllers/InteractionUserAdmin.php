<?php

namespace App\Http\Controllers;

use App\Models\contact;
use App\Models\diplome;
use App\Models\personne;
use App\Mail\PostulerMail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\candidatureModel;
use App\Models\ConnexionAdminModel;
use App\Models\EnqueteQuestionModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\connexionRequest;
use App\Models\SuperAdminEnquetteModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;


class InteractionUserAdmin extends Controller
{

    public function search(Request $request)
{
    $keyword = $request->input('keyword');
    
    $contenus = SuperAdminEnquetteModel::where('nom', 'LIKE', "%$keyword%")->get();

    return response()->json($contenus);
}





    public function infoEnquette($id) {
        $contenus = SuperAdminEnquetteModel::find($id);
        $user_nom = Session::get('user_nom');
        $user_prenom = Session::get('user_prenom');
        $nomEnquete = $contenus->nom; // Récupérer le nom de l'enquête
        
        $userId = Session::get('user_id');
        $aDejaPostule = CandidatureModel::where('id_personne', $userId)
                                         ->where('id_enquete', $id)
                                         ->exists();
    
        // Vérifier si la date limite est atteinte
        $dateLimiteDepassee = now()->greaterThanOrEqualTo($contenus->date_expiration);
    
        return view('InfoEnquette.InfoEnquette', compact('contenus', 'user_nom', 'user_prenom', 'aDejaPostule', 'userId', 'nomEnquete', 'dateLimiteDepassee'));
    }
    

    


  
public function postulerOuAnnuler(Request $request, $idEnquete) {
    
    $userId = Session::get('user_id');
    $userNom = Session::get('user_nom');
    $userPrenom = Session::get('user_prenom');
    $userEmail = Session::get('user_email');
//dd($userId);


    // Vérifie si l'utilisateur a déjà postulé
    $candidatureExistante = CandidatureModel::where('id_personne', $userId)
                                            ->where('id_enquete', $idEnquete)
                                            ->exists();


 // Récupérer le nom de l'enquête depuis le formulaire
    $nomEnquete = $request->input('nom_enquete');

      // Récupérer le nom de l'enquête depuis le modèle SuperAdminEnquetteModel
      $enquete = SuperAdminEnquetteModel::find($idEnquete);
      $nomEnquete = $enquete->nom;
  

   // dd($nomEnquete);
    if ($candidatureExistante) {
        // Supprimer la candidature existante
        CandidatureModel::where('id_personne', $userId)
                        ->where('id_enquete', $idEnquete)
                        ->delete();
        return redirect()->back()->with('success', 'Annulation de la postulation avec succès.');
    } else {
        // Créer une nouvelle candidature
        $candidature = new CandidatureModel;
        $candidature->id_personne = $userId;
        $candidature->nom_personne = $userNom;
        $candidature->email_personne = $userEmail;
        $candidature->id_enquete = $idEnquete;
        $candidature->nom_enquete = $nomEnquete; // Assurez-vous de récupérer le nom de l'enquête
        $candidature->save();

        $message = "Salut " . $userNom . " ". $userPrenom .", vous venez de postuler pour l'enquête de :  ". $nomEnquete ."   ";
        $subject = "L'Institut National de la Statistique et de la Démographie vous remercie";

        Mail::to($userEmail)->send(new PostulerMail($message, $subject));


        return redirect()->back()->with('success', 'Candidature réussie.');
    }
}



    public function delateEnquette ($id){
        $contenus = SuperAdminEnquetteModel::find($id);

        return view('SuperAdmin.suprimerEnquette', compact('contenus'));
    }







  

    public function showEnqueteQuestions($id) {
        $contenus = SuperAdminEnquetteModel::find($id);
        $user_nom = Session::get('user_nom');
        $user_prenom = Session::get('user_prenom');
        $user_email = Session::get('user_email');
        $nomEnquete = $contenus->nom; // Récupérer le nom de l'enquête

        $userId = Session::get('user_id');
        $aDejaPostule = CandidatureModel::where('id_personne', $userId)
                                         ->where('id_enquete', $id)
                                         ->exists();
    
        // Récupérer les questions liées à l'enquête, triées par date de création (les plus récentes en premier)
        $questions = EnqueteQuestionModel::where('idEnquete', $id)->get();
                                         // ->orderBy('created_at', 'desc') // Trier par date de création
                                         // ->get();
                Alert::success('Succès', 'Bienvenu');

    
        return view('InfoEnquette.QestionEnquete', compact('contenus', 'user_nom', 'user_prenom', 'user_email', 'nomEnquete', 'questions' ,'aDejaPostule'));
    }
    
}
