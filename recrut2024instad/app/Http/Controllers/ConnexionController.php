<?php

namespace App\Http\Controllers;

use App\Models\contact;
use App\Models\diplome;
use App\Models\personne;
use Illuminate\Http\Request;
use App\Models\ConnexionAdminModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\connexionRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;




class ConnexionController extends Controller
{
    //

    public function connex() {

        return view ('Inscription.connex');
    }

     //affichage du formuaire de connexion
     public function connexion() {

        return view ('Inscription.connexion');
     }


public function verifySession(Request $request)
{
    // Vérifier si la session contient les informations nécessaires
    if (!Session::has('user_email')) {
        // Si la session n'est pas valide, rediriger vers la page de connexion
        return redirect()->route('connexion_page');
    }

    
// Vérifier l'email et l'ID de l'utilisateur dans la base de données
    $user = contact::where('email', Session::get('user_email'))
                              ->where('id', Session::get('user_id'))
                              ->first();
    if (!$user) {
        // Si l'utilisateur n'est pas trouvé, rediriger vers la page de connexion
        return redirect()->route('connexion_page');
    }


    // Si tout est correct, autoriser l'accès à la page
    return redirect()->route('accueiladmin_page');
}



public function connexionTraitement(Request $request) {

    // Validation des données du formulaire
    $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:6',
    ], [
        // Personnalisation des messages d'erreur
        'email.required' => 'L\'adresse email est obligatoire.',
        'email.email' => 'Veuillez saisir une adresse email valide.',
        'password.required' => 'Le mot de passe est obligatoire.',
        'password.min' => 'Le mot de passe doit contenir au moins :min caractères.',
    ]);

    // Vérifier si la validation a échoué
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    // Vérification de l'utilisateur admin spécifique
    if ($request->email === 'instadbenineja123@gmail.com' && $request->password === 'AdminAdmin@') {
        // Définir les données arbitraires pour l'administrateur
        $adminId = 13;
        $adminNom = explode('@', $request->email)[0];
        $adminEmail = $request->email;

        // Créer une session pour l'administrateur
        Session::put('user_id', $adminId);
        Session::put('user_nom', $adminNom);
        Session::put('user_prenom', ''); // Pas de prénom pour l'administrateur
        Session::put('user_email', $adminEmail);
        Session::put('userid', $adminId);

        // Rediriger l'administrateur vers la page d'accueil
        return redirect()->route('accueiladmin_page');
    }

    // Vérification de l'utilisateur
    $user = contact::where('email', $request->email)->first();

    if ($user) {
        if ($user->is_verified == 1) {
            if ($user && Hash::check($request->password, $user->password)) {
                // Le mot de passe correspond
                // Connecte l'utilisateur
                Auth::login($user);
                
                // Récupérer l'id de l'utilisateur connecté
                $userId = Auth::id();
                $request->session()->regenerate();

                // Récupérer les informations de l'utilisateur dans la table personne
                $otherTableEntry = personne::where('id', $userId)->first();

                // Mettre les informations de l'utilisateur dans la session
                Session::put('user_id', $userId);
                Session::put('user_nom', $otherTableEntry->nom);
                Session::put('user_prenom', $otherTableEntry->prenom);
                Session::put('user_email', $user->email);  // Ajouter l'email de l'utilisateur dans la session
                Session::put('userid', $otherTableEntry->id);

                // Rediriger l'utilisateur vers la page d'accueil
                // return redirect('/pageAcceuil');
        return redirect()->route('Accueil_page');

            } else {
                // Rediriger avec un message d'erreur si le mot de passe est incorrect
                return redirect()->back()->with('error', 'Adresse email ou mot de passe incorrect');
            }
        } else {
            // Si l'utilisateur n'a pas activé son compte
            return redirect()->back()->with('error', 'Vous n\'avez pas activé votre compte. Veuillez vérifier votre boîte mail et activer le compte.')->withInput();
        }
    } else {
        // Rediriger avec un message d'erreur si l'adresse email est incorrecte
        return redirect()->back()->with('error', 'Adresse email ou mot de passe incorrect');
    }
}





    public function connexionAdmin() {

        return view ('Inscription.connexionAdmin');
    }
    

    public function connexionAdminTraitement(Request $request) {

        // Validation des données du formulaire
        $validator = Validator::make($request->all(), [
           'email' => 'required|email',
           'password' => 'required|min:6',
        ], [
           // Personnalisation des messages d'erreur
           'email.required' => 'L\'adresse email est obligatoire.',
           'email.email' => 'Veuillez saisir une adresse email valide.',
           'password.required' => 'Le mot de passe est obligatoire.',
           'password.min' => 'Le mot de passe doit contenir au moins :min caractères.',
        ]);
    
        // Vérifier si la validation a échoué
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
            // Cette ligne redirige l'utilisateur vers la page précédente avec les erreurs de validation et les données saisies précédemment
        }

         // Si la validation réussit, tu peux continuer avec la logique de connexion ici
   
        // Vérification de l'utilisateur
        $user = ConnexionAdminModel::where('email', $request->email)->first();
   
        if ($user && Hash::check($request->password, $user->password)) {
            // Le mot de passe correspond
            // Connecte l'utilisateur
            Auth::login($user);
            //récupérer l'id de l'utilisateur connecter
            $userId = Auth::id();
               
          

    
            // Redirige l'utilisateur vers la page d'accueil
            return redirect()->intended('/pageUserAdmin');
        } 
   }

   public function inscriptionAdmin() {

    return view ('Inscription.formInsAdmin');
}
   

   
   public function inscriptionAdminTraitement(Request $request) {

    // Validation des données du formulaire
    $validator = Validator::make($request->all(), [

        'prenom' => 'required|string',
        'nom' => 'required|string',
       'email' => 'required|email',
       'password' => 'required|min:6',
       
      'password_confirmation' => 'required|min:6|confirmed', // Validation du mot de passe
    ]);


    $messages = [
// Personnalisation des messages d'erreur

       
'prenom.required' => 'Le champs prenom est obligatoire.',
'nom.required' => 'Le champs nom est obligatoire.',
'email.required' => 'L\'adresse email est obligatoire.',
'email.email' => 'Veuillez saisir une adresse email valide.',
'password.required' => 'Le mot de passe est obligatoire.',
'password.min' => 'Le mot de passe doit contenir au moins :min caractères.',
'password.confirmed' => 'Les mots de passe ne correspondent pas.',

'password_confirmation.required' => 'Le mot de passe est obligatoire.',
'password_confirmation.min' => 'Le mot de passe est obligatoire.',
'password_confirmation.confirmed' => 'Les mots de passe ne correspondent pas.',
    ];

  
  // Application des messages personnalisés
  $validator->setCustomMessages($messages);
 
  // Vérification de la validation
  if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
  } 
     

  


       // Insertion dans la table 'personnes'
       $admin = new ConnexionAdminModel();
       $admin->prenom = $request->prenom;
       $admin->nom = $request->nom;
       $admin->email = $request->email;
       $admin->password = $request->password = bcrypt($request->password);;
       $admin->password_confirmation = $request->password_confirmation = bcrypt($request->password_confirmation);;
       $admin->save();
       return redirect('/pageUserAdminConnexion')->with('success', 'Inscription réussie !');

    }


    public function showLoginPage(Request $request)
{
    // Vérifiez l'URL pour voir si l'utilisateur souhaite se connecter en tant qu'administrateur
    if ($request->is('/pageConnexion/-admin-profile')) {
        // Rediriger vers la page de connexion administrateur
        return view('SuperAdmin.Admin.ProfileAdminConnexion');
    } else {
        // Rediriger vers la page de connexion utilisateur normal
        return view('Inscription.connexion');
    }
}
    
}
