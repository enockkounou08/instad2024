<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\personne;
use App\Models\diplome;
use App\Models\contact;
use App\Models\SuperAdminEnquetteModel;

class SearchController extends Controller
{
    

    public function index()
    {
        return view('layouts.app');
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('search');

        $results = contact::where('email', 'like', '%' . $searchTerm . '%')->get();

        return view('layouts.app')->with('results', $results);
    }
}
