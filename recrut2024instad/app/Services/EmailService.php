<?php

namespace App\Services;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class EmailService
{
    protected $app_name;
    protected $host;
    protected $port;
    protected $username;
    protected $password;
    protected $from_address;
    protected $from_name;

    function __construct()
    {
        $this->app_name = config('app.name');
        $this->host = config('mail.mailers.smtp.host');
        $this->port = config('mail.mailers.smtp.port');
        $this->username = config('mail.mailers.smtp.username');
        $this->password = config('mail.mailers.smtp.password');
        $this->from_address = config('mail.from.address');
        $this->from_name = config('mail.from.name');
    }

    public function sendEmail($subject, $emailUser, $nameUser, $isHtml, $message)
    {
        $email = new PHPMailer(true);

        
            $email->isSMTP();
            $email->SMTPDebug = 0;
            $email->Host = $this->host;
            $email->Port = $this->port;
            $email->SMTPAuth = true;
            $email->Username = $this->username;
            $email->Password = $this->password;
            $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

            $email->setFrom($this->from_address, $this->from_name);
            $email->addReplyTo($this->from_address, $this->from_name);
            $email->addAddress($emailUser, $nameUser);

            $email->isHTML($isHtml);
            $email->Subject = $subject;
            $email->Body = $message;

            $email->send();
      
    }
}
