<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;


class admin_action_model extends Model implements Authenticatable
{
    use AuthenticableTrait;

    // Specify the table if it doesn't follow the convention
    protected $table = 'admin_action_models';

    // Allow mass assignment for these fields
    protected $fillable = ['nom', 'prenom', 'email', 'password', 'poste_id'];

    // Optionally, you can define hidden fields like the password
    protected $hidden = ['password'];

      public function poste(): BelongsTo
    {
        return $this->belongsTo(PosteadminModel::class);
    }
}
