<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;



class personne extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    use HasFactory;
    use Authenticatable;
    protected $table = 'personnes';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(contact::class, 'id');
    }

    /**
     * Relation inverse : Une personne peut avoir un contact associé.
     */
    public function contact()
    {
        return $this->hasOne(contact::class, 'id', 'id');
    }
}
