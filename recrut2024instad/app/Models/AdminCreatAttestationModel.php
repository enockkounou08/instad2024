<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminCreatAttestationModel extends Model
{
    use HasFactory;
     protected $table = 'admin_creat_attestation';
    protected $fillable = [
        'titre', 
        'contenu',
        'enquete_id', // Clé étrangère vers l'enquête
        // 'type_attestation',
        'type_attestation_id',
];


    public function enquete()
    {
        return $this->belongsTo(SuperAdminEnquetteModel::class, 'enquete_id');
    }

    public function type()
    {
        return $this->belongsTo(admin_creat_type_attestation::class, 'type_attestation_id');
    }



}
