<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Yajra\DataTables\Html\Editor\Fields\BelongsTo;


class contact extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    use HasFactory;
    use Authenticatable;
    protected $table = 'contacts';
    protected $guarded = [];

   /* public function personnes()
    {
        return $this->hasMany(Personne::class, 'user_id');
    }*/
    // Relation avec les enquêtes
    public function enquêtes()
    {
        return $this->belongsToMany(SuperAdminEnquetteModel::class, 'enquete_contact', 'contact_id', 'enquete_id');
    }
    
/**
     * Définir la relation entre contact et personne.
     */
    public function personne()
    {
        return $this->belongsTo(personne::class, 'id', 'id');
    }

      
}
