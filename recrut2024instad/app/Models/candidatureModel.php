<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class candidatureModel extends Model
{
    use HasFactory;
    
    protected $table = 'candidature_models';
    protected $guarded = [];
}
