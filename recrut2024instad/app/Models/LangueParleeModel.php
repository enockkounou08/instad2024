<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LangueParleeModel extends Model
{
    use HasFactory;
    
    // Spécifiez les colonnes qui peuvent être massivement assignées
    protected $table = 'langueparlee';
    protected $primaryKey = 'id';
    protected $fillable = ['nom_langue'];
}
