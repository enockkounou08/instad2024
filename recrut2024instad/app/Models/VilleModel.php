<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VilleModel extends Model
{
    use HasFactory;

    protected $table = 'ville';
    protected $primaryKey = 'id';
    
    // Spécifiez les colonnes qui peuvent être massivement assignées
    protected $fillable = ['ville'];
    
}
