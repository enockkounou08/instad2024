<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Roleadmin extends Model
{
    use HasFactory;

    // Définissez les colonnes qui peuvent être massivement assignées
    // protected $table = 'roleadmin';

    protected $table = 'roleadmin';
    protected $primaryKey = 'id';
    protected $fillable = ['nom_role', 'classe_desactive', 'posteadmin_id'];
     protected $casts = [
        'posteadmin_id' => 'json'
    ];

    // // Relation avec Posteadmin
    // public function posteadmins()
    // {
    //     return $this->hasMany(PosteadminModel::class);
    // }
    
    
    public function adminAction(): BelongsTo
    {
        return $this->belongsTo(admin_action_model::class,'user_id');
    }

    // Définir la relation many-to-many avec PosteadminModel
    public function posteadmins()
    {
        return $this->belongsToMany(PosteadminModel::class, 'posteadmin_roleadmin', 'roleadmin_id', 'posteadmin_id');
    }

    
  
}
