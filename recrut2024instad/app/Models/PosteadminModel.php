<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PosteadminModel extends Model
{
    use HasFactory;

    // Définissez les colonnes qui peuvent être massivement assignées
    protected $table = 'posteadmin';
    protected $primaryKey = 'id';
    protected $casts = [
        'role_id' => 'json'
    ];
    
    protected $fillable = ['nom_poste', 'role_id',];

    

      // Définir la relation many-to-many avec Roleadmin
    public function roleadmins()
    {
        return $this->belongsToMany(Roleadmin::class, 'posteadmin_roleadmin', 'posteadmin_id', 'role_id');
    }
}
