<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReponseUtilisateurPourEnquete extends Model
{
    use HasFactory;

protected $table = 'reponse_utilisateur_pour_enquetes';

    protected $fillable = [
        'user_id',
        'enquete_id',
        'question_id',
        'type_reponse',
        'reponse',
    ];

    
}
