<?php

namespace App\Models\test;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class testModel extends Model 
{
    use HasFactory;
    use Authenticatable;
    protected $table = 'mailt';
    protected $guarded = [];
    
}
