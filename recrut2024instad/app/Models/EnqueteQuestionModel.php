<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnqueteQuestionModel extends Model
{
    use HasFactory;

   


     protected $table = 'enquete_question';

    protected $fillable = [
        'question', 'typeReponse', 'nbrOption', 'option1', 'option2', 'option3', 'option4', 'option5', 'option6', 'idEnquete', 'nomEnquete'
    ];
}
