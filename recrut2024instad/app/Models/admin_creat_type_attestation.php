<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class admin_creat_type_attestation extends Model
{
    use HasFactory;

     protected $table = 'admin_creat_type_attestation';
    

     protected $fillable = [
        'nom',
];
}
