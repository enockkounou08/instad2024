<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\candidatureModel;


class SuperAdminEnquetteModel extends Model
{
    use HasFactory;
   
protected $table = 'super_admin_enquette_models';


protected $fillable = [
        'nom', 'info', 'id_postulant', 'nbr_postule', 'date_expiration', 'etat_enquete'
    ];


    
protected $casts = [
    'date_expiration' => 'datetime',
];
     // Relation avec les contacts
     public function contacts()
     {
         return $this->belongsToMany(Contact::class, 'enquete_contact', 'enquete_id', 'contact_id');
     }


     public function attestations()
{
    return $this->hasMany(AdminCreatAttestation::class, 'enquete_id');
}


    
    
}
