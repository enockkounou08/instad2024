<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReponseModel extends Model
{
    use HasFactory;
    
    protected $table = 'reponse';
    protected $guarded = [];
}
