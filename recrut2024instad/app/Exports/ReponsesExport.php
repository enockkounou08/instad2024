<?php

namespace App\Exports;

use App\Models\ReponseUtilisateurPourEnquete;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReponsesExport implements FromCollection, WithHeadings
{
        protected $enqueteId;

    public function __construct($enqueteId)
    {
        $this->enqueteId = $enqueteId;
    }

      /**
     * Retourne la collection de données à exporter
     */
      
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ReponseUtilisateurPourEnquete::all();
    }
}
