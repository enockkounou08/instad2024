<?php

namespace App\Exports;

use App\Models\SuperAdminEnquetteModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EnqueteExport implements FromCollection, WithHeadings
{
    protected $enquetes;

    // Constructeur pour passer les résultats de la recherche
    public function __construct($enquetes)
    {
        $this->enquetes = $enquetes;
    }

    // Retourner les données à exporter
    public function collection()
    {
        return $this->enquetes;
    }

    // Définir les en-têtes de colonnes
    public function headings(): array
    {
        return [
            'ID',
            'Nom',
            'Description',
            'Date d\'expiration',
            'État'
        ];
    }
}

