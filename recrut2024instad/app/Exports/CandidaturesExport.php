<?php

namespace App\Exports;

use App\Models\CandidatureModel;
use App\Models\SuperAdminEnquetteModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CandidaturesExport implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    protected $id;
    protected $enqueteName;

    public function __construct($id)     
    {
        $this->id = $id;
        $this->enqueteName = SuperAdminEnquetteModel::find($id)->nom;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return CandidatureModel::where('id_enquete', $this->id)
            ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
            ->leftJoin('contacts', 'personnes.id', '=', 'contacts.id')
            ->leftJoin('diplomes', 'personnes.id', '=', 'diplomes.id')
            ->select(
                'personnes.nom',
                'personnes.prenom',
                'personnes.date_of_birth',
                'personnes.nationalite',
                'contacts.num_whatsapp',
                'contacts.num_tel',
                'contacts.email',
                'contacts.type_piece',
                'contacts.num_piece',
                'contacts.ifu',
                'personnes.langue1',
                'personnes.langue2',
                'personnes.langue3',
                'diplomes.diplome'
            )
            ->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Nom',
            'Prénom',
            'Date de naissance',
            'Nationalité',
            'Numéro WhatsApp',
            'Téléphone',
            'Email',
            'Type de pièce',
            'Numéro de pièce',
            'Numéro IFU',
            '1ère langue parlée',
            '2ème langue parlée',
            '3ème langue parlée',
            'Diplôme',
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Candidatures';
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $sheet->mergeCells('A1:M1');
                $sheet->setCellValue('A1', $this->enqueteName);
                $sheet->getStyle('A1')->getFont()->setBold(true)->setSize(14);
                $sheet->getRowDimension('1')->setRowHeight(30);

                // Adjust the headings and data to start from row 3
                $sheet->fromArray($this->headings(), null, 'A2');
                $sheet->fromArray($this->collection()->toArray(), null, 'A3');
            },
        ];
    }
}
