<?php

namespace App\Exports;

use App\Models\Personne;
use Barryvdh\DomPDF\Facade\Pdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartCell;
use Illuminate\Support\Facades\View as ViewFacade;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
class UtilisateursExport implements FromCollection, WithHeadings, WithMapping, WithCustomStartCell

{
    public function collection()
    {
        // return Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
        //     ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp')
        //     ->get();


        return Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
        ->join('diplomes', 'personnes.id', '=', 'diplomes.id')
        ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1', 'personnes.langue2', 'personnes.langue3', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp', 'diplomes.diplome')
        ->get();
    }

    public function headings(): array
    {
        // return [
        //     'Nom',
        //     'Prénoms',
        //     'Email',
        //     'Date de Naissance',
        //     'Nationalité',
        //     'Langue',
        //     'Tel 1',
        //     'Tel 2'
        // ];

        return [
            'Nom',
            'Prénoms',
            'Email',
            'Date de Naissance',
            'Nationalité',
            'Langue 1',
            'Langue 2',
            'Langue 3',
            'Type de pièce',
            'Numéro de pièce',
            'Numéro IFU',
            'Numéro Téléphone',
            'Numéro WhatsApp',
            'Diplôme'
        ];
    }


    public function map($personne): array
    {
        return [
            $personne->nom,
            $personne->prenom,
            $personne->email,
            $personne->date_of_birth,
            $personne->nationalite,
            $personne->langue1,
            $personne->langue2,
            $personne->langue3,
            $personne->type_piece,
            $personne->num_piece,
            $personne->ifu,
            $personne->num_tel,
            $personne->num_whatsapp,
            $personne->diplome
        ];
    }


    public function startCell(): string
    {
        return 'B2';
    }


    // public function view(): View
    // {
    //     $results = Personne::join('contacts', 'personnes.id', '=', 'contacts.id')
    //         ->select('personnes.nom', 'personnes.prenom', 'personnes.date_of_birth', 'personnes.nationalite', 'personnes.langue1', 'contacts.email', 'contacts.num_tel', 'contacts.num_whatsapp')
    //         ->get();

    //     return ViewFacade::make('Excel.utilisateurs', [
    //         'results' => $results
    //     ]);
    // }

    // public function startCell(): string
    // {
    //     return 'A1';
    // }

    // public function export()
    // {
    //     // Render the view to HTML
    //     $html = $this->view()->render();

    //     // Convert HTML to PDF
    //     $pdf = PDF::loadHTML($html);
    //     $pdf->save(storage_path('app/public/utilisateurs.pdf'));

    //     // Convert PDF to Excel
    //     $spreadsheet = IOFactory::load(storage_path('app/public/utilisateurs.pdf'));
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save(storage_path('app/public/utilisateurs.xlsx'));

    //     return response()->download(storage_path('app/public/utilisateurs.xlsx'));
    // }
}
