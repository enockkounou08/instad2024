<?php
namespace App\Exports;

use App\Models\SuperAdminEnquetteModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;

class EnquetesExport implements FromCollection, WithHeadings, WithMapping, WithEvents
{
    use RegistersEventListeners;

    public function collection()
    {
        return SuperAdminEnquetteModel::select(
            'super_admin_enquette_models.nom as nom_enquete', 
            'super_admin_enquette_models.info', 
            'super_admin_enquette_models.date_expiration', 
            \DB::raw('COUNT(candidature_models.id) as occurrences'), 
            \DB::raw('GROUP_CONCAT(personnes.prenom, " ", personnes.nom) as personnes_associées')
        )
        ->leftJoin('candidature_models', 'super_admin_enquette_models.id', '=', 'candidature_models.id_enquete')
        ->leftJoin('personnes', 'candidature_models.id_personne', '=', 'personnes.id')
        ->groupBy('super_admin_enquette_models.id', 'super_admin_enquette_models.nom', 'super_admin_enquette_models.info', 'super_admin_enquette_models.date_expiration')
        ->get();
    }

    public function headings(): array
    {
        return [
            'Nom',
            'Information',
            'Date Expiration',
            'Nombre de personne ayant posuler ',
            'Les personnes ayant postulées'
        ];
    }

    public function map($enquete): array
    {
        return [
            $enquete->nom_enquete,
            $enquete->info,
            //$enquete->date_expiration,
            \Carbon\Carbon::parse($enquete->date_expiration)->format('d-m-Y'), // Formater la date
            $enquete->occurrences,
            $enquete->personnes_associées
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        // // Insérer une ligne vide au début pour décaler les données à partir de B2
        // $event->sheet->getDelegate()->insertNewRowBefore(2, 1);

        // // Définir les en-têtes à partir de la cellule B2
        // $event->sheet->getDelegate()->setCellValue('B2', 'Nom');
        // $event->sheet->getDelegate()->setCellValue('C2', 'Info');
        // $event->sheet->getDelegate()->setCellValue('D2', 'Date Expiration');
        // $event->sheet->getDelegate()->setCellValue('E2', 'Occurrences');
        // $event->sheet->getDelegate()->setCellValue('F2', 'Personnes Associées');

        // // Appliquer des styles aux en-têtes
        // $event->sheet->getDelegate()->getStyle('B2:F2')->applyFromArray([
        //     'font' => ['bold' => true],
        //     'borders' => [
        //         'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN]
        //     ]
        // ]);

        // // Appliquer des styles aux données
        // $dataRange = 'B3:F' . (count($event->sheet->getDelegate()->toArray()) + 2);
        // $event->sheet->getDelegate()->getStyle($dataRange)->applyFromArray([
        //     'borders' => [
        //         'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN]
        //     ]
        // ]);
    }
}


